package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachBindDataBo;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachCreateBo;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachDeleteBo;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachDownloadPathBo;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachListBo;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachModelBo;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachPageBo;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachUpdateBo;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachUploadBo;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachViewBo;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachBindDataDto;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachCreateDto;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachDeleteDto;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachDownloadPathDto;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachListDto;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachModelDto;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachPageDto;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachUpdateDto;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachUploadDto;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachViewDto;
import com.unswift.cloud.pojo.vo.ListVo;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachBindDataVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachCreateVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachDeleteVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachDownloadPathVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachDownloadVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachListVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachModelVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachPageVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachUpdateVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachUploadVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachViewVo;
import com.unswift.cloud.service.SystemAttachService;

import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/systemAttach")
@Api(value="附件对外接口", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachController extends BaseController{
	
	@Autowired
	private SystemAttachService systemAttachService;
	
	@Active
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@Auth({"system.attach.temp.pageList","system.attach.model.pageList","system.attach.business.pageList"})
	@ApiMethod(value="附件分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemAttachPageVo>> findPageList(@RequestBody SystemAttachPageDto searchDto){
		SystemAttachPageBo searchBo=this.convertPojo(searchDto, SystemAttachPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemAttachService.findPageList(searchBo));
	}
	
	@RequestMapping(value="/findList", method=RequestMethod.POST)
	@ApiMethod(value="查询附件列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的附件列表"))
	public ResponseBody<ListVo<SystemAttachListVo>> findList(@RequestBody SystemAttachListDto searchDto){
		SystemAttachListBo searchBo=this.convertPojo(searchDto, SystemAttachListBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemAttachService.findList(searchBo));
	}
	
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="附件详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemAttachViewVo> view(@RequestBody SystemAttachViewDto viewDto){
		SystemAttachViewBo viewBo=this.convertPojo(viewDto, SystemAttachViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemAttachService.view(viewBo));
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建附件", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemAttachCreateVo> create(@RequestBody SystemAttachCreateDto createDto){
		SystemAttachCreateBo createBo=this.convertPojo(createDto, SystemAttachCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemAttachService.create(createBo));
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新附件", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemAttachUpdateVo> update(@RequestBody SystemAttachUpdateDto updateDto){
		SystemAttachUpdateBo updateBo=this.convertPojo(updateDto, SystemAttachUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemAttachService.update(updateBo));
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除附件", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemAttachDeleteVo> delete(@RequestBody SystemAttachDeleteDto deleteDto){
		SystemAttachDeleteBo deleteBo=this.convertPojo(deleteDto, SystemAttachDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemAttachService.delete(deleteBo));
	}
	
	@Active
    @RequestMapping(value="/upload", method=RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiMethod(value="上传附件", params=@ApiField("附件对象"), returns=@ApiField("上传结果Vo"))
    public ResponseBody<ListVo<SystemAttachUploadVo>> upload(SystemAttachUploadDto uploadDto){
        SystemAttachUploadBo deleteBo=this.convertPojo(uploadDto, SystemAttachUploadBo.class);//将Dto转换为Bo
        return ResponseBody.success(systemAttachService.upload(deleteBo));
    }
    
	@Active
    @RequestMapping(value="/bindData", method=RequestMethod.POST)
    @ApiMethod(value="绑定业务数据", params=@ApiField("业务对象"), returns=@ApiField("上传结果Vo"))
    public ResponseBody<SystemAttachBindDataVo> bindData(SystemAttachBindDataDto bindDataDto){
        SystemAttachBindDataBo bindDataBo=this.convertPojo(bindDataDto, SystemAttachBindDataBo.class);//将Dto转换为Bo
        return ResponseBody.success(systemAttachService.bindData(bindDataBo));
    }
    
	@Active
    @RequestMapping(value="/download/{fileUrlName}", method=RequestMethod.GET)
    @ApiMethod(value="下载附件", params=@ApiField("附件路径"))
    public void download(@PathVariable("fileUrlName")String fileUrlName, HttpServletResponse response){
        systemAttachService.download(fileUrlName, response);
    }
    
	@Active
    @RequestMapping(value="/downloadByte/{fileUrlName}", method=RequestMethod.GET)
    @ApiMethod(value="下载附件-返回byte数组", params=@ApiField("附件路径"))
    public ResponseBody<SystemAttachDownloadVo> downloadByte(@PathVariable("fileUrlName")String fileUrlName){
    	return ResponseBody.success(systemAttachService.downloadByte(fileUrlName));
    }
 
	@Active
    @RequestMapping(value="/getDownloadPath", method=RequestMethod.POST)
	@ApiMethod(value="根据业务id获取下载路径", params=@ApiField("业务id Dto对象"), returns=@ApiField("路径对象"))
	public ResponseBody<SystemAttachDownloadPathVo> getDownloadPath(@RequestBody SystemAttachDownloadPathDto searchDto){
    	SystemAttachDownloadPathBo searchBo=this.convertPojo(searchDto, SystemAttachDownloadPathBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemAttachService.getDownloadPath(searchBo));
	}
	
	@Active
    @RequestMapping(value="/findModelAttach", method=RequestMethod.POST)
	@ApiMethod(value="获取模板附件", params=@ApiField("业务模块对象"), returns=@ApiField("模板附件"))
	public ResponseBody<SystemAttachModelVo> findModelAttach(@RequestBody SystemAttachModelDto searchDto){
    	SystemAttachModelBo searchBo=this.convertPojo(searchDto, SystemAttachModelBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemAttachService.findModelAttach(searchBo));
	}
}
