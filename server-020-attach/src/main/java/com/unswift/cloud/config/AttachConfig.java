package com.unswift.cloud.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "unswift.attach")
@ApiEntity(value="附件相关yml配置实体", author = "unswift", date = "2024-01-21", version = "1.0.0")
public class AttachConfig {
	
	@ApiField("附件基础路径")
	private String basePath;
	
	@ApiField("附件url前缀")
	private String attachUrlPrefix;
	
	@ApiField("是否备份{true：是，false：否}")
	private Boolean backup;
	
	@ApiField("附件备份基础路径")
	private String basePathBackup;
}
