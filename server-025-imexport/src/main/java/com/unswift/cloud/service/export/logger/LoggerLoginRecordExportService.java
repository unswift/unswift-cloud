package com.unswift.cloud.service.export.logger;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.ResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerExportTaskAdapter;
import com.unswift.cloud.adapter.logger.LoggerLoginRecordAdapter;
import com.unswift.cloud.enums.export.task.ExportTaskEventEnum;
import com.unswift.cloud.excel.IExcelTranslate;
import com.unswift.cloud.export.IExportBusinessService;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordExportBo;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordExportStopBo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskInsertDo;
import com.unswift.cloud.pojo.dao.logger.login.record.LoggerLoginRecordBigDataDo;
import com.unswift.cloud.pojo.dao.logger.login.record.LoggerLoginRecordDataDo;
import com.unswift.cloud.pojo.mo.logger.login.record.LoggerLoginRecordExportMo;
import com.unswift.cloud.pojo.mso.logger.export.task.LoggerExportTaskQueueMso;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordExportVo;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordExportStopVo;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.service.BaseService;
import com.unswift.cloud.sql.logger.login.record.LoggerLoginRecordSql;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="登录记录导出服务", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordExportService extends BaseService implements IExportBusinessService<LoggerLoginRecordExportBo, LoggerLoginRecordDataDo, LoggerLoginRecordExportMo>, IExcelTranslate, RabbitConstant{
	
	@Autowired
	@ApiField("登录记录公共服务")
	private LoggerLoginRecordAdapter loggerLoginRecordAdapter;
	
	@Autowired
	@ApiField("登录记录自定义Sql")
	private LoggerLoginRecordSql loggerLoginRecordSql;
	
	@Autowired
	@ApiField("导出任务服务")
	private LoggerExportTaskAdapter loggerExportTaskAdapter;
	
	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	@ApiMethod(value="根据条件导出登录记录数据", params=@ApiField("查询条件"))
	public LoggerLoginRecordExportVo export(LoggerLoginRecordExportBo exportBo){
		ExceptionUtils.trueException(loggerExportTaskAdapter.existsTasking(this.getUserId(), loggerLoginRecordAdapter.getModule()), "task.is.currently.executing", "有登录记录导出");
		Date date=new Date();
		LoggerExportTaskInsertDo task=new LoggerExportTaskInsertDo();
		task.setName("登录记录");
		task.setModule(loggerLoginRecordAdapter.getModule());
		task.setDocumentType("excel");
		task.setStatus((byte)0);
		task.setStartTime(date);
		task.setExportBeanClass(this.getClass().getName());
		task.setExportModelClass(LoggerLoginRecordExportBo.class.getName());
		task.setExportCondition(JsonUtils.toJson(exportBo));
		task.setResult((byte)0);
		int result=loggerExportTaskAdapter.save(task, false);//保存任务，不用加事务，防止队列消费的时候还没保存完数据
		rabbitQueue.sendMessage(EXPORT_EXCHANGE_NAME, EXPORT_ROUTE_KEY, new LoggerExportTaskQueueMso(task.getId(), ExportTaskEventEnum.START, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送导出队列
		return new LoggerLoginRecordExportVo(task.getId(), result);
	}
	
	@ApiMethod(value="停止导出登录记录", params=@ApiField("停止导出对象"))
	public LoggerLoginRecordExportStopVo stop(LoggerLoginRecordExportStopBo exportStopBo){
		rabbitQueue.sendMessage(EXPORT_STOP_EXCHANGE_NAME, EXPORT_STOP_ROUTE_KEY, new LoggerExportTaskQueueMso(exportStopBo.getId(), ExportTaskEventEnum.STOP, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送停止导出队列
		return new LoggerLoginRecordExportStopVo(1);
	}
	
	@Override
	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	public Class<LoggerLoginRecordExportMo> getModule(){
		return LoggerLoginRecordExportMo.class;
	}
	
	@Override
	@ApiMethod(value="根据条件查询登录记录数量", params=@ApiField("查询条件"))
	public int findExportCount(LoggerLoginRecordExportBo exportBo){
		LoggerLoginRecordBigDataDo search=this.convertPojo(exportBo, LoggerLoginRecordBigDataDo.class);//将Bo转换为So
		loggerLoginRecordAdapter.setFieldToLike(search, "account", ObjectUtils.asList("account_"));
		loggerLoginRecordAdapter.addWhereBetweenList(search, "login_time_", exportBo.getLoginStartTime(), exportBo.getLoginEndTime(), "yyyy-MM-dd HH:mm:ss");
		return loggerLoginRecordAdapter.findCount(search);
	}
	
	@Override
	@ApiMethod(value="查询导出的数据", params=@ApiField("查询条件"), returns=@ApiField("导出的数据游标对象，支持大数据"))
	public void findExportData(LoggerLoginRecordExportBo exportBo, ResultHandler<LoggerLoginRecordDataDo> out){
		LoggerLoginRecordBigDataDo search=this.convertPojo(exportBo, LoggerLoginRecordBigDataDo.class);//将Bo转换为So
		loggerLoginRecordAdapter.findBigData(search, out);
	}
	
	@Override
	@ApiMethod(value="MyBatis流式查询读取的过程数据列表，开发者可对此数据加工", params=@ApiField("过程数据列表"))
	public void rowDataHandle(List<LoggerLoginRecordDataDo> exportDataList){
		
	}
	
	@Override
	@ApiMethod(value="ExcelColumn注解中translateBean的翻译方法", params={@ApiField("数据字典类型"), @ApiField("当前语言"), @ApiField("当前值")})
	public String translate(String type, String language, Object key) {
		if(ObjectUtils.isNull(key)){
			return null;
		}
		if(ObjectUtils.isEmpty(key)){
			return key.toString();
		}
		return cacheAdapter.findDictionaryValueByKey(type, language, key.toString());//翻译逻辑，默认返回自己
	}
}
