package com.unswift.cloud.service.export.system;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.ResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerExportTaskAdapter;
import com.unswift.cloud.adapter.system.auth.SystemUserAdapter;
import com.unswift.cloud.enums.export.task.ExportTaskEventEnum;
import com.unswift.cloud.excel.IExcelTranslate;
import com.unswift.cloud.export.IExportBusinessService;
import com.unswift.cloud.pojo.bo.system.user.SystemUserExportBo;
import com.unswift.cloud.pojo.bo.system.user.SystemUserExportStopBo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskInsertDo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.user.SystemUserBigDataDo;
import com.unswift.cloud.pojo.dao.system.user.SystemUserDataDo;
import com.unswift.cloud.pojo.mo.system.user.SystemUserExportMo;
import com.unswift.cloud.pojo.mso.logger.export.task.LoggerExportTaskQueueMso;
import com.unswift.cloud.pojo.vo.system.user.SystemUserExportStopVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserExportVo;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.service.BaseService;
import com.unswift.cloud.sql.system.user.SystemUserSql;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="会员导出服务", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemUserExportService extends BaseService implements IExportBusinessService<SystemUserExportBo, SystemUserDataDo, SystemUserExportMo>, IExcelTranslate, RabbitConstant{
	
	@Autowired
	@ApiField("会员公共服务")
	private SystemUserAdapter systemUserAdapter;
	
	@Autowired
	@ApiField("会员自定义sql")
	private SystemUserSql systemUserSql;
	
	@Autowired
	@ApiField("导出任务服务")
	private LoggerExportTaskAdapter loggerExportTaskAdapter;
	
	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	@ApiMethod(value="根据条件导出会员数据(不要加事务，否则有可能导致任务还没创建，但是队列已经消费查询，最终导致导出报错)", params=@ApiField("查询条件"))
	public SystemUserExportVo export(SystemUserExportBo exportBo){
		ExceptionUtils.trueException(loggerExportTaskAdapter.existsTasking(this.getUserId(), systemUserAdapter.getModule()), "task.is.currently.executing", "有会员导出");
		Date date=new Date();
		LoggerExportTaskInsertDo task=new LoggerExportTaskInsertDo();
		task.setName("会员");
		task.setModule(systemUserAdapter.getModule());
		task.setDocumentType("excel");
		task.setStatus((byte)0);
		task.setStartTime(date);
		task.setExportBeanClass(this.getClass().getName());
		task.setExportModelClass(SystemUserExportBo.class.getName());
		task.setExportCondition(JsonUtils.toJson(exportBo));
		task.setResult((byte)0);
		int result=loggerExportTaskAdapter.save(task, false);//保存任务，不用加事务，防止队列消费的时候还没保存完数据
		rabbitQueue.sendMessage(EXPORT_EXCHANGE_NAME, EXPORT_ROUTE_KEY, new LoggerExportTaskQueueMso(task.getId(), ExportTaskEventEnum.START, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送导出队列
		return new SystemUserExportVo(task.getId(), result);
	}
	
	@ApiMethod(value="停止导出会员", params=@ApiField("停止导出对象"))
	public SystemUserExportStopVo stop(SystemUserExportStopBo exportStopBo){
		rabbitQueue.sendMessage(EXPORT_STOP_EXCHANGE_NAME, EXPORT_STOP_ROUTE_KEY, new LoggerExportTaskQueueMso(exportStopBo.getId(), ExportTaskEventEnum.STOP, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送停止导出队列
		return new SystemUserExportStopVo(1);
	}
	
	@Override
	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	public Class<SystemUserExportMo> getModule(){
		return SystemUserExportMo.class;
	}
	
	@Override
	@ApiMethod(value="根据条件查询会员数量", params=@ApiField("查询条件"))
	public int findExportCount(SystemUserExportBo exportBo){
		SystemUserBigDataDo search=this.convertPojo(exportBo, SystemUserBigDataDo.class);//将Bo转换为So
		return systemUserAdapter.findCount(search);
	}
	
	@Override
	@ApiMethod(value="查询导出的数据", params=@ApiField("查询条件"), returns=@ApiField("导出的数据游标对象，支持大数据"))
	public void findExportData(SystemUserExportBo exportBo, ResultHandler<SystemUserDataDo> out){
		SystemUserBigDataDo search=this.convertPojo(exportBo, SystemUserBigDataDo.class);//将Bo转换为So
		Sql sql=Sql.createSql();
		sql.addSelect(systemUserSql.findAccountSql(), "account");
		sql.addSelect(systemUserSql.findPhoneSql(), "phone");
		sql.addSelect(systemUserSql.findEmailSql(), "email");
		sql.addSelect(systemUserSql.findRoleNamesSql(), "roleNames");
		search.setSql(sql);
		systemUserAdapter.findBigData(search, out);
	}
	
	@Override
	@ApiMethod(value="MyBatis流式查询读取的过程数据列表，开发者可对此数据加工", params=@ApiField("过程数据列表"))
	public void rowDataHandle(List<SystemUserDataDo> exportDataList){
		
	}
	
	@Override
	@ApiMethod(value="翻译数据", params = {@ApiField("数据字典类型"), @ApiField("数据字典key")})
	public String translate(String type, String language, Object key) {
		if(ObjectUtils.isNull(key)){
			return null;
		}
		if(ObjectUtils.isEmpty(key)){
			return key.toString();
		}
		return cacheAdapter.findDictionaryValueByKey(type, language, key.toString());//翻译逻辑，默认返回自己
	}
}
