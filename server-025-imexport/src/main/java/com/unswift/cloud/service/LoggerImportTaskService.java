package com.unswift.cloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerImportTaskAdapter;
import com.unswift.cloud.annotation.logger.OperatorLogger;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskCreateBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskDeleteBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskPageBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskProgressBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskUpdateBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskViewBo;
import com.unswift.cloud.pojo.dao.logger.imports.task.LoggerImportTaskDataDo;
import com.unswift.cloud.pojo.dao.logger.imports.task.LoggerImportTaskDeleteDo;
import com.unswift.cloud.pojo.dao.logger.imports.task.LoggerImportTaskInsertDo;
import com.unswift.cloud.pojo.dao.logger.imports.task.LoggerImportTaskPageDo;
import com.unswift.cloud.pojo.dao.logger.imports.task.LoggerImportTaskSingleDo;
import com.unswift.cloud.pojo.dao.logger.imports.task.LoggerImportTaskUpdateDo;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskCreateVo;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskDeleteVo;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskPageVo;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskProgressVo;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskUpdateVo;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskViewVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.utils.LoggerUtils;
import com.unswift.utils.ExceptionUtils;

@Service
@Api(value="导入任务记录表服务", author="liyunlong", date="2024-01-09", version="1.0.0")
public class LoggerImportTaskService extends BaseService{
	
	@Autowired
	@ApiField("导入任务记录表公共服务")
	private LoggerImportTaskAdapter loggerImportTaskAdapter;
	
	@ApiMethod(value="查询导入任务记录表分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<LoggerImportTaskPageVo> findPageList(LoggerImportTaskPageBo searchBo){
		LoggerImportTaskPageDo search=this.convertPojo(searchBo, LoggerImportTaskPageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<LoggerImportTaskDataDo> page=loggerImportTaskAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, LoggerImportTaskPageVo.class);
	}
	
	@ApiMethod(value="查询导入任务记录表详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("导入任务记录表详情数据"))
	public LoggerImportTaskViewVo view(LoggerImportTaskViewBo viewBo){
		LoggerImportTaskSingleDo search=this.convertPojo(viewBo, LoggerImportTaskSingleDo.class);
		LoggerImportTaskDataDo single=loggerImportTaskAdapter.findSingle(search);
		return this.convertPojo(single, LoggerImportTaskViewVo.class);//将Do转换为Vo
	}
	
	@ApiMethod(value="查询导出任务进度", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("导出任务记录表进度数据"))
	public LoggerImportTaskProgressVo findProgress(LoggerImportTaskProgressBo progressBo){
		LoggerImportTaskSingleDo search=this.convertPojo(progressBo, LoggerImportTaskSingleDo.class);
		LoggerImportTaskDataDo single=loggerImportTaskAdapter.findSingle(search);
		LoggerImportTaskProgressVo result = this.convertPojo(single, LoggerImportTaskProgressVo.class);
		return result;//将Do转换为Vo
	}

	@OperatorLogger(type = OperatorTypeEnum.CREATE)
	@ApiMethod(value="创建导入任务记录表", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public LoggerImportTaskCreateVo create(LoggerImportTaskCreateBo createBo){
		LoggerUtils.setModule(loggerImportTaskAdapter.getModule());//设置日志所属模块
		LoggerImportTaskInsertDo insert=this.convertPojo(createBo, LoggerImportTaskInsertDo.class);//将Bo转换为Do
		int result=loggerImportTaskAdapter.save(insert, true);
		LoggerUtils.setId(insert.getId());//创建日志需要设置数据id
		return new LoggerImportTaskCreateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新导入任务记录表", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public LoggerImportTaskUpdateVo update(LoggerImportTaskUpdateBo updateBo){
		LoggerUtils.setModule(loggerImportTaskAdapter.getModule());//设置日志所属模块
		LoggerImportTaskDataDo view=loggerImportTaskAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		LoggerImportTaskUpdateDo update=this.convertPojo(updateBo, LoggerImportTaskUpdateDo.class);//将Bo转换为Do
		int result=loggerImportTaskAdapter.update(update, true);
		return new LoggerImportTaskUpdateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.DELETE)
	@ApiMethod(value="删除导入任务记录表", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public LoggerImportTaskDeleteVo delete(LoggerImportTaskDeleteBo deleteBo){
		LoggerUtils.setModule(loggerImportTaskAdapter.getModule());//设置日志所属模块
		LoggerImportTaskDataDo deleteData=loggerImportTaskAdapter.findById(deleteBo.getId());
		ExceptionUtils.empty(deleteData, "delete.object.not.exists", "导入任务记录表信息");//删除对象必须存在
		LoggerImportTaskDeleteDo delete=this.convertPojo(deleteBo, LoggerImportTaskDeleteDo.class);//将Bo转换为Do
		int result=loggerImportTaskAdapter.delete(delete);
		return new LoggerImportTaskDeleteVo(result);
	}
}