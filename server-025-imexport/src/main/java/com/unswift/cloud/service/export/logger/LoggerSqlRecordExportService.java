package com.unswift.cloud.service.export.logger;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.ResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerExportTaskAdapter;
import com.unswift.cloud.adapter.logger.LoggerSqlRecordAdapter;
import com.unswift.cloud.enums.export.task.ExportTaskEventEnum;
import com.unswift.cloud.excel.IExcelTranslate;
import com.unswift.cloud.export.IExportBusinessService;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordExportBo;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordExportStopBo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskInsertDo;
import com.unswift.cloud.pojo.dao.logger.sql.record.LoggerSqlRecordBigDataDo;
import com.unswift.cloud.pojo.dao.logger.sql.record.LoggerSqlRecordDataDo;
import com.unswift.cloud.pojo.mo.logger.sql.record.LoggerSqlRecordExportMo;
import com.unswift.cloud.pojo.mso.logger.export.task.LoggerExportTaskQueueMso;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordExportStopVo;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordExportVo;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.service.BaseService;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="sql执行记录导出服务", author="unswift", date="2023-09-05", version="1.0.0")
public class LoggerSqlRecordExportService extends BaseService implements IExportBusinessService<LoggerSqlRecordExportBo, LoggerSqlRecordDataDo, LoggerSqlRecordExportMo>, IExcelTranslate, RabbitConstant{
	
	@Autowired
	@ApiField("sql执行记录公共服务")
	private LoggerSqlRecordAdapter loggerSqlRecordAdapter;
	
	@Autowired
	@ApiField("导出任务服务")
	private LoggerExportTaskAdapter loggerExportTaskAdapter;
	
	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	@ApiMethod(value="根据条件导出sql执行记录数据", params=@ApiField("查询条件"))
	public LoggerSqlRecordExportVo export(LoggerSqlRecordExportBo exportBo){
		Date date=new Date();
		LoggerExportTaskInsertDo task=new LoggerExportTaskInsertDo();
		task.setName("sql执行记录");
		task.setModule(loggerSqlRecordAdapter.getModule());
		task.setDocumentType("excel");
		task.setStatus((byte)0);
		task.setStartTime(date);
		task.setExportBeanClass(this.getClass().getName());
		task.setExportModelClass(LoggerSqlRecordExportBo.class.getName());
		task.setExportCondition(JsonUtils.toJson(exportBo));
		task.setResult((byte)0);
		int result=loggerExportTaskAdapter.save(task, false);//保存任务，不用加事务，防止队列消费的时候还没保存完数据
		rabbitQueue.sendMessage(EXPORT_EXCHANGE_NAME, EXPORT_ROUTE_KEY, new LoggerExportTaskQueueMso(task.getId(), ExportTaskEventEnum.START, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送导出队列
		return new LoggerSqlRecordExportVo(task.getId(), result);
	}
	
	@ApiMethod(value="停止导出sql执行记录", params=@ApiField("停止导出对象"))
	public LoggerSqlRecordExportStopVo stop(LoggerSqlRecordExportStopBo exportStopBo){
		rabbitQueue.sendMessage(EXPORT_STOP_EXCHANGE_NAME, EXPORT_STOP_ROUTE_KEY, new LoggerExportTaskQueueMso(exportStopBo.getId(), ExportTaskEventEnum.STOP, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送停止导出队列
		return new LoggerSqlRecordExportStopVo(1);
	}
	
	@Override
	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	public Class<LoggerSqlRecordExportMo> getModule(){
		return LoggerSqlRecordExportMo.class;
	}
	
	@Override
	@ApiMethod(value="根据条件查询sql执行记录数量", params=@ApiField("查询条件"))
	public int findExportCount(LoggerSqlRecordExportBo exportBo){
		LoggerSqlRecordBigDataDo search=this.convertPojo(exportBo, LoggerSqlRecordBigDataDo.class);//将Bo转换为So
		return loggerSqlRecordAdapter.findCount(search);
	}
	
	@Override
	@ApiMethod(value="查询导出的数据", params=@ApiField("查询条件"), returns=@ApiField("导出的数据游标对象，支持大数据"))
	public void findExportData(LoggerSqlRecordExportBo exportBo, ResultHandler<LoggerSqlRecordDataDo> out){
		LoggerSqlRecordBigDataDo search=this.convertPojo(exportBo, LoggerSqlRecordBigDataDo.class);//将Bo转换为So
		loggerSqlRecordAdapter.findBigData(search, out);
	}
	
	@Override
	@ApiMethod(value="MyBatis流式查询读取的过程数据列表，开发者可对此数据加工", params=@ApiField("过程数据列表"))
	public void rowDataHandle(List<LoggerSqlRecordDataDo> exportDataList){
		
	}
	
	@Override
	@ApiMethod(value="翻译数据", params = {@ApiField("数据字典类型"), @ApiField("数据字典key")})
	public String translate(String type, String language, Object key) {
		if(ObjectUtils.isNull(key)){
			return null;
		}
		if(ObjectUtils.isEmpty(key)){
			return key.toString();
		}
		return cacheAdapter.findDictionaryValueByKey(type, language, key.toString());//翻译逻辑，默认返回自己
	}
}
