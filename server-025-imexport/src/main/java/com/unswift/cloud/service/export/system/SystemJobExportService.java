package com.unswift.cloud.service.export.system;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.ResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerExportTaskAdapter;
import com.unswift.cloud.adapter.system.organization.SystemJobAdapter;
import com.unswift.cloud.enums.export.task.ExportTaskEventEnum;
import com.unswift.cloud.excel.IExcelTranslate;
import com.unswift.cloud.export.IExportBusinessService;
import com.unswift.cloud.pojo.bo.system.job.SystemJobExportBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobExportStopBo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskInsertDo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.job.SystemJobBigDataDo;
import com.unswift.cloud.pojo.dao.system.job.SystemJobDataDo;
import com.unswift.cloud.pojo.mo.system.job.SystemJobExportMo;
import com.unswift.cloud.pojo.mso.logger.export.task.LoggerExportTaskQueueMso;
import com.unswift.cloud.pojo.vo.system.job.SystemJobExportStopVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobExportVo;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.service.BaseService;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="职位导出服务", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemJobExportService extends BaseService implements IExportBusinessService<SystemJobExportBo, SystemJobDataDo, SystemJobExportMo>, IExcelTranslate, RabbitConstant{
	
	@Autowired
	@ApiField("职位公共服务")
	private SystemJobAdapter systemJobAdapter;
	
	@Autowired
	@ApiField("导出任务服务")
	private LoggerExportTaskAdapter loggerExportTaskAdapter;
	
	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	@ApiMethod(value="根据条件导出职位数据", params=@ApiField("查询条件"))
	public SystemJobExportVo export(SystemJobExportBo exportBo){
		ExceptionUtils.trueException(loggerExportTaskAdapter.existsTasking(this.getUserId(), systemJobAdapter.getModule()), "task.is.currently.executing", "有职位导出");
		Date date=new Date();
		LoggerExportTaskInsertDo task=new LoggerExportTaskInsertDo();
		task.setName("职位");
		task.setModule(systemJobAdapter.getModule());
		task.setDocumentType("excel");
		task.setStatus((byte)0);
		task.setStartTime(date);
		task.setExportBeanClass(this.getClass().getName());
		task.setExportModelClass(SystemJobExportBo.class.getName());
		task.setExportCondition(JsonUtils.toJson(exportBo));
		task.setResult((byte)0);
		int result=loggerExportTaskAdapter.save(task, false);//保存任务，不用加事务，防止队列消费的时候还没保存完数据
		rabbitQueue.sendMessage(EXPORT_EXCHANGE_NAME, EXPORT_ROUTE_KEY, new LoggerExportTaskQueueMso(task.getId(), ExportTaskEventEnum.START, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送导出队列
		return new SystemJobExportVo(task.getId(), result);
	}
	
	@ApiMethod(value="停止导出职位", params=@ApiField("停止导出对象"))
	public SystemJobExportStopVo stop(SystemJobExportStopBo exportStopBo){
		rabbitQueue.sendMessage(EXPORT_STOP_EXCHANGE_NAME, EXPORT_STOP_ROUTE_KEY, new LoggerExportTaskQueueMso(exportStopBo.getId(), ExportTaskEventEnum.STOP, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送停止导出队列
		return new SystemJobExportStopVo(1);
	}
	
	@Override
	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	public Class<SystemJobExportMo> getModule(){
		return SystemJobExportMo.class;
	}
	
	@Override
	@ApiMethod(value="根据条件查询职位数量", params=@ApiField("查询条件"))
	public int findExportCount(SystemJobExportBo exportBo){
		SystemJobBigDataDo search=this.convertPojo(exportBo, SystemJobBigDataDo.class);//将Bo转换为So
		return systemJobAdapter.findCount(search);
	}
	
	@Override
	@ApiMethod(value="查询导出的数据", params=@ApiField("查询条件"), returns=@ApiField("导出的数据游标对象，支持大数据"))
	public void findExportData(SystemJobExportBo exportBo, ResultHandler<SystemJobDataDo> out){
		SystemJobBigDataDo search=this.convertPojo(exportBo, SystemJobBigDataDo.class);//将Bo转换为So
		systemJobAdapter.setCreateUserSql(search, "createUserName");
		systemJobAdapter.addOrderBy(search, "sort_", Sql.ORDER_BY_ASC);
		systemJobAdapter.findBigData(search, out);
	}
	
	@Override
	@ApiMethod(value="MyBatis流式查询读取的过程数据列表，开发者可对此数据加工", params=@ApiField("过程数据列表"))
	public void rowDataHandle(List<SystemJobDataDo> exportDataList){
		
	}
	
	@Override
	@ApiMethod(value="ExcelColumn注解中translateBean的翻译方法", params={@ApiField("数据字典类型"), @ApiField("当前值")})
	public String translate(String type, String language, Object key) {
		if(ObjectUtils.isNull(key)){
			return null;
		}
		if(ObjectUtils.isEmpty(key)){
			return key.toString();
		}
		return cacheAdapter.findDictionaryValueByKey(type, language, key.toString());//翻译逻辑，默认返回自己
	}
}
