package com.unswift.cloud.service.imports.system;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerImportTaskAdapter;
import com.unswift.cloud.adapter.system.config.SystemDictionaryAdapter;
import com.unswift.cloud.adapter.system.config.SystemDictionaryTypeAdapter;
import com.unswift.cloud.adapter.system.config.SystemLanguageAdapter;
import com.unswift.cloud.adapter.validate.IValidateAdapter;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.enums.imports.task.ImportTaskEventEnum;
import com.unswift.cloud.imports.IImportBusinessService;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryImportBo;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryImportStopBo;
import com.unswift.cloud.pojo.dao.logger.imports.task.LoggerImportTaskInsertDo;
import com.unswift.cloud.pojo.dao.system.dictionary.SystemDictionaryDataDo;
import com.unswift.cloud.pojo.dao.system.dictionary.SystemDictionaryInsertDo;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeDataDo;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageDataDo;
import com.unswift.cloud.pojo.mo.system.dictionary.SystemDictionaryImportMo;
import com.unswift.cloud.pojo.mso.logger.imports.task.LoggerImportTaskQueueMso;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryImportStopVo;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryImportVo;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.service.BaseService;
import com.unswift.cloud.service.LoggerService;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="数据字典导入服务", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryImportService extends BaseService implements IImportBusinessService<SystemDictionaryImportMo, SystemDictionaryImportBo>, RabbitConstant{
	
	@Autowired
	@ApiField("数据字典公共服务")
	private SystemDictionaryAdapter systemDictionaryAdapter;
	
	@Autowired
	@ApiField("数据字典类型公共服务")
	private SystemDictionaryTypeAdapter systemDictionaryTypeAdapter;
	
	@Autowired
	@ApiField("语言公共服务")
	private SystemLanguageAdapter systemLanguageAdapter;
	
	@Autowired
	@ApiField("导出任务服务")
	private LoggerImportTaskAdapter loggerImportTaskAdapter;
	
	@Autowired
	@ApiField("操作日志服务")
	private LoggerService loggerService;
	
	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	@Autowired
	@ApiField("表单验证公共操作")
	private IValidateAdapter validateAdapter;
	
	
	@ApiMethod(value="导入会员(不要加事务，否则有可能导致任务还没创建，但是队列已经消费查询，最终导致导入报错)", params=@ApiField("导入业务实体，包含附件信息"), returns=@ApiField("导入结果Vo->result:{0：未被导入，1：已成功导入}"))
	public SystemDictionaryImportVo imports(SystemDictionaryImportBo importBo) {
		ExceptionUtils.trueException(loggerImportTaskAdapter.existsTasking(this.getUserId(), systemDictionaryAdapter.getModule()), "task.is.currently.executing", "有数据字典导入");
		Date date=new Date();
		LoggerImportTaskInsertDo task=new LoggerImportTaskInsertDo();
		task.setName("数据字典");
		task.setModule(systemDictionaryAdapter.getModule());
		task.setDocumentType("excel");
		task.setStatus((byte)0);
		task.setStartTime(date);
		task.setImportBeanClass(this.getClass().getName());
		task.setImportModelClass(SystemDictionaryImportBo.class.getName());
		task.setImportData(JsonUtils.toJson(importBo));
		task.setResult((byte)0);
		int result=loggerImportTaskAdapter.save(task, false);//保存任务，不用加事务，防止队列消费的时候还没保存完数据
		rabbitQueue.sendMessage(IMPORT_EXCHANGE_NAME, IMPORT_ROUTE_KEY, new LoggerImportTaskQueueMso(task.getId(), ImportTaskEventEnum.START, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送导出队列
		return new SystemDictionaryImportVo(task.getId(), result);
	}
	
	@ApiMethod(value="停止导出数据字典", params=@ApiField("停止导出对象"))
	public SystemDictionaryImportStopVo stop(SystemDictionaryImportStopBo importStopBo){
		rabbitQueue.sendMessage(IMPORT_STOP_EXCHANGE_NAME, IMPORT_STOP_ROUTE_KEY, new LoggerImportTaskQueueMso(importStopBo.getId(), ImportTaskEventEnum.STOP, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送停止导出队列
		return new SystemDictionaryImportStopVo(1);
	}
	
	@Override
	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	public Class<SystemDictionaryImportMo> getModule(){
		return SystemDictionaryImportMo.class;
	}
	
	@Override
	@ApiMethod(value="导入开始时执行")
	public void start() {
		validateAdapter.setAttribute("typeDb", systemDictionaryTypeAdapter);//设置数据字典类型公共操作变量，保证在验证对象中可查询数据字典类型相关的业务
		validateAdapter.setAttribute("languageDb", systemLanguageAdapter);//设置语言公共操作变量，保证在验证对象中可查询语言相关的业务
	}
	
	@Override
	@ApiMethod(value="导入行数据验证，请再此处处理完所有的验证，否则可能导致数据导入一部分", params = {@ApiField("需验证的行数据"), @ApiField("此数据在excel中的行次（从零开始）"), @ApiField("操作导出的用户token"), @ApiField("导入条件")})
	public void validate(SystemDictionaryImportMo rowData, int rowIndex, String token, SystemDictionaryImportBo condition) {
		rowData.setRowNumber(rowIndex+1);
		rowData.setParentNameExistsExcel(false);
		String parentNameKey=getUserToken(token)+"-ParentKeyPath";
		String parentKeyPath=ObjectUtils.isEmpty(rowData.getParentKeyPath())?rowData.getKey():rowData.getParentKeyPath()+"/"+rowData.getKey();
		String hashKey = String.format("%s-%s-%s", rowData.getTypeName(), rowData.getLanguage(), parentKeyPath, rowData.getLanguage());
		if(!memoryCache.existsHash(parentNameKey, hashKey)) {
			memoryCache.setHash(parentNameKey, hashKey, true);
		}
		if(ObjectUtils.isNotEmpty(rowData.getParentKeyPath())) {
			hashKey = String.format("%s-%s-%s", rowData.getTypeName(), rowData.getLanguage(), rowData.getParentKeyPath());
			if(memoryCache.existsHash(parentNameKey, hashKey)) {
				rowData.setParentNameExistsExcel(true);
			}
		}
		
		validateAdapter.validate(systemDictionaryAdapter.getModule(), "import", rowData, this.getUser(token), systemDictionaryAdapter);//验证表单
	}
	
	@Override
	@ApiMethod(value="导入数据批量处理，批次数据根据nocos配置确定批次大小", params = {@ApiField("批次数据"), @ApiField("此列表在excel中的开始行次（从零开始）"), @ApiField("操作导出的用户token"), @ApiField("导入条件")})
	public void handleBatch(List<SystemDictionaryImportMo> rowList, int startRowIndex, String token, SystemDictionaryImportBo condition) {
		SystemDictionaryInsertDo insert;
		SystemDictionaryTypeDataDo type;
		SystemLanguageDataDo language;
		SystemDictionaryDataDo parent;
		for (SystemDictionaryImportMo importData : rowList) {
			insert=new SystemDictionaryInsertDo();
			ObjectUtils.copy(importData, insert);
			type = systemDictionaryTypeAdapter.findByName(importData.getTypeName());
			language=systemLanguageAdapter.findByFullName(importData.getLanguage());
			insert.setType(type.getCode());
			insert.setLanguage(language.getShortName());
			if(ObjectUtils.isNotEmpty(importData.getParentKeyPath())) {
				parent=systemDictionaryAdapter.findByKeyPath(type.getCode(), language.getShortName(), importData.getParentKeyPath());
				insert.setParentId(parent.getId());
			}
			insert.setSort(systemDictionaryAdapter.findMaxSort(type.getCode(), insert.getParentId()));
			insert.setStatus((byte)1);
			systemDictionaryAdapter.save(insert, false);
			loggerService.writer(insert.getId(), systemDictionaryAdapter.getModule(), OperatorTypeEnum.IMPORT, token, importData);//记录日志
		}
	}
	
	@Override
	@ApiMethod(value="结束导入时触发，如清理临时缓存等", params = {@ApiField("用户token")})
	public void finish(String token) {
		validateAdapter.finish();//验证结束
	}
}