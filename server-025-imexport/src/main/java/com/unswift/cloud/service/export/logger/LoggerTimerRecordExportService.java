package com.unswift.cloud.service.export.logger;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.ResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerExportTaskAdapter;
import com.unswift.cloud.adapter.logger.LoggerTimerRecordAdapter;
import com.unswift.cloud.enums.export.task.ExportTaskEventEnum;
import com.unswift.cloud.excel.IExcelTranslate;
import com.unswift.cloud.export.IExportBusinessService;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordExportBo;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordExportStopBo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskInsertDo;
import com.unswift.cloud.pojo.dao.logger.timer.record.LoggerTimerRecordBigDataDo;
import com.unswift.cloud.pojo.dao.logger.timer.record.LoggerTimerRecordDataDo;
import com.unswift.cloud.pojo.mo.logger.timer.record.LoggerTimerRecordExportMo;
import com.unswift.cloud.pojo.mso.logger.export.task.LoggerExportTaskQueueMso;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordExportStopVo;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordExportVo;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.service.BaseService;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="定时器执行记录导出服务", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerTimerRecordExportService extends BaseService implements IExportBusinessService<LoggerTimerRecordExportBo, LoggerTimerRecordDataDo, LoggerTimerRecordExportMo>, IExcelTranslate, RabbitConstant{
	
	@Autowired
	@ApiField("定时器执行记录公共服务")
	private LoggerTimerRecordAdapter loggerTimerRecordAdapter;
	
	@Autowired
	@ApiField("导出任务服务")
	private LoggerExportTaskAdapter loggerExportTaskAdapter;
	
	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	@ApiMethod(value="根据条件导出定时器执行记录数据", params=@ApiField("查询条件"))
	public LoggerTimerRecordExportVo export(LoggerTimerRecordExportBo exportBo){
		Date date=new Date();
		LoggerExportTaskInsertDo task=new LoggerExportTaskInsertDo();
		task.setName("定时器执行记录");
		task.setModule(loggerTimerRecordAdapter.getModule());
		task.setDocumentType("excel");
		task.setStatus((byte)0);
		task.setStartTime(date);
		task.setExportBeanClass(this.getClass().getName());
		task.setExportModelClass(LoggerTimerRecordExportBo.class.getName());
		task.setExportCondition(JsonUtils.toJson(exportBo));
		task.setResult((byte)0);
		int result=loggerExportTaskAdapter.save(task, false);//保存任务，不用加事务，防止队列消费的时候还没保存完数据
		rabbitQueue.sendMessage(EXPORT_EXCHANGE_NAME, EXPORT_ROUTE_KEY, new LoggerExportTaskQueueMso(task.getId(), ExportTaskEventEnum.START, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送导出队列
		return new LoggerTimerRecordExportVo(task.getId(), result);
	}
	
	@ApiMethod(value="停止导出定时器执行记录", params=@ApiField("停止导出对象"))
	public LoggerTimerRecordExportStopVo stop(LoggerTimerRecordExportStopBo exportStopBo){
        rabbitQueue.sendMessage(EXPORT_STOP_EXCHANGE_NAME, EXPORT_STOP_ROUTE_KEY, new LoggerExportTaskQueueMso(exportStopBo.getId(), ExportTaskEventEnum.STOP, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送停止导出队列
		return new LoggerTimerRecordExportStopVo(1);
	}
	
	@Override
	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	public Class<LoggerTimerRecordExportMo> getModule(){
		return LoggerTimerRecordExportMo.class;
	}
	
	@Override
	@ApiMethod(value="根据条件查询定时器执行记录数量", params=@ApiField("查询条件"))
	public int findExportCount(LoggerTimerRecordExportBo exportBo){
		LoggerTimerRecordBigDataDo search=this.convertPojo(exportBo, LoggerTimerRecordBigDataDo.class);//将Bo转换为So
		return loggerTimerRecordAdapter.findCount(search);
	}
	
	@Override
	@ApiMethod(value="查询导出的数据", params=@ApiField("查询条件"), returns=@ApiField("导出的数据游标对象，支持大数据"))
	public void findExportData(LoggerTimerRecordExportBo exportBo, ResultHandler<LoggerTimerRecordDataDo> out){
		LoggerTimerRecordBigDataDo search=this.convertPojo(exportBo, LoggerTimerRecordBigDataDo.class);//将Bo转换为So
		loggerTimerRecordAdapter.findBigData(search, out);
	}
	
	@Override
	@ApiMethod(value="MyBatis流式查询读取的过程数据列表，开发者可对此数据加工", params=@ApiField("过程数据列表"))
	public void rowDataHandle(List<LoggerTimerRecordDataDo> exportDataList){
		
	}
	
	@Override
	@ApiMethod(value="翻译数据", params = {@ApiField("数据字典类型"), @ApiField("数据字典key")})
	public String translate(String type, String language, Object key) {
		if(ObjectUtils.isNull(key)){
			return null;
		}
		if(ObjectUtils.isEmpty(key)){
			return key.toString();
		}
		return cacheAdapter.findDictionaryValueByKey(type, language, key.toString());//翻译逻辑，默认返回自己
	}
}
