package com.unswift.cloud.service.export.issue;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.ResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.issue.environment.IssueEnvironmentAdapter;
import com.unswift.cloud.adapter.logger.LoggerExportTaskAdapter;
import com.unswift.cloud.enums.export.task.ExportTaskEventEnum;
import com.unswift.cloud.excel.IExcelTranslate;
import com.unswift.cloud.export.IExportBusinessService;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentExportBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentExportStopBo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskInsertDo;
import com.unswift.cloud.pojo.dao.issue.environment.IssueEnvironmentBigDataDo;
import com.unswift.cloud.pojo.dao.issue.environment.IssueEnvironmentDataDo;
import com.unswift.cloud.pojo.mo.issue.environment.IssueEnvironmentExportMo;
import com.unswift.cloud.pojo.mso.logger.export.task.LoggerExportTaskQueueMso;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentExportVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentExportStopVo;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.service.BaseService;
import com.unswift.cloud.sql.issue.environment.IssueEnvironmentSql;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="发布环境导出服务", author="liyunlong", date="2024-04-16", version="1.0.0")
public class IssueEnvironmentExportService extends BaseService implements IExportBusinessService<IssueEnvironmentExportBo, IssueEnvironmentDataDo, IssueEnvironmentExportMo>, IExcelTranslate, RabbitConstant{
	
	@Autowired
	@ApiField("发布环境公共服务")
	private IssueEnvironmentAdapter issueEnvironmentAdapter;
	
	@Autowired
	@ApiField("发布环境自定义Sql")
	private IssueEnvironmentSql issueEnvironmentSql;
	
	@Autowired
	@ApiField("导出任务服务")
	private LoggerExportTaskAdapter loggerExportTaskAdapter;
	
	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	@ApiMethod(value="根据条件导出发布环境数据", params=@ApiField("查询条件"))
	public IssueEnvironmentExportVo export(IssueEnvironmentExportBo exportBo){
		ExceptionUtils.trueException(loggerExportTaskAdapter.existsTasking(this.getUserId(), issueEnvironmentAdapter.getModule()), "task.is.currently.executing", "有发布环境导出");
		Date date=new Date();
		LoggerExportTaskInsertDo task=new LoggerExportTaskInsertDo();
		task.setName("发布环境");
		task.setModule(issueEnvironmentAdapter.getModule());
		task.setDocumentType("excel");
		task.setStatus((byte)0);
		task.setStartTime(date);
		task.setExportBeanClass(this.getClass().getName());
		task.setExportModelClass(IssueEnvironmentExportBo.class.getName());
		task.setExportCondition(JsonUtils.toJson(exportBo));
		task.setResult((byte)0);
		int result=loggerExportTaskAdapter.save(task, false);//保存任务，不用加事务，防止队列消费的时候还没保存完数据
		rabbitQueue.sendMessage(EXPORT_EXCHANGE_NAME, EXPORT_ROUTE_KEY, new LoggerExportTaskQueueMso(task.getId(), ExportTaskEventEnum.START, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送导出队列
		return new IssueEnvironmentExportVo(task.getId(), result);
	}
	
	@ApiMethod(value="停止导出发布环境", params=@ApiField("停止导出对象"))
	public IssueEnvironmentExportStopVo stop(IssueEnvironmentExportStopBo exportStopBo){
		rabbitQueue.sendMessage(EXPORT_STOP_EXCHANGE_NAME, EXPORT_STOP_ROUTE_KEY, new LoggerExportTaskQueueMso(exportStopBo.getId(), ExportTaskEventEnum.STOP, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送停止导出队列
		return new IssueEnvironmentExportStopVo(1);
	}
	
	@Override
	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	public Class<IssueEnvironmentExportMo> getModule(){
		return IssueEnvironmentExportMo.class;
	}
	
	@Override
	@ApiMethod(value="根据条件查询发布环境数量", params=@ApiField("查询条件"))
	public int findExportCount(IssueEnvironmentExportBo exportBo){
		IssueEnvironmentBigDataDo search=this.convertPojo(exportBo, IssueEnvironmentBigDataDo.class);//将Bo转换为So
		return issueEnvironmentAdapter.findCount(search);
	}
	
	@Override
	@ApiMethod(value="查询导出的数据", params=@ApiField("查询条件"), returns=@ApiField("导出的数据游标对象，支持大数据"))
	public void findExportData(IssueEnvironmentExportBo exportBo, ResultHandler<IssueEnvironmentDataDo> out){
		IssueEnvironmentBigDataDo search=this.convertPojo(exportBo, IssueEnvironmentBigDataDo.class);//将Bo转换为So
		issueEnvironmentAdapter.findBigData(search, out);
	}
	
	@Override
	@ApiMethod(value="MyBatis流式查询读取的过程数据列表，开发者可对此数据加工", params=@ApiField("过程数据列表"))
	public void rowDataHandle(List<IssueEnvironmentDataDo> exportDataList){
		
	}
	
	@Override
	@ApiMethod(value="ExcelColumn注解中translateBean的翻译方法", params={@ApiField("数据字典类型"), @ApiField("当前语言"), @ApiField("当前值")})
	public String translate(String type, String language, Object key) {
		if(ObjectUtils.isNull(key)){
			return null;
		}
		if(ObjectUtils.isEmpty(key)){
			return key.toString();
		}
		return cacheAdapter.findDictionaryValueByKey(type, language, key.toString());//翻译逻辑，默认返回自己
	}
}
