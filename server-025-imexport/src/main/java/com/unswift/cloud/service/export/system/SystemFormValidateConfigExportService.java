package com.unswift.cloud.service.export.system;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.ResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerExportTaskAdapter;
import com.unswift.cloud.adapter.system.config.SystemFormValidateConfigAdapter;
import com.unswift.cloud.enums.export.task.ExportTaskEventEnum;
import com.unswift.cloud.excel.IExcelTranslate;
import com.unswift.cloud.export.IExportBusinessService;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigExportBo;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigExportStopBo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskInsertDo;
import com.unswift.cloud.pojo.dao.system.form.validate.config.SystemFormValidateConfigBigDataDo;
import com.unswift.cloud.pojo.dao.system.form.validate.config.SystemFormValidateConfigDataDo;
import com.unswift.cloud.pojo.mo.system.form.validate.config.SystemFormValidateConfigExportMo;
import com.unswift.cloud.pojo.mso.logger.export.task.LoggerExportTaskQueueMso;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigExportStopVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigExportVo;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.service.BaseService;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="系统表单验证基础配置导出服务", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateConfigExportService extends BaseService implements IExportBusinessService<SystemFormValidateConfigExportBo, SystemFormValidateConfigDataDo, SystemFormValidateConfigExportMo>, IExcelTranslate, RabbitConstant{
	
	@Autowired
	@ApiField("系统表单验证基础配置公共服务")
	private SystemFormValidateConfigAdapter systemFormValidateConfigAdapter;
	
	@Autowired
	@ApiField("导出任务服务")
	private LoggerExportTaskAdapter loggerExportTaskAdapter;
	
	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	@ApiMethod(value="根据条件导出系统表单验证基础配置数据", params=@ApiField("查询条件"))
	public SystemFormValidateConfigExportVo export(SystemFormValidateConfigExportBo exportBo){
		ExceptionUtils.trueException(loggerExportTaskAdapter.existsTasking(this.getUserId(), systemFormValidateConfigAdapter.getModule()), "task.is.currently.executing", "有系统表单验证基础配置导出");
		Date date=new Date();
		LoggerExportTaskInsertDo task=new LoggerExportTaskInsertDo();
		task.setName("系统表单验证基础配置");
		task.setModule(systemFormValidateConfigAdapter.getModule());
		task.setDocumentType("excel");
		task.setStatus((byte)0);
		task.setStartTime(date);
		task.setExportBeanClass(this.getClass().getName());
		task.setExportModelClass(SystemFormValidateConfigExportBo.class.getName());
		task.setExportCondition(JsonUtils.toJson(exportBo));
		task.setResult((byte)0);
		int result=loggerExportTaskAdapter.save(task, false);//保存任务，不用加事务，防止队列消费的时候还没保存完数据
		rabbitQueue.sendMessage(EXPORT_EXCHANGE_NAME, EXPORT_ROUTE_KEY, new LoggerExportTaskQueueMso(task.getId(), ExportTaskEventEnum.START, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送导出队列
		return new SystemFormValidateConfigExportVo(task.getId(), result);
	}
	
	@ApiMethod(value="停止导出系统表单验证基础配置", params=@ApiField("停止导出对象"))
	public SystemFormValidateConfigExportStopVo stop(SystemFormValidateConfigExportStopBo exportStopBo){
		rabbitQueue.sendMessage(EXPORT_STOP_EXCHANGE_NAME, EXPORT_STOP_ROUTE_KEY, new LoggerExportTaskQueueMso(exportStopBo.getId(), ExportTaskEventEnum.STOP, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送停止导出队列
		return new SystemFormValidateConfigExportStopVo(1);
	}
	
	@Override
	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	public Class<SystemFormValidateConfigExportMo> getModule(){
		return SystemFormValidateConfigExportMo.class;
	}
	
	@Override
	@ApiMethod(value="根据条件查询系统表单验证基础配置数量", params=@ApiField("查询条件"))
	public int findExportCount(SystemFormValidateConfigExportBo exportBo){
		SystemFormValidateConfigBigDataDo search=this.convertPojo(exportBo, SystemFormValidateConfigBigDataDo.class);//将Bo转换为So
		return systemFormValidateConfigAdapter.findCount(search);
	}
	
	@Override
	@ApiMethod(value="查询导出的数据", params=@ApiField("查询条件"), returns=@ApiField("导出的数据游标对象，支持大数据"))
	public void findExportData(SystemFormValidateConfigExportBo exportBo, ResultHandler<SystemFormValidateConfigDataDo> out){
		SystemFormValidateConfigBigDataDo search=this.convertPojo(exportBo, SystemFormValidateConfigBigDataDo.class);//将Bo转换为So
		systemFormValidateConfigAdapter.findBigData(search, out);
	}
	
	@Override
	@ApiMethod(value="MyBatis流式查询读取的过程数据列表，开发者可对此数据加工", params=@ApiField("过程数据列表"))
	public void rowDataHandle(List<SystemFormValidateConfigDataDo> exportDataList){
		
	}
	
	@Override
	@ApiMethod(value="翻译数据", params = {@ApiField("数据字典类型"), @ApiField("数据字典key")})
	public String translate(String type, String language, Object key) {
		if(ObjectUtils.isNull(key)){
			return null;
		}
		if(ObjectUtils.isEmpty(key)){
			return key.toString();
		}
		return cacheAdapter.findDictionaryValueByKey(type, language, key.toString());//翻译逻辑，默认返回自己
	}
}
