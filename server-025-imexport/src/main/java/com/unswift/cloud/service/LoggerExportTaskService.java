package com.unswift.cloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerExportTaskAdapter;
import com.unswift.cloud.feign.AttachFeign;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskCreateBo;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskDeleteBo;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskPageBo;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskProgressBo;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskUpdateBo;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskViewBo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskDataDo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskDeleteDo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskInsertDo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskPageDo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskSingleDo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskUpdateDo;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachDownloadPathDto;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskCreateVo;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskDeleteVo;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskPageVo;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskProgressVo;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskUpdateVo;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskViewVo;
import com.unswift.cloud.pojo.vo.page.IPageVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachDownloadPathVo;
import com.unswift.utils.ExceptionUtils;

@Service
@Api(value="导出任务记录表服务", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerExportTaskService extends BaseService{
	
	@Autowired
	@ApiField("附件feign")
	private AttachFeign attachFeign;
	
	@Autowired
	@ApiField("导出任务记录表公共服务")
	private LoggerExportTaskAdapter loggerExportTaskAdapter;
	
	@ApiMethod(value="查询导出任务记录表分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public IPageVo<LoggerExportTaskPageVo> findPageList(LoggerExportTaskPageBo searchBo){
		LoggerExportTaskPageDo search=this.convertPojo(searchBo, LoggerExportTaskPageDo.class);//将Bo转换为So
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<LoggerExportTaskDataDo> page=loggerExportTaskAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, LoggerExportTaskPageVo.class);
	}
	
	@ApiMethod(value="查询导出任务记录表详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("导出任务记录表详情数据"))
	public LoggerExportTaskViewVo view(LoggerExportTaskViewBo viewBo){
		LoggerExportTaskSingleDo search=this.convertPojo(viewBo, LoggerExportTaskSingleDo.class);
		LoggerExportTaskDataDo single=loggerExportTaskAdapter.findSingle(search);
		return this.convertPojo(single, LoggerExportTaskViewVo.class);//将Do转换为Vo
	}
	
	@ApiMethod(value="查询导出任务进度", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("导出任务记录表进度数据"))
	public LoggerExportTaskProgressVo findProgress(LoggerExportTaskProgressBo progressBo){
		LoggerExportTaskSingleDo search=this.convertPojo(progressBo, LoggerExportTaskSingleDo.class);
		LoggerExportTaskDataDo single=loggerExportTaskAdapter.findSingle(search);
		LoggerExportTaskProgressVo result = this.convertPojo(single, LoggerExportTaskProgressVo.class);
		if(single.getResult()==1) {
			SystemAttachDownloadPathVo body = attachFeign.getAttachDownloadPath(new SystemAttachDownloadPathDto(result.getId(), result.getModule())).getBody();
			ExceptionUtils.empty(body, "data.not.found", "附件信息");
			ExceptionUtils.empty(body.getFileUrl(), "data.not.found", "附件下载路径");
			result.setExportFileUrl(body.getFileUrl());
			result.setExportFileName(body.getName());
		}
		return result;//将Do转换为Vo
	}

	@ApiMethod(value="创建导出任务记录表", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public LoggerExportTaskCreateVo create(LoggerExportTaskCreateBo createBo){
		LoggerExportTaskInsertDo insert=this.convertPojo(createBo, LoggerExportTaskInsertDo.class);//将Bo转换为Po
		int result=loggerExportTaskAdapter.save(insert, true);
		return new LoggerExportTaskCreateVo(result);
	}
	
	@ApiMethod(value="更新导出任务记录表", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public LoggerExportTaskUpdateVo update(LoggerExportTaskUpdateBo updateBo){
		LoggerExportTaskUpdateDo update=this.convertPojo(updateBo, LoggerExportTaskUpdateDo.class);//将Bo转换为Po
		int result=loggerExportTaskAdapter.update(update, true);
		return new LoggerExportTaskUpdateVo(result);
	}
	
	@ApiMethod(value="删除导出任务记录表", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public LoggerExportTaskDeleteVo delete(LoggerExportTaskDeleteBo deleteBo){
		LoggerExportTaskDeleteDo delete=this.convertPojo(deleteBo, LoggerExportTaskDeleteDo.class);//将Bo转换为Po
		int result=loggerExportTaskAdapter.delete(delete);
		return new LoggerExportTaskDeleteVo(result);
	}
}
