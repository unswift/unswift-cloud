package com.unswift.cloud.service.export.system;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.ResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerExportTaskAdapter;
import com.unswift.cloud.adapter.system.config.SystemDictionaryTypeAdapter;
import com.unswift.cloud.enums.export.task.ExportTaskEventEnum;
import com.unswift.cloud.excel.IExcelTranslate;
import com.unswift.cloud.export.IExportBusinessService;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeExportBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeExportStopBo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskInsertDo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeBigDataDo;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeDataDo;
import com.unswift.cloud.pojo.mo.system.dictionary.type.SystemDictionaryTypeExportMo;
import com.unswift.cloud.pojo.mso.logger.export.task.LoggerExportTaskQueueMso;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeExportStopVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeExportVo;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.service.BaseService;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="数据字典类型导出服务", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypeExportService extends BaseService implements IExportBusinessService<SystemDictionaryTypeExportBo, SystemDictionaryTypeDataDo, SystemDictionaryTypeExportMo>, IExcelTranslate, RabbitConstant{
	
	@Autowired
	@ApiField("数据字典类型公共服务")
	private SystemDictionaryTypeAdapter systemDictionaryTypeAdapter;
	
	@Autowired
	@ApiField("导出任务服务")
	private LoggerExportTaskAdapter loggerExportTaskAdapter;
	
	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	@ApiMethod(value="根据条件导出数据字典类型数据", params=@ApiField("查询条件"))
	public SystemDictionaryTypeExportVo export(SystemDictionaryTypeExportBo exportBo){
		ExceptionUtils.trueException(loggerExportTaskAdapter.existsTasking(this.getUserId(), systemDictionaryTypeAdapter.getModule()), "task.is.currently.executing", "有数据字典类型导出");
		Date date=new Date();
		LoggerExportTaskInsertDo task=new LoggerExportTaskInsertDo();
		task.setName("数据字典类型");
		task.setModule(systemDictionaryTypeAdapter.getModule());
		task.setDocumentType("excel");
		task.setStatus((byte)0);
		task.setStartTime(date);
		task.setExportBeanClass(this.getClass().getName());
		task.setExportModelClass(SystemDictionaryTypeExportBo.class.getName());
		task.setExportCondition(JsonUtils.toJson(exportBo));
		task.setResult((byte)0);
		int result=loggerExportTaskAdapter.save(task, false);//保存任务，不用加事务，防止队列消费的时候还没保存完数据
		rabbitQueue.sendMessage(EXPORT_EXCHANGE_NAME, EXPORT_ROUTE_KEY, new LoggerExportTaskQueueMso(task.getId(), ExportTaskEventEnum.START, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送导出队列
		return new SystemDictionaryTypeExportVo(task.getId(), result);
	}
	
	@ApiMethod(value="停止导出数据字典类型", params=@ApiField("停止导出对象"))
	public SystemDictionaryTypeExportStopVo stop(SystemDictionaryTypeExportStopBo exportStopBo){
		rabbitQueue.sendMessage(EXPORT_STOP_EXCHANGE_NAME, EXPORT_STOP_ROUTE_KEY, new LoggerExportTaskQueueMso(exportStopBo.getId(), ExportTaskEventEnum.STOP, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送停止导出队列
		return new SystemDictionaryTypeExportStopVo(1);
	}
	
	@Override
	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	public Class<SystemDictionaryTypeExportMo> getModule(){
		return SystemDictionaryTypeExportMo.class;
	}
	
	@Override
	@ApiMethod(value="根据条件查询数据字典类型数量", params=@ApiField("查询条件"))
	public int findExportCount(SystemDictionaryTypeExportBo exportBo){
		SystemDictionaryTypeBigDataDo search=this.convertPojo(exportBo, SystemDictionaryTypeBigDataDo.class);//将Bo转换为So
		systemDictionaryTypeAdapter.setFieldToLike(search, "code", ObjectUtils.asList("code_", "name_"));
		return systemDictionaryTypeAdapter.findCount(search);
	}
	
	@Override
	@ApiMethod(value="查询导出的数据", params=@ApiField("查询条件"), returns=@ApiField("导出的数据游标对象，支持大数据"))
	public void findExportData(SystemDictionaryTypeExportBo exportBo, ResultHandler<SystemDictionaryTypeDataDo> out){
		SystemDictionaryTypeBigDataDo search=this.convertPojo(exportBo, SystemDictionaryTypeBigDataDo.class);//将Bo转换为So
		systemDictionaryTypeAdapter.setCreateUserSql(search, "createUserName");
		systemDictionaryTypeAdapter.setFieldToLike(search, "code", ObjectUtils.asList("code_", "name_"));
		systemDictionaryTypeAdapter.addOrderBy(search, "create_time_", Sql.ORDER_BY_DESC);
		systemDictionaryTypeAdapter.findBigData(search, out);
	}
	
	@Override
	@ApiMethod(value="MyBatis流式查询读取的过程数据列表，开发者可对此数据加工", params=@ApiField("过程数据列表"))
	public void rowDataHandle(List<SystemDictionaryTypeDataDo> exportDataList){
		
	}
	
	@Override
	@ApiMethod(value="ExcelColumn注解中translateBean的翻译方法", params={@ApiField("数据字典类型"), @ApiField("当前值")})
	public String translate(String type, String language, Object key) {
		if(ObjectUtils.isNull(key)){
			return null;
		}
		if(ObjectUtils.isEmpty(key)){
			return key.toString();
		}
		return cacheAdapter.findDictionaryValueByKey(type, language, key.toString());//翻译逻辑，默认返回自己
	}
}