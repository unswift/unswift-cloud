package com.unswift.cloud.service.export.system;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.ResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerExportTaskAdapter;
import com.unswift.cloud.adapter.system.config.SystemLanguageAdapter;
import com.unswift.cloud.enums.export.task.ExportTaskEventEnum;
import com.unswift.cloud.excel.IExcelTranslate;
import com.unswift.cloud.export.IExportBusinessService;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageExportBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageExportStopBo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskInsertDo;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageBigDataDo;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageDataDo;
import com.unswift.cloud.pojo.mo.system.language.SystemLanguageExportMo;
import com.unswift.cloud.pojo.mso.logger.export.task.LoggerExportTaskQueueMso;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageExportStopVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageExportVo;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.service.BaseService;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="系统语言导出服务", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemLanguageExportService extends BaseService implements IExportBusinessService<SystemLanguageExportBo, SystemLanguageDataDo, SystemLanguageExportMo>, IExcelTranslate, RabbitConstant{
	
	@Autowired
	@ApiField("系统语言公共服务")
	private SystemLanguageAdapter systemLanguageAdapter;
	
	@Autowired
	@ApiField("导出任务服务")
	private LoggerExportTaskAdapter loggerExportTaskAdapter;
	
	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	@ApiMethod(value="根据条件导出系统语言数据", params=@ApiField("查询条件"))
	public SystemLanguageExportVo export(SystemLanguageExportBo exportBo){
		ExceptionUtils.trueException(loggerExportTaskAdapter.existsTasking(this.getUserId(), systemLanguageAdapter.getModule()), "task.is.currently.executing", "有系统语言导出");
		Date date=new Date();
		LoggerExportTaskInsertDo task=new LoggerExportTaskInsertDo();
		task.setName("系统语言");
		task.setModule(systemLanguageAdapter.getModule());
		task.setDocumentType("excel");
		task.setStatus((byte)0);
		task.setStartTime(date);
		task.setExportBeanClass(this.getClass().getName());
		task.setExportModelClass(SystemLanguageExportBo.class.getName());
		task.setExportCondition(JsonUtils.toJson(exportBo));
		task.setResult((byte)0);
		int result=loggerExportTaskAdapter.save(task, false);//保存任务，不用加事务，防止队列消费的时候还没保存完数据
		rabbitQueue.sendMessage(EXPORT_EXCHANGE_NAME, EXPORT_ROUTE_KEY, new LoggerExportTaskQueueMso(task.getId(), ExportTaskEventEnum.START, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送导出队列
		return new SystemLanguageExportVo(task.getId(), result);
	}
	
	@ApiMethod(value="停止导出系统语言", params=@ApiField("停止导出对象"))
	public SystemLanguageExportStopVo stop(SystemLanguageExportStopBo exportStopBo){
		rabbitQueue.sendMessage(EXPORT_STOP_EXCHANGE_NAME, EXPORT_STOP_ROUTE_KEY, new LoggerExportTaskQueueMso(exportStopBo.getId(), ExportTaskEventEnum.STOP, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送停止导出队列
		return new SystemLanguageExportStopVo(1);
	}
	
	@Override
	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	public Class<SystemLanguageExportMo> getModule(){
		return SystemLanguageExportMo.class;
	}
	
	@Override
	@ApiMethod(value="根据条件查询系统语言数量", params=@ApiField("查询条件"))
	public int findExportCount(SystemLanguageExportBo exportBo){
		SystemLanguageBigDataDo search=this.convertPojo(exportBo, SystemLanguageBigDataDo.class);//将Bo转换为So
		return systemLanguageAdapter.findCount(search);
	}
	
	@Override
	@ApiMethod(value="查询导出的数据", params=@ApiField("查询条件"), returns=@ApiField("导出的数据游标对象，支持大数据"))
	public void findExportData(SystemLanguageExportBo exportBo, ResultHandler<SystemLanguageDataDo> out){
		SystemLanguageBigDataDo search=this.convertPojo(exportBo, SystemLanguageBigDataDo.class);//将Bo转换为So
		systemLanguageAdapter.findBigData(search, out);
	}
	
	@Override
	@ApiMethod(value="MyBatis流式查询读取的过程数据列表，开发者可对此数据加工", params=@ApiField("过程数据列表"))
	public void rowDataHandle(List<SystemLanguageDataDo> exportDataList){
		
	}
	
	@Override
	@ApiMethod(value="ExcelColumn注解中translateBean的翻译方法", params={@ApiField("数据字典类型"), @ApiField("当前值")})
	public String translate(String type, String language, Object key) {
		if(ObjectUtils.isNull(key)){
			return null;
		}
		if(ObjectUtils.isEmpty(key)){
			return key.toString();
		}
		return cacheAdapter.findDictionaryValueByKey(type, language, key.toString());//翻译逻辑，默认返回自己
	}
}
