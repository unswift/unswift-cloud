package com.unswift.cloud.service.export.system;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.ResultHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerExportTaskAdapter;
import com.unswift.cloud.adapter.system.config.SystemFormValidateAdapter;
import com.unswift.cloud.enums.export.task.ExportTaskEventEnum;
import com.unswift.cloud.excel.IExcelTranslate;
import com.unswift.cloud.export.IExportBusinessService;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateExportBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateExportStopBo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskInsertDo;
import com.unswift.cloud.pojo.dao.system.form.validate.SystemFormValidateBigDataDo;
import com.unswift.cloud.pojo.dao.system.form.validate.SystemFormValidateDataDo;
import com.unswift.cloud.pojo.mo.system.form.validate.SystemFormValidateExportMo;
import com.unswift.cloud.pojo.mso.logger.export.task.LoggerExportTaskQueueMso;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateExportStopVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateExportVo;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.service.BaseService;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="系统表单验证导出服务", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateExportService extends BaseService implements IExportBusinessService<SystemFormValidateExportBo, SystemFormValidateDataDo, SystemFormValidateExportMo>, IExcelTranslate, RabbitConstant{
	
	@Autowired
	@ApiField("系统表单验证公共服务")
	private SystemFormValidateAdapter systemFormValidateAdapter;
	
	@Autowired
	@ApiField("导出任务服务")
	private LoggerExportTaskAdapter loggerExportTaskAdapter;
	
	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	@ApiMethod(value="根据条件导出系统表单验证数据", params=@ApiField("查询条件"))
	public SystemFormValidateExportVo export(SystemFormValidateExportBo exportBo){
		ExceptionUtils.trueException(loggerExportTaskAdapter.existsTasking(this.getUserId(), systemFormValidateAdapter.getModule()), "task.is.currently.executing", "有系统表单验证导出");
		Date date=new Date();
		LoggerExportTaskInsertDo task=new LoggerExportTaskInsertDo();
		task.setName("系统表单验证");
		task.setModule(systemFormValidateAdapter.getModule());
		task.setDocumentType("excel");
		task.setStatus((byte)0);
		task.setStartTime(date);
		task.setExportBeanClass(this.getClass().getName());
		task.setExportModelClass(SystemFormValidateExportBo.class.getName());
		task.setExportCondition(JsonUtils.toJson(exportBo));
		task.setResult((byte)0);
		int result=loggerExportTaskAdapter.save(task, false);//保存任务，不用加事务，防止队列消费的时候还没保存完数据
		rabbitQueue.sendMessage(EXPORT_EXCHANGE_NAME, EXPORT_ROUTE_KEY, new LoggerExportTaskQueueMso(task.getId(), ExportTaskEventEnum.START, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送导出队列
		return new SystemFormValidateExportVo(task.getId(), result);
	}
	
	@ApiMethod(value="停止导出系统表单验证", params=@ApiField("停止导出对象"))
	public SystemFormValidateExportStopVo stop(SystemFormValidateExportStopBo exportStopBo){
		rabbitQueue.sendMessage(EXPORT_STOP_EXCHANGE_NAME, EXPORT_STOP_ROUTE_KEY, new LoggerExportTaskQueueMso(exportStopBo.getId(), ExportTaskEventEnum.STOP, this.getHeader(tokenConfig.getKeyName()), getLanguage()));//发送停止导出队列
		return new SystemFormValidateExportStopVo(1);
	}
	
	@Override
	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	public Class<SystemFormValidateExportMo> getModule(){
		return SystemFormValidateExportMo.class;
	}
	
	@Override
	@ApiMethod(value="根据条件查询系统表单验证数量", params=@ApiField("查询条件"))
	public int findExportCount(SystemFormValidateExportBo exportBo){
		SystemFormValidateBigDataDo search=this.convertPojo(exportBo, SystemFormValidateBigDataDo.class);//将Bo转换为So
		return systemFormValidateAdapter.findCount(search);
	}
	
	@Override
	@ApiMethod(value="查询导出的数据", params=@ApiField("查询条件"), returns=@ApiField("导出的数据游标对象，支持大数据"))
	public void findExportData(SystemFormValidateExportBo exportBo, ResultHandler<SystemFormValidateDataDo> out){
		SystemFormValidateBigDataDo search=this.convertPojo(exportBo, SystemFormValidateBigDataDo.class);//将Bo转换为So
		systemFormValidateAdapter.findBigData(search, out);
	}
	
	@Override
	@ApiMethod(value="MyBatis流式查询读取的过程数据列表，开发者可对此数据加工", params=@ApiField("过程数据列表"))
	public void rowDataHandle(List<SystemFormValidateDataDo> exportDataList){
		
	}
	
	@Override
	@ApiMethod(value="翻译数据", params = {@ApiField("数据字典类型"), @ApiField("数据字典key")})
	public String translate(String type, String language, Object key) {
		if(ObjectUtils.isNull(key)){
			return null;
		}
		if(ObjectUtils.isEmpty(key)){
			return key.toString();
		}
		return cacheAdapter.findDictionaryValueByKey(type, language, key.toString());//翻译逻辑，默认返回自己
	}
}
