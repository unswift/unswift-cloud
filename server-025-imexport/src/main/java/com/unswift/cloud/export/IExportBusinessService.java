package com.unswift.cloud.export;

import java.util.List;

import org.apache.ibatis.session.ResultHandler;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.bo.BaseBo;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.mo.BaseMo;

@Api(value="导出业务顶层接口，开发者实现导出业务时需实现此接口", author="unswift", date="2023-07-12", version="1.0.0")
public interface IExportBusinessService<T extends BaseBo, V extends BaseDo, M extends BaseMo> {

	@ApiMethod(value="获取导出模板的类类型", returns=@ApiField("导出模板类类型"))
	Class<M> getModule();
	
	@ApiMethod(value="查询导出的总数量", params=@ApiField("查询条件"), returns=@ApiField("导出的总数量"))
	int findExportCount(T searchBo);
	
	@ApiMethod(value="查询导出的数据", params={@ApiField("查询条件"), @ApiField("MyBatis流式查询输出对象")})
	void findExportData(T searchBo, ResultHandler<V> out);
	
	@ApiMethod(value="MyBatis流式查询读取的过程数据列表，开发者可对此数据加工", params=@ApiField("过程数据列表"))
	void rowDataHandle(List<V> exportDataList);
}
