package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.message.SystemMessageExportBo;
import com.unswift.cloud.pojo.bo.system.message.SystemMessageExportStopBo;
import com.unswift.cloud.pojo.dto.system.message.SystemMessageExportDto;
import com.unswift.cloud.pojo.dto.system.message.SystemMessageExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.message.SystemMessageExportStopVo;
import com.unswift.cloud.pojo.vo.system.message.SystemMessageExportVo;
import com.unswift.cloud.service.export.system.SystemMessageExportService;

@RestController
@RequestMapping("/systemMessage")
@Api(value="系统消息对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemMessageImexportController extends BaseController{
	
	@Autowired
	private SystemMessageExportService systemMessageExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemMessageExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有系统消息导出")
	@ApiMethod(value="导出系统消息", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemMessageExportVo> export(@RequestBody SystemMessageExportDto exportDto){
		SystemMessageExportBo exportBo=this.convertPojo(exportDto, SystemMessageExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemMessageExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出系统消息", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemMessageExportStopVo> export(@RequestBody SystemMessageExportStopDto exportStopDto){
		SystemMessageExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemMessageExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemMessageExportService.stop(exportStopBo));
	}
}
