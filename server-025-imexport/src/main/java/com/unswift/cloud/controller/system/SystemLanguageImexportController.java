package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageExportBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageExportStopBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageImportBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageImportStopBo;
import com.unswift.cloud.pojo.dto.system.language.SystemLanguageExportDto;
import com.unswift.cloud.pojo.dto.system.language.SystemLanguageExportStopDto;
import com.unswift.cloud.pojo.dto.system.language.SystemLanguageImportDto;
import com.unswift.cloud.pojo.dto.system.language.SystemLanguageImportStopDto;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageExportVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageExportStopVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageImportVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageImportStopVo;
import com.unswift.cloud.service.export.system.SystemLanguageExportService;
import com.unswift.cloud.service.imports.system.SystemLanguageImportService;

@RestController
@RequestMapping("/systemLanguage")
@Api(value="系统语言对外接口", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemLanguageImexportController extends BaseController{
	
	@Autowired
	@ApiField("系统语言导出服务")
	private SystemLanguageExportService systemLanguageExportService;
	
	@Autowired
	@ApiField("系统语言导入服务")
	private SystemLanguageImportService systemLanguageImportService;
	
	@Active
	@Auth("system.language.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemLanguageExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有系统语言导出")
	@ApiMethod(value="导出系统语言", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemLanguageExportVo> export(@RequestBody SystemLanguageExportDto exportDto){
		SystemLanguageExportBo exportBo=this.convertPojo(exportDto, SystemLanguageExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemLanguageExportService.export(exportBo));
	}
	
	@Active
	@Auth("system.language.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出系统语言", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemLanguageExportStopVo> exportStop(@RequestBody SystemLanguageExportStopDto exportStopDto){
		SystemLanguageExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemLanguageExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemLanguageExportService.stop(exportStopBo));
	}
	
	@Active
	@Auth("system.language.import")
	@RequestMapping(value="/import", method=RequestMethod.POST)
	@LockException(prefix = "systemLanguageImport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有系统语言导入")
	@ApiMethod(value="导入系统语言", params=@ApiField("导入实体"), returns=@ApiField("导入结果Vo->id:任务id,result:{0：未导入，1：已导入}"))
	public ResponseBody<SystemLanguageImportVo> imports(@RequestBody SystemLanguageImportDto importDto){
		SystemLanguageImportBo importBo=this.convertPojo(importDto, SystemLanguageImportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemLanguageImportService.imports(importBo));
	}
	
	@Active
	@Auth("system.language.import")
	@RequestMapping(value="/import/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导入系统语言", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemLanguageImportStopVo> importStop(@RequestBody SystemLanguageImportStopDto importStopDto){
		SystemLanguageImportStopBo importStopBo=this.convertPojo(importStopDto, SystemLanguageImportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemLanguageImportService.stop(importStopBo));
	}
}
