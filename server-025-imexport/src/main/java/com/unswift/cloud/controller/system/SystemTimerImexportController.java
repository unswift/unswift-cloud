package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerExportBo;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerExportStopBo;
import com.unswift.cloud.pojo.dto.system.timer.SystemTimerExportDto;
import com.unswift.cloud.pojo.dto.system.timer.SystemTimerExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerExportStopVo;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerExportVo;
import com.unswift.cloud.service.export.system.SystemTimerExportService;

@RestController
@RequestMapping("/systemTimer")
@Api(value="系统定时器对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemTimerImexportController extends BaseController{
	
	@Autowired
	private SystemTimerExportService systemTimerExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemTimerExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有定时器导出")
	@ApiMethod(value="导出系统定时器", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemTimerExportVo> export(@RequestBody SystemTimerExportDto exportDto){
		SystemTimerExportBo exportBo=this.convertPojo(exportDto, SystemTimerExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemTimerExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出系统定时器", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemTimerExportStopVo> export(@RequestBody SystemTimerExportStopDto exportStopDto){
		SystemTimerExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemTimerExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemTimerExportService.stop(exportStopBo));
	}
}
