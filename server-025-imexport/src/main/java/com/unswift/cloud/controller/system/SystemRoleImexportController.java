package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleExportBo;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleExportStopBo;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleExportDto;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.role.SystemRoleExportStopVo;
import com.unswift.cloud.pojo.vo.system.role.SystemRoleExportVo;
import com.unswift.cloud.service.export.system.SystemRoleExportService;

@RestController
@RequestMapping("/systemRole")
@Api(value="系统角色对外接口", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRoleImexportController extends BaseController{
	
	@Autowired
	private SystemRoleExportService systemRoleExportService;
	
	@Active
	@Auth("system.role.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemRoleExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有角色导出")
	@ApiMethod(value="导出系统角色", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemRoleExportVo> export(@RequestBody SystemRoleExportDto exportDto){
		SystemRoleExportBo exportBo=this.convertPojo(exportDto, SystemRoleExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemRoleExportService.export(exportBo));
	}
	
	@Active
	@Auth("system.role.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出系统角色", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemRoleExportStopVo> export(@RequestBody SystemRoleExportStopDto exportStopDto){
		SystemRoleExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemRoleExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemRoleExportService.stop(exportStopBo));
	}
}
