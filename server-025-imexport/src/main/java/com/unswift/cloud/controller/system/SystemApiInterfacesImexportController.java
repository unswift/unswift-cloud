package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.api.interfaces.SystemApiInterfacesExportBo;
import com.unswift.cloud.pojo.bo.system.api.interfaces.SystemApiInterfacesExportStopBo;
import com.unswift.cloud.pojo.dto.system.api.interfaces.SystemApiInterfacesExportDto;
import com.unswift.cloud.pojo.dto.system.api.interfaces.SystemApiInterfacesExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.api.interfaces.SystemApiInterfacesExportStopVo;
import com.unswift.cloud.pojo.vo.system.api.interfaces.SystemApiInterfacesExportVo;
import com.unswift.cloud.service.export.system.SystemApiInterfacesExportService;

@RestController
@RequestMapping("/systemApiInterfaces")
@Api(value="接口api对外接口", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiInterfacesImexportController extends BaseController{
	
	@Autowired
	private SystemApiInterfacesExportService systemApiInterfacesExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemApiInterfacesExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有接口api导出")
	@ApiMethod(value="导出接口api", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemApiInterfacesExportVo> export(@RequestBody SystemApiInterfacesExportDto exportDto){
		SystemApiInterfacesExportBo exportBo=this.convertPojo(exportDto, SystemApiInterfacesExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemApiInterfacesExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出接口api", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemApiInterfacesExportStopVo> export(@RequestBody SystemApiInterfacesExportStopDto exportStopDto){
		SystemApiInterfacesExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemApiInterfacesExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemApiInterfacesExportService.stop(exportStopBo));
	}
}
