package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.job.SystemJobExportBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobExportStopBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobImportBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobImportStopBo;
import com.unswift.cloud.pojo.dto.system.job.SystemJobExportDto;
import com.unswift.cloud.pojo.dto.system.job.SystemJobExportStopDto;
import com.unswift.cloud.pojo.dto.system.job.SystemJobImportDto;
import com.unswift.cloud.pojo.dto.system.job.SystemJobImportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.job.SystemJobExportStopVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobExportVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobImportStopVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobImportVo;
import com.unswift.cloud.service.export.system.SystemJobExportService;
import com.unswift.cloud.service.imports.system.SystemJobImportService;

@RestController
@RequestMapping("/systemJob")
@Api(value="职位对外接口", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemJobImexportController extends BaseController{
	
	@Autowired
	@ApiField("职位导出服务")
	private SystemJobExportService systemJobExportService;
	
	@Autowired
	@ApiField("职位导入服务")
	private SystemJobImportService systemJobImportService;
	
	@Active
	@Auth("system.job.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemJobExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有职位导出")
	@ApiMethod(value="导出职位", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemJobExportVo> export(@RequestBody SystemJobExportDto exportDto){
		SystemJobExportBo exportBo=this.convertPojo(exportDto, SystemJobExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemJobExportService.export(exportBo));
	}
	
	@Active
	@Auth("system.job.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出职位", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemJobExportStopVo> exportStop(@RequestBody SystemJobExportStopDto exportStopDto){
		SystemJobExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemJobExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemJobExportService.stop(exportStopBo));
	}
	
	@Active
	@Auth("system.job.import")
	@RequestMapping(value="/import", method=RequestMethod.POST)
	@LockException(prefix = "systemJobImport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有职位导入")
	@ApiMethod(value="导入职位", params=@ApiField("导入实体"), returns=@ApiField("导入结果Vo->id:任务id,result:{0：未导入，1：已导入}"))
	public ResponseBody<SystemJobImportVo> imports(@RequestBody SystemJobImportDto importDto){
		SystemJobImportBo importBo=this.convertPojo(importDto, SystemJobImportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemJobImportService.imports(importBo));
	}
	
	@Active
	@Auth("system.job.import")
	@RequestMapping(value="/import/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导入职位", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemJobImportStopVo> importStop(@RequestBody SystemJobImportStopDto importStopDto){
		SystemJobImportStopBo importStopBo=this.convertPojo(importStopDto, SystemJobImportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemJobImportService.stop(importStopBo));
	}
}
