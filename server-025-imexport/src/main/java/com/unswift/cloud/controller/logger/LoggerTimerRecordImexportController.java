package com.unswift.cloud.controller.logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordExportBo;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordExportStopBo;
import com.unswift.cloud.pojo.dto.logger.timer.record.LoggerTimerRecordExportDto;
import com.unswift.cloud.pojo.dto.logger.timer.record.LoggerTimerRecordExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordExportStopVo;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordExportVo;
import com.unswift.cloud.service.export.logger.LoggerTimerRecordExportService;

@RestController
@RequestMapping("/loggerTimerRecord")
@Api(value="定时器执行记录对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerTimerRecordImexportController extends BaseController{
	
	@Autowired
	private LoggerTimerRecordExportService loggerTimerRecordExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@ApiMethod(value="导出定时器执行记录", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<LoggerTimerRecordExportVo> export(@RequestBody LoggerTimerRecordExportDto exportDto){
		LoggerTimerRecordExportBo exportBo=this.convertPojo(exportDto, LoggerTimerRecordExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerTimerRecordExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出定时器执行记录", params=@ApiField("停止对象"), returns=@ApiField("导出结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<LoggerTimerRecordExportStopVo> export(@RequestBody LoggerTimerRecordExportStopDto exportStopDto){
		LoggerTimerRecordExportStopBo exportStopBo=this.convertPojo(exportStopDto, LoggerTimerRecordExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerTimerRecordExportService.stop(exportStopBo));
	}
}
