package com.unswift.cloud.controller.logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.logger.api.request.LoggerApiRequestExportBo;
import com.unswift.cloud.pojo.bo.logger.api.request.LoggerApiRequestExportStopBo;
import com.unswift.cloud.pojo.dto.logger.api.request.LoggerApiRequestExportDto;
import com.unswift.cloud.pojo.dto.logger.api.request.LoggerApiRequestExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.logger.api.request.LoggerApiRequestExportStopVo;
import com.unswift.cloud.pojo.vo.logger.api.request.LoggerApiRequestExportVo;
import com.unswift.cloud.service.export.logger.LoggerApiRequestExportService;

@RestController
@RequestMapping("/loggerApiRequest")
@Api(value="接口请求日志对外接口", author="liyunlong", date="2023-12-08", version="1.0.0")
public class LoggerApiRequestImexportController extends BaseController{
	
	@Autowired
	private LoggerApiRequestExportService loggerApiRequestExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@ApiMethod(value="导出接口请求日志", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<LoggerApiRequestExportVo> export(@RequestBody LoggerApiRequestExportDto exportDto){
		LoggerApiRequestExportBo exportBo=this.convertPojo(exportDto, LoggerApiRequestExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerApiRequestExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出接口请求日志", params=@ApiField("停止对象"), returns=@ApiField("导出结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<LoggerApiRequestExportStopVo> export(@RequestBody LoggerApiRequestExportStopDto exportStopDto){
		LoggerApiRequestExportStopBo exportStopBo=this.convertPojo(exportStopDto, LoggerApiRequestExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerApiRequestExportService.stop(exportStopBo));
	}
}
