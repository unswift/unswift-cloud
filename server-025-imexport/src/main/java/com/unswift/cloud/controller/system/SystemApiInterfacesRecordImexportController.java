package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.api.interfaces.record.SystemApiInterfacesRecordExportBo;
import com.unswift.cloud.pojo.bo.system.api.interfaces.record.SystemApiInterfacesRecordExportStopBo;
import com.unswift.cloud.pojo.dto.system.api.interfaces.record.SystemApiInterfacesRecordExportDto;
import com.unswift.cloud.pojo.dto.system.api.interfaces.record.SystemApiInterfacesRecordExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.api.interfaces.record.SystemApiInterfacesRecordExportStopVo;
import com.unswift.cloud.pojo.vo.system.api.interfaces.record.SystemApiInterfacesRecordExportVo;
import com.unswift.cloud.service.export.system.SystemApiInterfacesRecordExportService;

@RestController
@RequestMapping("/systemApiInterfacesRecord")
@Api(value="接口api执行记录对外接口", author="unswift", date="2023-09-16", version="1.0.0")
public class SystemApiInterfacesRecordImexportController extends BaseController{
	
	@Autowired
	private SystemApiInterfacesRecordExportService systemApiInterfacesRecordExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemApiInterfacesRecordExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有接口api执行记录导出")
	@ApiMethod(value="导出接口api执行记录", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemApiInterfacesRecordExportVo> export(@RequestBody SystemApiInterfacesRecordExportDto exportDto){
		SystemApiInterfacesRecordExportBo exportBo=this.convertPojo(exportDto, SystemApiInterfacesRecordExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemApiInterfacesRecordExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出接口api执行记录", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemApiInterfacesRecordExportStopVo> export(@RequestBody SystemApiInterfacesRecordExportStopDto exportStopDto){
		SystemApiInterfacesRecordExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemApiInterfacesRecordExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemApiInterfacesRecordExportService.stop(exportStopBo));
	}
}