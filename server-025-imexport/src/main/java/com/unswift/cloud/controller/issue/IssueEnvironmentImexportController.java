package com.unswift.cloud.controller.issue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentExportBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentExportStopBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentImportBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentImportStopBo;
import com.unswift.cloud.pojo.dto.issue.environment.IssueEnvironmentExportDto;
import com.unswift.cloud.pojo.dto.issue.environment.IssueEnvironmentExportStopDto;
import com.unswift.cloud.pojo.dto.issue.environment.IssueEnvironmentImportDto;
import com.unswift.cloud.pojo.dto.issue.environment.IssueEnvironmentImportStopDto;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentExportVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentExportStopVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentImportVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentImportStopVo;
import com.unswift.cloud.service.export.issue.IssueEnvironmentExportService;
import com.unswift.cloud.service.imports.issue.IssueEnvironmentImportService;

@RestController
@RequestMapping("/issueEnvironment")
@Api(value="发布环境对外接口", author="liyunlong", date="2024-04-16", version="1.0.0")
public class IssueEnvironmentImexportController extends BaseController{
	
	@Autowired
	@ApiField("发布环境导出服务")
	private IssueEnvironmentExportService issueEnvironmentExportService;
	
	@Autowired
	@ApiField("发布环境导入服务")
	private IssueEnvironmentImportService issueEnvironmentImportService;
	
	@Auth("issue.environment.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "issueEnvironmentExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有发布环境导出")
	@ApiMethod(value="导出发布环境", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<IssueEnvironmentExportVo> export(@RequestBody IssueEnvironmentExportDto exportDto){
		IssueEnvironmentExportBo exportBo=this.convertPojo(exportDto, IssueEnvironmentExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(issueEnvironmentExportService.export(exportBo));
	}
	
	@Auth("issue.environment.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出发布环境", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<IssueEnvironmentExportStopVo> exportStop(@RequestBody IssueEnvironmentExportStopDto exportStopDto){
		IssueEnvironmentExportStopBo exportStopBo=this.convertPojo(exportStopDto, IssueEnvironmentExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(issueEnvironmentExportService.stop(exportStopBo));
	}
	
	@Auth("issue.environment.import")
	@RequestMapping(value="/import", method=RequestMethod.POST)
	@LockException(prefix = "issueEnvironmentImport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有发布环境导入")
	@ApiMethod(value="导入发布环境", params=@ApiField("导入实体"), returns=@ApiField("导入结果Vo->id:任务id,result:{0：未导入，1：已导入}"))
	public ResponseBody<IssueEnvironmentImportVo> imports(@RequestBody IssueEnvironmentImportDto importDto){
		IssueEnvironmentImportBo importBo=this.convertPojo(importDto, IssueEnvironmentImportBo.class);//将Dto转换为Bo
		return ResponseBody.success(issueEnvironmentImportService.imports(importBo));
	}
	
	@Auth("issue.environment.import")
	@RequestMapping(value="/import/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导入发布环境", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<IssueEnvironmentImportStopVo> importStop(@RequestBody IssueEnvironmentImportStopDto importStopDto){
		IssueEnvironmentImportStopBo importStopBo=this.convertPojo(importStopDto, IssueEnvironmentImportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(issueEnvironmentImportService.stop(importStopBo));
	}
}
