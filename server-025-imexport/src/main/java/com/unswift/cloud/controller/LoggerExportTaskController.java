package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskCreateBo;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskDeleteBo;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskPageBo;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskProgressBo;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskUpdateBo;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskViewBo;
import com.unswift.cloud.pojo.dto.logger.export.task.LoggerExportTaskCreateDto;
import com.unswift.cloud.pojo.dto.logger.export.task.LoggerExportTaskDeleteDto;
import com.unswift.cloud.pojo.dto.logger.export.task.LoggerExportTaskPageDto;
import com.unswift.cloud.pojo.dto.logger.export.task.LoggerExportTaskProgressDto;
import com.unswift.cloud.pojo.dto.logger.export.task.LoggerExportTaskUpdateDto;
import com.unswift.cloud.pojo.dto.logger.export.task.LoggerExportTaskViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskCreateVo;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskDeleteVo;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskPageVo;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskProgressVo;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskUpdateVo;
import com.unswift.cloud.pojo.vo.logger.export.task.LoggerExportTaskViewVo;
import com.unswift.cloud.pojo.vo.page.IPageVo;
import com.unswift.cloud.service.LoggerExportTaskService;

@RestController
@RequestMapping("/loggerExportTask")
@Api(value="导出任务记录表对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerExportTaskController extends BaseController{
	
	@Autowired
	private LoggerExportTaskService loggerExportTaskService;
	
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="导出任务记录表分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<IPageVo<LoggerExportTaskPageVo>> findPageList(@RequestBody LoggerExportTaskPageDto searchDto){
		LoggerExportTaskPageBo searchBo=this.convertPojo(searchDto, LoggerExportTaskPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerExportTaskService.findPageList(searchBo));
	}
	
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="导出任务记录表详情", params=@ApiField("主键实体"), returns=@ApiField("详情数据"))
	public ResponseBody<LoggerExportTaskViewVo> view(@RequestBody LoggerExportTaskViewDto viewDto){
		LoggerExportTaskViewBo viewBo=this.convertPojo(viewDto, LoggerExportTaskViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerExportTaskService.view(viewBo));
	}
	
	@Active
	@RequestMapping(value="/findProgress", method=RequestMethod.POST)
	@ApiMethod(value="导出任务记录表进度", params=@ApiField("主键实体"), returns=@ApiField("详情数据"))
	public ResponseBody<LoggerExportTaskProgressVo> findProgress(@RequestBody LoggerExportTaskProgressDto progressDto){
		LoggerExportTaskProgressBo progressBo=this.convertPojo(progressDto, LoggerExportTaskProgressBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerExportTaskService.findProgress(progressBo));
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建导出任务记录表", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<LoggerExportTaskCreateVo> create(@RequestBody LoggerExportTaskCreateDto createDto){
		LoggerExportTaskCreateBo createBo=this.convertPojo(createDto, LoggerExportTaskCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerExportTaskService.create(createBo));
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新导出任务记录表", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<LoggerExportTaskUpdateVo> update(@RequestBody LoggerExportTaskUpdateDto updateDto){
		LoggerExportTaskUpdateBo updateBo=this.convertPojo(updateDto, LoggerExportTaskUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerExportTaskService.update(updateBo));
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除导出任务记录表", params=@ApiField("主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<LoggerExportTaskDeleteVo> delete(@RequestBody LoggerExportTaskDeleteDto deleteDto){
		LoggerExportTaskDeleteBo deleteBo=this.convertPojo(deleteDto, LoggerExportTaskDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerExportTaskService.delete(deleteBo));
	}
}
