package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateExportBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateExportStopBo;
import com.unswift.cloud.pojo.dto.system.form.validate.SystemFormValidateExportDto;
import com.unswift.cloud.pojo.dto.system.form.validate.SystemFormValidateExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateExportStopVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateExportVo;
import com.unswift.cloud.service.export.system.SystemFormValidateExportService;

@RestController
@RequestMapping("/systemFormValidate")
@Api(value="系统表单验证对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateImexportController extends BaseController{
	
	@Autowired
	private SystemFormValidateExportService systemFormValidateExportService;
	
	@Active
	@Auth("system.formValidate.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemFormValidateExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有表单验证导出")
	@ApiMethod(value="导出系统表单验证", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemFormValidateExportVo> export(@RequestBody SystemFormValidateExportDto exportDto){
		SystemFormValidateExportBo exportBo=this.convertPojo(exportDto, SystemFormValidateExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateExportService.export(exportBo));
	}
	
	@Active
	@Auth("system.formValidate.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出系统表单验证", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemFormValidateExportStopVo> export(@RequestBody SystemFormValidateExportStopDto exportStopDto){
		SystemFormValidateExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemFormValidateExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateExportService.stop(exportStopBo));
	}
}
