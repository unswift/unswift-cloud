package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigExportBo;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigExportStopBo;
import com.unswift.cloud.pojo.dto.system.form.validate.config.SystemFormValidateConfigExportDto;
import com.unswift.cloud.pojo.dto.system.form.validate.config.SystemFormValidateConfigExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigExportStopVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigExportVo;
import com.unswift.cloud.service.export.system.SystemFormValidateConfigExportService;

@RestController
@RequestMapping("/systemFormValidateConfig")
@Api(value="系统表单验证基础配置对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateConfigImexportController extends BaseController{
	
	@Autowired
	private SystemFormValidateConfigExportService systemFormValidateConfigExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemFormValidateConfigExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有表单验证基础配置导出")
	@ApiMethod(value="导出系统表单验证基础配置", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemFormValidateConfigExportVo> export(@RequestBody SystemFormValidateConfigExportDto exportDto){
		SystemFormValidateConfigExportBo exportBo=this.convertPojo(exportDto, SystemFormValidateConfigExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateConfigExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出系统表单验证基础配置", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemFormValidateConfigExportStopVo> export(@RequestBody SystemFormValidateConfigExportStopDto exportStopDto){
		SystemFormValidateConfigExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemFormValidateConfigExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateConfigExportService.stop(exportStopBo));
	}
}
