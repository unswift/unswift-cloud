package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.resource.SystemResourceExportBo;
import com.unswift.cloud.pojo.bo.system.resource.SystemResourceExportStopBo;
import com.unswift.cloud.pojo.dto.system.resource.SystemResourceExportDto;
import com.unswift.cloud.pojo.dto.system.resource.SystemResourceExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.resource.SystemResourceExportStopVo;
import com.unswift.cloud.pojo.vo.system.resource.SystemResourceExportVo;
import com.unswift.cloud.service.export.system.SystemResourceExportService;

@RestController
@RequestMapping("/systemResource")
@Api(value="系统资源对外接口", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemResourceImexportController extends BaseController{
	
	@Autowired
	private SystemResourceExportService systemResourceExportService;
	
	@Active
	@Auth("system.resource.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemResourceExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有系统资源导出")
	@ApiMethod(value="导出系统资源", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemResourceExportVo> export(@RequestBody SystemResourceExportDto exportDto){
		SystemResourceExportBo exportBo=this.convertPojo(exportDto, SystemResourceExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemResourceExportService.export(exportBo));
	}
	
	@Active
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出系统资源", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemResourceExportStopVo> export(@RequestBody SystemResourceExportStopDto exportStopDto){
		SystemResourceExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemResourceExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemResourceExportService.stop(exportStopBo));
	}
}
