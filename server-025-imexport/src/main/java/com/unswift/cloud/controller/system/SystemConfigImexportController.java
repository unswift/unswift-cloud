package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigExportBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigExportStopBo;
import com.unswift.cloud.pojo.dto.system.config.SystemConfigExportDto;
import com.unswift.cloud.pojo.dto.system.config.SystemConfigExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigExportStopVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigExportVo;
import com.unswift.cloud.service.export.system.SystemConfigExportService;

@RestController
@RequestMapping("/systemConfig")
@Api(value="系统配置对外接口", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigImexportController extends BaseController{
	
	@Autowired
	private SystemConfigExportService systemConfigExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemConfigExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有系统配置导出")
	@ApiMethod(value="导出系统配置", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemConfigExportVo> export(@RequestBody SystemConfigExportDto exportDto){
		SystemConfigExportBo exportBo=this.convertPojo(exportDto, SystemConfigExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemConfigExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出系统配置", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemConfigExportStopVo> export(@RequestBody SystemConfigExportStopDto exportStopDto){
		SystemConfigExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemConfigExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemConfigExportService.stop(exportStopBo));
	}
}