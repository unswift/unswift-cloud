package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentExportBo;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentExportStopBo;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentImportBo;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentImportStopBo;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentUserImportBo;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentUserImportStopBo;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentExportDto;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentExportStopDto;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentImportDto;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentImportStopDto;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentUserImportDto;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentUserImportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentExportStopVo;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentExportVo;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentImportStopVo;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentImportVo;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentUserImportStopVo;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentUserImportVo;
import com.unswift.cloud.service.export.system.SystemDepartmentExportService;
import com.unswift.cloud.service.imports.system.SystemDepartmentImportService;
import com.unswift.cloud.service.imports.system.SystemDepartmentUserImportService;

@RestController
@RequestMapping("/systemDepartment")
@Api(value="部门对外接口", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentImexportController extends BaseController{
	
	@Autowired
	@ApiField("部门导出服务")
	private SystemDepartmentExportService systemDepartmentExportService;
	
	@Autowired
	@ApiField("部门导入服务")
	private SystemDepartmentImportService systemDepartmentImportService;
	
	@Autowired
	@ApiField("部门用户导入服务")
	private SystemDepartmentUserImportService systemDepartmentUserImportService;
	
	@Active
	@Auth("system.department.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemDepartmentExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有部门导出")
	@ApiMethod(value="导出部门", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemDepartmentExportVo> export(@RequestBody SystemDepartmentExportDto exportDto){
		SystemDepartmentExportBo exportBo=this.convertPojo(exportDto, SystemDepartmentExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDepartmentExportService.export(exportBo));
	}
	
	@Active
	@Auth("system.department.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出部门", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemDepartmentExportStopVo> exportStop(@RequestBody SystemDepartmentExportStopDto exportStopDto){
		SystemDepartmentExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemDepartmentExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDepartmentExportService.stop(exportStopBo));
	}
	
	@Active
	@Auth("system.department.import")
	@RequestMapping(value="/import", method=RequestMethod.POST)
	@LockException(prefix = "systemDepartmentImport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有部门导入")
	@ApiMethod(value="导入部门", params=@ApiField("导入实体"), returns=@ApiField("导入结果Vo->id:任务id,result:{0：未导入，1：已导入}"))
	public ResponseBody<SystemDepartmentImportVo> imports(@RequestBody SystemDepartmentImportDto importDto){
		SystemDepartmentImportBo importBo=this.convertPojo(importDto, SystemDepartmentImportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDepartmentImportService.imports(importBo));
	}
	
	@Active
	@Auth("system.department.import")
	@RequestMapping(value="/import/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导入部门", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemDepartmentImportStopVo> importStop(@RequestBody SystemDepartmentImportStopDto importStopDto){
		SystemDepartmentImportStopBo importStopBo=this.convertPojo(importStopDto, SystemDepartmentImportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDepartmentImportService.stop(importStopBo));
	}
	
	@Active
	@Auth("system.department.user.import")
	@RequestMapping(value="/user/import", method=RequestMethod.POST)
	@ApiMethod(value="导入部门会员", params=@ApiField("导入的数据对象"), returns=@ApiField("导入结果Vo->result:{0：未被导入，1：已成功导入}"))
	@LockException(prefix = "systemUserImport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有会员导入")
	public ResponseBody<SystemDepartmentUserImportVo> imports(@RequestBody SystemDepartmentUserImportDto createDto){
		SystemDepartmentUserImportBo createBo=this.convertPojo(createDto, SystemDepartmentUserImportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDepartmentUserImportService.imports(createBo));
	}
	
	@Active
	@Auth("system.department.user.import")
	@RequestMapping(value="/user/import/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导入部门会员", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemDepartmentUserImportStopVo> export(@RequestBody SystemDepartmentUserImportStopDto exportStopDto){
		SystemDepartmentUserImportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemDepartmentUserImportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDepartmentUserImportService.stop(exportStopBo));
	}
}
