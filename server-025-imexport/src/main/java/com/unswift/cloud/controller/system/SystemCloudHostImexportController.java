package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostExportBo;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostExportStopBo;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostImportBo;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostImportStopBo;
import com.unswift.cloud.pojo.dto.system.cloud.host.SystemCloudHostExportDto;
import com.unswift.cloud.pojo.dto.system.cloud.host.SystemCloudHostExportStopDto;
import com.unswift.cloud.pojo.dto.system.cloud.host.SystemCloudHostImportDto;
import com.unswift.cloud.pojo.dto.system.cloud.host.SystemCloudHostImportStopDto;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostExportVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostExportStopVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostImportVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostImportStopVo;
import com.unswift.cloud.service.export.system.SystemCloudHostExportService;
import com.unswift.cloud.service.imports.system.SystemCloudHostImportService;

@RestController
@RequestMapping("/systemCloudHost")
@Api(value="微服务主机对外接口", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudHostImexportController extends BaseController{
	
	@Autowired
	@ApiField("微服务主机导出服务")
	private SystemCloudHostExportService systemCloudHostExportService;
	
	@Autowired
	@ApiField("微服务主机导入服务")
	private SystemCloudHostImportService systemCloudHostImportService;
	
	@Auth("system.cloud.host.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemCloudHostExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有微服务主机导出")
	@ApiMethod(value="导出微服务主机", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemCloudHostExportVo> export(@RequestBody SystemCloudHostExportDto exportDto){
		SystemCloudHostExportBo exportBo=this.convertPojo(exportDto, SystemCloudHostExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudHostExportService.export(exportBo));
	}
	
	@Auth("system.cloud.host.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出微服务主机", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemCloudHostExportStopVo> exportStop(@RequestBody SystemCloudHostExportStopDto exportStopDto){
		SystemCloudHostExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemCloudHostExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudHostExportService.stop(exportStopBo));
	}
	
	@Auth("system.cloud.host.import")
	@RequestMapping(value="/import", method=RequestMethod.POST)
	@LockException(prefix = "systemCloudHostImport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有微服务主机导入")
	@ApiMethod(value="导入微服务主机", params=@ApiField("导入实体"), returns=@ApiField("导入结果Vo->id:任务id,result:{0：未导入，1：已导入}"))
	public ResponseBody<SystemCloudHostImportVo> imports(@RequestBody SystemCloudHostImportDto importDto){
		SystemCloudHostImportBo importBo=this.convertPojo(importDto, SystemCloudHostImportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudHostImportService.imports(importBo));
	}
	
	@Auth("system.cloud.host.import")
	@RequestMapping(value="/import/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导入微服务主机", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemCloudHostImportStopVo> importStop(@RequestBody SystemCloudHostImportStopDto importStopDto){
		SystemCloudHostImportStopBo importStopBo=this.convertPojo(importStopDto, SystemCloudHostImportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudHostImportService.stop(importStopBo));
	}
}