package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.api.classes.SystemApiClassExportBo;
import com.unswift.cloud.pojo.bo.system.api.classes.SystemApiClassExportStopBo;
import com.unswift.cloud.pojo.dto.system.api.classes.SystemApiClassExportDto;
import com.unswift.cloud.pojo.dto.system.api.classes.SystemApiClassExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.api.classes.SystemApiClassExportStopVo;
import com.unswift.cloud.pojo.vo.system.api.classes.SystemApiClassExportVo;
import com.unswift.cloud.service.export.system.SystemApiClassExportService;

@RestController
@RequestMapping("/systemApiClass")
@Api(value="类api对外接口", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiClassImexportController extends BaseController{
	
	@Autowired
	private SystemApiClassExportService systemApiClassExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemApiClassExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有类api导出")
	@ApiMethod(value="导出类api", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemApiClassExportVo> export(@RequestBody SystemApiClassExportDto exportDto){
		SystemApiClassExportBo exportBo=this.convertPojo(exportDto, SystemApiClassExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemApiClassExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出类api", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemApiClassExportStopVo> export(@RequestBody SystemApiClassExportStopDto exportStopDto){
		SystemApiClassExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemApiClassExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemApiClassExportService.stop(exportStopBo));
	}
}
