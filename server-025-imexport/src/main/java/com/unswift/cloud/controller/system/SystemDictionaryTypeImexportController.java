package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeExportBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeExportStopBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeImportBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeImportStopBo;
import com.unswift.cloud.pojo.dto.system.dictionary.type.SystemDictionaryTypeExportDto;
import com.unswift.cloud.pojo.dto.system.dictionary.type.SystemDictionaryTypeExportStopDto;
import com.unswift.cloud.pojo.dto.system.dictionary.type.SystemDictionaryTypeImportDto;
import com.unswift.cloud.pojo.dto.system.dictionary.type.SystemDictionaryTypeImportStopDto;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeExportVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeExportStopVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeImportVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeImportStopVo;
import com.unswift.cloud.service.export.system.SystemDictionaryTypeExportService;
import com.unswift.cloud.service.imports.system.SystemDictionaryTypeImportService;

@RestController
@RequestMapping("/systemDictionaryType")
@Api(value="数据字典类型对外接口", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypeImexportController extends BaseController{
	
	@Autowired
	@ApiField("数据字典类型导出服务")
	private SystemDictionaryTypeExportService systemDictionaryTypeExportService;
	
	@Autowired
	@ApiField("数据字典类型导入服务")
	private SystemDictionaryTypeImportService systemDictionaryTypeImportService;
	
	@Active
	@Auth("system.dictionary.type.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemDictionaryTypeExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有数据字典类型导出")
	@ApiMethod(value="导出数据字典类型", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemDictionaryTypeExportVo> export(@RequestBody SystemDictionaryTypeExportDto exportDto){
		SystemDictionaryTypeExportBo exportBo=this.convertPojo(exportDto, SystemDictionaryTypeExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryTypeExportService.export(exportBo));
	}
	
	@Active
	@Auth("system.dictionary.type.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出数据字典类型", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemDictionaryTypeExportStopVo> exportStop(@RequestBody SystemDictionaryTypeExportStopDto exportStopDto){
		SystemDictionaryTypeExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemDictionaryTypeExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryTypeExportService.stop(exportStopBo));
	}
	
	@Active
	@Auth("system.dictionary.type.import")
	@RequestMapping(value="/import", method=RequestMethod.POST)
	@LockException(prefix = "systemDictionaryTypeImport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有数据字典类型导入")
	@ApiMethod(value="导入数据字典类型", params=@ApiField("导入实体"), returns=@ApiField("导入结果Vo->id:任务id,result:{0：未导入，1：已导入}"))
	public ResponseBody<SystemDictionaryTypeImportVo> imports(@RequestBody SystemDictionaryTypeImportDto importDto){
		SystemDictionaryTypeImportBo importBo=this.convertPojo(importDto, SystemDictionaryTypeImportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryTypeImportService.imports(importBo));
	}
	
	@Active
	@Auth("system.dictionary.type.import")
	@RequestMapping(value="/import/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导入数据字典类型", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemDictionaryTypeImportStopVo> importStop(@RequestBody SystemDictionaryTypeImportStopDto importStopDto){
		SystemDictionaryTypeImportStopBo importStopBo=this.convertPojo(importStopDto, SystemDictionaryTypeImportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryTypeImportService.stop(importStopBo));
	}
}