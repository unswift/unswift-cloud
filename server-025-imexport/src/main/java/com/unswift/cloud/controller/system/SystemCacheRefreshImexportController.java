package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshExportBo;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshExportStopBo;
import com.unswift.cloud.pojo.dto.system.cache.refresh.SystemCacheRefreshExportDto;
import com.unswift.cloud.pojo.dto.system.cache.refresh.SystemCacheRefreshExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshExportStopVo;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshExportVo;
import com.unswift.cloud.service.export.system.SystemCacheRefreshExportService;

@RestController
@RequestMapping("/systemCacheRefresh")
@Api(value="缓存刷新对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemCacheRefreshImexportController extends BaseController{
	
	@Autowired
	private SystemCacheRefreshExportService systemCacheRefreshExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemCacheRefreshExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有缓存刷新导出")
	@ApiMethod(value="导出缓存刷新", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemCacheRefreshExportVo> export(@RequestBody SystemCacheRefreshExportDto exportDto){
		SystemCacheRefreshExportBo exportBo=this.convertPojo(exportDto, SystemCacheRefreshExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCacheRefreshExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出缓存刷新", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemCacheRefreshExportStopVo> export(@RequestBody SystemCacheRefreshExportStopDto exportStopDto){
		SystemCacheRefreshExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemCacheRefreshExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCacheRefreshExportService.stop(exportStopBo));
	}
}
