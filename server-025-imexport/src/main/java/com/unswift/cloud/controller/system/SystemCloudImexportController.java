package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudExportBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudExportStopBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudImportBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudImportStopBo;
import com.unswift.cloud.pojo.dto.system.cloud.SystemCloudExportDto;
import com.unswift.cloud.pojo.dto.system.cloud.SystemCloudExportStopDto;
import com.unswift.cloud.pojo.dto.system.cloud.SystemCloudImportDto;
import com.unswift.cloud.pojo.dto.system.cloud.SystemCloudImportStopDto;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudExportVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudExportStopVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudImportVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudImportStopVo;
import com.unswift.cloud.service.export.system.SystemCloudExportService;
import com.unswift.cloud.service.imports.system.SystemCloudImportService;

@RestController
@RequestMapping("/systemCloud")
@Api(value="系统微服务对外接口", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudImexportController extends BaseController{
	
	@Autowired
	@ApiField("系统微服务导出服务")
	private SystemCloudExportService systemCloudExportService;
	
	@Autowired
	@ApiField("系统微服务导入服务")
	private SystemCloudImportService systemCloudImportService;
	
	@Active
	@Auth("system.cloud.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemCloudExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有系统微服务导出")
	@ApiMethod(value="导出系统微服务", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemCloudExportVo> export(@RequestBody SystemCloudExportDto exportDto){
		SystemCloudExportBo exportBo=this.convertPojo(exportDto, SystemCloudExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudExportService.export(exportBo));
	}
	
	@Active
	@Auth("system.cloud.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出系统微服务", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemCloudExportStopVo> exportStop(@RequestBody SystemCloudExportStopDto exportStopDto){
		SystemCloudExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemCloudExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudExportService.stop(exportStopBo));
	}
	
	@Active
	@Auth("system.cloud.import")
	@RequestMapping(value="/import", method=RequestMethod.POST)
	@LockException(prefix = "systemCloudImport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有系统微服务导入")
	@ApiMethod(value="导入系统微服务", params=@ApiField("导入实体"), returns=@ApiField("导入结果Vo->id:任务id,result:{0：未导入，1：已导入}"))
	public ResponseBody<SystemCloudImportVo> imports(@RequestBody SystemCloudImportDto importDto){
		SystemCloudImportBo importBo=this.convertPojo(importDto, SystemCloudImportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudImportService.imports(importBo));
	}
	
	@Active
	@Auth("system.cloud.import")
	@RequestMapping(value="/import/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导入系统微服务", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemCloudImportStopVo> importStop(@RequestBody SystemCloudImportStopDto importStopDto){
		SystemCloudImportStopBo importStopBo=this.convertPojo(importStopDto, SystemCloudImportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudImportService.stop(importStopBo));
	}
}
