package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.user.SystemUserExportBo;
import com.unswift.cloud.pojo.bo.system.user.SystemUserExportStopBo;
import com.unswift.cloud.pojo.bo.system.user.SystemUserImportBo;
import com.unswift.cloud.pojo.bo.system.user.SystemUserImportStopBo;
import com.unswift.cloud.pojo.dto.system.user.SystemUserExportDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserExportStopDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserImportDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserImportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.user.SystemUserExportStopVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserExportVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserImportStopVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserImportVo;
import com.unswift.cloud.service.export.system.SystemUserExportService;
import com.unswift.cloud.service.imports.system.SystemUserImportService;

@RestController
@RequestMapping("/systemUser")
@Api(value="会员对外接口", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemUserImexportController extends BaseController{
	
	@Autowired
	private SystemUserExportService systemUserExportService;
	@Autowired
	private SystemUserImportService systemUserImportService;
	
	@Active
	@Auth("system.user.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@ApiMethod(value="导出会员", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	@LockException(prefix = "systemUserImport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有会员导出")
	public ResponseBody<SystemUserExportVo> export(@RequestBody SystemUserExportDto exportDto){
		SystemUserExportBo exportBo=this.convertPojo(exportDto, SystemUserExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserExportService.export(exportBo));
	}
	
	@Active
	@Auth("system.user.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出会员", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemUserExportStopVo> export(@RequestBody SystemUserExportStopDto exportStopDto){
		SystemUserExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemUserExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserExportService.stop(exportStopBo));
	}
	
	@Active
	@Auth("system.user.import")
	@RequestMapping(value="/import", method=RequestMethod.POST)
	@ApiMethod(value="导入会员", params=@ApiField("导入的数据对象"), returns=@ApiField("导入结果Vo->result:{0：未被导入，1：已成功导入}"))
	@LockException(prefix = "systemUserImport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有会员导入")
	public ResponseBody<SystemUserImportVo> imports(@RequestBody SystemUserImportDto createDto){
		SystemUserImportBo createBo=this.convertPojo(createDto, SystemUserImportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserImportService.imports(createBo));
	}
	
	@Active
	@Auth("system.user.import")
	@RequestMapping(value="/import/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导入会员", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemUserImportStopVo> export(@RequestBody SystemUserImportStopDto exportStopDto){
		SystemUserImportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemUserImportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserImportService.stop(exportStopBo));
	}
}
