package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachExportBo;
import com.unswift.cloud.pojo.bo.system.attach.SystemAttachExportStopBo;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachExportDto;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachExportStopVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachExportVo;
import com.unswift.cloud.service.export.system.SystemAttachExportService;

@RestController
@RequestMapping("/systemAttach")
@Api(value="附件对外接口", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachImexportController extends BaseController{
	
	@Autowired
	private SystemAttachExportService systemAttachExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemAttachExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有附件导出")
	@ApiMethod(value="导出附件", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemAttachExportVo> export(@RequestBody SystemAttachExportDto exportDto){
		SystemAttachExportBo exportBo=this.convertPojo(exportDto, SystemAttachExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemAttachExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出附件", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemAttachExportStopVo> export(@RequestBody SystemAttachExportStopDto exportStopDto){
		SystemAttachExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemAttachExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemAttachExportService.stop(exportStopBo));
	}
}
