package com.unswift.cloud.controller.logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueueExportBo;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueueExportStopBo;
import com.unswift.cloud.pojo.dto.logger.queue.LoggerQueueExportDto;
import com.unswift.cloud.pojo.dto.logger.queue.LoggerQueueExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueueExportStopVo;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueueExportVo;
import com.unswift.cloud.service.export.logger.LoggerQueueExportService;

@RestController
@RequestMapping("/loggerQueue")
@Api(value="接口请求日志对外接口", author="liyunlong", date="2024-01-25", version="1.0.0")
public class LoggerQueueImexportController extends BaseController{
	
	@Autowired
	@ApiField("接口请求日志导出服务")
	private LoggerQueueExportService loggerQueueExportService;
	
	@Auth("logger.queue.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "loggerQueueExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有接口请求日志导出")
	@ApiMethod(value="导出接口请求日志", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<LoggerQueueExportVo> export(@RequestBody LoggerQueueExportDto exportDto){
		LoggerQueueExportBo exportBo=this.convertPojo(exportDto, LoggerQueueExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerQueueExportService.export(exportBo));
	}
	
	@Auth("logger.queue.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出接口请求日志", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<LoggerQueueExportStopVo> exportStop(@RequestBody LoggerQueueExportStopDto exportStopDto){
		LoggerQueueExportStopBo exportStopBo=this.convertPojo(exportStopDto, LoggerQueueExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerQueueExportService.stop(exportStopBo));
	}
}