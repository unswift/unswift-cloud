package com.unswift.cloud.controller.logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.logger.operator.LoggerOperatorExportBo;
import com.unswift.cloud.pojo.bo.logger.operator.LoggerOperatorExportStopBo;
import com.unswift.cloud.pojo.dto.logger.operator.LoggerOperatorExportDto;
import com.unswift.cloud.pojo.dto.logger.operator.LoggerOperatorExportStopDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.logger.operator.LoggerOperatorExportStopVo;
import com.unswift.cloud.pojo.vo.logger.operator.LoggerOperatorExportVo;
import com.unswift.cloud.service.export.logger.LoggerOperatorExportService;

@RestController
@RequestMapping("/loggerOperator")
@Api(value="操作日志对外接口", author="liyunlong", date="2023-12-12", version="1.0.0")
public class LoggerOperatorImexportController extends BaseController{
	
	@Autowired
	private LoggerOperatorExportService loggerOperatorExportService;
	
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@ApiMethod(value="导出操作日志", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<LoggerOperatorExportVo> export(@RequestBody LoggerOperatorExportDto exportDto){
		LoggerOperatorExportBo exportBo=this.convertPojo(exportDto, LoggerOperatorExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerOperatorExportService.export(exportBo));
	}
	
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出操作日志", params=@ApiField("停止对象"), returns=@ApiField("导出结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<LoggerOperatorExportStopVo> export(@RequestBody LoggerOperatorExportStopDto exportStopDto){
		LoggerOperatorExportStopBo exportStopBo=this.convertPojo(exportStopDto, LoggerOperatorExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerOperatorExportService.stop(exportStopBo));
	}
}