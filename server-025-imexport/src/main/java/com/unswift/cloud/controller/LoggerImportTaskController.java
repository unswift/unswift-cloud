package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskCreateBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskDeleteBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskPageBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskProgressBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskUpdateBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskViewBo;
import com.unswift.cloud.pojo.dto.logger.imports.task.LoggerImportTaskCreateDto;
import com.unswift.cloud.pojo.dto.logger.imports.task.LoggerImportTaskDeleteDto;
import com.unswift.cloud.pojo.dto.logger.imports.task.LoggerImportTaskPageDto;
import com.unswift.cloud.pojo.dto.logger.imports.task.LoggerImportTaskProgressDto;
import com.unswift.cloud.pojo.dto.logger.imports.task.LoggerImportTaskUpdateDto;
import com.unswift.cloud.pojo.dto.logger.imports.task.LoggerImportTaskViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskCreateVo;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskDeleteVo;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskPageVo;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskProgressVo;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskUpdateVo;
import com.unswift.cloud.pojo.vo.logger.imports.task.LoggerImportTaskViewVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.service.LoggerImportTaskService;

@RestController
@RequestMapping("/loggerImportTask")
@Api(value="导入任务记录表对外接口", author="liyunlong", date="2024-01-09", version="1.0.0")
public class LoggerImportTaskController extends BaseController{
	
	@Autowired
	@ApiField("导入任务记录表服务")
	private LoggerImportTaskService loggerImportTaskService;
	
	@Auth("logger.imports.task.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="导入任务记录表分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<LoggerImportTaskPageVo>> findPageList(@RequestBody LoggerImportTaskPageDto searchDto){
		LoggerImportTaskPageBo searchBo=this.convertPojo(searchDto, LoggerImportTaskPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerImportTaskService.findPageList(searchBo));
	}
	
	@Auth("logger.imports.task.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="导入任务记录表详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<LoggerImportTaskViewVo> view(@RequestBody LoggerImportTaskViewDto viewDto){
		LoggerImportTaskViewBo viewBo=this.convertPojo(viewDto, LoggerImportTaskViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerImportTaskService.view(viewBo));
	}
	
	@Active
	@RequestMapping(value="/findProgress", method=RequestMethod.POST)
	@ApiMethod(value="导入任务记录表进度", params=@ApiField("主键实体"), returns=@ApiField("详情数据"))
	public ResponseBody<LoggerImportTaskProgressVo> findProgress(@RequestBody LoggerImportTaskProgressDto progressDto){
		LoggerImportTaskProgressBo progressBo=this.convertPojo(progressDto, LoggerImportTaskProgressBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerImportTaskService.findProgress(progressBo));
	}
	
	@Auth("logger.imports.task.create")
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建导入任务记录表", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<LoggerImportTaskCreateVo> create(@RequestBody LoggerImportTaskCreateDto createDto){
		LoggerImportTaskCreateBo createBo=this.convertPojo(createDto, LoggerImportTaskCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerImportTaskService.create(createBo));
	}
	
	@Auth("logger.imports.task.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新导入任务记录表", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<LoggerImportTaskUpdateVo> update(@RequestBody LoggerImportTaskUpdateDto updateDto){
		LoggerImportTaskUpdateBo updateBo=this.convertPojo(updateDto, LoggerImportTaskUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerImportTaskService.update(updateBo));
	}
	
	@Auth("logger.imports.task.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除导入任务记录表", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<LoggerImportTaskDeleteVo> delete(@RequestBody LoggerImportTaskDeleteDto deleteDto){
		LoggerImportTaskDeleteBo deleteBo=this.convertPojo(deleteDto, LoggerImportTaskDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerImportTaskService.delete(deleteBo));
	}
}