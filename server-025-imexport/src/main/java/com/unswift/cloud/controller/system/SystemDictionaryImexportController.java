package com.unswift.cloud.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryExportBo;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryExportStopBo;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryImportBo;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryImportStopBo;
import com.unswift.cloud.pojo.dto.system.dictionary.SystemDictionaryExportDto;
import com.unswift.cloud.pojo.dto.system.dictionary.SystemDictionaryExportStopDto;
import com.unswift.cloud.pojo.dto.system.dictionary.SystemDictionaryImportDto;
import com.unswift.cloud.pojo.dto.system.dictionary.SystemDictionaryImportStopDto;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryExportVo;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryExportStopVo;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryImportVo;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryImportStopVo;
import com.unswift.cloud.service.export.system.SystemDictionaryExportService;
import com.unswift.cloud.service.imports.system.SystemDictionaryImportService;

@RestController
@RequestMapping("/systemDictionary")
@Api(value="数据字典对外接口", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryImexportController extends BaseController{
	
	@Autowired
	@ApiField("数据字典导出服务")
	private SystemDictionaryExportService systemDictionaryExportService;
	
	@Autowired
	@ApiField("数据字典导入服务")
	private SystemDictionaryImportService systemDictionaryImportService;
	
	@Active
	@Auth("system.dictionary.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "systemDictionaryExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有数据字典导出")
	@ApiMethod(value="导出数据字典", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemDictionaryExportVo> export(@RequestBody SystemDictionaryExportDto exportDto){
		SystemDictionaryExportBo exportBo=this.convertPojo(exportDto, SystemDictionaryExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryExportService.export(exportBo));
	}
	
	@Active
	@Auth("system.dictionary.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出数据字典", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemDictionaryExportStopVo> exportStop(@RequestBody SystemDictionaryExportStopDto exportStopDto){
		SystemDictionaryExportStopBo exportStopBo=this.convertPojo(exportStopDto, SystemDictionaryExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryExportService.stop(exportStopBo));
	}
	
	@Active
	@Auth("system.dictionary.import")
	@RequestMapping(value="/import", method=RequestMethod.POST)
	@LockException(prefix = "systemDictionaryImport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有数据字典导入")
	@ApiMethod(value="导入数据字典", params=@ApiField("导入实体"), returns=@ApiField("导入结果Vo->id:任务id,result:{0：未导入，1：已导入}"))
	public ResponseBody<SystemDictionaryImportVo> imports(@RequestBody SystemDictionaryImportDto importDto){
		SystemDictionaryImportBo importBo=this.convertPojo(importDto, SystemDictionaryImportBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryImportService.imports(importBo));
	}
	
	@Active
	@Auth("system.dictionary.import")
	@RequestMapping(value="/import/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导入数据字典", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<SystemDictionaryImportStopVo> importStop(@RequestBody SystemDictionaryImportStopDto importStopDto){
		SystemDictionaryImportStopBo importStopBo=this.convertPojo(importStopDto, SystemDictionaryImportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryImportService.stop(importStopBo));
	}
}
