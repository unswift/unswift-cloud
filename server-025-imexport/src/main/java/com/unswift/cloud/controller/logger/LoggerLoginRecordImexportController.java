package com.unswift.cloud.controller.logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordExportBo;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordExportStopBo;
import com.unswift.cloud.pojo.dto.logger.login.record.LoggerLoginRecordExportDto;
import com.unswift.cloud.pojo.dto.logger.login.record.LoggerLoginRecordExportStopDto;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordExportVo;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordExportStopVo;
import com.unswift.cloud.service.export.logger.LoggerLoginRecordExportService;

@RestController
@RequestMapping("/loggerLoginRecord")
@Api(value="登录记录对外接口", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordImexportController extends BaseController{
	
	@Autowired
	@ApiField("登录记录导出服务")
	private LoggerLoginRecordExportService loggerLoginRecordExportService;
	
	@Active
	@Auth("logger.login.record.export")
	@RequestMapping(value="/export", method=RequestMethod.POST)
	@LockException(prefix = "loggerLoginRecordExport", unique = "{currUserId}", messageCode = "task.is.currently.executing", messageArgs = "有登录记录导出")
	@ApiMethod(value="导出登录记录", params=@ApiField("导出条件实体"), returns=@ApiField("导出结果Vo->id:任务id,result:{0：未创建，1：已创建}"))
	public ResponseBody<LoggerLoginRecordExportVo> export(@RequestBody LoggerLoginRecordExportDto exportDto){
		LoggerLoginRecordExportBo exportBo=this.convertPojo(exportDto, LoggerLoginRecordExportBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerLoginRecordExportService.export(exportBo));
	}
	
	@Active
	@Auth("logger.login.record.export")
	@RequestMapping(value="/export/stop", method=RequestMethod.POST)
	@ApiMethod(value="停止导出登录记录", params=@ApiField("停止对象"), returns=@ApiField("停止结果Vo->result:{0：未停止，1：已停止}"))
	public ResponseBody<LoggerLoginRecordExportStopVo> exportStop(@RequestBody LoggerLoginRecordExportStopDto exportStopDto){
		LoggerLoginRecordExportStopBo exportStopBo=this.convertPojo(exportStopDto, LoggerLoginRecordExportStopBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerLoginRecordExportService.stop(exportStopBo));
	}
}
