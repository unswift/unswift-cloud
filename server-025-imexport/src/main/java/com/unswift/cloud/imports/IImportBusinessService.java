package com.unswift.cloud.imports;

import java.util.List;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.bo.BaseBo;
import com.unswift.cloud.pojo.mo.ImportMo;

@Api(value="导入业务顶层接口，开发者实现导入业务时需实现此接口", author="unswift", date="2024-01-09", version="1.0.0")
public interface IImportBusinessService<E extends ImportMo, C extends BaseBo> {

	@ApiMethod(value="获取导入模板的类类型", returns=@ApiField("导入模板类类型"))
	Class<E> getModule();
	
	@ApiMethod(value="开始导入时执行，如有特殊需求，请重写此方法")
	default void start() {
		
	}
	
	@ApiMethod(value="导入行数据验证，请再此处处理完所有的验证，否则可能导致数据导入一部分", params = {@ApiField("需验证的行数据"), @ApiField("此数据在excel中的行次（从零开始）")})
	void validate(E rowData, int rowIndex, String token, C condition);
	
	@ApiMethod(value="导入数据批量处理，批次数据根据nocos配置确定批次大小", params = {@ApiField("批次数据"), @ApiField("此列表在excel中的开始行次（从零开始）"), @ApiField("用户token")})
	void handleBatch(List<E> rowList, int startRowIndex, String token, C condition);
	
	@ApiMethod(value="结束导入时触发，如清理临时缓存等", params = {@ApiField("用户token")})
	void finish(String token);
}
