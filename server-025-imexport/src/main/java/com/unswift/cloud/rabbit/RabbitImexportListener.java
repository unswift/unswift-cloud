package com.unswift.cloud.rabbit;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;
import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.logger.QueueReceive;
import com.unswift.cloud.core.CommonOperator;
import com.unswift.cloud.export.ExportService;
import com.unswift.cloud.imports.ImportService;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskQueueBo;
import com.unswift.cloud.pojo.bo.logger.export.task.LoggerExportTaskStopQueueBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskQueueBo;
import com.unswift.cloud.pojo.bo.logger.imports.task.LoggerImportTaskStopQueueBo;
import com.unswift.cloud.pojo.mro.logger.export.task.LoggerExportTaskQueueMro;
import com.unswift.cloud.pojo.mro.logger.imports.task.LoggerImportTaskQueueMro;
import com.unswift.utils.ExceptionUtils;

@Component
@Api(value="Rabbit Mq 导出服务队列监听", author="unswift", date="2023-07-14", version="1.0.0")
public class RabbitImexportListener extends CommonOperator implements RabbitConstant{

	@Autowired
	@ApiField("导出逻辑业务处理对象")
	private ExportService exportService;
	
	@Autowired
	@ApiField("导入逻辑业务处理对象")
	private ImportService importService;
	
	@QueueReceive
	@RabbitListener(queues = EXPORT_QUEUE_NAME, containerFactory = "rabbitListenerContainerFactory")
	@ApiMethod(value="导出任务Rabbit Mq监听，接收导出任务队列消息", params = {@ApiField("导出任务对象"), @ApiField("Rabbit Mq消息对象"), @ApiField("Rabbit Mq管道对象")})
	public void exportListener(LoggerExportTaskQueueMro exportTask, Message message, Channel channel) throws Exception {
		final long deliveryTag = message.getMessageProperties().getDeliveryTag();
		try {
			LoggerExportTaskQueueBo exportTaskBo=this.convertPojo(exportTask, LoggerExportTaskQueueBo.class);//将Dto转为Bo
			exportService.export(exportTaskBo);
		} catch (Exception e) {
			e.printStackTrace();
			throw ExceptionUtils.exception("export.adapter.exception", e, e.getMessage());
		}
		channel.basicAck(deliveryTag, false);
	}
	
	@QueueReceive
	@RabbitListener(queues = EXPORT_STOP_QUEUE_NAME, containerFactory = "rabbitListenerContainerFactory")
	@ApiMethod(value="停止导出任务Rabbit Mq监听，接收停止导出任务队列消息", params = {@ApiField("停止导出任务对象"), @ApiField("Rabbit Mq消息对象"), @ApiField("Rabbit Mq管道对象")})
	public void exportStopListener(LoggerExportTaskQueueMro exportTask, Message message, Channel channel) throws Exception {
		final long deliveryTag = message.getMessageProperties().getDeliveryTag();
		try {
			LoggerExportTaskStopQueueBo exportTaskBo=this.convertPojo(exportTask, LoggerExportTaskStopQueueBo.class);//将Dto转为Bo
			exportService.stop(exportTaskBo);
		} catch (Exception e) {
			e.printStackTrace();
			throw ExceptionUtils.exception("export.adapter.exception", e, e.getMessage());
		}
		channel.basicAck(deliveryTag, false);
	}
	
	@QueueReceive
	@RabbitListener(queues = IMPORT_QUEUE_NAME, containerFactory = "rabbitListenerContainerFactory")
	@ApiMethod(value="导入任务Rabbit Mq监听，接收导入任务队列消息", params = {@ApiField("导入任务对象"), @ApiField("Rabbit Mq消息对象"), @ApiField("Rabbit Mq管道对象")})
	public void importListener(LoggerImportTaskQueueMro importTask, Message message, Channel channel) throws Exception {
		final long deliveryTag = message.getMessageProperties().getDeliveryTag();
		try {
			LoggerImportTaskQueueBo exportTaskBo=this.convertPojo(importTask, LoggerImportTaskQueueBo.class);//将Dto转为Bo
			importService.imports(exportTaskBo);
		} catch (Exception e) {
			e.printStackTrace();
			throw ExceptionUtils.exception("import.adapter.exception", e, e.getMessage());
		}
		channel.basicAck(deliveryTag, false);
	}
	
	@QueueReceive
	@RabbitListener(queues = IMPORT_STOP_QUEUE_NAME, containerFactory = "rabbitListenerContainerFactory")
	@ApiMethod(value="停止导入任务Rabbit Mq监听，接收停止导入任务队列消息", params = {@ApiField("停止导入任务对象"), @ApiField("Rabbit Mq消息对象"), @ApiField("Rabbit Mq管道对象")})
	public void importStopListener(LoggerImportTaskQueueMro importTask, Message message, Channel channel) throws Exception {
		final long deliveryTag = message.getMessageProperties().getDeliveryTag();
		try {
			LoggerImportTaskStopQueueBo exportTaskBo=this.convertPojo(importTask, LoggerImportTaskStopQueueBo.class);//将Dto转为Bo
			importService.stop(exportTaskBo);
		} catch (Exception e) {
			e.printStackTrace();
			throw ExceptionUtils.exception("import.adapter.exception", e, e.getMessage());
		}
		channel.basicAck(deliveryTag, false);
	}
}
