package com.unswift.cloud.controller;

import java.io.ByteArrayOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.pojo.bo.login.login.LogingBo;
import com.unswift.cloud.pojo.dto.login.login.LogingDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.login.LoginVo;
import com.unswift.cloud.pojo.vo.login.LogoutVo;
import com.unswift.cloud.pojo.vo.login.TokenUserVo;
import com.unswift.cloud.pojo.vo.login.ValidateCodeVo;
import com.unswift.cloud.service.LoginService;
import com.unswift.exception.ImageException;
import com.unswift.utils.EncryptionUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ImageUtils;
import com.unswift.utils.StringUtils;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping
@Api(value="登录对外接口", author="unswift", date="2023-05-29", version="1.0.0")
public class LoginController extends BaseController{
	
	@Autowired
	private LoginService loginService;
	
	@Active
	@TrimHandle
	@RequestMapping(value="/loging", method=RequestMethod.POST)
	@ApiMethod(value="用户名密码登录", params= {@ApiField("登录信息"), @ApiField(value="Http请求对象", requestParam = false)}, returns=@ApiField("登录回调信息"))
	public ResponseBody<LoginVo> loging(@RequestBody LogingDto loginDto, HttpServletRequest request){
		LogingBo loginBo=this.convertPojo(loginDto, LogingBo.class);
		return ResponseBody.success(loginService.loging(loginBo, request));
	}
	
	@Active
	@ApiMethod(value="生成图形验证码", returns=@ApiField("请求回调对象"))
	@RequestMapping(value="/genericsImageCaptcha", method=RequestMethod.POST)
	public ResponseBody<ValidateCodeVo> genericsImageCaptcha(){
		try {
			String sessionId=StringUtils.getUuid();
			ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
			String captcha=ImageUtils.generateCaptcha(outputStream, 4, 100, 30);
			String base64Captcha=EncryptionUtils.base64Encode(outputStream.toByteArray());
			redisAdapter.set(sessionId, captcha);
			logger.info("获取验证码成功："+captcha);
			return ResponseBody.success(new ValidateCodeVo(sessionId, base64Captcha));
		} catch (ImageException e){
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw ExceptionUtils.exception("generate.verify.code.exception", e, e.getMessage());
		}
	}
	
	@Active
	@RequestMapping(value="/logout", method=RequestMethod.POST)
	@ApiMethod(value="用户登出", returns=@ApiField("登出回调信息"))
	public ResponseBody<LogoutVo> logout(){
		return ResponseBody.success(loginService.logout());
	}
	
	@Active
	@RequestMapping(value="/getUserInfo", method=RequestMethod.POST)
	@ApiMethod(value="获取用户信息", returns=@ApiField("用户信息"))
	public ResponseBody<TokenUserVo> getUserInfo(){
		return ResponseBody.success(loginService.getUserInfo());
	}
}