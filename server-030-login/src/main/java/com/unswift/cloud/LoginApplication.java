package com.unswift.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.utils.ExceptionUtils;

@EnableScheduling
@EnableFeignClients
@EnableDiscoveryClient
@EnableTransactionManagement
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@Api(value="登录服务器启动类", author="unswift", date="2023-05-29", version="1.0.0")
public class LoginApplication {
	
	@ApiMethod(value="登录服务器启动方法", params=@ApiField("启动参数"))
	public static void main(String[] args) {
		try {
			SpringApplication.run(LoginApplication.class, args);
		} catch (Exception e) {
			e.printStackTrace();
			throw ExceptionUtils.exception("server.start.exception", e, e.getMessage());
		}
	}
}
