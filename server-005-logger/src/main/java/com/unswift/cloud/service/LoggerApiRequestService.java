package com.unswift.cloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerApiRequestAdapter;
import com.unswift.cloud.pojo.bo.logger.api.request.LoggerApiRequestCreateBo;
import com.unswift.cloud.pojo.bo.logger.api.request.LoggerApiRequestDeleteBo;
import com.unswift.cloud.pojo.bo.logger.api.request.LoggerApiRequestPageBo;
import com.unswift.cloud.pojo.bo.logger.api.request.LoggerApiRequestUpdateBo;
import com.unswift.cloud.pojo.bo.logger.api.request.LoggerApiRequestViewBo;
import com.unswift.cloud.pojo.dao.logger.api.request.LoggerApiRequestDataDo;
import com.unswift.cloud.pojo.dao.logger.api.request.LoggerApiRequestDeleteDo;
import com.unswift.cloud.pojo.dao.logger.api.request.LoggerApiRequestInsertDo;
import com.unswift.cloud.pojo.dao.logger.api.request.LoggerApiRequestPageDo;
import com.unswift.cloud.pojo.dao.logger.api.request.LoggerApiRequestSingleDo;
import com.unswift.cloud.pojo.dao.logger.api.request.LoggerApiRequestUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.logger.api.request.LoggerApiRequestCreateVo;
import com.unswift.cloud.pojo.vo.logger.api.request.LoggerApiRequestDeleteVo;
import com.unswift.cloud.pojo.vo.logger.api.request.LoggerApiRequestPageVo;
import com.unswift.cloud.pojo.vo.logger.api.request.LoggerApiRequestUpdateVo;
import com.unswift.cloud.pojo.vo.logger.api.request.LoggerApiRequestViewVo;

@Service
@Api(value="接口请求日志服务", author="liyunlong", date="2023-12-08", version="1.0.0")
public class LoggerApiRequestService extends BaseService{
	
	@Autowired
	@ApiField("接口请求日志公共服务")
	private LoggerApiRequestAdapter loggerApiRequestAdapter;
	
	@ApiMethod(value="查询接口请求日志分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<LoggerApiRequestPageVo> findPageList(LoggerApiRequestPageBo searchBo){
		LoggerApiRequestPageDo search=this.convertPojo(searchBo, LoggerApiRequestPageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<LoggerApiRequestDataDo> page=loggerApiRequestAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, LoggerApiRequestPageVo.class);
	}
	
	@ApiMethod(value="查询接口请求日志详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("接口请求日志详情数据"))
	public LoggerApiRequestViewVo view(LoggerApiRequestViewBo viewBo){
		LoggerApiRequestSingleDo search=this.convertPojo(viewBo, LoggerApiRequestSingleDo.class);
		LoggerApiRequestDataDo single=loggerApiRequestAdapter.findSingle(search);
		return this.convertPojo(single, LoggerApiRequestViewVo.class);//将Do转换为Vo
	}

	@ApiMethod(value="创建接口请求日志", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public LoggerApiRequestCreateVo create(LoggerApiRequestCreateBo createBo){
		LoggerApiRequestInsertDo insert=this.convertPojo(createBo, LoggerApiRequestInsertDo.class);//将Bo转换为Do
		int result=loggerApiRequestAdapter.save(insert, true);
		return new LoggerApiRequestCreateVo(result);
	}
	
	@ApiMethod(value="更新接口请求日志", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public LoggerApiRequestUpdateVo update(LoggerApiRequestUpdateBo updateBo){
		LoggerApiRequestUpdateDo update=this.convertPojo(updateBo, LoggerApiRequestUpdateDo.class);//将Bo转换为Do
		int result=loggerApiRequestAdapter.update(update, true);
		return new LoggerApiRequestUpdateVo(result);
	}
	
	@ApiMethod(value="删除接口请求日志", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public LoggerApiRequestDeleteVo delete(LoggerApiRequestDeleteBo deleteBo){
		LoggerApiRequestDeleteDo delete=this.convertPojo(deleteBo, LoggerApiRequestDeleteDo.class);//将Bo转换为Do
		int result=loggerApiRequestAdapter.delete(delete);
		return new LoggerApiRequestDeleteVo(result);
	}
}
