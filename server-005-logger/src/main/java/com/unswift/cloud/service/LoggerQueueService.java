package com.unswift.cloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerQueueAdapter;
import com.unswift.cloud.annotation.logger.OperatorLogger;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueueCreateBo;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueueDeleteBo;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueuePageBo;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueueUpdateBo;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueueViewBo;
import com.unswift.cloud.pojo.dao.logger.queue.LoggerQueueDataDo;
import com.unswift.cloud.pojo.dao.logger.queue.LoggerQueueDeleteDo;
import com.unswift.cloud.pojo.dao.logger.queue.LoggerQueueInsertDo;
import com.unswift.cloud.pojo.dao.logger.queue.LoggerQueuePageDo;
import com.unswift.cloud.pojo.dao.logger.queue.LoggerQueueSingleDo;
import com.unswift.cloud.pojo.dao.logger.queue.LoggerQueueUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueueCreateVo;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueueDeleteVo;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueuePageVo;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueueUpdateVo;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueueViewVo;
import com.unswift.cloud.utils.LoggerUtils;
import com.unswift.utils.ExceptionUtils;

@Service
@Api(value="接口请求日志服务", author="liyunlong", date="2024-01-25", version="1.0.0")
public class LoggerQueueService extends BaseService{
	
	@Autowired
	@ApiField("接口请求日志公共服务")
	private LoggerQueueAdapter loggerQueueAdapter;
	
	@ApiMethod(value="查询接口请求日志分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<LoggerQueuePageVo> findPageList(LoggerQueuePageBo searchBo){
		LoggerQueuePageDo search=this.convertPojo(searchBo, LoggerQueuePageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<LoggerQueueDataDo> page=loggerQueueAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, LoggerQueuePageVo.class);
	}
	
	@ApiMethod(value="查询接口请求日志详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("接口请求日志详情数据"))
	public LoggerQueueViewVo view(LoggerQueueViewBo viewBo){
		LoggerQueueSingleDo search=this.convertPojo(viewBo, LoggerQueueSingleDo.class);
		LoggerQueueDataDo single=loggerQueueAdapter.findSingle(search);
		return this.convertPojo(single, LoggerQueueViewVo.class);//将Do转换为Vo
	}

	@OperatorLogger(type = OperatorTypeEnum.CREATE)
	@ApiMethod(value="创建接口请求日志", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public LoggerQueueCreateVo create(LoggerQueueCreateBo createBo){
		LoggerUtils.setModule(loggerQueueAdapter.getModule());//设置日志所属模块
		LoggerQueueInsertDo insert=this.convertPojo(createBo, LoggerQueueInsertDo.class);//将Bo转换为Do
		int result=loggerQueueAdapter.save(insert, true);
		LoggerUtils.setId(insert.getId());//创建日志需要设置数据id
		return new LoggerQueueCreateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新接口请求日志", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public LoggerQueueUpdateVo update(LoggerQueueUpdateBo updateBo){
		LoggerUtils.setModule(loggerQueueAdapter.getModule());//设置日志所属模块
		LoggerQueueDataDo view=loggerQueueAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		LoggerQueueUpdateDo update=this.convertPojo(updateBo, LoggerQueueUpdateDo.class);//将Bo转换为Do
		int result=loggerQueueAdapter.update(update, true);
		return new LoggerQueueUpdateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.DELETE)
	@ApiMethod(value="删除接口请求日志", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public LoggerQueueDeleteVo delete(LoggerQueueDeleteBo deleteBo){
		LoggerUtils.setModule(loggerQueueAdapter.getModule());//设置日志所属模块
		LoggerQueueDataDo deleteData=loggerQueueAdapter.findById(deleteBo.getId());
		ExceptionUtils.empty(deleteData, "delete.object.not.exists", "接口请求日志信息");//删除对象必须存在
		LoggerQueueDeleteDo delete=this.convertPojo(deleteBo, LoggerQueueDeleteDo.class);//将Bo转换为Do
		int result=loggerQueueAdapter.delete(delete);
		return new LoggerQueueDeleteVo(result);
	}
}