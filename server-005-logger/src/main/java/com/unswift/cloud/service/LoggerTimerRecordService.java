package com.unswift.cloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerTimerRecordAdapter;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordCreateBo;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordDeleteBo;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordPageBo;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordUpdateBo;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordViewBo;
import com.unswift.cloud.pojo.dao.logger.timer.record.LoggerTimerRecordDataDo;
import com.unswift.cloud.pojo.dao.logger.timer.record.LoggerTimerRecordDeleteDo;
import com.unswift.cloud.pojo.dao.logger.timer.record.LoggerTimerRecordInsertDo;
import com.unswift.cloud.pojo.dao.logger.timer.record.LoggerTimerRecordPageDo;
import com.unswift.cloud.pojo.dao.logger.timer.record.LoggerTimerRecordSingleDo;
import com.unswift.cloud.pojo.dao.logger.timer.record.LoggerTimerRecordUpdateDo;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordCreateVo;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordDeleteVo;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordPageVo;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordUpdateVo;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordViewVo;
import com.unswift.cloud.pojo.vo.page.PageVo;

@Service
@Api(value="定时器执行记录服务", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerTimerRecordService extends BaseService{
	
	@Autowired
	@ApiField("定时器执行记录公共服务")
	private LoggerTimerRecordAdapter loggerTimerRecordAdapter;
	
	@ApiMethod(value="查询定时器执行记录分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<LoggerTimerRecordPageVo> findPageList(LoggerTimerRecordPageBo searchBo){
		LoggerTimerRecordPageDo search=this.convertPojo(searchBo, LoggerTimerRecordPageDo.class);//将Bo转换为So
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<LoggerTimerRecordDataDo> page=loggerTimerRecordAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, LoggerTimerRecordPageVo.class);
	}
	
	@ApiMethod(value="查询定时器执行记录详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("定时器执行记录详情数据"))
	public LoggerTimerRecordViewVo view(LoggerTimerRecordViewBo viewBo){
		LoggerTimerRecordSingleDo search=this.convertPojo(viewBo, LoggerTimerRecordSingleDo.class);
		LoggerTimerRecordDataDo single=loggerTimerRecordAdapter.findSingle(search);
		return this.convertPojo(single, LoggerTimerRecordViewVo.class);//将Do转换为Vo
	}

	@ApiMethod(value="创建定时器执行记录", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public LoggerTimerRecordCreateVo create(LoggerTimerRecordCreateBo createBo){
		LoggerTimerRecordInsertDo insert=this.convertPojo(createBo, LoggerTimerRecordInsertDo.class);//将Bo转换为Po
		int result=loggerTimerRecordAdapter.save(insert, true);
		return new LoggerTimerRecordCreateVo(result);
	}
	
	@ApiMethod(value="更新定时器执行记录", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public LoggerTimerRecordUpdateVo update(LoggerTimerRecordUpdateBo updateBo){
		LoggerTimerRecordUpdateDo update=this.convertPojo(updateBo, LoggerTimerRecordUpdateDo.class);//将Bo转换为Po
		int result=loggerTimerRecordAdapter.update(update, true);
		return new LoggerTimerRecordUpdateVo(result);
	}
	
	@ApiMethod(value="删除定时器执行记录", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public LoggerTimerRecordDeleteVo delete(LoggerTimerRecordDeleteBo deleteBo){
		LoggerTimerRecordDeleteDo delete=this.convertPojo(deleteBo, LoggerTimerRecordDeleteDo.class);//将Bo转换为Po
		int result=loggerTimerRecordAdapter.delete(delete);
		return new LoggerTimerRecordDeleteVo(result);
	}
}
