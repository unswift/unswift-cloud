package com.unswift.cloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerSqlRecordAdapter;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordCreateBo;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordDeleteBo;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordPageBo;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordUpdateBo;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordViewBo;
import com.unswift.cloud.pojo.dao.logger.sql.record.LoggerSqlRecordDataDo;
import com.unswift.cloud.pojo.dao.logger.sql.record.LoggerSqlRecordDeleteDo;
import com.unswift.cloud.pojo.dao.logger.sql.record.LoggerSqlRecordInsertDo;
import com.unswift.cloud.pojo.dao.logger.sql.record.LoggerSqlRecordPageDo;
import com.unswift.cloud.pojo.dao.logger.sql.record.LoggerSqlRecordSingleDo;
import com.unswift.cloud.pojo.dao.logger.sql.record.LoggerSqlRecordUpdateDo;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordCreateVo;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordDeleteVo;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordPageVo;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordUpdateVo;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordViewVo;
import com.unswift.cloud.pojo.vo.page.PageVo;

@Service
@Api(value="sql执行记录服务", author="unswift", date="2023-09-05", version="1.0.0")
public class LoggerSqlRecordService extends BaseService{
	
	@Autowired
	@ApiField("sql执行记录公共服务")
	private LoggerSqlRecordAdapter loggerSqlRecordAdapter;
	
	@ApiMethod(value="查询sql执行记录分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<LoggerSqlRecordPageVo> findPageList(LoggerSqlRecordPageBo searchBo){
		LoggerSqlRecordPageDo search=this.convertPojo(searchBo, LoggerSqlRecordPageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<LoggerSqlRecordDataDo> page=loggerSqlRecordAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, LoggerSqlRecordPageVo.class);
	}
	
	@ApiMethod(value="查询sql执行记录详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("sql执行记录详情数据"))
	public LoggerSqlRecordViewVo view(LoggerSqlRecordViewBo viewBo){
		LoggerSqlRecordSingleDo search=this.convertPojo(viewBo, LoggerSqlRecordSingleDo.class);
		LoggerSqlRecordDataDo single=loggerSqlRecordAdapter.findSingle(search);
		return this.convertPojo(single, LoggerSqlRecordViewVo.class);//将Do转换为Vo
	}

	@ApiMethod(value="创建sql执行记录", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public LoggerSqlRecordCreateVo create(LoggerSqlRecordCreateBo createBo){
		LoggerSqlRecordInsertDo insert=this.convertPojo(createBo, LoggerSqlRecordInsertDo.class);//将Bo转换为Do
		int result=loggerSqlRecordAdapter.save(insert, true);
		return new LoggerSqlRecordCreateVo(result);
	}
	
	@ApiMethod(value="更新sql执行记录", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public LoggerSqlRecordUpdateVo update(LoggerSqlRecordUpdateBo updateBo){
		LoggerSqlRecordUpdateDo update=this.convertPojo(updateBo, LoggerSqlRecordUpdateDo.class);//将Bo转换为Do
		int result=loggerSqlRecordAdapter.update(update, true);
		return new LoggerSqlRecordUpdateVo(result);
	}
	
	@ApiMethod(value="删除sql执行记录", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public LoggerSqlRecordDeleteVo delete(LoggerSqlRecordDeleteBo deleteBo){
		LoggerSqlRecordDeleteDo delete=this.convertPojo(deleteBo, LoggerSqlRecordDeleteDo.class);//将Bo转换为Do
		int result=loggerSqlRecordAdapter.delete(delete);
		return new LoggerSqlRecordDeleteVo(result);
	}
}