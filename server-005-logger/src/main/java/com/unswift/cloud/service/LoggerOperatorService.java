package com.unswift.cloud.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerOperatorAdapter;
import com.unswift.cloud.adapter.system.auth.SystemUserAdapter;
import com.unswift.cloud.pojo.bo.logger.operator.LoggerOperatorCreateBo;
import com.unswift.cloud.pojo.bo.logger.operator.LoggerOperatorDeleteBo;
import com.unswift.cloud.pojo.bo.logger.operator.LoggerOperatorPageBo;
import com.unswift.cloud.pojo.bo.logger.operator.LoggerOperatorUpdateBo;
import com.unswift.cloud.pojo.bo.logger.operator.LoggerOperatorViewBo;
import com.unswift.cloud.pojo.dao.logger.operator.LoggerOperatorDataDo;
import com.unswift.cloud.pojo.dao.logger.operator.LoggerOperatorDeleteDo;
import com.unswift.cloud.pojo.dao.logger.operator.LoggerOperatorInsertDo;
import com.unswift.cloud.pojo.dao.logger.operator.LoggerOperatorPageDo;
import com.unswift.cloud.pojo.dao.logger.operator.LoggerOperatorSingleDo;
import com.unswift.cloud.pojo.dao.logger.operator.LoggerOperatorUpdateDo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.user.SystemUserDataDo;
import com.unswift.cloud.pojo.vo.logger.operator.LoggerOperatorCreateVo;
import com.unswift.cloud.pojo.vo.logger.operator.LoggerOperatorDeleteVo;
import com.unswift.cloud.pojo.vo.logger.operator.LoggerOperatorPageVo;
import com.unswift.cloud.pojo.vo.logger.operator.LoggerOperatorUpdateVo;
import com.unswift.cloud.pojo.vo.logger.operator.LoggerOperatorViewVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="操作日志服务", author="liyunlong", date="2023-12-12", version="1.0.0")
public class LoggerOperatorService extends BaseService{
	
	@Autowired
	@ApiField("操作日志公共服务")
	private LoggerOperatorAdapter loggerOperatorAdapter;
	@Autowired
	@ApiField("系统用户公共服务")
	private SystemUserAdapter systemUserAdapter;
	
	@ApiMethod(value="查询操作日志分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<LoggerOperatorPageVo> findPageList(LoggerOperatorPageBo searchBo){
		LoggerOperatorPageDo search=this.convertPojo(searchBo, LoggerOperatorPageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		loggerOperatorAdapter.setFieldToLike(search, "operatorContent", ObjectUtils.asList("t.operator_content_"));//将name转为like查询
		loggerOperatorAdapter.addOrderBy(search, "create_time_", Sql.ORDER_BY_DESC);//create_time_倒叙
		PageVo<LoggerOperatorDataDo> page=loggerOperatorAdapter.findPageList(search);//将Do转换为Vo
		PageVo<LoggerOperatorPageVo> pageVo = this.convertPage(page, LoggerOperatorPageVo.class);
		//翻译数据
		this.translate(pageVo, new DataTranslate<LoggerOperatorPageVo>() {
			@Override
			public void init(List<LoggerOperatorPageVo> dataList) {
				List<Long> createUserList=dataList.stream().map(d -> d.getCreateUser()).distinct().collect(Collectors.toList());
				List<SystemUserDataDo> userList = systemUserAdapter.findByIds(createUserList);
				if(ObjectUtils.isNotEmpty(userList)){
					this.setCache("userMap", userList.stream().collect(Collectors.toMap(u -> u.getId(), u -> u.getNickname())));
				}
			}
			@Override
			public void translate(LoggerOperatorPageVo entity) {
				entity.setOperatorTypeName(cacheAdapter.findDictionaryValueByKey("operatorType", getLanguage(), entity.getOperatorType()));
				entity.setCreateUserName(this.getMapCache("userMap", entity.getCreateUser()));
			}
		});
		return pageVo;
	}
	
	@ApiMethod(value="查询操作日志详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("操作日志详情数据"))
	public LoggerOperatorViewVo view(LoggerOperatorViewBo viewBo){
		LoggerOperatorSingleDo search=this.convertPojo(viewBo, LoggerOperatorSingleDo.class);
		LoggerOperatorDataDo single=loggerOperatorAdapter.findSingle(search);
		return this.convertPojo(single, LoggerOperatorViewVo.class);//将Do转换为Vo
	}

	@ApiMethod(value="创建操作日志", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public LoggerOperatorCreateVo create(LoggerOperatorCreateBo createBo){
		LoggerOperatorInsertDo insert=this.convertPojo(createBo, LoggerOperatorInsertDo.class);//将Bo转换为Do
		int result=loggerOperatorAdapter.save(insert, true);
		return new LoggerOperatorCreateVo(result);
	}
	
	@ApiMethod(value="更新操作日志", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public LoggerOperatorUpdateVo update(LoggerOperatorUpdateBo updateBo){
		LoggerOperatorUpdateDo update=this.convertPojo(updateBo, LoggerOperatorUpdateDo.class);//将Bo转换为Do
		int result=loggerOperatorAdapter.update(update, true);
		return new LoggerOperatorUpdateVo(result);
	}
	
	@ApiMethod(value="删除操作日志", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public LoggerOperatorDeleteVo delete(LoggerOperatorDeleteBo deleteBo){
		LoggerOperatorDeleteDo delete=this.convertPojo(deleteBo, LoggerOperatorDeleteDo.class);//将Bo转换为Do
		int result=loggerOperatorAdapter.delete(delete);
		return new LoggerOperatorDeleteVo(result);
	}
}