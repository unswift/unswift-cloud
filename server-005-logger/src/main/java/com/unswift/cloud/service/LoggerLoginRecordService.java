package com.unswift.cloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerLoginRecordAdapter;
import com.unswift.cloud.annotation.logger.OperatorLogger;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordCreateBo;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordDeleteBo;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordPageBo;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordUpdateBo;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordViewBo;
import com.unswift.cloud.pojo.dao.logger.login.record.LoggerLoginRecordDataDo;
import com.unswift.cloud.pojo.dao.logger.login.record.LoggerLoginRecordDeleteDo;
import com.unswift.cloud.pojo.dao.logger.login.record.LoggerLoginRecordInsertDo;
import com.unswift.cloud.pojo.dao.logger.login.record.LoggerLoginRecordPageDo;
import com.unswift.cloud.pojo.dao.logger.login.record.LoggerLoginRecordSingleDo;
import com.unswift.cloud.pojo.dao.logger.login.record.LoggerLoginRecordUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.page.PageVo.PageRowHandler;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordCreateVo;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordDeleteVo;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordPageVo;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordUpdateVo;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordViewVo;
import com.unswift.cloud.utils.LoggerUtils;
import com.unswift.cloud.sql.logger.login.record.LoggerLoginRecordSql;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="登录记录服务", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordService extends BaseService{
	
	@Autowired
	@ApiField("登录记录公共服务")
	private LoggerLoginRecordAdapter loggerLoginRecordAdapter;
	
	@Autowired
	@ApiField("登录记录自定义Sql")
	private LoggerLoginRecordSql loggerLoginRecordSql;
	
	@ApiMethod(value="查询登录记录分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<LoggerLoginRecordPageVo> findPageList(LoggerLoginRecordPageBo searchBo){
		LoggerLoginRecordPageDo search=this.convertPojo(searchBo, LoggerLoginRecordPageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		loggerLoginRecordAdapter.setFieldToLike(search, "account", ObjectUtils.asList("account_"));
		loggerLoginRecordAdapter.addWhereBetweenList(search, "login_time_", searchBo.getLoginStartTime(), searchBo.getLoginEndTime(), "yyyy-MM-dd HH:mm:ss");
		PageVo<LoggerLoginRecordDataDo> page=loggerLoginRecordAdapter.findPageList(search);//将Do转换为Vo
		PageVo<LoggerLoginRecordPageVo> pageVo = this.convertPage(page, LoggerLoginRecordPageVo.class);
		pageVo.forEach(new PageRowHandler<LoggerLoginRecordPageVo>() {
			@Override
			public void handler(LoggerLoginRecordPageVo rowData, int index) {
				rowData.setLoginResultName(cacheAdapter.findDictionaryValueByKey("successFail", getLanguage(), rowData.getLoginResult()+""));
				super.handler(rowData, index);
			}
		});
		return pageVo;
	}
	
	@ApiMethod(value="查询登录记录详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("登录记录详情数据"))
	public LoggerLoginRecordViewVo view(LoggerLoginRecordViewBo viewBo){
		LoggerLoginRecordSingleDo search=this.convertPojo(viewBo, LoggerLoginRecordSingleDo.class);
		LoggerLoginRecordDataDo single=loggerLoginRecordAdapter.findSingle(search);
		return this.convertPojo(single, LoggerLoginRecordViewVo.class);//将Do转换为Vo
	}

	@OperatorLogger(type = OperatorTypeEnum.CREATE)
	@ApiMethod(value="创建登录记录", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public LoggerLoginRecordCreateVo create(LoggerLoginRecordCreateBo createBo){
		LoggerUtils.setModule(loggerLoginRecordAdapter.getModule());//设置日志所属模块
		LoggerLoginRecordInsertDo insert=this.convertPojo(createBo, LoggerLoginRecordInsertDo.class);//将Bo转换为Do
		int result=loggerLoginRecordAdapter.save(insert, true);
		LoggerUtils.setId(insert.getId());//创建日志需要设置数据id
		return new LoggerLoginRecordCreateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新登录记录", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public LoggerLoginRecordUpdateVo update(LoggerLoginRecordUpdateBo updateBo){
		LoggerUtils.setModule(loggerLoginRecordAdapter.getModule());//设置日志所属模块
		LoggerLoginRecordDataDo view=loggerLoginRecordAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		LoggerLoginRecordUpdateDo update=this.convertPojo(updateBo, LoggerLoginRecordUpdateDo.class);//将Bo转换为Do
		int result=loggerLoginRecordAdapter.update(update, true);
		return new LoggerLoginRecordUpdateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.DELETE)
	@ApiMethod(value="删除登录记录", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public LoggerLoginRecordDeleteVo delete(LoggerLoginRecordDeleteBo deleteBo){
		LoggerUtils.setModule(loggerLoginRecordAdapter.getModule());//设置日志所属模块
		LoggerLoginRecordDataDo deleteData=loggerLoginRecordAdapter.findById(deleteBo.getId());
		ExceptionUtils.empty(deleteData, "delete.object.not.exists", "登录记录信息");//删除对象必须存在
		LoggerLoginRecordDeleteDo delete=this.convertPojo(deleteBo, LoggerLoginRecordDeleteDo.class);//将Bo转换为Do
		int result=loggerLoginRecordAdapter.delete(delete);
		return new LoggerLoginRecordDeleteVo(result);
	}
}
