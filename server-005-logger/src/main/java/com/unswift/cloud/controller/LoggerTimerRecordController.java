package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordCreateBo;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordDeleteBo;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordPageBo;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordUpdateBo;
import com.unswift.cloud.pojo.bo.logger.timer.record.LoggerTimerRecordViewBo;
import com.unswift.cloud.pojo.dto.logger.timer.record.LoggerTimerRecordCreateDto;
import com.unswift.cloud.pojo.dto.logger.timer.record.LoggerTimerRecordDeleteDto;
import com.unswift.cloud.pojo.dto.logger.timer.record.LoggerTimerRecordPageDto;
import com.unswift.cloud.pojo.dto.logger.timer.record.LoggerTimerRecordUpdateDto;
import com.unswift.cloud.pojo.dto.logger.timer.record.LoggerTimerRecordViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordCreateVo;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordDeleteVo;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordPageVo;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordUpdateVo;
import com.unswift.cloud.pojo.vo.logger.timer.record.LoggerTimerRecordViewVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.service.LoggerTimerRecordService;

@RestController
@RequestMapping("/loggerTimerRecord")
@Api(value="定时器执行记录对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerTimerRecordController extends BaseController{
	
	@Autowired
	private LoggerTimerRecordService loggerTimerRecordService;
	
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="定时器执行记录分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<LoggerTimerRecordPageVo>> findPageList(@RequestBody LoggerTimerRecordPageDto searchDto){
		LoggerTimerRecordPageBo searchBo=this.convertPojo(searchDto, LoggerTimerRecordPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerTimerRecordService.findPageList(searchBo));
	}
	
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="定时器执行记录详情", params=@ApiField("主键"), returns=@ApiField("详情数据"))
	public ResponseBody<LoggerTimerRecordViewVo> view(@RequestBody LoggerTimerRecordViewDto viewDto){
		LoggerTimerRecordViewBo viewBo=this.convertPojo(viewDto, LoggerTimerRecordViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerTimerRecordService.view(viewBo));
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建定时器执行记录", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<LoggerTimerRecordCreateVo> create(@RequestBody LoggerTimerRecordCreateDto createDto){
		LoggerTimerRecordCreateBo createBo=this.convertPojo(createDto, LoggerTimerRecordCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerTimerRecordService.create(createBo));
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新定时器执行记录", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<LoggerTimerRecordUpdateVo> update(@RequestBody LoggerTimerRecordUpdateDto updateDto){
		LoggerTimerRecordUpdateBo updateBo=this.convertPojo(updateDto, LoggerTimerRecordUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerTimerRecordService.update(updateBo));
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除定时器执行记录", params=@ApiField("主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<LoggerTimerRecordDeleteVo> delete(@RequestBody LoggerTimerRecordDeleteDto deleteDto){
		LoggerTimerRecordDeleteBo deleteBo=this.convertPojo(deleteDto, LoggerTimerRecordDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerTimerRecordService.delete(deleteBo));
	}
}
