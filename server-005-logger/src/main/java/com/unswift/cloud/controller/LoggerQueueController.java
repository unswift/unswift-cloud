package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueueCreateBo;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueueDeleteBo;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueuePageBo;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueueUpdateBo;
import com.unswift.cloud.pojo.bo.logger.queue.LoggerQueueViewBo;
import com.unswift.cloud.pojo.dto.logger.queue.LoggerQueueCreateDto;
import com.unswift.cloud.pojo.dto.logger.queue.LoggerQueueDeleteDto;
import com.unswift.cloud.pojo.dto.logger.queue.LoggerQueuePageDto;
import com.unswift.cloud.pojo.dto.logger.queue.LoggerQueueUpdateDto;
import com.unswift.cloud.pojo.dto.logger.queue.LoggerQueueViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueueCreateVo;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueueDeleteVo;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueuePageVo;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueueUpdateVo;
import com.unswift.cloud.pojo.vo.logger.queue.LoggerQueueViewVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.service.LoggerQueueService;

@RestController
@RequestMapping("/loggerQueue")
@Api(value="接口请求日志对外接口", author="liyunlong", date="2024-01-25", version="1.0.0")
public class LoggerQueueController extends BaseController{
	
	@Autowired
	@ApiField("接口请求日志服务")
	private LoggerQueueService loggerQueueService;
	
	@Auth("logger.queue.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="接口请求日志分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<LoggerQueuePageVo>> findPageList(@RequestBody LoggerQueuePageDto searchDto){
		LoggerQueuePageBo searchBo=this.convertPojo(searchDto, LoggerQueuePageBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerQueueService.findPageList(searchBo));
	}
	
	@Auth("logger.queue.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="接口请求日志详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<LoggerQueueViewVo> view(@RequestBody LoggerQueueViewDto viewDto){
		LoggerQueueViewBo viewBo=this.convertPojo(viewDto, LoggerQueueViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerQueueService.view(viewBo));
	}
	
	@Auth("logger.queue.create")
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建接口请求日志", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<LoggerQueueCreateVo> create(@RequestBody LoggerQueueCreateDto createDto){
		LoggerQueueCreateBo createBo=this.convertPojo(createDto, LoggerQueueCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerQueueService.create(createBo));
	}
	
	@Auth("logger.queue.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新接口请求日志", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<LoggerQueueUpdateVo> update(@RequestBody LoggerQueueUpdateDto updateDto){
		LoggerQueueUpdateBo updateBo=this.convertPojo(updateDto, LoggerQueueUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerQueueService.update(updateBo));
	}
	
	@Auth("logger.queue.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除接口请求日志", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<LoggerQueueDeleteVo> delete(@RequestBody LoggerQueueDeleteDto deleteDto){
		LoggerQueueDeleteBo deleteBo=this.convertPojo(deleteDto, LoggerQueueDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerQueueService.delete(deleteBo));
	}
}