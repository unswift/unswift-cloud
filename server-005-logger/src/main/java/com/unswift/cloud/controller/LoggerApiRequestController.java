package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.bo.logger.api.request.LoggerApiRequestPageBo;
import com.unswift.cloud.pojo.dto.logger.api.request.LoggerApiRequestPageDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.logger.api.request.LoggerApiRequestPageVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.service.LoggerApiRequestService;

@RestController
@RequestMapping("/loggerApiRequest")
@Api(value="接口请求日志对外接口", author="liyunlong", date="2023-12-08", version="1.0.0")
public class LoggerApiRequestController extends BaseController{
	
	@Autowired
	private LoggerApiRequestService loggerApiRequestService;
	
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="接口请求日志分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<LoggerApiRequestPageVo>> findPageList(@RequestBody LoggerApiRequestPageDto searchDto){
		LoggerApiRequestPageBo searchBo=this.convertPojo(searchDto, LoggerApiRequestPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerApiRequestService.findPageList(searchBo));
	}
	
}
