package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.pojo.bo.logger.operator.LoggerOperatorPageBo;
import com.unswift.cloud.pojo.dto.logger.operator.LoggerOperatorPageDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.logger.operator.LoggerOperatorPageVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.service.LoggerOperatorService;

@RestController
@RequestMapping("/loggerOperator")
@Api(value="操作日志对外接口", author="liyunlong", date="2023-12-12", version="1.0.0")
public class LoggerOperatorController extends BaseController{
	
	@Autowired
	private LoggerOperatorService loggerOperatorService;
	
	@Active
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="操作日志分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<LoggerOperatorPageVo>> findPageList(@RequestBody LoggerOperatorPageDto searchDto){
		LoggerOperatorPageBo searchBo=this.convertPojo(searchDto, LoggerOperatorPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerOperatorService.findPageList(searchBo));
	}
	
}