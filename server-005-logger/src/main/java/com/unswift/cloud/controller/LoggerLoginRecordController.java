package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordCreateBo;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordDeleteBo;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordPageBo;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordUpdateBo;
import com.unswift.cloud.pojo.bo.logger.login.record.LoggerLoginRecordViewBo;
import com.unswift.cloud.pojo.dto.logger.login.record.LoggerLoginRecordCreateDto;
import com.unswift.cloud.pojo.dto.logger.login.record.LoggerLoginRecordDeleteDto;
import com.unswift.cloud.pojo.dto.logger.login.record.LoggerLoginRecordPageDto;
import com.unswift.cloud.pojo.dto.logger.login.record.LoggerLoginRecordUpdateDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.dto.logger.login.record.LoggerLoginRecordViewDto;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordCreateVo;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordDeleteVo;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordPageVo;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordUpdateVo;
import com.unswift.cloud.pojo.vo.logger.login.record.LoggerLoginRecordViewVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.service.LoggerLoginRecordService;

@RestController
@RequestMapping("/loggerLoginRecord")
@Api(value="登录记录对外接口", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordController extends BaseController{
	
	@Autowired
	@ApiField("登录记录服务")
	private LoggerLoginRecordService loggerLoginRecordService;
	
    @Active
	@Auth("logger.login.record.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="登录记录分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<LoggerLoginRecordPageVo>> findPageList(@RequestBody LoggerLoginRecordPageDto searchDto){
		LoggerLoginRecordPageBo searchBo=this.convertPojo(searchDto, LoggerLoginRecordPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerLoginRecordService.findPageList(searchBo));
	}
	
	@Auth("logger.login.record.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="登录记录详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<LoggerLoginRecordViewVo> view(@RequestBody LoggerLoginRecordViewDto viewDto){
		LoggerLoginRecordViewBo viewBo=this.convertPojo(viewDto, LoggerLoginRecordViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerLoginRecordService.view(viewBo));
	}
	
	@Auth("logger.login.record.create")
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建登录记录", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<LoggerLoginRecordCreateVo> create(@RequestBody LoggerLoginRecordCreateDto createDto){
		LoggerLoginRecordCreateBo createBo=this.convertPojo(createDto, LoggerLoginRecordCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerLoginRecordService.create(createBo));
	}
	
	@Auth("logger.login.record.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新登录记录", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<LoggerLoginRecordUpdateVo> update(@RequestBody LoggerLoginRecordUpdateDto updateDto){
		LoggerLoginRecordUpdateBo updateBo=this.convertPojo(updateDto, LoggerLoginRecordUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerLoginRecordService.update(updateBo));
	}
	
	@Auth("logger.login.record.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除登录记录", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<LoggerLoginRecordDeleteVo> delete(@RequestBody LoggerLoginRecordDeleteDto deleteDto){
		LoggerLoginRecordDeleteBo deleteBo=this.convertPojo(deleteDto, LoggerLoginRecordDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerLoginRecordService.delete(deleteBo));
	}
}
