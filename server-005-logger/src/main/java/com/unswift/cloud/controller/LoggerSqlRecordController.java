package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordCreateBo;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordDeleteBo;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordPageBo;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordUpdateBo;
import com.unswift.cloud.pojo.bo.logger.sql.record.LoggerSqlRecordViewBo;
import com.unswift.cloud.pojo.dto.logger.sql.record.LoggerSqlRecordCreateDto;
import com.unswift.cloud.pojo.dto.logger.sql.record.LoggerSqlRecordDeleteDto;
import com.unswift.cloud.pojo.dto.logger.sql.record.LoggerSqlRecordPageDto;
import com.unswift.cloud.pojo.dto.logger.sql.record.LoggerSqlRecordUpdateDto;
import com.unswift.cloud.pojo.dto.logger.sql.record.LoggerSqlRecordViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordCreateVo;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordDeleteVo;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordPageVo;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordUpdateVo;
import com.unswift.cloud.pojo.vo.logger.sql.record.LoggerSqlRecordViewVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.service.LoggerSqlRecordService;

@RestController
@RequestMapping("/loggerSqlRecord")
@Api(value="sql执行记录对外接口", author="unswift", date="2023-09-05", version="1.0.0")
public class LoggerSqlRecordController extends BaseController{
	
	@Autowired
	private LoggerSqlRecordService loggerSqlRecordService;
	
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="sql执行记录分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<LoggerSqlRecordPageVo>> findPageList(@RequestBody LoggerSqlRecordPageDto searchDto){
		LoggerSqlRecordPageBo searchBo=this.convertPojo(searchDto, LoggerSqlRecordPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerSqlRecordService.findPageList(searchBo));
	}
	
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="sql执行记录详情", params=@ApiField("主键"), returns=@ApiField("详情数据"))
	public ResponseBody<LoggerSqlRecordViewVo> view(@RequestBody LoggerSqlRecordViewDto viewDto){
		LoggerSqlRecordViewBo viewBo=this.convertPojo(viewDto, LoggerSqlRecordViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerSqlRecordService.view(viewBo));
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建sql执行记录", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<LoggerSqlRecordCreateVo> create(@RequestBody LoggerSqlRecordCreateDto createDto){
		LoggerSqlRecordCreateBo createBo=this.convertPojo(createDto, LoggerSqlRecordCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerSqlRecordService.create(createBo));
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新sql执行记录", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<LoggerSqlRecordUpdateVo> update(@RequestBody LoggerSqlRecordUpdateDto updateDto){
		LoggerSqlRecordUpdateBo updateBo=this.convertPojo(updateDto, LoggerSqlRecordUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerSqlRecordService.update(updateBo));
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除sql执行记录", params=@ApiField("主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<LoggerSqlRecordDeleteVo> delete(@RequestBody LoggerSqlRecordDeleteDto deleteDto){
		LoggerSqlRecordDeleteBo deleteBo=this.convertPojo(deleteDto, LoggerSqlRecordDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(loggerSqlRecordService.delete(deleteBo));
	}
}