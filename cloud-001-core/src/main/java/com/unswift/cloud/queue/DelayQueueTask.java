package com.unswift.cloud.queue;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

@ApiEntity(value="队列任务实体", author = "unswift", date = "2023-09-08", version = "1.0.0")
public class DelayQueueTask<T> extends QueueTask<T> implements Delayed{
	
	@ApiField("延迟任务，单位毫秒，开发者值需要设置延迟的毫秒数，在设置时会自动转换为具体的时间毫秒数")
	private Long delay;
	
	public Long getDelay() {
		return delay;
	}

	public void setDelay(Long delay) {
		this.delay = System.currentTimeMillis() + delay;
	}
	
	@Override
	public int compareTo(Delayed o) {
		if(this.getDelay(TimeUnit.NANOSECONDS) < o.getDelay(TimeUnit.NANOSECONDS)) {
			return -1;
		}else if(this.getDelay(TimeUnit.NANOSECONDS) > o.getDelay(TimeUnit.NANOSECONDS)) {
			return 1;
		}
		return 0;
	}
	
	@Override
	public long getDelay(TimeUnit unit) {
		return unit.convert(delay - System.currentTimeMillis(), TimeUnit.NANOSECONDS);
	}
}
