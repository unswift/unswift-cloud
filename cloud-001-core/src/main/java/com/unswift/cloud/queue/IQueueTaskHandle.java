package com.unswift.cloud.queue;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="任务处理接口", author = "unswift", date = "2023-09-08", version = "1.0.0")
public interface IQueueTaskHandle<T> {

	@ApiMethod(value="任务处理方法", params = @ApiField("传递的参数"))
	void handle(T data);
	
}
