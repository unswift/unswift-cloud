package com.unswift.cloud.queue;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import lombok.Data;

@Data
@ApiEntity(value="队列任务实体", author = "unswift", date = "2023-09-08", version = "1.0.0")
public class QueueTask<T>{

	@ApiField("队列任务执行的bean")
	private IQueueTaskHandle<T> taskBean;
	
	@ApiField("任务传递的数据")
	private T taskData;
	
}
