package com.unswift.cloud.json;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.unswift.utils.ObjectUtils;

public class Decimal4Serializer extends JsonSerializer<BigDecimal>{

	@Override
	public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		gen.writeString(ObjectUtils.isNull(value)?null:value.setScale(4, RoundingMode.HALF_UP).toString());
	}

}
