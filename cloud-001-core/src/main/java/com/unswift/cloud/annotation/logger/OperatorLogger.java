package com.unswift.cloud.annotation.logger;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.enums.OperatorTypeEnum;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Api(value="声明操作日志注解，需要记录操作日志时使用", author = "unswift", date = "2023-12-10", version = "1.0.0")
public @interface OperatorLogger {

	@ApiMethod(value="操作日志类型", returns = @ApiField("操作日志类型"))
	OperatorTypeEnum type();
	
	@ApiMethod(value="所属模块，如此处不设置，可通过LoggerUtils.setModule方法设置", returns = @ApiField("所属模块"))
	String module() default "";
	
	@ApiMethod(value="参与比较的字段，如果不设置，则使用实体里面的@Compare注解", returns = @ApiField("字段集合"))
	String[] joinFields() default {};
	
	@ApiMethod(value="主键字段名称，如果此处不设置，则主键字段名称为id", returns = @ApiField("主键字段名称"))
	String pkFileName() default "";
}
