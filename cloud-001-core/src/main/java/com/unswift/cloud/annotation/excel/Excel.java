package com.unswift.cloud.annotation.excel;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Api(value="excel导出注解类", author="unswift", date="2023-07-11", version="1.0.0")
public @interface Excel {

	@ApiMethod(value="导出Excel的标题，如果不配置标题，则使用@ApiEntity注解的value值", returns=@ApiField("标题"))
	String value() default "";
	
	@ApiMethod(value="导出Excel的标题高度", returns=@ApiField("标题高度"))
	int titleHeight() default 20;
	
	@ApiMethod(value="导出Excel的行高", returns=@ApiField("行的高度"))
	int rowHeight() default 16;
	
}
