package com.unswift.cloud.annotation.trim;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.trim.DontTrim;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Api(value="去空处理注解，使用Aop搜索此注解对方法进行拦截并进行去空处理")
public @interface TrimHandle {

	Class<?> value() default DontTrim.class;
	
}
