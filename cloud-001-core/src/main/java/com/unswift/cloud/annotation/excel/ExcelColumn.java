package com.unswift.cloud.annotation.excel;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Documented
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Api(value="excel列注解类", author="unswift", date="2023-07-11", version="1.0.0")
public @interface ExcelColumn {

	@ApiMethod(value="excel列的标题，如果不配置标题，则使用@ApiField注解的value值", returns=@ApiField("标题"))
	String value() default "";
	
	@ApiMethod(value="导出数据的格式，yyyy-MM-dd,#,###.##等(导出使用)", returns=@ApiField("格式"))
	String format() default "";
	
	@ApiMethod(value="excel翻译使用的数据字典类型(导出使用)", returns=@ApiField("数据字典类型"))
	String translateType() default "";
	
	@ApiMethod(value="excel翻译使用的数据字典spring bean 名称，必须实现IExcelTranslate接口(导出使用)", returns=@ApiField("spring bean类类型"))
	String translateBean() default "";
	
	@ApiMethod(value="标题顺序，值越大越靠后(导出使用)", returns=@ApiField("顺序"))
	int order() default -1;
	
	@ApiMethod(value="列宽(导出使用)", returns=@ApiField("宽度"))
	int width() default -1;
	
	@ApiMethod(value="唯一列(导入使用)", returns=@ApiField("是否唯一列{true：是，false：否}"))
	boolean unique() default false;
	
	@ApiMethod(value="必须(导入使用)", returns=@ApiField("是否必须{true：是，false：否}"))
	boolean required() default false;
}
