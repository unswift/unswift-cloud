package com.unswift.cloud.annotation.logger;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Api(value="操作日志字段比较注解类，字段中文名称取@ApiField注解的value", author="unswift", date="2023-12-10", version="1.0.0")
public @interface Compare {
	
	@ApiMethod(value="参与比较字段的描述", returns = @ApiField("字段的描述"))
	String value() default "";
	
	@ApiMethod(value="主键字段名称，如果注解是一个列表，可指定此方法的值，对比时可通过此字段确定数据唯一性后比较")
	String pkFieldName() default "";
	
	@ApiMethod(value="比较字段数据的格式，yyyy-MM-dd,#,###.##等", returns=@ApiField("格式"))
	String format() default "";
	
	@ApiMethod(value="比较字段使用的数据字典类型", returns=@ApiField("数据字典类型"))
	String translateType() default "";
}
