package com.unswift.cloud.annotation.request;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unswift.annotation.api.Api;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Api(value="请求激活，只有激活的请求才能被访问，否则不允许访问，后续可以知道那些请求被使用，那些没被使用", author = "unswift", date = "2024-01-11", version = "1.0.0")
public @interface Active {

}
