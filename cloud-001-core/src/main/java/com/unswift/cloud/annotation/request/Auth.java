package com.unswift.cloud.annotation.request;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Api(value="声明权限key的注解，在前后端通信时中使用", author = "unswift", date = "2023-10-05", version = "1.0.0")
public @interface Auth {

	@ApiMethod(value="对应权限key的值", returns = @ApiField("权限key的值"))
	String[] value();
	
}
