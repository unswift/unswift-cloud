package com.unswift.cloud.annotation.logger;

import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unswift.annotation.api.Api;

@Documented
@Target(METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Api(value="队列消息发送注解类，此注解类用来记录队列消息发送时的日志", author="unswift", date="2023-12-10", version="1.0.0")
public @interface QueueSend {
	
}
