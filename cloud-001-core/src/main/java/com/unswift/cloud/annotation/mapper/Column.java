package com.unswift.cloud.annotation.mapper;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Api(value="声明数据库字段的注解，在SqlDefine中使用", author = "unswift", date = "2023-10-05", version = "1.0.0")
public @interface Column {

	@ApiMethod(value="对应字段的值", returns = @ApiField("字段的值"))
	String value();
	
}
