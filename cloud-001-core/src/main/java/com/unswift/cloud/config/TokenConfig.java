package com.unswift.cloud.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.utils.ObjectUtils;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "unswift.token")
@ApiEntity(value="Token相关yml配置实体", author = "unswift", date = "2023-09-28", version = "1.0.0")
public class TokenConfig {

	@ApiField("token的头，token格式为{header} {token}")
	private String header;
	
	@ApiField("请求Header的名称")
	private String keyName;
	
	@ApiField("token过期时间（单位秒）")
	private String expire;
	
	@ApiField("token过期时间（单位秒）-整型")
	private Integer expireInt;
	
	public Integer getExpireInt() {
		if(ObjectUtils.isEmpty(expireInt) && ObjectUtils.isNotEmpty(expire)) {
			expireInt=ObjectUtils.evalJs(expire, Integer.class);
		}
		return expireInt;
	}
}
