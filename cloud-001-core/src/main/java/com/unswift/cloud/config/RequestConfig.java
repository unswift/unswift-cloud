package com.unswift.cloud.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.utils.ObjectUtils;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "unswift.request")
@ApiEntity(value="请求相关yml配置实体", author = "unswift", date = "2023-08-13", version = "1.0.0")
public class RequestConfig {

	@ApiField("允许get请求的url")
	private List<String> notPostUrlList;
	
	@ApiField("允许get请求的url")
	private List<String> notPostUrlRegexList;

	@ApiField("签名的默认key")
	private String signDefaultKey;
	
	@ApiField("签名超时时间")
	private Long signTimeout;
	
	@ApiField("不需要签名白名单")
	private List<String> signWhiteList;
	
	@ApiField("不需要签名的白名单正则，再初次访问时初始化")
	private List<String> signWhiteRegexList;
	
	@ApiField("不需要登录的白名单")
	private List<String> authWhiteList;
	
	@ApiField("不需要登录的白名单正则，再初次访问时初始化")
	private List<String> authWhiteRegexList;
	
	@ApiField("权限扫描的包")
	private String authScanPackage;
	
	@ApiField("控制器白名单，宇燕规定所有的请求都是POST，且入参必须继承BaseDto，出参必须继承BaseVo")
	private List<String> requestParamsRuleWhiteList;
	
	@ApiField("控制器白名单正则，再初次访问时初始化")
	private List<String> requestParamsRuleWhiteRegexList;
	
	@ApiMethod(value="对控制器白名单正则列表初始化", returns = @ApiField("白名单正则表达式集合"))
	public List<String> initNotPostUrlRegexList(){
		if(ObjectUtils.isNull(notPostUrlRegexList)) {
			synchronized (this) {
				if(ObjectUtils.isNull(notPostUrlRegexList)) {
					notPostUrlRegexList=new ArrayList<String>();
					if(ObjectUtils.isNotEmpty(notPostUrlList)) {
						for (String notPostUrl : notPostUrlList) {
							notPostUrlRegexList.add("^"+notPostUrl.replace("**", ".*").replace("/*", "/[^/]+")+"$");
						}
					}
				}
			}
		}
		return notPostUrlRegexList;
	}
	
	@ApiMethod(value="匹配路径，在签名白名单中匹配路径", params = @ApiField("路径"), returns = @ApiField("是否匹配，true：在白名单，false：不在白名单"))
	public boolean matchNotPostUrl(String path) {
		this.initNotPostUrlRegexList();
		for (String notPostUrl : notPostUrlRegexList) {
			if(path.matches(notPostUrl)) {
				return true;
			}
		}
		return false;
	}
	
	@ApiMethod(value="对签名白名单正则列表初始化", returns = @ApiField("白名单正则表达式集合"))
	public List<String> initSignWhiteRegexList(){
		if(ObjectUtils.isNull(signWhiteRegexList)) {
			synchronized (this) {
				if(ObjectUtils.isNull(signWhiteRegexList)) {
					signWhiteRegexList=new ArrayList<String>();
					if(ObjectUtils.isNotEmpty(signWhiteList)) {
						for (String signWhite : signWhiteList) {
							signWhiteRegexList.add("^"+signWhite.replace("**", ".*").replace("/*", "/[^/]+")+"$");
						}
					}
				}
			}
		}
		return signWhiteRegexList;
	}
	
	@ApiMethod(value="匹配路径，在签名白名单中匹配路径", params = @ApiField("路径"), returns = @ApiField("是否匹配，true：在白名单，false：不在白名单"))
	public boolean matchSignWhite(String path) {
		this.initSignWhiteRegexList();
		for (String signWhite : signWhiteRegexList) {
			if(path.matches(signWhite)) {
				return true;
			}
		}
		return false;
	}
	
	@ApiMethod(value="对权限白名单正则列表初始化", returns = @ApiField("白名单正则表达式集合"))
	public List<String> initAuthWhiteRegexList(){
		if(ObjectUtils.isNull(authWhiteRegexList)) {
			synchronized (this) {
				if(ObjectUtils.isNull(authWhiteRegexList)) {
					authWhiteRegexList=new ArrayList<String>();
					if(ObjectUtils.isNotEmpty(authWhiteList)) {
						for (String authWhite : authWhiteList) {
							authWhiteRegexList.add("^"+authWhite.replace("**", ".*").replace("/*", "/[^/]+")+"$");
						}
					}
				}
			}
		}
		return authWhiteRegexList;
	}
	
	@ApiMethod(value="匹配路径，在权限白名单中匹配路径", params = @ApiField("路径"), returns = @ApiField("是否匹配，true：在白名单，false：不在白名单"))
	public boolean matchAuthWhite(String path) {
		this.initAuthWhiteRegexList();
		for (String authWhite : authWhiteRegexList) {
			if(path.matches(authWhite)) {
				return true;
			}
		}
		return false;
	}
	
	@ApiMethod(value="对控制器白名单正则列表初始化", returns = @ApiField("白名单正则表达式集合"))
	public List<String> initRequestParamsRuleWhiteRegexList(){
		if(ObjectUtils.isNull(requestParamsRuleWhiteRegexList)) {
			synchronized (this) {
				if(ObjectUtils.isNull(requestParamsRuleWhiteRegexList)) {
					requestParamsRuleWhiteRegexList=new ArrayList<String>();
					if(ObjectUtils.isNotEmpty(requestParamsRuleWhiteList)) {
						for (String ruleWhite : requestParamsRuleWhiteList) {
							requestParamsRuleWhiteRegexList.add("^"+ruleWhite.replace("**", ".*").replace("/*", "/[^/]+")+"$");
						}
					}
				}
			}
		}
		return requestParamsRuleWhiteRegexList;
	}
	
	@ApiMethod(value="匹配路径，在控制器白名单中匹配路径", params = @ApiField("路径"), returns = @ApiField("是否匹配，true：在白名单，false：不在白名单"))
	public boolean matchRequestParamsRuleWhite(String path) {
		this.initRequestParamsRuleWhiteRegexList();
		for (String ruleWhite : requestParamsRuleWhiteRegexList) {
			if(path.matches(ruleWhite)) {
				return true;
			}
		}
		return false;
	}
}
