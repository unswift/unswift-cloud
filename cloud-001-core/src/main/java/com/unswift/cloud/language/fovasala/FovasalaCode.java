package com.unswift.cloud.language.fovasala;

import java.util.List;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.utils.ObjectUtils;

import lombok.Data;

@Data
@ApiEntity(value="语言代码实体，语言表达式解析后可得到代码实体列表", author="unswift", date="2023-06-08", version="1.0.0")
public class FovasalaCode {

	@ApiField("符号")
	private FovasalaSymbolEnum symbol;
	
	@ApiField("代码")
	private Object code;
	
	@ApiField("下一行代码值")
	private Object nextCode;
	
	@ApiField("方法参数")
	private Object[] methodArgs;
	
	@ApiField("子代码列表")
	private List<FovasalaCode> childCodeList;
	
	@ApiConstructor(value="代码实体构造", params={@ApiField("符号"), @ApiField("代码")})
	public FovasalaCode(FovasalaSymbolEnum symbol, Object code) {
		super();
		this.symbol = symbol;
		this.code = code;
	}
	
	public String getChildCodes(){
		StringBuilder childCodes=new StringBuilder();
		if(ObjectUtils.isNotEmpty(childCodeList)){
			for (FovasalaCode childCode : childCodeList) {
				if(ObjectUtils.isNotEmpty(childCode.getSymbol())){
					if(FovasalaSymbolEnum.STRING.equals(childCode.getSymbol())){
						childCodes.append(FovasalaSymbolEnum.STRING.getSymbol()).append(childCode.getCode()).append(FovasalaSymbolEnum.STRING.getSymbol());
					}else if(FovasalaSymbolEnum.STRING2.equals(childCode.getSymbol())){
						childCodes.append(FovasalaSymbolEnum.STRING2.getSymbol()).append(childCode.getCode()).append(FovasalaSymbolEnum.STRING2.getSymbol());
					}else if(FovasalaSymbolEnum.BRACKET_LEFT.equals(childCode.getSymbol())){
						childCodes.append(FovasalaSymbolEnum.BRACKET_LEFT.getSymbol()).append(childCode.getChildCodes()).append(FovasalaSymbolEnum.BRACKET_RIGHT.getSymbol());
					}else{
						childCodes.append(childCode.getSymbol().getSymbol());
					}
				}else{
					childCodes.append(childCode.getCode());
				}
			}
		}
		return childCodes.toString();
	}

}
