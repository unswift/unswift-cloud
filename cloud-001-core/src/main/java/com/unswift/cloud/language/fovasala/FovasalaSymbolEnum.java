package com.unswift.cloud.language.fovasala;

import com.unswift.annotation.api.Api;

@Api(value="表单验证标准语言符号枚举,!、=、>、>=、<、<=、!=、+、-、*、/")
public enum FovasalaSymbolEnum {
	ALL("!=><+-*/\"'(),","所有符号"),
	EQUAL("=","等于"),
	BE_NOT("!","非"),
	NOT_EQUAL("!=","不等于"),
	GT(">","大于"),
	GTE(">=","大于等于"),
	LT("<","小于"),
	LTE("<=","小于等于"),
	ADD("+","加"),
	ADD_ONE("++","加加"),
	SUB("-","减"),
	SUB_ONE("--","减"),
	MUL("*","乘"),
	DIV("/","除"),
	STRING("\"","字符串包含"),
	STRING2("'","字符串包含"),
	BRACKET_LEFT("(","左括号"),
	BRACKET_RIGHT(")","右括号"),
	METHOD_MULTI_PARAMETER(",","逗号"),
	METHOD("method","方法"),
	;
	
	private String symbol;
	private String describe;
	private FovasalaSymbolEnum(String symbol, String describe) {
		this.symbol = symbol;
		this.describe = describe;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
}
