package com.unswift.cloud.enums;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

@Api(value="操作类型，增删改查等", author = "unswift", date = "2023-12-10", version = "1.0.0")
public enum OperatorTypeEnum {
	CREATE("create", "新增"),
	UPDATE("update", "修改"),
	DELETE("delete", "删除"),
	SEARCH("search", "查询"),
	EXPORT("export", "导出"),
	IMPORT("import", "导入"),
	;
	
	@ApiField("操作类型key")
	private String key;
	@ApiField("操作类型")
	private String value;
	private OperatorTypeEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
