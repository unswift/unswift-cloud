package com.unswift.cloud.excel;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="excel数据翻译，对原始数据进行翻译后得到可阅读的值并存入excel中", author="unswift", date="2023-07-11", version="1.0.0")
public interface IExcelTranslate {

	@ApiMethod(value="翻译数据", params = {@ApiField("数据字典类型"), @ApiField("数据字典key")})
	String translate(String type, String language, Object key);
	
}
