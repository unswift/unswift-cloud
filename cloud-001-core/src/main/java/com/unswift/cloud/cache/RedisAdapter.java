package com.unswift.cloud.cache;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.redisson.api.RBucket;
import org.redisson.api.RList;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.constant.ApplicationContant;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

import jakarta.annotation.PostConstruct;

@Service
@Api(value="redis缓存操作", author="unswift", date="2023-04-09", version="1.0.0")
public class RedisAdapter {
	
	@ApiField("日志对象")
	protected final Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@PostConstruct
	@ApiMethod(value="内存缓存组件初始化")
	public void cacheInit(){
		this.set(CacheEnum.SYSTEM_LANGUAGE.getKey(), ApplicationContant.DEFAULT_LANGUAGE);
		logger.info("redis缓存初始化");
	}
	
	@Autowired
	@ApiField("redisson客户端对象")
	private RedissonClient redissonClient;

	@ApiMethod(value="设置字符类型redis缓存，不超时", params={@ApiField("缓存的键"), @ApiField("缓存值")})
	public <T> void set(String key, T value){
		RBucket<T> bucket = redissonClient.getBucket(key);
		bucket.set(value);
	}
	
	@ApiMethod(value="设置带有超时时间的字符类型redis缓存", params={@ApiField("缓存的键"), @ApiField("缓存值"), @ApiField("失效时间，单位秒")})
	public <T> void set(String key, T value, int second){
		RBucket<T> bucket = redissonClient.getBucket(key);
		bucket.set(value, second, TimeUnit.SECONDS);
	}
	
	@ApiMethod(value="获取字符类型redis缓存", params={@ApiField("缓存的键")})
	public <T> T get(String key){
		RBucket<T> bucket = redissonClient.getBucket(key);
		return bucket.get();
	}
	
	@ApiMethod(value="key表示的缓存是否存在", params={@ApiField("缓存的键")})
	public <T> boolean exists(String key){
		RBucket<T> bucket = redissonClient.getBucket(key);
		return bucket.isExists();
	}
	
	@ApiMethod(value="删除key表示的缓存", params={@ApiField("缓存的键")})
	public <T> T delete(String key){
		RBucket<T> bucket = redissonClient.getBucket(key);
		T value=bucket.get();
		bucket.delete();
		return value;
	}
	
	@ApiMethod(value="设置key表示缓存的超时时间", params={@ApiField("缓存的键"), @ApiField("失效时间，单位秒")})
	public <T> void expire(String key, int second){
		RBucket<T> bucket = redissonClient.getBucket(key);
		bucket.expire(second, TimeUnit.SECONDS);
	}
	
	@ApiMethod(value="设置hash缓存值", params={@ApiField("缓存的键"), @ApiField("hash值")})
	public <K, V> void setHash(String key, Map<K, V> hash){
		RMap<K, V> bucket = redissonClient.getMap(key);
		bucket.putAll(hash);
	}
	
	@ApiMethod(value="设置带有超时时间的hash缓存值", params={@ApiField("缓存的键"), @ApiField("hash值"), @ApiField("失效时间，单位秒")})
	public <K, V> void setHash(String key, Map<K, V> hash, long second){
		RMap<K, V> bucket = redissonClient.getMap(key);
		bucket.putAll(hash);
		bucket.expire(second, TimeUnit.SECONDS);
	}
	
	@ApiMethod(value="设置hash缓存值", params={@ApiField("缓存的键"), @ApiField("hash键"), @ApiField("hash值")})
	public <K, V> void setHash(String key, K hashKey, V value){
		RMap<K, V> bucket = redissonClient.getMap(key);
		bucket.put(hashKey, value);
	}
	
	@ApiMethod(value="设置带有超时时间的hash缓存值", params={@ApiField("缓存的键"), @ApiField("hash键"), @ApiField("hash值"), @ApiField("失效时间，单位秒")})
	public <K, V> void setHash(String key, K hashKey, V value, long second){
		RMap<K, V> bucket = redissonClient.getMap(key);
		bucket.put(hashKey, value);
		if(bucket.isExists()){
			bucket.expire(second, TimeUnit.SECONDS);
		}
	}
	
	@ApiMethod(value="获取hash key的集合", params={@ApiField("缓存的键")}, returns=@ApiField("hash键集合，Set对象"))
	public <K, V> Set<K> getHashKeys(String key){
		Map<K, V> map=this.get(key);
		return ObjectUtils.isEmpty(map)?null:map.keySet();
	}
	
	@ApiMethod(value="获取hash value的集合", params={@ApiField("缓存的键")}, returns=@ApiField("hash值集合，Collection对象"))
	public <K, V> Collection<V> getHashValues(String key){
		Map<K, V> map=this.get(key);
		return ObjectUtils.isEmpty(map)?null:map.values();
	}
	
	@ApiMethod(value="设置list值", params={@ApiField("缓存的键"), @ApiField("java list")})
	public <V> void setList(String key, List<V> list){
		RList<V> bucket = redissonClient.getList(key);
		bucket.addAll(list);
	}
	
	@ApiMethod(value="设置带有超时时间的list值", params={@ApiField("缓存的键"), @ApiField("java list"), @ApiField("失效时间，单位秒")})
	public <V> void setList(String key, List<V> list, int second){
		RList<V> bucket = redissonClient.getList(key);
		bucket.addAll(list);
		bucket.expire(second, TimeUnit.SECONDS);
	}
	
	@ApiMethod(value="获取列表值", params={@ApiField("缓存的键")}, returns=@ApiField("java列表值"))
	public <V> List<V> getList(String key){
		return get(key);
	}
	
	@ApiMethod(value="获取锁，如果当前prefix和lockKey已经被锁，则等待锁的释放", params={@ApiField("锁的前缀"), @ApiField("锁的key"), @ApiField("锁失效时间，单位秒")}, returns=@ApiField("锁对象"))
	public RLock lockWait(String prefix, String lockKey, long second){
		String key=String.format("%s-%s", prefix, lockKey);
		RLock lock = redissonClient.getLock(key);
		lock.lock(second, TimeUnit.SECONDS);
		return lock;
	}
	
	@ApiMethod(value="获取锁，如果当前prefix和lockKey已经被锁，则抛出异常", params={@ApiField("锁的前缀"), @ApiField("锁的key"), @ApiField("锁失效时间，单位秒"), @ApiField("异常key"), @ApiField("异常参数")}, returns=@ApiField("锁对象"))
	public synchronized RLock lockException(String prefix, String lockKey, long second, String errorKey, Object...args){
		String key=String.format("%s-%s", prefix, lockKey);
		RLock lock = redissonClient.getLock(key);
		boolean tryLock;
		try {
			tryLock = lock.tryLock(second, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.printStackTrace();
			throw ExceptionUtils.exception("redis.lock.exception", e, key, e.getMessage());
		}
		if(!tryLock){
			throw ExceptionUtils.message(errorKey, args);
		}
		return lock;
	}
	
	@ApiMethod(value="释放锁", params=@ApiField("锁对象"))
	public void unlock(RLock lock){
		if(lock.isLocked()){
			lock.unlock();
		}
	}
}
