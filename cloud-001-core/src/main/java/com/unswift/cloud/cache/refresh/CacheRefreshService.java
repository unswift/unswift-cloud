package com.unswift.cloud.cache.refresh;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cache.MemoryCache;
import com.unswift.cloud.cache.CacheEnum;
import com.unswift.cloud.cache.RedisAdapter;

@Service
@Api(value="一般缓存刷新服务，只是清空某各缓存", author="unswift", date="2023-06-02", version="1.0.0")
public class CacheRefreshService implements ICacheRefreshService{

	@ApiField("日志对象")
	protected final Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MemoryCache memoryCache;
	@Autowired
	private RedisAdapter redisAdapter;
	
	@Override
	@ApiMethod(value="刷新缓存实现方法", params=@ApiField("缓存key"))
	public void refresh(CacheEnum cacheEnum) {
		if("memory".equals(cacheEnum.getCacheType())){
			memoryCache.remove(cacheEnum.getKey());
			logger.debug("删除内存缓存："+cacheEnum.getKey());
		}else if("redis".equals(cacheEnum.getCacheType())){
			redisAdapter.delete(cacheEnum.getKey());
			logger.debug("删除Redis缓存："+cacheEnum.getKey());
		}else{
			logger.error("无法识别缓存类型："+cacheEnum.getCacheType());
		}
	}

}
