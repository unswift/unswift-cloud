package com.unswift.cloud.cache;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

@Api(value="缓存枚举", author="unswift", date="2022-12-16", version="1.0.0")
public enum CacheEnum {
	
	SYSTEM_MESSAGE("messageMap", "系统消息", -1L, "com.unswift.cache.refresh.MessageCacheRefreshService", "memory"),
	SYSTEM_LANGUAGE("language", "系统语言", -1L, "", "redis"),
	SYSTEM_TIMER_DATA_LIST("timerDataList", "定时器数据列表，用于跟下一次timer数据进行对比，进而对定时器进行调整", -1L, "", "memory"),
	SYSTEM_TIMER_MAP("timerMap", "定时器map缓存", -1L, "", "memory"),
	FORM_VALIDATE_MAP("formValidateMap", "表单验证map缓存", 24*60*60L, "com.unswift.cache.refresh.CacheRefreshService", "memory"),
	FORM_VALIDATE_CONFIG_MAP("formValidateConfigMap", "表单验证基础配置map缓存", 24*60*60L, "com.unswift.cache.refresh.CacheRefreshService", "memory"),
	EXPORT_STOP("exportStop", "导出停止缓存", -1L, "", "memory"),
	IMPORT_STOP("importStop", "导入停止缓存", -1L, "", "memory"),
	DICTIONARY_MAP("dictionaryMap", "数据字典Map缓存", 24*60*60L, "com.unswift.cache.refresh.CacheRefreshService", "memory"),
	DICTIONARY_TYPE_MAP("dictionaryTypeMap", "数据字典类型Map缓存", 24*60*60L, "com.unswift.cache.refresh.CacheRefreshService", "memory"),
	LANGUAGE_LIST("languageList", "系统语言List缓存", 24*60*60L, "com.unswift.cache.refresh.CacheRefreshService", "memory"),
	SQL_FILE_LOADDING("sqlFileLoadding", "脚本文件加载", -1L, "", "memory"),
	CONFIG_MAP("configMap", "系统配置Map", 24*60*60L, "om.unswift.cache.refresh.CacheRefreshService", "memory"),
	CLOUD_MAP("cloudMap", "系统微服务Map", 24*60*60L, "om.unswift.cache.refresh.CacheRefreshService", "memory"),
	;
	@ApiField("缓存的键")
	private String key;
	@ApiField("键的中文名称及描述")
	private String name;
	@ApiField("超时时间")
	private Long expire;
	@ApiField("刷新bean")
	private String refreshBean;
	@ApiField("缓存类型，memory:内存缓存，redis:redis缓存")
	private String cacheType;
	private CacheEnum(String key, String name, Long expire, String refreshBean, String cacheType) {
		this.key = key;
		this.name = name;
		this.expire = expire;
		this.refreshBean=refreshBean;
		this.cacheType=cacheType;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getExpire() {
		return expire;
	}
	public void setExpire(long expire) {
		this.expire = expire;
	}
	public String getRefreshBean() {
		return refreshBean;
	}
	public void setRefreshBean(String refreshBean) {
		this.refreshBean = refreshBean;
	}
	public void setExpire(Long expire) {
		this.expire = expire;
	}
	public String getCacheType() {
		return cacheType;
	}
	public void setCacheType(String cacheType) {
		this.cacheType = cacheType;
	}
}
