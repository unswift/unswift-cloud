package com.unswift.cloud.cache;

import java.util.List;
import java.util.Map;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.IBaseCho;

@Api(value="业务缓存接口，封装一些缓存的操作", author="unswift", date="2023-04-16", version="1.0.0")
public interface ICacheAdapter {

	@ApiMethod(value="获取表单验证配置缓存数据", returns=@ApiField("配置信息"))
	Map<String, String> findFormValidateConfigMap();
	
	@ApiMethod(value="获取表单验证缓存数据", params={@ApiField("所属模块"), @ApiField("所属操作")}, returns=@ApiField("指定模块下指定操作的表单验证规则"))
	<T> List<T> findFormValidateList(String module, String operator);
	
	@ApiMethod(value="获取数据字典类型名称", params = {@ApiField("数据字典类型编码")})
	String findDictionaryTypeNameByCode(String code);
	
	@ApiMethod(value="获取数据字典值", params = {@ApiField("数据字典类型"), @ApiField("数据字典key")})
	String findDictionaryValueByKey(String type, String language, String key);
	
	@ApiMethod(value="获取数据字典键", params = {@ApiField("数据字典类型"), @ApiField("数据字典value")})
	String findDictionaryKeyByValue(String type, String value);
	
	@ApiMethod(value="查询某个数据字典值是否存在", params = {@ApiField("数据字典类型"), @ApiField("数据字典value")})
	boolean existsDictionaryKeyByValue(String type, String value);
	
	@ApiMethod(value="查询语言缓存列表", params = {@ApiField("激活状态{0：未激活，1：已激活，null：查询所有}")}, returns = @ApiField("语言列表"))
	<T> List<T> findLanguageList(Byte activeStatus);
	
	@ApiMethod(value="根据简写语言查询语言全称", params = {@ApiField("语言简称")}, returns = @ApiField("语言全称"))
	String findLanguageByShortName(String shortName);
	
	@ApiMethod(value="根据系统配置key获取配置项的值", params = {@ApiField("配置项的key")}, returns = @ApiField("配置项的值"))
	String findConfigValueByKey(String key);
	
	@ApiMethod(value="根据系统配置key获取配置项", params = {@ApiField("配置项的key")}, returns = @ApiField("配置项对象"))
	<T extends IBaseCho> T findConfigByKey(String key);
	
	@ApiMethod(value="根据系统微服务code获取微服务对象", params = {@ApiField("微服务code")}, returns = @ApiField("微服务对象"))
	<T extends IBaseCho> T findCloudByCode(String code);
	
	@ApiMethod(value="根据系统微服务code获取微服务对象", params = {@ApiField("微服务code")}, returns = @ApiField("微服务对象"))
	String findCloudNameByCode(String code);
}
