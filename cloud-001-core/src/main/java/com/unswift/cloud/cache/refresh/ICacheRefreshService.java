package com.unswift.cloud.cache.refresh;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.cache.CacheEnum;

@Api(value="缓存刷新服务接口", author="unswift", date="2023-05-05", version="1.0.0")
public interface ICacheRefreshService {

	@ApiMethod(value="刷新执行方法", params=@ApiField("缓存枚举"))
	void refresh(CacheEnum cache);
	
}
