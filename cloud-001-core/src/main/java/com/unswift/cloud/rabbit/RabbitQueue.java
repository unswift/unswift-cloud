package com.unswift.cloud.rabbit;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.logger.QueueSend;
import com.unswift.cloud.pojo.IBaseMso;

@Component
@Api(value="Rabbit Mq 发送消息Bean，根据rabbitTemplate发送消息，使用此bean发送消息可记录发送前和发送后的日志", author = "unswift", date = "2024-01-25", version = "1.0.0")
public class RabbitQueue {

	@Autowired
	@ApiField("Rabbit Mq发送消息模板")
	private RabbitTemplate rabbitTemplate;
	
	@QueueSend
	@ApiMethod(value="Rabbit Mq 发送消息")
	public void sendMessage(String exchangeName, String routeKey, IBaseMso message) {
		rabbitTemplate.convertAndSend(exchangeName, routeKey, message);//发送导出队列
	}
}
