package com.unswift.cloud.rabbit;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate.ConfirmCallback;
import org.springframework.amqp.rabbit.core.RabbitTemplate.ReturnsCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

import jakarta.annotation.PostConstruct;

@Configuration
@Api(value="Rabbit Mq 消息Bean配置", author = "unswift", date = "2024-01-25", version = "1.0.0")
public class RabbitConfig implements ReturnsCallback, ConfirmCallback, RabbitConstant{

	@Autowired
	@ApiField("Rabbit Mq发送消息模板")
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	@ApiField("Rabbit Mq链接工厂")
	private CachingConnectionFactory ronnectionFactory;
	
	@Value("${spring.rabbitmq.mandatory}")
	@ApiField("开启消息失败,返回队列模式")
	private Boolean mandatory;
	
	@PostConstruct
	public void init() {
		rabbitTemplate.setConnectionFactory(ronnectionFactory);
		rabbitTemplate.setMandatory(mandatory); // 开启消息失败,返回队列模式
		rabbitTemplate.setConfirmCallback(this); // 指定 ConfirmCallback
		rabbitTemplate.setReturnsCallback(this); // 指定 ReturnCallback
	}
	
	/*******************************************Rabbit Mq消息配置*************************************/
	@Override
	public void confirm(CorrelationData correlationData, boolean ack, String cause) {
		if (!ack) {
			logger.info("消息发送到Exchange失败, {}, cause: {}", correlationData, cause);
		}
	}
	
	@Override
	public void returnedMessage(ReturnedMessage returned) {
		logger.info("消息从Exchange路由到Queue失败: message: {}", returned);
	}
	/*******************************************Rabbit Mq消息配置*************************************/

	/*******************************************初始化导出队列Bean*************************************/
	@Bean
	public Queue exportQueue() {
		return new Queue(EXPORT_QUEUE_NAME, true);// 款式上下架队列
	}
	@Bean
	public DirectExchange exprotExchange() {
		return new DirectExchange(EXPORT_EXCHANGE_NAME, true, false);
	}
	@Bean
	public Binding bindingExportExchangeMessage() {
		return BindingBuilder.bind(exportQueue()).to(exprotExchange()).with(EXPORT_ROUTE_KEY);
	}
	/*******************************************初始化导出队列Bean*************************************/
	/*******************************************初始化停止导出队列Bean*************************************/
	@Bean
	public Queue stopExportQueue() {
		return new Queue(EXPORT_STOP_QUEUE_NAME, true);// 款式上下架队列
	}
	@Bean
	public DirectExchange stopExprotExchange() {
		return new DirectExchange(EXPORT_STOP_EXCHANGE_NAME, true, false);
	}
	@Bean
	public Binding bindingStopExportExchangeMessage() {
		return BindingBuilder.bind(stopExportQueue()).to(stopExprotExchange()).with(EXPORT_STOP_ROUTE_KEY);
	}
	/*******************************************初始化停止导出队列Bean*************************************/
	/*******************************************初始化导入队列Bean*************************************/
	@Bean
	public Queue importQueue() {
		return new Queue(IMPORT_QUEUE_NAME, true);// 款式上下架队列
	}
	@Bean
	public DirectExchange improtExchange() {
		return new DirectExchange(IMPORT_EXCHANGE_NAME, true, false);
	}
	@Bean
	public Binding bindingImportExchangeMessage() {
		return BindingBuilder.bind(importQueue()).to(improtExchange()).with(IMPORT_ROUTE_KEY);
	}
	/*******************************************初始化导入队列Bean*************************************/
	/*******************************************初始化停止导入队列Bean*************************************/
	@Bean
	public Queue stopImportQueue() {
		return new Queue(IMPORT_STOP_QUEUE_NAME, true);// 款式上下架队列
	}
	@Bean
	public DirectExchange stopImprotExchange() {
		return new DirectExchange(IMPORT_STOP_EXCHANGE_NAME, true, false);
	}
	@Bean
	public Binding bindingStopImportExchangeMessage() {
		return BindingBuilder.bind(stopImportQueue()).to(stopImprotExchange()).with(IMPORT_STOP_ROUTE_KEY);
	}
	/*******************************************初始化停止导入队列Bean*************************************/
	/*******************************************API请求日志队列Bean*************************************/
	@Bean
	public Queue loggerApiRequestQueue() {
		return new Queue(LOGGER_API_REQUEST_QUEUE_NAME, true);// 款式上下架队列
	}
	@Bean
	public DirectExchange loggerApiRequestExchange() {
		return new DirectExchange(LOGGER_API_REQUEST_EXCHANGE_NAME, true, false);
	}
	@Bean
	public Binding bindingLoggerApiRequestExchangeMessage() {
		return BindingBuilder.bind(loggerApiRequestQueue()).to(loggerApiRequestExchange()).with(LOGGER_API_REQUEST_ROUTE_KEY);
	}
	/*******************************************API请求日志队列Bean*************************************/
	/*******************************************操作日志队列Bean*************************************/
	@Bean
	public Queue loggerOperatorQueue() {
		return new Queue(LOGGER_OPERATOR_QUEUE_NAME, true);// 款式上下架队列
	}
	@Bean
	public DirectExchange loggerOperatorExchange() {
		return new DirectExchange(LOGGER_OPERATOR_EXCHANGE_NAME, true, false);
	}
	@Bean
	public Binding bindingLoggerOperatorExchangeMessage() {
		return BindingBuilder.bind(loggerOperatorQueue()).to(loggerOperatorExchange()).with(LOGGER_OPERATOR_ROUTE_KEY);
	}
	/*******************************************操作日志队列Bean*************************************/
}
