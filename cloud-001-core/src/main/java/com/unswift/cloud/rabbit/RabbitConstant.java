package com.unswift.cloud.rabbit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

@Api(value="Rabbit Mq 队列常量，规定队列的名称、交换机、路由等", author="unswift", date="2023-07-14", version="1.0.0")
public interface RabbitConstant {

	@ApiField("日志对象")
	final Logger logger = LoggerFactory.getLogger(RabbitConstant.class);
	
	@ApiField("导出-队列名称")
	final static String EXPORT_QUEUE_NAME="export.queue.name";
	@ApiField("导出-交换机名称")
	final static String EXPORT_EXCHANGE_NAME="export.exchange.name";
	@ApiField("导出-路由")
	final static String EXPORT_ROUTE_KEY="export.route.key";
	
	@ApiField("停止导出-队列名称")
	final static String EXPORT_STOP_QUEUE_NAME="export.stop.queue.name";
	@ApiField("停止导出-交换机名称")
	final static String EXPORT_STOP_EXCHANGE_NAME="export.stop.exchange.name";
	@ApiField("停止导出-路由")
	final static String EXPORT_STOP_ROUTE_KEY="export.stop.route.key";
	
	@ApiField("导入-队列名称")
	final static String IMPORT_QUEUE_NAME="import.queue.name";
	@ApiField("导入-交换机名称")
	final static String IMPORT_EXCHANGE_NAME="import.exchange.name";
	@ApiField("导入-路由")
	final static String IMPORT_ROUTE_KEY="import.route.key";
	
	@ApiField("停止导入-队列名称")
	final static String IMPORT_STOP_QUEUE_NAME="import.stop.queue.name";
	@ApiField("停止导入-交换机名称")
	final static String IMPORT_STOP_EXCHANGE_NAME="import.stop.exchange.name";
	@ApiField("停止导入-路由")
	final static String IMPORT_STOP_ROUTE_KEY="import.stop.route.key";
	
	@ApiField("接口请求日志-队列名称")
	final static String LOGGER_API_REQUEST_QUEUE_NAME="logger.api.request.queue.name";
	@ApiField("接口请求日志-交换机名称")
	final static String LOGGER_API_REQUEST_EXCHANGE_NAME="logger.api.request.exchange.name";
	@ApiField("接口请求日志-路由")
	final static String LOGGER_API_REQUEST_ROUTE_KEY="logger.api.request.route.key";
	
	@ApiField("操作日志-队列名称")
	final static String LOGGER_OPERATOR_QUEUE_NAME="logger.operator.queue.name";
	@ApiField("操作日志-交换机名称")
	final static String LOGGER_OPERATOR_EXCHANGE_NAME="logger.operator.exchange.name";
	@ApiField("操作日志-路由")
	final static String LOGGER_OPERATOR_ROUTE_KEY="logger.operator.route.key";
}
