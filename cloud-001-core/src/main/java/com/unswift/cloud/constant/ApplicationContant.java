package com.unswift.cloud.constant;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.utils.ExceptionUtils;

@Api(value="应用服务器常量类", author="unswift", date="2023-04-16", version="1.0.0")
public interface ApplicationContant {

	@ApiField("默认语言")
	static final String DEFAULT_LANGUAGE="ZH_CN";
	
	static void loadExceptionMessage() {
		ExceptionUtils.loadingMessageXml(Thread.currentThread().getContextClassLoader().getResourceAsStream("cloud-exception.xml"), "cloud-exception.xml");
	}
}
