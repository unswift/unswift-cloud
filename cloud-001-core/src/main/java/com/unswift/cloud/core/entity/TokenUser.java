package com.unswift.cloud.core.entity;

import java.io.Serializable;

import com.unswift.annotation.api.ApiEntity;

@ApiEntity(value="用户Token信息接口", author="unswift", date="2023-06-14", version="1.0.0")
public interface TokenUser extends Serializable{

	Long getId();
	void setId(Long id);
	
	String getToken();
	void setToken(String token);
	
	Byte getIsAdmin();
	void setIsAdmin(Byte isAdmin);
}
