package com.unswift.cloud.pojo;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@ApiEntity(value="队列发送消息的顶层接口，在发送队列时设置业务id", author = "unswift", date = "2024-03-07", version = "1.0.0")
public interface IBaseMro {

	@ApiMethod(value="获取业务id", returns = @ApiField("业务id"))
	Long getBusinessId();
	
	@ApiMethod(value="设置业务id", params = {@ApiField("业务id")})
	void setBusinessId(Long businessId);
	
	@ApiMethod(value="获取用户Token", returns = @ApiField("用户Token"))
	String getToken();
	
	@ApiMethod(value="设置用户Token", params = {@ApiField("用户Token")})
	void setToken(String token);
	
	@ApiMethod(value="获取请求语言", returns = @ApiField("请求语言"))
	String getLanguage();
	
	@ApiMethod(value="设置请求语言", params = {@ApiField("请求语言")})
	void setLanguage(String language);
}
