package com.unswift.cloud.pojo;

import java.io.Serializable;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.utils.LoggerUtils;

@ApiEntity(value="顶层实体类，系统所有实体的基类", author="unswift", date="2023-06-14", version="1.0.0")
public interface Pojo extends Serializable{
	
	@ApiMethod(value="根据@Compare注解输出实体日志内容", returns = @ApiField("日志内容"))
	default String toLoggerString() {
		return LoggerUtils.toLoggerString(this);
	}
	
	@ApiMethod(value="根据@Compare注解输出实体对比日志内容", params = {@ApiField("对比实体"), @ApiField("新数据或原数据")}, returns = @ApiField("日志内容"))
	default String toLoggerString(Pojo pojo, boolean newOrOriginal) {
		return LoggerUtils.toLoggerString(this, pojo, newOrOriginal);
	}
}
