package com.unswift.cloud.pojo.vo.page;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.vo.IBaseVo;

@ApiEntity(value="分页接口", author="unswift", date="2023-07-03", version="1.0.0")
public interface IPageVo<E> extends IBaseVo{

	@ApiMethod(value="获取当前页", returns=@ApiField("当前页"))
	Integer getCurrPage();
	
	@ApiMethod(value="设置当前页", params=@ApiField("当前页"))
	void setCurrPage(Integer currPage);
	
	@ApiMethod(value="获取页大小", returns=@ApiField("页大小"))
	Integer getPageSize();
	
	@ApiMethod(value="设置页大小", params=@ApiField("页大小"))
	void setPageSize(Integer pageSize);
	
	@ApiMethod(value="获取开始记录数", returns=@ApiField("开始记录数"))
	Integer getFirstSize();
	
	@ApiMethod(value="设置开始记录数", params=@ApiField("开始记录数"))
	void setFirstSize(Integer firstSize);
	
	@ApiMethod(value="获取结束记录数，如果是第一页，每页10条，则lastSize=10", returns=@ApiField("结束记录数"))
	Integer getLastSize();
	
	@ApiMethod(value="设置结束记录数，如果是第一页，每页10条，则lastSize=10", params=@ApiField("结束记录数"))
	void setLastSize(Integer lastSize);
	
	@ApiMethod(value="获取总条数", returns=@ApiField("总条数"))
	Integer getTotalSize();
	
	@ApiMethod(value="设置总条数", params=@ApiField("总条数"))
	void setTotalSize(Integer totalSize);
	
	@ApiMethod(value="获取总页数", returns=@ApiField("总页数"))
	Integer getTotalPage();
	
	@ApiMethod(value="设置总页数", params=@ApiField("总页数"))
	void setTotalPage(Integer totalPage);
	
	@ApiMethod(value="获取当前页数据列表", returns=@ApiField("数据列表"))
	List<E> getDataList();
	
	@ApiMethod(value="设置当前页数据列表", params=@ApiField("数据列表"))
	void setDataList(List<E> dataList);
	
}
