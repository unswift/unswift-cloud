package com.unswift.cloud.exception;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.exception.CoreException;

@SuppressWarnings("serial")
@Api(value="宇燕脚本异常类", author="unswift", date="2023-04-08", version="1.0.0")
public class ScriptException extends CoreException {
	
	@ApiConstructor(value="脚本异常类构造", params={@ApiField("异常编码"), @ApiField("异常消息")})
	public ScriptException(String code, String message) {
		super(code, message);
	}

	@ApiConstructor(value="脚本异常类构造", params={@ApiField("异常编码"), @ApiField("异常消息"), @ApiField("异常")})
	public ScriptException(String code, String message, Throwable e) {
		super(code, message, e);
	}

}
