package com.unswift.cloud.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "unswift.api")
@Api(value="api配置实体", author = "unswift", date = "2023-09-01", version = "1.0.0")
public class ApiConfig {

	@ApiField("接口API是否开启，true：开启，false：关闭，开启状态才可以访问")
	private Boolean interfaceOpen;
	
	@ApiField("类API是否开启，true：开启，false：关闭，开启状态才可以访问")
	private Boolean classOpen;
	
	@ApiField("api需要扫描的包")
	private List<String> scanPackage;
	
}
