package com.unswift.cloud.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.api.ApiHandle;

@Configuration
@Api(value="Api手动加载Bean", author="unswift", date="2023-09-05", version="1.0.0")
public class ApiAutoBean {

	@Autowired
	@ApiField("api配置")
	private ApiConfig apiConfig;
	
	/********************************Api处理Bean*******************************/
	@Bean
	@ApiMethod(value="创建Api处理Bean", returns=@ApiField("Api处理Bean"))
	public ApiHandle getApiHandle(){
		return new ApiHandle(apiConfig.getScanPackage());
	}
	/********************************Api处理Bean*******************************/

}
