package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.bo.system.api.interfaces.SystemApiInterfacesDeleteBo;
import com.unswift.cloud.pojo.bo.system.api.interfaces.SystemApiInterfacesTreeBo;
import com.unswift.cloud.pojo.bo.system.api.interfaces.record.SystemApiInterfacesRecordCreateBo;
import com.unswift.cloud.pojo.dto.system.api.interfaces.SystemApiInterfacesDeleteDto;
import com.unswift.cloud.pojo.dto.system.api.interfaces.SystemApiInterfacesTreeDto;
import com.unswift.cloud.pojo.dto.system.api.interfaces.record.SystemApiInterfacesRecordCreateDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.api.interfaces.SystemApiInterfacesDeleteVo;
import com.unswift.cloud.pojo.vo.system.api.interfaces.SystemApiInterfacesProgressVo;
import com.unswift.cloud.pojo.vo.system.api.interfaces.SystemApiInterfacesSynchVo;
import com.unswift.cloud.pojo.vo.system.api.interfaces.SystemApiInterfacesTreeVo;
import com.unswift.cloud.pojo.vo.system.api.interfaces.record.SystemApiInterfacesRecordCreateVo;
import com.unswift.cloud.service.SystemApiInterfacesService;

@RestController
@RequestMapping("/systemApiInterfaces")
@Api(value="接口api对外接口", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiInterfacesController extends BaseController{
	
	@Autowired
	private SystemApiInterfacesService systemApiInterfacesService;
	
	@RequestMapping(value="/findTree", method=RequestMethod.POST)
	@ApiMethod(value="获取接口api树结构", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的树数据"))
	public ResponseBody<SystemApiInterfacesTreeVo> findTree(@RequestBody SystemApiInterfacesTreeDto searchDto){
		SystemApiInterfacesTreeBo searchBo=this.convertPojo(searchDto, SystemApiInterfacesTreeBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemApiInterfacesService.findTree(searchBo));
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除接口api", params=@ApiField("主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemApiInterfacesDeleteVo> delete(@RequestBody SystemApiInterfacesDeleteDto deleteDto){
		SystemApiInterfacesDeleteBo deleteBo=this.convertPojo(deleteDto, SystemApiInterfacesDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemApiInterfacesService.delete(deleteBo));
	}
	
	@RequestMapping(value="/synch", method=RequestMethod.POST)
	@ApiMethod(value="类api同步", returns=@ApiField("同步结果Vo->result:{0：未同步，1：已同步}"))
	public ResponseBody<SystemApiInterfacesSynchVo> synch(){
		return ResponseBody.success(systemApiInterfacesService.synch());
	}
	
	@ApiMethod(value="获取同步进度", returns=@ApiField("进度条Vo"))
	@RequestMapping(value="/getSynchProgress", method=RequestMethod.POST)
	public ResponseBody<SystemApiInterfacesProgressVo> getSynchProgress(){
		return ResponseBody.success(systemApiInterfacesService.getSynchProgress());
	}
	
	@ApiMethod(value="保存执行记录", params = @ApiField("执行记录Dto"), returns=@ApiField("保存结果Vo"))
	@RequestMapping(value="/createRunningRecord", method=RequestMethod.POST)
	public ResponseBody<SystemApiInterfacesRecordCreateVo> createRunningRecord(@RequestBody SystemApiInterfacesRecordCreateDto createDto){
		SystemApiInterfacesRecordCreateBo createBo=this.convertPojo(createDto, SystemApiInterfacesRecordCreateBo.class);
		return ResponseBody.success(systemApiInterfacesService.createRunningRecord(createBo));
	}
}