package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockException;
import com.unswift.cloud.pojo.bo.system.api.classes.SystemApiClassPageBo;
import com.unswift.cloud.pojo.bo.system.api.classes.SystemApiClassViewBo;
import com.unswift.cloud.pojo.dto.system.api.classes.SystemApiClassPageDto;
import com.unswift.cloud.pojo.dto.system.api.classes.SystemApiClassViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.IPageVo;
import com.unswift.cloud.pojo.vo.system.api.classes.SystemApiClassPageVo;
import com.unswift.cloud.pojo.vo.system.api.classes.SystemApiClassProgressVo;
import com.unswift.cloud.pojo.vo.system.api.classes.SystemApiClassSynchVo;
import com.unswift.cloud.pojo.vo.system.api.classes.SystemApiClassViewVo;
import com.unswift.cloud.service.SystemApiClassService;

@RestController
@RequestMapping("/systemApiClass")
@Api(value="类api对外接口", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiClassController extends BaseController{
	
	@Autowired
	private SystemApiClassService systemApiClassService;
	
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="类api分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<IPageVo<SystemApiClassPageVo>> findPageList(@RequestBody SystemApiClassPageDto searchDto){
		SystemApiClassPageBo searchBo=this.convertPojo(searchDto, SystemApiClassPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemApiClassService.findPageList(searchBo));
	}
	
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="类api详情", params=@ApiField("主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemApiClassViewVo> view(@RequestBody SystemApiClassViewDto viewDto){
		SystemApiClassViewBo viewBo=this.convertPojo(viewDto, SystemApiClassViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemApiClassService.view(viewBo));
	}
	
	@LockException(prefix = "systemApiClassSynch", unique = "all", messageCode="task.is.currently.executing", messageArgs = "{@keyword.synchronous}") //同时只能一个人执行此方法，如果此接口正在被执行，则抛出异常
	@RequestMapping(value="/synch", method=RequestMethod.POST)
	@ApiMethod(value="类api同步", returns=@ApiField("同步结果Vo->result:{0：未同步，1：已同步}"))
	public ResponseBody<SystemApiClassSynchVo> synch(){
		return ResponseBody.success(systemApiClassService.synch());
	}
	
	@ApiMethod(value="获取同步进度", returns=@ApiField("进度条Vo"))
	@RequestMapping(value="/getSynchProgress", method=RequestMethod.POST)
	public ResponseBody<SystemApiClassProgressVo> getSynchProgress(){
		return ResponseBody.success(systemApiClassService.getSynchProgress());
	}
}
