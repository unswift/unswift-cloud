package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.config.ApiConfig;
import com.unswift.cloud.core.CommonOperator;
import com.unswift.cloud.pojo.bo.system.api.interfaces.SystemApiInterfacesViewBo;
import com.unswift.cloud.service.SystemApiInterfacesService;
import com.unswift.utils.ObjectUtils;

@Controller
@RequestMapping("/apis")
@Api(value="API文档接口", author = "unswift", date = "2023-09-04", version = "1.0.0", requestApi = false)
public class ApisController extends CommonOperator{

	@Value("${unswift.server-route}")
	@ApiField("当前服务路由在header里面的key")
	private String serverRoute;
	
	@Autowired
	@ApiField("api文档配置")
	private ApiConfig apiConfig;
	
	@Autowired
	@ApiField("接口文档服务")
	private SystemApiInterfacesService systemApiInterfacesService;
	
	@GetMapping({"","/"})
	@ApiMethod(value="API文档缺省路径请求", describe="进入API文档管理界面", returns = @ApiField("Spring Mvc 对象"))
	private ModelAndView home() {
		return index();
	}
	
	@GetMapping("/index")
	@ApiMethod(value="API文档的首页请求", describe="进入API文档管理界面", returns = @ApiField("Spring Mvc 对象"))
	private ModelAndView index() {
		ModelAndView model = new ModelAndView("/apis/index");
		model.addObject("serverRoute", ObjectUtils.init(getHeader(serverRoute), ""));
		if(!apiConfig.getInterfaceOpen() && !apiConfig.getClassOpen()) {
			model.setViewName("/apis/index-no");
		}else {
			model.addObject("requestApiOpen", apiConfig.getInterfaceOpen());
			model.addObject("classApiOpen", apiConfig.getClassOpen());
		}
		return model;
	}
	
	@GetMapping("/interface-view")
	@ApiMethod(value="API接口文档详情", describe="接在API文档详情界面", params = @ApiField("接口主键"), returns = @ApiField("Spring Mvc 对象"))
	private ModelAndView interfaceView(@RequestParam Long id) {
		ModelAndView model = new ModelAndView("/apis/interface-view");
		model.addObject("serverRoute", ObjectUtils.init(getHeader(serverRoute), ""));
		model.addObject("entity", systemApiInterfacesService.view(new SystemApiInterfacesViewBo(id)));
		return model;
	}
	
}
