(function(){
	$.textarea={
		 fullScreen: function(textarea, title){
			if(window.getSelection() && window.getSelection().toString().length>1){
				return ;
			}
			textarea=$(textarea);
			var value=textarea.val()||'{\n\t\n}';
			var readonly=textarea.attr('readonly');
			$.layer.open({
				type: 1,
				title: title,
				area: ['100%', '100%'], // 宽高
				content: '<textarea id="full-textarea" class="layui-textarea" style="height:100%;"'+(readonly?' readonly':' onkeydown="return $.textarea.inputFormat(this, event, \'keydown\');" onkeyup="$.textarea.inputFormat(this, event, \'keyup\')"')+'>'+value+'</textarea>',
				btn: ["确定", "取消"],
				btn1: function(layerId){
					textarea.val($("#full-textarea").val());
					$.layer.close(layerId);
				}
			}); 
		 },
		 inputFormat: function(textarea, event, type){
			 event=event||window.event;
			var keyCode=event.keyCode;
			//console.log(event);
			if(keyCode==9){//\t
				if(type=='keydown'){
					if (event.preventDefault) {
						event.preventDefault();
					}else{
						event.returnValue = false;
					}
					textareaInsert(textarea, '\t');
				}
				return false;
			}else if(keyCode==13){//\n
				if(type=='keydown'){
					var addText=getAddEnter(textarea);
					textareaInsert(textarea, addText);
				}
				return false;
			}else if(event.ctrlKey && keyCode==68){//ctrl+d  删除一行
				if(type=='keyup'){
					deleteRow(textarea);
				}
				return false;
			}else{
				return true;
			}
		 }
	};
	function textareaInsert(textarea, insert){
		var start=textarea.selectionStart;
		var end=textarea.selectionEnd;
		var value=textarea.value;
		var length=value.length;
		var newValue;
		if(start>length-1){
			newValue=value+insert;
		}else if(end>length-1){
			newValue=value.substring(0, start)+insert;
		}else{
			newValue=value.substring(0, start);
			newValue+=insert;
			newValue+=value.substring(end);
		}
		textarea.value=newValue;
		start+=insert.length;
		textarea.selectionStart=start;
		textarea.selectionEnd=start;
	}
	function getAddEnter(textarea){
		var start=textarea.selectionStart;
		//var end=textarea.selectionEnd;
		var value=textarea.value;
		var length=value.length;
		var newValue;
		if(start>length-1){
			newValue=value;
		}else{
			newValue=value.substring(0, start);
		}
		var beforeIndex=newValue.lastIndexOf('\n');
		if(beforeIndex!=-1){
			newValue=newValue.substring(beforeIndex+1);
		}
		var length=newValue.length;
		var addText="\n";
		if(newValue){
			var charItem;
			for(var i=0; i<length; i++){
				charItem=newValue.charAt(i);
				if(charItem=='\t' || charItem==' '){
					addText+=charItem;
				}else{
					break;
				}
			}
			if(new RegExp("\"[^\"]*\" *[:] *[\"]?[^\"]*[\"]?", 'g').test(newValue)){
				addText+="\"\":\"\"";
				if(newValue.charAt(length-1)!=','){
					addText=','+addText;
				}
			}
		}
		return addText;
	}
	function deleteRow(textarea){
		var start=textarea.selectionStart;
		var end=textarea.selectionEnd;
		var value=textarea.value;
		var length=value.length;
		if(start>length-1){
			var lastIndex=value.lastIndexOf('\n');
			if(lastIndex==-1){
				textarea.value="";
			}else{
				textarea.value=value.substring(0, lastIndex);
			}
		}else if(end > length-1){
			value=value.substring(0, start);
			var lastIndex=value.lastIndexOf('\n');
			if(lastIndex==-1){
				textarea.value="";
			}else{
				textarea.value=value.substring(0, lastIndex);
			}
		}else{
			var endValue=value.substring(end);
			value=value.substring(0, start);
			var lastIndex=value.lastIndexOf('\n');
			if(lastIndex==-1){
				value="";
			}else{
				value=value.substring(0, lastIndex);
			}
			var index=endValue.indexOf('\n');
			if(index==-1){
				value+="";
			}else{
				value+=endValue.substring(index);
			}
			textarea.value=value;
		}
	}
})();