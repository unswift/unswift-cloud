<meta charset="UTF-8">
<#if entity??>
<div class="api-view-body">
	<div class="layui-row">
		<div class="layui-col-xs12">
			<span class="parent-name">${entity.parentName} /</span>${entity.name}
		</div>
	</div>
	<div class="layui-row">
		<div class="layui-col-xs12">
			<h2 class="view-title">${entity.name}</h2>
		</div>
	</div>
	<#if entity.describe??>
	<div class="layui-row">
		<div class="layui-col-xs12">
			<h2 class="view-title">${entity.describe!''}</h2>
		</div>
	</div>
	</#if>
	<div class="layui-tab">
		<ul class="layui-tab-title">
			<li class="layui-this" lay-id="11"><i class="layui-icon layui-icon-form font16"></i> 文档</li>
			<li lay-id="22"><i class="layui-icon layui-icon-triangle-r font16"></i> 执行</li>
		</ul>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show">
				<div class="layui-form">
					<div class="layui-form-item">
						<div class="layui-input-group">
							<div class="layui-input-split layui-input-prefix layui-input-prefix-80">
								请求地址
							</div>
							<input type="text" placeholder="请求地址" class="layui-input" disabled value="${serverRoute}${entity.path}">
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-input-group">
							<div class="layui-input-split layui-input-prefix layui-input-prefix-80">
								请求方式
							</div>
							<input type="text" placeholder="请求方式" class="layui-input" disabled value="${entity.method}">
						</div>
					</div>
					<h3 class="param-title">PATH参数：</h3>
					<#if entity.pathParamList?? && entity.pathParamList?size &gt; 0>
					<table class="layui-table">
						<colgroup>
							<col width="20%">
							<col width="20%">
							<col width="20%">
							<col width="20%">
							<col>
						</colgroup>
						<thead>
							<tr>
								<th>属性名称</th>
								<th>属性说明</th>
								<th>属性类型</th>
								<th>属性规则</th>
								<th>必传</th>
							</tr>
						</thead>
						<tbody>
							<#list entity.pathParamList as pathParam>
							<tr>
								<td>${pathParam.paramKey}</td>
								<td>${pathParam.paramComment}</td>
								<td>${pathParam.paramType}</td>
								<td>${pathParam.rule!'无'}</td>
								<td>${pathParam.required?string('是','否')}</td>
							</tr>
							</#list>
						</tbody>
					</table>
					<#else>
					<div class="layui-row">
						<div class="layui-col-xs12">
							无
						</div>
					</div>
					</#if>
					<h3 class="param-title">URL参数：</h3>
					<#if entity.urlParamList?? && entity.urlParamList?size &gt; 0>
					<table class="layui-table">
						<colgroup>
							<col width="20%">
							<col width="20%">
							<col width="20%">
							<col width="20%">
							<col>
						</colgroup>
						<thead>
							<tr>
								<th>属性名称</th>
								<th>属性说明</th>
								<th>属性类型</th>
								<th>属性规则</th>
								<th>必传</th>
							</tr>
						</thead>
						<tbody>
							<#list entity.urlParamList as urlParam>
							<tr>
								<td>${urlParam.paramKey}</td>
								<td>${urlParam.paramComment}</td>
								<td>${urlParam.paramType}</td>
								<td>${urlParam.rule!'无'}</td>
								<td>${urlParam.required?string('是','否')}</td>
							</tr>
							</#list>
						</tbody>
					</table>
					<#else>
					<div class="layui-row">
						<div class="layui-col-xs12">
							无
						</div>
					</div>
					</#if>
					<h3 class="param-title">FORM表单参数：</h3>
					<#if entity.formParamList?? && entity.formParamList?size &gt; 0>
					<table class="layui-table">
						<colgroup>
							<col width="20%">
							<col width="20%">
							<col width="20%">
							<col width="20%">
							<col>
						</colgroup>
						<thead>
							<tr>
								<th>属性名称</th>
								<th>属性说明</th>
								<th>属性类型</th>
								<th>属性规则</th>
								<th>必传</th>
							</tr>
						</thead>
						<tbody>
							<#list entity.formParamList as formParam>
							<tr>
								<td>${formParam.paramKey}</td>
								<td>${formParam.paramComment}</td>
								<td>${formParam.paramType}</td>
								<td>${formParam.rule!'无'}</td>
								<td>${formParam.required?string('是','否')}</td>
							</tr>
							</#list>
						</tbody>
					</table>
					<#else>
					<div class="layui-row">
						<div class="layui-col-xs12">
							无
						</div>
					</div>
					</#if>
					<h3 class="param-title">BODY参数：</h3>
					<#if entity.bodyParamJson??>
					<table class="layui-hide" id="interface-view-body-param-table"></table>
					<#else>
					<div class="layui-row">
						<div class="layui-col-xs12">
							无
						</div>
					</div>
					</#if>
					<h3 class="param-title">返回：</h3>
					<#if entity.returnParamJson??>
					<table class="layui-hide" id="interface-view-return-param-table"></table>
					<#else>
					<div class="layui-row">
						<div class="layui-col-xs12">
							无
						</div>
					</div>
					</#if>
				</div>
			</div>
			<div class="layui-tab-item">
				<form id="interface-form" class="layui-form" action="" lay-filter="interface-form">
					<input type="hidden" name="id" value="${entity.id}"/>
					<div class="layui-form-item">
						<label class="layui-form-label">请求地址</label>
    					<div class="layui-input-block">
							<input type="text" name="url" placeholder="请求地址" class="layui-input" value="${serverRoute}${entity.testPath!''}">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">请求方式</label>
						<div class="layui-input-block">
							<input type="text" name="method" placeholder="请求方式" class="layui-input" value="${entity.method}">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">Headers</label>
						<div class="layui-input-block">
							<textarea name="headers" placeholder="请输入请求头" class="layui-textarea" style="height:150px;" 
							ondblclick="$.textarea.fullScreen(this, 'Headers');" 
							onkeydown="return $.textarea.inputFormat(this, event, 'keydown');" 
							onkeyup="$.textarea.inputFormat(this, event, 'keyup')">${entity.headers!''}</textarea>
						</div>
					</div>
					<#if entity.formParamList?? && entity.formParamList?size &gt; 0>
						<h3 class="param-title">FORM表单参数：</h3>
						<#list entity.formParamList as formParam>
							<div class="layui-form-item">
								<label class="layui-form-label">${formParam.paramKey!''}</label>
								<div class="layui-input-block">
									<div class="layui-col-md10">
										<input type="text" placeholder="参数值" class="layui-input" value="${formParam.testExample!''}">
									</div>
									<div class="layui-col-md2">
										<select name="paremType-${formParam.paramKey!''}">
											<option value="text"<#if formParam.paramType=='varchar' || formParam.paramType=='char'> selected</#if>>文本</option>
											<option value="number"<#if formParam.paramType=='int' || formParam.paramType=='double' || formParam.paramType=='number'> selected</#if>>数字</option>
											<option value="number"<#if formParam.paramType=='datetime'> selected</#if>>日期</option>
											<option value="number"<#if formParam.paramType=='boolean'> selected</#if>>布尔</option>
											<option value="file"<#if formParam.paramType=='file'> selected</#if>>文件</option>
										</select>
									</div>
									<div class="layui-col-md10">
										<input type="text" name="paramValue-${formParam.paramKey!''}" placeholder="参数值" class="layui-input" value="${formParam.testExample!''}">
									</div>
								</div>
							</div>
						</#list>
					</#if>
					<#if entity.testBodyParamJson??>
					<div class="layui-form-item">
						<label class="layui-form-label">Body</label>
						<div class="layui-input-block">
							<textarea name="body" placeholder="请输入Body" class="layui-textarea" style="height:150px;" 
							ondblclick="$.textarea.fullScreen(this, 'Body');" 
							onkeydown="return $.textarea.inputFormat(this, event, 'keydown');" 
							onkeyup="$.textarea.inputFormat(this, event, 'keyup')">${entity.testBodyParamJson!''}</textarea>
						</div>
					</div>
					</#if>
					<div class="layui-form-item">
						<div class="layui-input-block">
							<button type="button" class="layui-btn layui-bg-blue" onclick="sendRequest(this)">
								<i class="layui-icon layui-icon-triangle-r font16" style="color:#FFF;"></i>
								执行
							</button>
							
							<button type="button" class="layui-btn layui-bg-blue" onclick="deleteInterface(${entity.id})">
								<i class="layui-icon layui-icon-error font16" style="color:#FFF;"></i>
								删除
							</button>
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">Response</label>
						<div class="layui-input-block">
							<span class="executeTime">
								<span class="layui-badge-dot"></span>
								<span id="executeTimeText"></span>
							</span>
							<textarea id="interface-response" name="interface-response" placeholder="请求回调" class="layui-textarea" style="height:150px;" readonly  
							ondblclick="$.textarea.fullScreen(this, 'Response');">${entity.testReturnParamJson!''}</textarea>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		<#if entity.bodyParamJson??>
		$.treeTable.render({
			id:'interface-view-body-param-table',
			elem:'#interface-view-body-param-table',
			data:${entity.bodyParamJson!'{}'},
			tree:{
				customName:{
					name:"paramKey"
				},
				view:{
					showIcon:false,
					indent:20
				}
			},
			cols: [[
				{field: 'paramKey', title: '属性名称', width: '20%', fixed: 'left'},
				{field: 'paramComment', title: '属性说明', width: '20%'},
				{field: 'paramType', title: '属性类型', width: '20%'},
				{field: 'rule', title: '属性规则', width: '20%'},
				{field: 'required', title: '必传', width: '20%'}
			]]
		});
		$.treeTable.expandAll('interface-view-body-param-table', true);
		</#if>
		
		<#if entity.returnParamJson??>
		$.treeTable.render({
			id:'interface-view-return-param-table',
			elem:'#interface-view-return-param-table',
			data:${entity.returnParamJson!'{}'},
			tree:{
				customName:{
					name:"paramKey"
				},
				view:{
					showIcon:false,
					indent:20
				}
			},
			cols: [[
				{field: 'paramKey', title: '属性名称', width: '25%', fixed: 'left'},
				{field: 'paramComment', title: '属性说明', width: '25%'},
				{field: 'paramType', title: '属性类型', width: '25%'},
				{field: 'rule', title: '属性规则', width: '25%'},
			]]
		});
		$.treeTable.expandAll('interface-view-return-param-table', true);
		</#if>
		$.form.render('select','interface-form');
		function sendRequest(){
			var body=$.formToData('interface-form');
			var headers=window.eval('('+(body.headers||'{}')+')');
			headers.originalData=true;
			var start=new Date().getTime();
			var requestBody={
				url: body.url,
				type: body.method,
				headers: headers,
				dataType: 'json',
				success:function(res){
					var resJson=$.toJson(res,'\t');
					var time=new Date().getTime()-start;
					$("#executeTimeText").html($.dateFormat('yyyy-MM-dd HH:mm:ss')+" 耗时："+$.timeConvert(time));
					$("#interface-response").val(resJson);
					if(res.code && res.code=='0'){
						var recordData={
							apiInterfaceId:body.id,
							path: body.url,
							method: body.method,
							response: resJson,
							headers: body.headers||'{}'
						};
						console.log(body);
						if(body.body){
							recordData.bodyParamMap=body.body;
						}
						$.post(serverRoute+"/systemApiInterfaces/createRunningRecord", recordData, function(res){
							
						}, 'json', false);
					}
				}
			};
			if(body.body){
				requestBody.data=body.body;
			}
			$.ajax(requestBody);
		}
		function deleteInterface(id){
			$.post(serverRoute+'/systemApiInterfaces/delete', {id: id}, function(data){
				getInterfacesView();
			}, 'json');
		}
	</script>
</div>
<#else>
<div class="api-view-body">
	
</div>
</#if>
