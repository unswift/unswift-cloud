<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link rel="icon" href="${serverRoute}/apis/images/logo.png">
		<link rel="stylesheet" href="${serverRoute}/apis/js/framework/layui/css/layui.css">
		<link rel="stylesheet" href="${serverRoute}/apis/css/index.css">
		<script type="text/javascript" src="${serverRoute}/apis/js/framework/layui/layui.js"></script>
		<script type="text/javascript" src="${serverRoute}/apis/js/framework/jquery.min.js"></script>
		<script type="text/javascript" src="${serverRoute}/apis/js/framework/md5.js"></script>
		<script type="text/javascript" src="${serverRoute}/apis/js/framework/model.js"></script>
		<script type="text/javascript" src="${serverRoute}/apis/js/framework/core.js"></script>
		<script type="text/javascript" src="${serverRoute}/apis/js/framework/textarea.js"></script>
		<title>API</title>
	</head>
	</head>
	<body>
		<div class="layui-tab">
			<ul class="layui-tab-title">
				<li class="layui-this" lay-id="11"><i class="layui-icon layui-icon-website font16"></i> 请求API</li>
				<li lay-id="22"><i class="layui-icon layui-icon-about font16"></i> 类API</li>
			</ul>
			<div class="layui-tab-content">
				<div class="layui-tab-item layui-show">
					<div class="layui-layout layui-layout-admin">
						<div class="layui-side">
							<div class="layui-input-wrap">
								<div class="layui-input-prefix">
						          <i class="layui-icon layui-icon-search"></i>
						        </div>
								<input class="layui-input" placeholder="接口名称">
							</div>
							<div id="interfaces-tree" class="api-tree">
								
							</div>
						</div>
						<div class="layui-body">
							<div class="layui-input-wrap margin-left-10">
								<button type="button" class="layui-btn" onclick="synchInterfaces();">
									<i class="layui-icon layui-icon-refresh"></i> 同步
								</button>
								<div class="progress-content margin-left-10">
									<div class="layui-progress" lay-filter="interfaces-synch-progress">
										<div class="layui-progress-bar" lay-percent="0"></div>
									</div>
								</div>
							</div>
							<div id="interfaces-view" class="api-view"></div>
						</div>
					</div>
				</div>
				<div class="layui-tab-item">
					<div class="layui-layout layui-layout-admin">
						<div class="layui-side">
							<div class="layui-input-wrap">
								<div class="layui-input-prefix">
						          <i class="layui-icon layui-icon-search"></i>
						        </div>
								<input class="layui-input" placeholder="接口名称">
							</div>
							<div id="class-tree" class="api-tree"></div>
						</div>
						<div class="layui-body">
							<div class="layui-input-wrap margin-left-10">
								<button type="button" class="layui-btn" onclick="synchClass();">
									<i class="layui-icon layui-icon-refresh"></i> 同步
								</button>
								<div class="progress-content margin-left-10">
									<div class="layui-progress" lay-filter="class-synch-progress">
										<div class="layui-progress-bar" lay-percent="0"></div>
									</div>
								</div>
							</div>
							<div id="class-view" class="api-view"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	
	<script type="text/javascript">
		if(!$.cookie('api.user.id')){
			$.cookie('api.user.id', $.uuid());
		}
		var serverRoute='${serverRoute}';
		//layui加载模块
		layui.use(['tree', 'element', 'layer', 'treeTable','form'], function(){
			$.tree=layui.tree;
			$.element=layui.element;
			$.layer=layui.layer;
			$.treeTable=layui.treeTable;
			$.form=layui.form;
			init();
		});
		//页面初始化方法
		function init(){
			getInterfacesTree();
			getInterfacesProgress();
			getClassProgress();
		}
		//获取接口API数
		function getInterfacesTree(keyword){
			var paramObj=$.parseHrefParam();
			var selectNodeId=undefined;
			if(paramObj && paramObj.page=='interfaces'){
				selectNodeId=paramObj.id;
			}
			$.post(serverRoute+"/systemApiInterfaces/findTree",{keyword:keyword, selectNodeId: selectNodeId}, function(res){
				$.tree.render({
					id:"interfaces-tree",
    				elem: '#interfaces-tree',
					data: [res],
					showLine: false,  // 是否开启连接线
					click: function (obj) {
						if(obj.data.type=='interface'){
							getInterfacesView(obj.data.id);
						}
					}
				});
				if(selectNodeId){
					getInterfacesView(selectNodeId);
				}
			}, 'json')
		}
		//获取接口api详情
		function getInterfacesView(id){
			if(id){
				$.get(serverRoute+"/apis/interface-view?id="+id, function(html){
					$("#interfaces-view").html(html);
					var href=window.location.href;
					if(href.indexOf('#')!=-1){
						href=href.substring(0, href.indexOf('#'));
					}
					href=href+"#page=interfaces&id="+id;
					window.location.href=href;
				},'html');
			}else{
				var href=window.location.href;
				if(href.indexOf('#')!=-1){
					href=href.substring(0, href.indexOf('#'));
				}
				window.location.href=href;
			}
			
		}
		//同步接口API
		function synchInterfaces(){
			$.layer.confirm('确定要同步接口API吗？', {
				btn: ['确定', '关闭'] //按钮
			}, function (layerIndex) {
				$.element.progress('interfaces-synch-progress', '0%');
				$.post(serverRoute+"/systemApiInterfaces/synch", function(res){
					if(res.result==1){
						$.layer.close(layerIndex);
						getInterfacesProgress();
					}
				},"json");
			}, function () {});
		};
		//获取接口API同步进度
		function getInterfacesProgress(){
			$.post(serverRoute+"/systemApiInterfaces/getSynchProgress", function(res){
				if(res.errorMessage){
					$.layer.msg(res.errorMessage);
					$.element.progress('interfaces-synch-progress', res.progress+'%');
					return ;
				}
				$.element.progress('interfaces-synch-progress', res.progress+'%');
				if(res.progress>0 && res.progress<100){
					window.setTimeout(getInterfacesProgress, 100);
				}else if(res.progress==100){
					$.layer.msg("同步完成");
					getInterfacesTree();
				}
			}, 'json', false);
		};
		//同步类API
		function synchClass(){
			$.layer.confirm('确定要同步类API吗？', {
				btn: ['确定', '关闭'] //按钮
			}, function (layerIndex) {
				$.element.progress('class-synch-progress', '0%');
				$.post(serverRoute+"/systemApiClass/synch", function(res){
					if(res.result==1){
						$.layer.close(layerIndex);
						getClassProgress();
					}
				},"json");
			}, function () {});
		};
		//获取类API同步进度
		function getClassProgress(){
			$.post(serverRoute+"/systemApiClass/getSynchProgress", function(res){
				if(res.errorMessage){
					$.layer.msg(res.errorMessage);
					$.element.progress('class-synch-progress', res.progress+'%');
					return ;
				}
				$.element.progress('class-synch-progress', res.progress+'%');
				if(res.progress>0 && res.progress<100){
					window.setTimeout(getClassProgress, 100);
				}else if(res.progress==100){
					$.layer.msg("同步完成");
				}
			}, 'json', false);
		};
	</script>
</html>