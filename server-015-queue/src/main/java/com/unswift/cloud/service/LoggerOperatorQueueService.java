package com.unswift.cloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerOperatorAdapter;
import com.unswift.cloud.pojo.bo.logger.operator.LoggerOperatorQueueBo;
import com.unswift.cloud.pojo.dao.logger.operator.LoggerOperatorInsertDo;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="操作日志队列服务", author="liyunlong", date="2023-12-08", version="1.0.0")
public class LoggerOperatorQueueService extends BaseService{
	
	@Autowired
	@ApiField("操作日志公共服务")
	private LoggerOperatorAdapter loggerOperatorAdapter;
	
	@ApiMethod(value="创建操作日志", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public void create(LoggerOperatorQueueBo createBo){
		LoggerOperatorInsertDo insert=this.convertPojo(createBo, LoggerOperatorInsertDo.class);//将Bo转换为Do
		String token=createBo.getToken();
		Long userId=this.getUserId(token);
		insert.setCreateUser(userId);
		insert.setChangeUser(userId);
		if(ObjectUtils.isNotEmpty(insert.getOperatorContent()) && insert.getOperatorContent().length()>2048) {
			insert.setOperatorContent(insert.getOperatorContent().substring(0, 2048));//日志内容太长就截掉
		}
		loggerOperatorAdapter.save(insert, true);
	}
}
