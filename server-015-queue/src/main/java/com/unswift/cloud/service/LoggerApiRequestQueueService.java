package com.unswift.cloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.logger.LoggerApiRequestAdapter;
import com.unswift.cloud.pojo.bo.logger.api.request.LoggerApiRequestQueueBo;
import com.unswift.cloud.pojo.dao.logger.api.request.LoggerApiRequestInsertDo;

@Service
@Api(value="接口请求日志队列服务", author="liyunlong", date="2023-12-08", version="1.0.0")
public class LoggerApiRequestQueueService extends BaseService{
	
	@Autowired
	@ApiField("接口请求日志公共服务")
	private LoggerApiRequestAdapter loggerApiRequestAdapter;
	
	@ApiMethod(value="创建接口请求日志", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public void create(LoggerApiRequestQueueBo createBo){
		LoggerApiRequestInsertDo insert=this.convertPojo(createBo, LoggerApiRequestInsertDo.class);//将Bo转换为Do
		String token=createBo.getToken();
		Long userId = this.getUserId(token);
		insert.setCreateUser(userId);
		insert.setChangeUser(userId);
		loggerApiRequestAdapter.save(insert, true);
	}
}
