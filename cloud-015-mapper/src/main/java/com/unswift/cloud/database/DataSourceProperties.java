package com.unswift.cloud.database;

import java.util.Map;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

import lombok.Data;

@Data
@Api(value="数据源配置属性对象类", author="unswift", date="2023-04-16", version="1.0.0")
public class DataSourceProperties {

	@ApiField("数据源配置map")
	private Map<String, Object> datasource;
	
	@ApiField("自定义sql配置map")
	private Map<String, Object> customizeSql;

}
