package com.unswift.cloud.sql;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="自定义sql动态代理处理类", author = "unswift", date = "2024-03-07", version = "1.0.0")
public class SqlInvocationHandler implements InvocationHandler{

	@ApiField("自定义sql映射，key=id,value=sql语句")
	private Map<String, String> sqlMapper;
	
	@ApiConstructor(value="带参构造", params = {@ApiField("自定义sql映射")})
	public SqlInvocationHandler(Map<String, String> sqlMapper) {
		this.sqlMapper=sqlMapper;
	}

	@Override
	@ApiMethod(value="代理方法", params = {@ApiField("被代理的对象"), @ApiField("被代理的方法"), @ApiField("被代理的方法参数")}, returns = @ApiField("被代理方法的返回值，根据被代理方法决定"))
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		String methodName=method.getName();
		return sqlMapper.get(methodName);
	}

}
