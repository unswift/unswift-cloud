package com.unswift.cloud.sql.system.department;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="系统部门模块自定义sql", author = "unswift", date = "2024-03-19", version = "1.0.0")
public interface SystemDepartmentSql {

	@ApiMethod(value="获取父部门名称的sql", returns = @ApiField("sql语句"))
	String findParentNameSql();
	
}
