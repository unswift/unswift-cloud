package com.unswift.cloud.sql.system.cloud;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiMethod;

@Api(value="系统微服务自定义sql", author="liyunlong", date="2024-04-18", version="1.0.0")
public interface SystemCloudSql {
	
	@ApiMethod(value="获取最大排序值")
	String findMaxSortSql();
}
