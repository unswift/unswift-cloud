package com.unswift.cloud.sql.system.resource;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="系统资源模块自定义sql", author = "unswift", date = "2024-03-04", version = "1.0.0")
public interface SystemResourceSql {

	@ApiMethod(value="获取父资源名称的sql", returns = @ApiField("sql语句"))
	String findParentNameSql();
	
}
