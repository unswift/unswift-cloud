package com.unswift.cloud.sql.system.user.role;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="系统用户角色模块自定义sql", author = "unswift", date = "2024-03-04", version = "1.0.0")
public interface SystemUserRoleSql {

	@ApiMethod(value="获取角色名称的sql", returns = @ApiField("sql语句"))
	String findRoleNameSql();
	
}
