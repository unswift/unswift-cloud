package com.unswift.cloud.sql.system.department;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="系统部门岗位用户模块自定义sql", author = "unswift", date = "2024-03-19", version = "1.0.0")
public interface SystemDepartmentJobUserSql {

	@ApiMethod(value="获取根据用户id查询的sql", returns = @ApiField("sql语句"))
	String findSearchUserIdSql();
	
}
