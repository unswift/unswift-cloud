package com.unswift.cloud.sql.system.job;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="系统职位模块自定义sql", author = "unswift", date = "2024-03-04", version = "1.0.0")
public interface SystemJobSql {

	@ApiMethod(value="根据部门code查询职位code的sql", returns = @ApiField("sql语句"))
	String findJobCodeByDepartmentCodeSql();
	
}
