package com.unswift.cloud.sql;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="公共自定义sql", author = "unswift", date = "2024-03-04", version = "1.0.0")
public interface AutoSql {

	@ApiMethod(value="获取创建人姓名的sql", returns = @ApiField("sql语句"))
	String findCreateUserNameSql();
	
}
