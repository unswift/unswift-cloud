package com.unswift.cloud.sql.system.role.input;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="系统角色入参模块自定义sql", author = "unswift", date = "2024-03-04", version = "1.0.0")
public interface SystemRoleInputSql {

	@ApiMethod(value="获取角色入参名称sql", returns = @ApiField("sql语句"))
	String findRoleInputNameSql();
	
}
