package com.unswift.cloud.sql.system.role.output;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="系统角色出参模块自定义sql", author = "unswift", date = "2024-03-04", version = "1.0.0")
public interface SystemRoleOutputSql {

	@ApiMethod(value="获取角色出参名称sql", returns = @ApiField("sql语句"))
	String findRoleOutputNameSql();
	
}
