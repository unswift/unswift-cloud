package com.unswift.cloud.sql.system.dictionary;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="系统数据字典模块自定义sql", author = "unswift", date = "2024-03-19", version = "1.0.0")
public interface SystemDictionarySql {

	@ApiMethod(value="获取父项名称的sql", returns = @ApiField("sql语句"))
	String findParentNameSql();
	
	@ApiMethod(value="获取最大排序的sql", returns = @ApiField("sql语句"))
	String findMaxSortSql();
}
