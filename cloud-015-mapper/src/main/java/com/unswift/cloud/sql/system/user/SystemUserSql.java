package com.unswift.cloud.sql.system.user;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="系统用户模块自定义sql", author = "unswift", date = "2024-03-04", version = "1.0.0")
public interface SystemUserSql {

	@ApiMethod(value="获取关联登录账号信息的sql", returns = @ApiField("sql语句"))
	String findAccountSql();
	
	@ApiMethod(value="获取关联手机号信息的sql", returns = @ApiField("sql语句"))
	String findPhoneSql();
	
	@ApiMethod(value="获取关联电子邮箱信息的sql", returns = @ApiField("sql语句"))
	String findEmailSql();
	
	@ApiMethod(value="获取角色名称信息的sql", returns = @ApiField("sql语句"))
	String findRoleNamesSql();
	
	@ApiMethod(value="获取关联部门岗位Id信息的sql", returns = @ApiField("sql语句"))
	String findDepartmentJobUserIdsSql();
	
	@ApiMethod(value="获取关联部门岗位名称信息的sql", returns = @ApiField("sql语句"))
	String findDepartmentJobUserNamesSql();
	
	@ApiMethod(value="根据登录账号查询的sql语句", returns = @ApiField("sql语句"))
	String findSearchAccountSql();
	
	@ApiMethod(value="根据登录账号查询的sql语句", returns = @ApiField("sql语句"))
	String findSearchDepartmentIdSql();
	
	@ApiMethod(value="根据用户id查询角色id的sql语句", returns = @ApiField("sql语句"))
	String findSearchRoleIdByUserIdSql();
}
