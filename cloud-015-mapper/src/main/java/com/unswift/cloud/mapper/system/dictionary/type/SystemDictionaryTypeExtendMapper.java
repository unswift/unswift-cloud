package com.unswift.cloud.mapper.system.dictionary.type;

import com.unswift.annotation.api.Api;

@Api(value="数据字典类型表扩展操作映射类", author="liyunlong", date="2024-02-06", version="1.0.0")
public interface SystemDictionaryTypeExtendMapper{
	
}
