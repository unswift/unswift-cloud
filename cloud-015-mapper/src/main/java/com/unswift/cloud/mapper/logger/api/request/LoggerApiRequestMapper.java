package com.unswift.cloud.mapper.logger.api.request;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="接口请求日志表操作映射类", author="liyunlong", date="2023-12-08", version="1.0.0")
public interface LoggerApiRequestMapper extends BaseMapper{
	
}
