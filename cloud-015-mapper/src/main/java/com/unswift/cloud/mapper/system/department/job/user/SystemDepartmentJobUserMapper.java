package com.unswift.cloud.mapper.system.department.job.user;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="部门职位用户表操作映射类", author="liyunlong", date="2024-01-11", version="1.0.0")
public interface SystemDepartmentJobUserMapper extends BaseMapper{
	
}
