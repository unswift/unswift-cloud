package com.unswift.cloud.mapper.system.user.account;

import com.unswift.annotation.api.Api;

@Api(value="会员登录账号表扩展操作映射类", author="unswift", date="2023-08-13", version="1.0.0")
public interface SystemUserAccountExtendMapper{
	
}
