package com.unswift.cloud.mapper.system.api.version;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="类版本api表操作映射类", author="unswift", date="2023-09-06", version="1.0.0")
public interface SystemApiVersionMapper extends BaseMapper{
	
}
