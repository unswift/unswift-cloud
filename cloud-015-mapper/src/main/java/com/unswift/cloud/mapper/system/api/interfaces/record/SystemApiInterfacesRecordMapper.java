package com.unswift.cloud.mapper.system.api.interfaces.record;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="接口api执行记录表操作映射类", author="unswift", date="2023-09-24", version="1.0.0")
public interface SystemApiInterfacesRecordMapper extends BaseMapper{
	
}
