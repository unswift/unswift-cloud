package com.unswift.cloud.mapper.system.api.constructor;

import com.unswift.annotation.api.Api;

@Api(value="类构造函数api表扩展操作映射类", author="unswift", date="2023-09-06", version="1.0.0")
public interface SystemApiConstructorExtendMapper{
	
}
