package com.unswift.cloud.mapper.system.user;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="会员表操作映射类", author="liyunlong", date="2023-09-28", version="1.0.0")
public interface SystemUserMapper extends BaseMapper{
	
}
