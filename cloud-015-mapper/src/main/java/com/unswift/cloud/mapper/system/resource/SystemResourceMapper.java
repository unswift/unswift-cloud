package com.unswift.cloud.mapper.system.resource;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="系统资源表操作映射类", author="liyunlong", date="2023-11-23", version="1.0.0")
public interface SystemResourceMapper extends BaseMapper{
	
}
