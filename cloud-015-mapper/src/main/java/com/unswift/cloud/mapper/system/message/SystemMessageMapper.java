package com.unswift.cloud.mapper.system.message;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="系统消息表操作映射类", author="unswift", date="2023-08-13", version="1.0.0")
public interface SystemMessageMapper extends BaseMapper{
	
}
