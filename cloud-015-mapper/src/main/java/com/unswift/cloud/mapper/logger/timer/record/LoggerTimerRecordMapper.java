package com.unswift.cloud.mapper.logger.timer.record;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="定时器执行记录表操作映射类", author="unswift", date="2023-08-13", version="1.0.0")
public interface LoggerTimerRecordMapper extends BaseMapper{
	
}
