package com.unswift.cloud.mapper.logger.sql.record;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="sql执行记录表操作映射类", author="unswift", date="2023-09-05", version="1.0.0")
public interface LoggerSqlRecordMapper extends BaseMapper{
	
}
