package com.unswift.cloud.mapper.system.department.user;

import com.unswift.annotation.api.Api;

@Api(value="部门用户表扩展操作映射类", author="liyunlong", date="2024-01-11", version="1.0.0")
public interface SystemDepartmentUserExtendMapper{
	
}
