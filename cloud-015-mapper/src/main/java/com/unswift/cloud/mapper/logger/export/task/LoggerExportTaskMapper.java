package com.unswift.cloud.mapper.logger.export.task;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="导出任务记录表表操作映射类", author="liyunlong", date="2024-01-10", version="1.0.0")
public interface LoggerExportTaskMapper extends BaseMapper{
	
}
