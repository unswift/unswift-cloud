package com.unswift.cloud.mapper.system.config;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="系统配置表操作映射类", author="liyunlong", date="2023-11-27", version="1.0.0")
public interface SystemConfigMapper extends BaseMapper{
	
}
