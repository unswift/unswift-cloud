package com.unswift.cloud.mapper.system.api.interfaces.record;

import com.unswift.annotation.api.Api;

@Api(value="接口api执行记录表扩展操作映射类", author="unswift", date="2023-09-24", version="1.0.0")
public interface SystemApiInterfacesRecordExtendMapper{
	
}
