package com.unswift.cloud.mapper.system.language;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="系统语言表操作映射类", author="liyunlong", date="2024-02-10", version="1.0.0")
public interface SystemLanguageMapper extends BaseMapper{
	
}
