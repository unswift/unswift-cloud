package com.unswift.cloud.mapper.system.config;

import com.unswift.annotation.api.Api;

@Api(value="系统配置表扩展操作映射类", author="liyunlong", date="2023-11-27", version="1.0.0")
public interface SystemConfigExtendMapper{
	
}
