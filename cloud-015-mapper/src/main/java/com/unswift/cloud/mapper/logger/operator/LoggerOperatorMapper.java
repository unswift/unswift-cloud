package com.unswift.cloud.mapper.logger.operator;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="操作日志表操作映射类", author="liyunlong", date="2023-12-12", version="1.0.0")
public interface LoggerOperatorMapper extends BaseMapper{
	
}
