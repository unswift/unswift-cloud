package com.unswift.cloud.mapper.system.cloud.host;

import com.unswift.annotation.api.Api;

@Api(value="微服务主机表扩展操作映射类", author="liyunlong", date="2024-04-18", version="1.0.0")
public interface SystemCloudHostExtendMapper{
	
}
