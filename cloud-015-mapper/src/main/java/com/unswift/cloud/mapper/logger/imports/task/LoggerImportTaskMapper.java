package com.unswift.cloud.mapper.logger.imports.task;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="导入任务记录表表操作映射类", author="liyunlong", date="2024-01-10", version="1.0.0")
public interface LoggerImportTaskMapper extends BaseMapper{
	
}
