package com.unswift.cloud.mapper.system.attach;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="附件表操作映射类", author="liyunlong", date="2024-01-21", version="1.0.0")
public interface SystemAttachMapper extends BaseMapper{
	
}
