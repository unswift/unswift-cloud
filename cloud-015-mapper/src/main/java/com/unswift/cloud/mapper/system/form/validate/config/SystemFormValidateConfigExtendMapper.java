package com.unswift.cloud.mapper.system.form.validate.config;

import com.unswift.annotation.api.Api;

@Api(value="系统表单验证基础配置表扩展操作映射类", author="unswift", date="2023-08-13", version="1.0.0")
public interface SystemFormValidateConfigExtendMapper{
	
}
