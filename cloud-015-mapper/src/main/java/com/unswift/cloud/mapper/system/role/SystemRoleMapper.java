package com.unswift.cloud.mapper.system.role;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="系统角色表操作映射类", author="liyunlong", date="2023-09-28", version="1.0.0")
public interface SystemRoleMapper extends BaseMapper{
	
}
