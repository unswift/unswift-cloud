package com.unswift.cloud.mapper.logger.timer.record;

import com.unswift.annotation.api.Api;

@Api(value="定时器执行记录表扩展操作映射类", author="unswift", date="2023-08-13", version="1.0.0")
public interface LoggerTimerRecordExtendMapper{
	
}
