package com.unswift.cloud.mapper.system.dictionary.type;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="数据字典类型表操作映射类", author="liyunlong", date="2024-02-06", version="1.0.0")
public interface SystemDictionaryTypeMapper extends BaseMapper{
	
}
