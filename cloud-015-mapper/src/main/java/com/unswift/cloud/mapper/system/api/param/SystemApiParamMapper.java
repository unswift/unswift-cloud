package com.unswift.cloud.mapper.system.api.param;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="接口参数api表操作映射类", author="unswift", date="2023-09-10", version="1.0.0")
public interface SystemApiParamMapper extends BaseMapper{
	
}
