package com.unswift.cloud.mapper.system.job;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="职位表操作映射类", author="liyunlong", date="2024-01-18", version="1.0.0")
public interface SystemJobMapper extends BaseMapper{
	
}
