package com.unswift.cloud.mapper;

import java.util.List;

import org.apache.ibatis.session.ResultHandler;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.dao.SearchDo;
import com.unswift.cloud.pojo.dao.SingleDo;

@Api(value="数据库查询公共Mapper，此Mapper主要提供查询方法", author="unswift", date="2023-07-05")
public abstract interface BaseSearchMapper {

	@ApiMethod(value="根据主键或自定义sql条件从数据库中查询单个Do对象，如果自定义sql不为null，则使用自定义sql的whereList查询数据，否则，使用主键查询", params=@ApiField("查询的Do对象"), returns=@ApiField("结果的Do对象"))
	<T extends BaseDo, S extends SingleDo> T findSingle(S search);
	
	@ApiMethod(value="根据查询条件Do实体查询数量", params=@ApiField("查询的Do对象"), returns=@ApiField("数量"))
	<S extends SearchDo> int findCount(S search);
	
	@ApiMethod(value="根据查询条件Do实体查询出列表对象，\n\t说明：此查询适合小数据量得查询，大数据量请使用：findCursor方法查询", params=@ApiField("查询的Do对象"), returns=@ApiField("列表Do对象"))
	<T extends BaseDo, S extends SearchDo> List<T> findList(S search);
	
	@ApiMethod(value="根据查询条件Do实体查询MyBatis流式对象，此方法适用于大数据量得查询，如：数据导出等", params={@ApiField("查询条件对象"), @ApiField("MyBatis流式对象，用于接收数据")})
	<T extends BaseDo, S extends SearchDo> void findBigData(S search, ResultHandler<T> out);
	
}
