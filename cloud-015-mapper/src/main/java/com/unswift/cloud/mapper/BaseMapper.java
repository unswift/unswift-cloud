package com.unswift.cloud.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.dao.BatchDo;
import com.unswift.cloud.pojo.dao.PkListDo;
import com.unswift.cloud.pojo.dao.SingleDo;

@Api(value="mybatis操作数据库的公共接口", author="unswift", date="2023-04-16", version="1.0.0")
public abstract interface BaseMapper extends BaseSearchMapper{

	@ApiMethod(value="将单个Do实体数据写入到数据库，相当于sql：insert into [table]([字段]) values([字段对应值])", params=@ApiField("实体数据"), returns=@ApiField("插入数据的数量"))
	<T extends BaseDo> int insert(T insert);
	
	@ApiMethod(value="将多个Do实体数据写入到数据库，相当于：insert into [table]([字段]) values([字段对应值]),([字段对应值2])...", params=@ApiField("实体列表数据"), returns=@ApiField("插入数据的数量"))
	<T extends BaseDo> int insertBatch(@Param("list")List<T> batch);
	
	@ApiMethod(value="根据主键或自定义sql将数据更新到数据库(为空不更新)，语句如：update [table] set [字段]=[值],... where [a]=? and [b]=?，如果自定义sql的whereList不为null，则使用自定义sql的whereList更新数据，否则，使用主键更新", params=@ApiField("实体数据"), returns=@ApiField("更新数据的数量"))
	<T extends SingleDo> int update(T update);
	
	@ApiMethod(value="根据主键或自定义sql将数据更新到数据库(更新sql指定字段)，语句如：：update [table] set [字段]=[值],... where [a]=? and [b]=?，如果自定义sql的whereList不为null，则使用自定义sql的whereList更新数据，否则，使用主键更新", params=@ApiField("实体数据"), returns=@ApiField("更新数据的数量"))
	<T extends SingleDo> int updateAuto(T update);

	@ApiMethod(value="根据主键将多个Do实体数据更新到数据库(为空不更新)，语句如：update [table] set [字段]=[值],... where id=?,update [table]...", params=@ApiField("实体列表数据"), returns=@ApiField("更新数据的数量"))
	<T extends BatchDo<?>> int updateBatch(T batch);

	@ApiMethod(value="根据主键或自定义sql逻辑删除一条记录，语句如：update [table] set is_delete_=1 where [a]=? and [b]=?，如果自定义sql的whereList不为null，则使用自定义sql的whereList更新数据，否则，使用主键更新", params={@ApiField("删除条件的Do对象")}, returns=@ApiField("删除数据的数量"))
	<T extends SingleDo> int delete(T delete);
	
	@ApiMethod(value="根据主键或自定义sql删除一条记录，语句如：delete from [table] where id=?，如果自定义sql的whereList不为null，则使用自定义sql的whereList更新数据，否则，使用主键更新", params={@ApiField("删除条件的Do对象")}, returns=@ApiField("删除数据的数量"))
	<T extends SingleDo> int deleteReal(T delete);
	
	@ApiMethod(value="根据主键集合删除多条记录，语句如：update [table] set is_delete_=1 where id in(?,...)", params={@ApiField("主键列表")}, returns=@ApiField("删除数据的数量"))
	<T extends PkListDo<?>> int deleteBatch(T pkList);
	
	@ApiMethod(value="根据主键集合删除多条记录，语句如：delete from [table] where id in(?,...)", params={@ApiField("主键列表")}, returns=@ApiField("删除数据的数量"))
	<T extends PkListDo<?>> int deleteRealBatch(T pkList);
}
