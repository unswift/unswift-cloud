package com.unswift.cloud.mapper.system.cache.refresh;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="缓存刷新表操作映射类", author="unswift", date="2023-08-13", version="1.0.0")
public interface SystemCacheRefreshMapper extends BaseMapper{
	
}
