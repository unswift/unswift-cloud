package com.unswift.cloud.mapper.system.role.resource;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="角色资源表操作映射类", author="liyunlong", date="2023-12-02", version="1.0.0")
public interface SystemRoleResourceMapper extends BaseMapper{
	
}
