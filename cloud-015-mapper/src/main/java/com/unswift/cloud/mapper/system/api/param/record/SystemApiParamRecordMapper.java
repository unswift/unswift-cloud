package com.unswift.cloud.mapper.system.api.param.record;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="api接口参数执行记录表操作映射类", author="unswift", date="2023-09-16", version="1.0.0")
public interface SystemApiParamRecordMapper extends BaseMapper{
	
}
