package com.unswift.cloud.mapper.system.api.interfaces;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="接口api表操作映射类", author="unswift", date="2023-09-09", version="1.0.0")
public interface SystemApiInterfacesMapper extends BaseMapper{
	
}
