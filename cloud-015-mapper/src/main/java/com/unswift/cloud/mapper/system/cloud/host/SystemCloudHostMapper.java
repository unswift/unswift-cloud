package com.unswift.cloud.mapper.system.cloud.host;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="微服务主机表操作映射类", author="liyunlong", date="2024-04-18", version="1.0.0")
public interface SystemCloudHostMapper extends BaseMapper{
	
}
