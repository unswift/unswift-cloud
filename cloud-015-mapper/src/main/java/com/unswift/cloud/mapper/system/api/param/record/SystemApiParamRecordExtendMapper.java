package com.unswift.cloud.mapper.system.api.param.record;

import com.unswift.annotation.api.Api;

@Api(value="api接口参数执行记录表扩展操作映射类", author="unswift", date="2023-09-16", version="1.0.0")
public interface SystemApiParamRecordExtendMapper{
	
}
