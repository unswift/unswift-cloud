package com.unswift.cloud.mapper.logger.login.record;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="登录记录表操作映射类", author="liyunlong", date="2024-04-06", version="1.0.0")
public interface LoggerLoginRecordMapper extends BaseMapper{
	
}
