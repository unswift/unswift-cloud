package com.unswift.cloud.mapper.system.resource.output;

import com.unswift.annotation.api.Api;

@Api(value="系统资源出参表扩展操作映射类", author="liyunlong", date="2023-11-23", version="1.0.0")
public interface SystemResourceOutputExtendMapper{
	
}
