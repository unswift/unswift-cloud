package com.unswift.cloud.mapper.issue.environment;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="发布环境表操作映射类", author="liyunlong", date="2024-04-16", version="1.0.0")
public interface IssueEnvironmentMapper extends BaseMapper{
	
}
