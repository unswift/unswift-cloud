package com.unswift.cloud.mapper.system.dictionary;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="数据字典表操作映射类", author="liyunlong", date="2024-02-06", version="1.0.0")
public interface SystemDictionaryMapper extends BaseMapper{
	
}
