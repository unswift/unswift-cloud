package com.unswift.cloud.mapper.system.api.classes;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="类api表操作映射类", author="unswift", date="2023-09-09", version="1.0.0")
public interface SystemApiClassMapper extends BaseMapper{
	
}
