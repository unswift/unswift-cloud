package com.unswift.cloud.mapper.logger.queue;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.mapper.BaseMapper;

@Api(value="接口请求日志表操作映射类", author="liyunlong", date="2024-01-25", version="1.0.0")
public interface LoggerQueueMapper extends BaseMapper{
	
}
