package com.unswift.cloud.adapter.system.auth;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.config.RequestConfig;
import com.unswift.cloud.mapper.system.resource.input.SystemResourceInputExtendMapper;
import com.unswift.cloud.mapper.system.resource.input.SystemResourceInputMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.resource.input.SystemResourceInputDataDo;
import com.unswift.cloud.pojo.dao.system.resource.input.SystemResourceInputDeleteDo;
import com.unswift.cloud.pojo.dao.system.resource.input.SystemResourceInputSearchDo;
import com.unswift.cloud.pojo.dao.system.resource.input.SystemResourceInputSingleDo;
import com.unswift.cloud.pojo.dto.BaseDto;
import com.unswift.cloud.pojo.vo.system.resource.input.SystemResourceInputAuthVo;
import com.unswift.utils.ClassUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="系统资源入参公共服务-此bean可以被任何Service引用", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemResourceInputAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("请求配置")
	private RequestConfig requestConfig;
	
	@Autowired
	@ApiField("系统资源入参数据库操作对象")
	private SystemResourceInputMapper systemResourceInputMapper;
	
	@Autowired
	@ApiField("系统资源入参数据库扩展操作对象")
	private SystemResourceInputExtendMapper systemResourceInputExtendMapper;
	
	@ApiMethod(value="获取系统资源入参数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemResourceInputMapper getMapper(){
		return systemResourceInputMapper;
	}
	
	@ApiMethod(value="获取系统资源入参模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemResourceInput";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemResourceInputDataDo findById(Long id){
		return this.findSingle(new SystemResourceInputSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemResourceInputDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemResourceInputSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemResourceInputDataDo findFirst(SystemResourceInputSearchDo search, String multipleException){
		List<SystemResourceInputDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="根据资源id删除关联入参", params = {@ApiField("资源id")}, returns = @ApiField("删除数量"))
	public int deleteByResourceId(long resourceId) {
		SystemResourceInputDeleteDo delete=new SystemResourceInputDeleteDo();
		Sql sql=Sql.createSql();
		sql.addWhere(Sql.LOGIC_AND, "resource_id_", Sql.COMPARE_EQUAL, resourceId);
		delete.setSql(sql);
		return this.delete(delete);
	}
	
	@ApiMethod(value="根据资源id查询入参列表", params = {@ApiField("资源id")}, returns = @ApiField("入参列表"))
	public List<SystemResourceInputDataDo> findByResourceId(long resourceId){
		SystemResourceInputSearchDo search=new SystemResourceInputSearchDo();
		search.setResourceId(resourceId);
		Sql sql=Sql.createSql();
		sql.addOrderBy("sort_", Sql.ORDER_BY_ASC);//排序
		search.setSql(sql);
		return this.findList(search);
	}
	
	@SuppressWarnings("unchecked")
	@ApiMethod(value="根据权限key解析请求入参", params = {@ApiField("权限key")}, returns = @ApiField("方法参数列表"))
	public List<SystemResourceInputAuthVo> getInputListByAuthKey(String authKey){
		List<Class<?>> controllerClassList=ClassUtils.findAnnotationList(ObjectUtils.asList(requestConfig.getAuthScanPackage().split("\\,")), ObjectUtils.asList(RestController.class));
		if(ObjectUtils.isNotEmpty(controllerClassList)) {
			Method authMethod=null;
			for (Class<?> controllerClass : controllerClassList) {
				List<Method> methodList=ClassUtils.getAnnotationMethod(controllerClass, Auth.class);
				if(ObjectUtils.isNotEmpty(methodList)) {
					Auth auth;
					for (Method method : methodList) {
						auth=method.getAnnotation(Auth.class);
						if(authKey.equals(auth.value())){
							authMethod=method;
							break;
						}
					}
				}
			}
			if(ObjectUtils.isNotEmpty(authMethod)) {
				List<SystemResourceInputAuthVo> inputList=new ArrayList<SystemResourceInputAuthVo>();
				Set<String> paramSet=new HashSet<String>();
				Parameter[] parameters = authMethod.getParameters();
				if(ObjectUtils.isNotEmpty(parameters)) {
					Class<?> paramType;
					for (Parameter parameter : parameters) {
						paramType=parameter.getType();
						if(ClassUtils.isSuperClass(paramType, BaseDto.class)) {
							List<SystemResourceInputAuthVo> subInputList=ClassUtils.getClassFieldList(paramType, SystemResourceInputAuthVo.class, new String[]{"paramKey","paramComment"}, BaseDto.class, 5, paramSet);
							if(ObjectUtils.isNotEmpty(subInputList)) {
								inputList.addAll(subInputList);
							}
						}
					}
				}
				return inputList;
			}
		}
		return null;
	}
	
}
