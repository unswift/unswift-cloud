package com.unswift.cloud.adapter.validate;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiField;

@Api(value="表单验证语言关键词枚举", author="unswift", date="2023-06-01", version="1.0.0")
public enum KeywordEnum {
	
	KEYWORD_IF("if", "如果语句关键词", ""),
	KEYWORD_ELSE_IF("else if", "否则如果语句关键词", ""),
	KEYWORD_ELSE("else", "否则语句关键词", ""),
	KEYWORD_END_IF("end if", "如果语句结束关键词", ""),
	KEYWORD_CASE("case", "case语句开始", ""),
	KEYWORD_WHEN_THEN("when then", "当xx时,那么xx", ""),
	KEYWORD_THEN("then", "那么语句", ""),
	KEYWORD_END_CASE("end case", "case语句结束", ""),
	KEYWORD_FOREACH("foreach", "foreach语句", "^var [a-zA-Z0-9_]+ as [a-zA-Z0-9_.\\(\\)'\"\\[\\]]+$"),
	KEYWORD_BREAK("break", "跳出循环", ""),
	KEYWORD_CONTINUE("continue", "结束本次循环", ""),
	KEYWORD_END_FOREACH("end foreach", "foreach语句结束", ""),
	KEYWORD_VAR("var", "变量语句", "^[a-zA-Z0-9_]+[=][a-zA-Z0-9_.\\(\\)'\"]+$"),
	KEYWORD_VALUE("value", "变量赋值", ""),
	KEYWORD_TIPS("tips", "提示语句，如果满足条件则提示，否则不提示", ""),
	;
	
	
	@ApiField("语言关键词")
	private String keyword;
	
	@ApiField("语言关键词描述")
	private String describe;
	
	@ApiField("语句正则表达式")
	private String statementRegexp;

	@ApiConstructor(value="表单验证语言枚举构造", params={@ApiField("关键词"), @ApiField("关键词描述")})
	private KeywordEnum(String keyword, String describe, String statementRegexp) {
		this.keyword = keyword;
		this.describe = describe;
		this.statementRegexp=statementRegexp;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getStatementRegexp() {
		return statementRegexp;
	}

	public void setStatementRegexp(String statementRegexp) {
		this.statementRegexp = statementRegexp;
	}
}
