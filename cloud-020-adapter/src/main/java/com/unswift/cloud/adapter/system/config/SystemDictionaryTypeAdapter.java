package com.unswift.cloud.adapter.system.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.dictionary.type.SystemDictionaryTypeExtendMapper;
import com.unswift.cloud.mapper.system.dictionary.type.SystemDictionaryTypeMapper;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeDataDo;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeSearchDo;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeSingleDo;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="数据字典类型公共服务-此bean可以被任何Service引用", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypeAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("数据字典类型数据库操作对象")
	private SystemDictionaryTypeMapper systemDictionaryTypeMapper;
	
	@Autowired
	@ApiField("数据字典类型数据库扩展操作对象")
	private SystemDictionaryTypeExtendMapper systemDictionaryTypeExtendMapper;
	
	@ApiMethod(value="获取数据字典类型数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemDictionaryTypeMapper getMapper(){
		return systemDictionaryTypeMapper;
	}
	
	@ApiMethod(value="获取数据字典类型模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemDictionaryType";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemDictionaryTypeDataDo findById(Long id){
		return this.findSingle(new SystemDictionaryTypeSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemDictionaryTypeDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemDictionaryTypeSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemDictionaryTypeDataDo findFirst(SystemDictionaryTypeSearchDo search, String multipleException){
		List<SystemDictionaryTypeDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="类型编码是否已存在", params = {@ApiField("类型编码")}, returns = @ApiField("是否存在，true：已存在，false：不存在"))
    public boolean existsByCode(String code) {
		SystemDictionaryTypeSearchDo search=new SystemDictionaryTypeSearchDo();
    	search.setCode(code);
    	return this.findCount(search)>0;
    }
	
	@ApiMethod(value="根据类型编码查询数据字典类型", params = {@ApiField("类型编码")}, returns = @ApiField("查询结果"))
    public SystemDictionaryTypeDataDo findByCode(String code) {
		SystemDictionaryTypeSearchDo search=new SystemDictionaryTypeSearchDo();
    	search.setCode(code);
    	return this.findFirst(search, String.format("%s（%s）", "类型编码", code));
    }
	
	@ApiMethod(value="类型名称是否已存在", params = {@ApiField("类型名称"), @ApiField("排除的类型id")}, returns = @ApiField("是否存在，true：已存在，false：不存在"))
    public boolean existsByName(String name, Long excludeId) {
		SystemDictionaryTypeSearchDo search=new SystemDictionaryTypeSearchDo();
    	search.setName(name);
    	this.addWhereNotEquals(search, "t.id_", excludeId, null);
    	return this.findCount(search)>0;
    }
	
	@ApiMethod(value="根据类型编码查询数据字典类型-有缓存", params = {@ApiField("类型编码")}, returns = @ApiField("查询结果"))
    public SystemDictionaryTypeDataDo findByName(String name) {
		Map<String, SystemDictionaryTypeDataDo> typeMap=validateAdapter.getAttribute("dictionaryTypeMap");
		if(ObjectUtils.isNull(typeMap)) {
			typeMap=new HashMap<String, SystemDictionaryTypeDataDo>();
			validateAdapter.setAttribute("dictionaryTypeMap", typeMap);
		}
		if(typeMap.containsKey(name)) {//走缓存
			return typeMap.get(name);
		}else {//走数据库查询
			SystemDictionaryTypeSearchDo search=new SystemDictionaryTypeSearchDo();
	    	search.setName(name);
	    	SystemDictionaryTypeDataDo type=this.findFirst(search, String.format("%s（%s）", "类型名称", name));
	    	typeMap.put(name, type);//设置缓存
	    	return type;
		}
    }
}
