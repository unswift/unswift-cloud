package com.unswift.cloud.adapter.logger;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.logger.login.record.LoggerLoginRecordExtendMapper;
import com.unswift.cloud.mapper.logger.login.record.LoggerLoginRecordMapper;
import com.unswift.cloud.pojo.cho.system.config.SystemConfigCho;
import com.unswift.cloud.pojo.dao.logger.login.record.LoggerLoginRecordDataDo;
import com.unswift.cloud.pojo.dao.logger.login.record.LoggerLoginRecordSearchDo;
import com.unswift.cloud.pojo.dao.logger.login.record.LoggerLoginRecordSingleDo;
import com.unswift.cloud.sql.logger.login.record.LoggerLoginRecordSql;
import com.unswift.utils.DateUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="登录记录公共服务-此bean可以被任何Service引用", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("登录记录数据库操作对象")
	private LoggerLoginRecordMapper loggerLoginRecordMapper;
	
	@Autowired
	@ApiField("登录记录自定义sql")
	private LoggerLoginRecordSql loggerLoginRecordSql;
	
	@Autowired
	@ApiField("登录记录数据库扩展操作对象")
	private LoggerLoginRecordExtendMapper loggerLoginRecordExtendMapper;
	
	@ApiMethod(value="获取登录记录数据库操作对象", returns=@ApiField("数据库操作对象"))
	public LoggerLoginRecordMapper getMapper(){
		return loggerLoginRecordMapper;
	}
	
	@ApiMethod(value="获取登录记录模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "loggerLoginRecord";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public LoggerLoginRecordDataDo findById(Long id){
		return this.findSingle(new LoggerLoginRecordSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<LoggerLoginRecordDataDo> findByIds(List<Long> idList){
		return this.findList(new LoggerLoginRecordSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public LoggerLoginRecordDataDo findFirst(LoggerLoginRecordSearchDo search, String multipleException){
		List<LoggerLoginRecordDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="识别是否冻结账户，在一定时间范围如果登录错误次数大于一定次数，则返回true", params = {@ApiField("登录账号")}, returns = @ApiField("是否需要冻结，true：需要，false：不需要"))
    public boolean isFreezeAccount(String account) {
        SystemConfigCho config=cacheAdapter.findConfigByKey("loginErrorTimeInterval");
        ExceptionUtils.empty(config, "field.empty", "登录错误时间间隔");
        Date maxTime=DateUtils.getDiffSubDate(Integer.parseInt(config.getValue()), config.getExtend01());
        String loginErrorCount=cacheAdapter.findConfigValueByKey("loginErrorCount");
        ExceptionUtils.empty(loginErrorCount, "field.empty", "登录错误次数");
        String loginErrorCode=cacheAdapter.findConfigValueByKey("loginErrorCode");
        ExceptionUtils.empty(loginErrorCode, "field.empty", "登录错误码");
        LoggerLoginRecordSearchDo search=new LoggerLoginRecordSearchDo();
        search.setAccount(account);
        search.setLoginResult((byte)0);
        this.addWhereInList(search, "error_code_", ObjectUtils.asList(loginErrorCode.split("\\,")));
        this.addWhereGt(search, "login_time_", maxTime, "yyyy-MM-dd HH:mm:ss");
        int count = findCount(search);
        if(count>=Integer.parseInt(loginErrorCount)) {
        	return true;
        }
        return false;
    }
}
