package com.unswift.cloud.adapter.system.organization;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.department.job.user.SystemDepartmentJobUserExtendMapper;
import com.unswift.cloud.mapper.system.department.job.user.SystemDepartmentJobUserMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.department.job.user.SystemDepartmentJobUserDataDo;
import com.unswift.cloud.pojo.dao.system.department.job.user.SystemDepartmentJobUserDeleteDo;
import com.unswift.cloud.pojo.dao.system.department.job.user.SystemDepartmentJobUserSearchDo;
import com.unswift.cloud.pojo.dao.system.department.job.user.SystemDepartmentJobUserSingleDo;
import com.unswift.cloud.sql.system.department.SystemDepartmentJobUserSql;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="部门职位用户公共服务-此bean可以被任何Service引用", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentJobUserAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("部门职位用户数据库操作对象")
	private SystemDepartmentJobUserMapper systemDepartmentJobUserMapper;
	
	@Autowired
	@ApiField("部门职位用户数据库扩展操作对象")
	private SystemDepartmentJobUserExtendMapper systemDepartmentJobUserExtendMapper;
	
	@Autowired
	@ApiField("部门职位用户自定义sql对象")
	private SystemDepartmentJobUserSql systemDepartmentJobUserSql;
	
	@ApiMethod(value="获取部门职位用户数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemDepartmentJobUserMapper getMapper(){
		return systemDepartmentJobUserMapper;
	}
	
	@ApiMethod(value="获取部门职位用户模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemDepartmentJobUser";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemDepartmentJobUserDataDo findById(Long id){
		return this.findSingle(new SystemDepartmentJobUserSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemDepartmentJobUserDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemDepartmentJobUserSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemDepartmentJobUserDataDo findFirst(SystemDepartmentJobUserSearchDo search, String multipleException){
		List<SystemDepartmentJobUserDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	public boolean existsByDepartmentJobId(Long departmentJobId) {
		SystemDepartmentJobUserSearchDo search=new SystemDepartmentJobUserSearchDo();
		search.setDepartmentJobId(departmentJobId);
		int count=this.findCount(search);
		return count>0;
	}
	
	@ApiMethod(value="根据用户id删除用户部门职位用户", params = {@ApiField("用户id")}, returns = @ApiField("删除数量"))
	public int deleteByUserId(long userId) {
		SystemDepartmentJobUserDeleteDo delete=new SystemDepartmentJobUserDeleteDo();
		this.addWhereIn(delete, "department_user_id_", Sql.format(systemDepartmentJobUserSql.findSearchUserIdSql(), userId));
		return this.delete(delete);
	}
}
