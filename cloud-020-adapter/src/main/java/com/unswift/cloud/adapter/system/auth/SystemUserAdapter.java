package com.unswift.cloud.adapter.system.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.user.SystemUserExtendMapper;
import com.unswift.cloud.mapper.system.user.SystemUserMapper;
import com.unswift.cloud.pojo.dao.system.user.SystemUserDataDo;
import com.unswift.cloud.pojo.dao.system.user.SystemUserSearchDo;
import com.unswift.cloud.pojo.dao.system.user.SystemUserSingleDo;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="会员公共服务-此bean可以被任何Service引用", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemUserAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("会员数据库操作对象")
	private SystemUserMapper systemUserMapper;
	
	@Autowired
	@ApiField("会员数据库扩展操作对象")
	private SystemUserExtendMapper systemUserExtendMapper;
	
	@ApiMethod(value="获取会员数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemUserMapper getMapper(){
		return systemUserMapper;
	}
	
	@ApiMethod(value="获取会员模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemUser";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemUserDataDo findById(Long id){
		return this.findSingle(new SystemUserSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemUserDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemUserSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemUserDataDo findFirst(SystemUserSearchDo search, String multipleException){
		List<SystemUserDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
}
