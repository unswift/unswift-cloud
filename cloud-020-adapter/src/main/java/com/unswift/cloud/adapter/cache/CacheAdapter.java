package com.unswift.cloud.adapter.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cache.MemoryCache;
import com.unswift.cloud.adapter.system.config.SystemCloudAdapter;
import com.unswift.cloud.adapter.system.config.SystemConfigAdapter;
import com.unswift.cloud.adapter.system.config.SystemDictionaryAdapter;
import com.unswift.cloud.adapter.system.config.SystemDictionaryTypeAdapter;
import com.unswift.cloud.adapter.system.config.SystemFormValidateAdapter;
import com.unswift.cloud.adapter.system.config.SystemFormValidateConfigAdapter;
import com.unswift.cloud.adapter.system.config.SystemLanguageAdapter;
import com.unswift.cloud.cache.CacheEnum;
import com.unswift.cloud.cache.ICacheAdapter;
import com.unswift.cloud.core.CommonOperator;
import com.unswift.cloud.pojo.IBaseCho;
import com.unswift.cloud.pojo.cho.system.cloud.SystemCloudCho;
import com.unswift.cloud.pojo.cho.system.config.SystemConfigCho;
import com.unswift.cloud.pojo.cho.system.dictionary.SystemDictionaryCho;
import com.unswift.cloud.pojo.cho.system.dictionary.type.SystemDictionaryTypeCho;
import com.unswift.cloud.pojo.cho.system.form.validate.SystemFormValidateCho;
import com.unswift.cloud.pojo.cho.system.language.SystemLanguageCho;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudDataDo;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudSearchDo;
import com.unswift.cloud.pojo.dao.system.config.SystemConfigDataDo;
import com.unswift.cloud.pojo.dao.system.config.SystemConfigSearchDo;
import com.unswift.cloud.pojo.dao.system.dictionary.SystemDictionaryDataDo;
import com.unswift.cloud.pojo.dao.system.dictionary.SystemDictionarySearchDo;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeDataDo;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeSearchDo;
import com.unswift.cloud.pojo.dao.system.form.validate.SystemFormValidateDataDo;
import com.unswift.cloud.pojo.dao.system.form.validate.SystemFormValidateSearchDo;
import com.unswift.cloud.pojo.dao.system.form.validate.config.SystemFormValidateConfigDataDo;
import com.unswift.cloud.pojo.dao.system.form.validate.config.SystemFormValidateConfigSearchDo;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageSearchDo;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="业务缓存实现，封装一些缓存的操作", author="unswift", date="2023-04-16", version="1.0.0")
public class CacheAdapter extends CommonOperator implements ICacheAdapter{

	@Autowired
	private MemoryCache memoryCache;
	
	@Lazy
	@Autowired
	@ApiField("系统表单验证公共组件")
	private SystemFormValidateAdapter systemFormValidateAdapter;
	
	@Lazy
	@Autowired
	@ApiField("系统表单验证公共组件")
	private SystemFormValidateConfigAdapter systemFormValidateConfigAdapter;
	
	@Lazy
	@Autowired
	@ApiField("系统数据字典公共组件")
	private SystemDictionaryAdapter systemDictionaryAdapter;
	
	@Lazy
	@Autowired
	@ApiField("系统语言公共组件")
	private SystemLanguageAdapter systemLanguageAdapter;
	
	@Lazy
	@Autowired
	@ApiField("系统语言公共组件")
	private SystemDictionaryTypeAdapter systemDictionaryTypeAdapter;
	
	@Lazy
	@Autowired
	@ApiField("系统配置公共组件")
	private SystemConfigAdapter systemConfigAdapter;
	
	@Lazy
	@Autowired
	@ApiField("系统微服务公共组件")
	private SystemCloudAdapter systemCloudAdapter;
	
	@ApiField("微服务名称")
	@Value("${spring.application.name}")
	private String serverName;
	
	@SuppressWarnings("unchecked")
	@ApiMethod(value="获取表单验证缓存数据", params={@ApiField("所属模块"), @ApiField("所属操作")})
	public synchronized List<SystemFormValidateCho> findFormValidateList(String module, String operator){
		List<SystemFormValidateCho> formValidateList=memoryCache.getHash(CacheEnum.FORM_VALIDATE_MAP.getKey(), module);
		if(ObjectUtils.isEmpty(formValidateList)){
			SystemFormValidateSearchDo search=new SystemFormValidateSearchDo();
			search.setServer(serverName);
			search.setModule(module);
			Sql sql=Sql.createSql();
			sql.addOrderBy("code_line_", Sql.ORDER_BY_ASC);
			search.setSql(sql);
			List<SystemFormValidateDataDo> formValidateDataList=systemFormValidateAdapter.findList(search);
			formValidateDataList=ObjectUtils.isEmpty(formValidateDataList)?new ArrayList<SystemFormValidateDataDo>():formValidateDataList;//如果存在就设置缓存，否则设置一个空对象，保证不会总是查询数据库
			formValidateList=this.convertPojoList(formValidateDataList, SystemFormValidateCho.class);//将Do转换未Vo
			memoryCache.setHash(CacheEnum.FORM_VALIDATE_MAP.getKey(), module, formValidateList);
		}
		if(ObjectUtils.isNotEmpty(operator)) {
			return formValidateList.stream().filter(v -> v.getOperator().equals(operator) || v.getOperator().equals("*")).collect(Collectors.toList());
		}else {
			return formValidateList.stream().filter(v -> ObjectUtils.isEmpty(v.getOperator()) || v.getOperator().equals("*")).collect(Collectors.toList());
		}
	}
	
	@Override
	@ApiMethod(value="获取表单验证缓存数据", params={@ApiField("所属模块"), @ApiField("所属操作")}, returns=@ApiField("指定模块下指定操作的表单验证规则"))
	public Map<String, String> findFormValidateConfigMap() {
		Map<String, String> configMap=memoryCache.get(CacheEnum.FORM_VALIDATE_MAP.getKey());
		if(ObjectUtils.isEmpty(configMap)){
			SystemFormValidateConfigSearchDo search=new SystemFormValidateConfigSearchDo();
			List<SystemFormValidateConfigDataDo> configList=systemFormValidateConfigAdapter.findList(search);
			configMap=configList.stream().collect(Collectors.toMap(c -> c.getKey(), c -> c.getValue()));
			memoryCache.set(CacheEnum.FORM_VALIDATE_MAP.getKey(), configMap);
		}
		return configMap;
	}
	
	@Override
	@ApiMethod(value="获取数据字典类型名称", params = {@ApiField("数据字典类型编码")})
	public String findDictionaryTypeNameByCode(String code) {
		Map<String, SystemDictionaryTypeCho> dictionaryTypeMap=memoryCache.get(CacheEnum.DICTIONARY_TYPE_MAP.getKey());
		if(ObjectUtils.isNull(dictionaryTypeMap)){//没有被初始化过
			dictionaryTypeMap=new HashMap<String, SystemDictionaryTypeCho>();//初始化，如果查询一次数据库查询不到，就不会再查询
			SystemDictionaryTypeSearchDo search=new SystemDictionaryTypeSearchDo();
			List<SystemDictionaryTypeDataDo> list = systemDictionaryTypeAdapter.findList(search);
			if(ObjectUtils.isNotEmpty(list)) {
				dictionaryTypeMap=list.stream().collect(Collectors.toMap(d -> d.getCode(), d -> convertPojo(d, SystemDictionaryTypeCho.class), (V1, V2)-> V1));
			}
			memoryCache.set(CacheEnum.DICTIONARY_TYPE_MAP.getKey(), dictionaryTypeMap);
		}
		SystemDictionaryTypeCho type=dictionaryTypeMap.get(code);
		if(ObjectUtils.isEmpty(type)){
			return null;
		}
		return type.getName();
	}
	
	@Override
	@ApiMethod(value="获取数据字典值", params = {@ApiField("数据字典类型"), @ApiField("语言"), @ApiField("数据字典key")})
	public synchronized String findDictionaryValueByKey(String type, String language, String key) {
		key=String.format("%s-%s", key, language);
		Map<String, SystemDictionaryCho> dictionaryMap=memoryCache.getHash(CacheEnum.DICTIONARY_MAP.getKey(), type);
		if(ObjectUtils.isNull(dictionaryMap)){//没有被初始化过
			dictionaryMap=new HashMap<String, SystemDictionaryCho>();//初始化，如果查询一次数据库查询不到，就不会再查询
			SystemDictionarySearchDo search=new SystemDictionarySearchDo();
			search.setType(type);
			List<SystemDictionaryDataDo> list = systemDictionaryAdapter.findList(search);
			if(ObjectUtils.isNotEmpty(list)) {
				dictionaryMap=list.stream().collect(Collectors.toMap(d -> String.format("%s-%s", d.getKey(), d.getLanguage()), d -> convertPojo(d, SystemDictionaryCho.class), (V1, V2)-> V1));
			}
			memoryCache.setHash(CacheEnum.DICTIONARY_MAP.getKey(), type, dictionaryMap);
		}
		SystemDictionaryCho dictionary = dictionaryMap.get(key);
		if(ObjectUtils.isEmpty(dictionary)){
			return null;
		}
		return dictionary.getValue();
	}
	
	@Override
	@ApiMethod(value="获取数据字典键", params = {@ApiField("数据字典类型"), @ApiField("数据字典value")})
	public synchronized String findDictionaryKeyByValue(String type, String value) {
		Map<String, SystemDictionaryCho> dictionaryMap=memoryCache.getHash(CacheEnum.DICTIONARY_MAP.getKey(), type);
		if(ObjectUtils.isNull(dictionaryMap)){//没有被初始化过
			dictionaryMap=new HashMap<String, SystemDictionaryCho>();//初始化，如果查询一次数据库查询不到，就不会再查询
			SystemDictionarySearchDo search=new SystemDictionarySearchDo();
			search.setType(type);
			List<SystemDictionaryDataDo> list = systemDictionaryAdapter.findList(search);
			if(ObjectUtils.isNotEmpty(list)) {
				dictionaryMap=list.stream().collect(Collectors.toMap(d -> String.format("%s-%s", d.getKey(), d.getLanguage()), d -> convertPojo(d, SystemDictionaryCho.class), (V1, V2)-> V1));
			}
			memoryCache.setHash(CacheEnum.DICTIONARY_MAP.getKey(), type, dictionaryMap);
		}
		List<SystemDictionaryCho> matchList = dictionaryMap.values().stream().filter(d -> value.equals(d.getValue())).collect(Collectors.toList());
		if(ObjectUtils.isEmpty(matchList)){
			return null;
		}
		return matchList.get(0).getKey();
	}
	
	@Override
	@ApiMethod(value="查询某个数据字典值是否存在", params = {@ApiField("数据字典类型"), @ApiField("数据字典value")})
	public boolean existsDictionaryKeyByValue(String type, String value) {
		Map<String, SystemDictionaryCho> dictionaryMap=memoryCache.getHash(CacheEnum.DICTIONARY_MAP.getKey(), type);
		if(ObjectUtils.isNull(dictionaryMap)){//没有被初始化过
			dictionaryMap=new HashMap<String, SystemDictionaryCho>();//初始化，如果查询一次数据库查询不到，就不会再查询
			SystemDictionarySearchDo search=new SystemDictionarySearchDo();
			search.setType(type);
			List<SystemDictionaryDataDo> list = systemDictionaryAdapter.findList(search);
			if(ObjectUtils.isNotEmpty(list)) {
				dictionaryMap=list.stream().collect(Collectors.toMap(d -> String.format("%s-%s", d.getKey(), d.getLanguage()), d -> convertPojo(d, SystemDictionaryCho.class), (V1, V2)-> V1));
			}
			memoryCache.setHash(CacheEnum.DICTIONARY_MAP.getKey(), type, dictionaryMap);
		}
		List<SystemDictionaryCho> matchList = dictionaryMap.values().stream().filter(d -> value.equals(d.getValue())).collect(Collectors.toList());
		if(ObjectUtils.isEmpty(matchList)){
			return false;
		}
		return true;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	@ApiMethod(value="查询语言缓存列表", params = {@ApiField("激活状态{0：未激活，1：已激活，null：查询所有}")}, returns = @ApiField("语言列表"))
	public List<SystemLanguageCho> findLanguageList(Byte activeStatus) {
		List<SystemLanguageCho> languageList=memoryCache.get(CacheEnum.LANGUAGE_LIST.getKey());
		if(ObjectUtils.isNull(languageList)){//没有被初始化过
			SystemLanguageSearchDo search=new SystemLanguageSearchDo();
			List<SystemDictionaryDataDo> list = systemLanguageAdapter.findList(search);
			languageList=this.convertPojoList(list, SystemLanguageCho.class);
			memoryCache.set(CacheEnum.LANGUAGE_LIST.getKey(), languageList);
		}
		if(ObjectUtils.isEmpty(languageList)) {
			return languageList;
		}
		if(ObjectUtils.isEmpty(activeStatus)) {
			return languageList;
		}
		return languageList.stream().filter(l -> l.getActiveStatus().equals(activeStatus)).collect(Collectors.toList());
	}
	
	@Override
	@ApiMethod(value="根据简写语言查询语言全称", params = {@ApiField("语言简称")}, returns = @ApiField("语言全称"))
	public String findLanguageByShortName(String shortName) {
		List<SystemLanguageCho> languageList=memoryCache.get(CacheEnum.LANGUAGE_LIST.getKey());
		if(ObjectUtils.isNull(languageList)){//没有被初始化过
			SystemLanguageSearchDo search=new SystemLanguageSearchDo();
			List<SystemDictionaryDataDo> list = systemLanguageAdapter.findList(search);
			languageList=this.convertPojoList(list, SystemLanguageCho.class);
			memoryCache.set(CacheEnum.LANGUAGE_LIST.getKey(), languageList);
		}
		if(ObjectUtils.isEmpty(languageList)) {
			return null;
		}
		List<SystemLanguageCho> language = languageList.stream().filter(l -> l.getShortName().equals(shortName)).collect(Collectors.toList());
		if(ObjectUtils.isEmpty(language)) {
			return null;
		}
		return language.get(0).getFullName();
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "hiding" })
	@ApiMethod(value="根据系统配置key获取配置项", params = {@ApiField("配置项的key")}, returns = @ApiField("配置项对象"))
	public <T extends IBaseCho> T findConfigByKey(String key) {
		Map<String, SystemConfigCho> configMap=memoryCache.get(CacheEnum.CONFIG_MAP.getKey());
		if(ObjectUtils.isNull(configMap)){//没有被初始化过
			SystemConfigSearchDo search=new SystemConfigSearchDo();
			search.setStatus((byte)1);
			List<SystemConfigDataDo> configList=systemConfigAdapter.findList(search);
			if(ObjectUtils.isNotEmpty(configList)) {
				configMap=configList.stream().collect(Collectors.toMap(c -> c.getKey(), c -> convertPojo(c, SystemConfigCho.class)));
			}
			memoryCache.set(CacheEnum.CONFIG_MAP.getKey(), configMap);
		}
		if(ObjectUtils.isNotEmpty(configMap)) {
			return (T)configMap.get(key);
		}
		return null;
	}
	
	@Override
	@ApiMethod(value="根据系统配置key获取配置项的值", params = {@ApiField("配置项的key")}, returns = @ApiField("配置项的值"))
	public String findConfigValueByKey(String key) {
		SystemConfigCho config=this.findConfigByKey(key);
		if(ObjectUtils.isNotEmpty(config)) {
			return config.getValue();
		}
		return null;
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "hiding" })
	@ApiMethod(value="根据系统微服务code获取微服务对象", params = {@ApiField("微服务code")}, returns = @ApiField("微服务对象"))
	public <T extends IBaseCho> T findCloudByCode(String code) {
		Map<String, SystemCloudCho> cloudMap=memoryCache.get(CacheEnum.CLOUD_MAP.getKey());
		if(ObjectUtils.isNull(cloudMap)){//没有被初始化过
			SystemCloudSearchDo search=new SystemCloudSearchDo();
			List<SystemCloudDataDo> cloudList=systemCloudAdapter.findList(search);
			if(ObjectUtils.isNotEmpty(cloudList)) {
				cloudMap=cloudList.stream().collect(Collectors.toMap(c -> c.getCode(), c -> convertPojo(c, SystemCloudCho.class)));
			}
			memoryCache.set(CacheEnum.CLOUD_MAP.getKey(), cloudMap);
		}
		if(ObjectUtils.isNotEmpty(cloudMap)) {
			return (T)cloudMap.get(code);
		}
		return null;
	}
	
	@Override
	@ApiMethod(value="根据系统微服务code获取微服务名称", params = {@ApiField("微服务code")}, returns = @ApiField("微服务名称"))
	public String findCloudNameByCode(String code) {
		SystemCloudCho cloud=this.findCloudByCode(code);
		if(ObjectUtils.isNotEmpty(cloud)) {
			return cloud.getName();
		}
		return null;
	}
}
