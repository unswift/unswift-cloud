package com.unswift.cloud.adapter.system.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.role.resource.SystemRoleResourceExtendMapper;
import com.unswift.cloud.mapper.system.role.resource.SystemRoleResourceMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.role.resource.SystemRoleResourceDataDo;
import com.unswift.cloud.pojo.dao.system.role.resource.SystemRoleResourceDeleteDo;
import com.unswift.cloud.pojo.dao.system.role.resource.SystemRoleResourceSearchDo;
import com.unswift.cloud.pojo.dao.system.role.resource.SystemRoleResourceSingleDo;
import com.unswift.cloud.sql.system.role.resource.SystemRoleResourceSql;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="角色资源公共服务-此bean可以被任何Service引用", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemRoleResourceAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("角色资源数据库操作对象")
	private SystemRoleResourceMapper systemRoleResourceMapper;
	
	@Autowired
	@ApiField("角色资源自定义sql")
	private SystemRoleResourceSql systemRoleResourceSql;
	
	@Autowired
	@ApiField("角色资源数据库扩展操作对象")
	private SystemRoleResourceExtendMapper systemRoleResourceExtendMapper;
	
	@ApiMethod(value="获取角色资源数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemRoleResourceMapper getMapper(){
		return systemRoleResourceMapper;
	}
	
	@ApiMethod(value="获取角色资源模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemRoleResource";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemRoleResourceDataDo findById(Long id){
		return this.findSingle(new SystemRoleResourceSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemRoleResourceDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemRoleResourceSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemRoleResourceDataDo findFirst(SystemRoleResourceSearchDo search, String multipleException){
		List<SystemRoleResourceDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="根据角色id和资源id查询角色资源", params = {@ApiField("角色id"), @ApiField("资源id")}, returns = @ApiField("角色资源"))
	public SystemRoleResourceDataDo findByRoleIdAndResourceId(long roleId, long resourceId) {
		SystemRoleResourceSearchDo search=new SystemRoleResourceSearchDo();
		search.setRoleId(roleId);
		search.setResourceId(resourceId);
		return this.findFirst(search, null);
	}
	
	@ApiMethod(value="根据角色id和资源id查询角色资源", params = {@ApiField("角色id")}, returns = @ApiField("角色资源"))
	public List<SystemRoleResourceDataDo> findByRoleId(long roleId) {
		SystemRoleResourceSearchDo search=new SystemRoleResourceSearchDo();
		search.setRoleId(roleId);
		Sql sql=Sql.createSql();
		sql.addSelect(systemRoleResourceSql.findResourceNameSql(), "resourceName");
		search.setSql(sql);
		List<SystemRoleResourceDataDo> list = this.findList(search);
		for (SystemRoleResourceDataDo roleResource : list) {
			roleResource.setOpenInputName(cacheAdapter.findDictionaryValueByKey("openClose", this.getLanguage(), roleResource.getOpenInput()+""));
			roleResource.setOpenOutputName(cacheAdapter.findDictionaryValueByKey("openClose", this.getLanguage(), roleResource.getOpenOutput()+""));
		}
		return list;
	}
	
	@ApiMethod(value="根据角色id删除对应角色资源", params = @ApiField("角色id"), returns = @ApiField("删除数量"))
	public int deleteByRoleId(long roleId) {
		SystemRoleResourceDeleteDo deleteDo=new SystemRoleResourceDeleteDo();
		Sql sql=Sql.createSql();
		sql.addWhere(Sql.LOGIC_AND, "role_id_", Sql.COMPARE_EQUAL, Sql.sqlValue(roleId));
		deleteDo.setSql(sql);
		return this.delete(deleteDo);
	}
}
