package com.unswift.cloud.adapter.system.organization;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.job.SystemJobExtendMapper;
import com.unswift.cloud.mapper.system.job.SystemJobMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.job.SystemJobDataDo;
import com.unswift.cloud.pojo.dao.system.job.SystemJobInsertDo;
import com.unswift.cloud.pojo.dao.system.job.SystemJobSearchDo;
import com.unswift.cloud.pojo.dao.system.job.SystemJobSingleDo;
import com.unswift.cloud.sql.system.job.SystemJobSql;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;
import com.unswift.utils.StringUtils;

@Component
@Api(value="职位公共服务-此bean可以被任何Service引用", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemJobAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("职位数据库操作对象")
	private SystemJobMapper systemJobMapper;
	
	@Autowired
	@ApiField("职位数据库扩展操作对象")
	private SystemJobExtendMapper systemJobExtendMapper;
	
	@Autowired
	@ApiField("职位自定义sql")
	private SystemJobSql systemJobSql;
	
	@ApiMethod(value="获取职位数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemJobMapper getMapper(){
		return systemJobMapper;
	}
	
	@ApiMethod(value="获取职位模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemJob";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemJobDataDo findById(Long id){
		return this.findSingle(new SystemJobSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemJobDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemJobSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemJobDataDo findFirst(SystemJobSearchDo search, String multipleException){
		List<SystemJobDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="职位编码是否已存在", params = {@ApiField("职位编码")}, returns = @ApiField("是否存在，true：已存在，false：不存在"))
    public boolean existsByCode(String code) {
    	SystemJobSearchDo search=new SystemJobSearchDo();
    	search.setCode(code);
    	return this.findCount(search)>0;
    }
	
	@ApiMethod(value="根据职位编码查询职位信息", params = {@ApiField("职位编码")}, returns = @ApiField("职位信息"))
	public SystemJobDataDo findByCode(String code) {
		SystemJobSearchDo search=new SystemJobSearchDo();
		search.setCode(code);
		return this.findFirst(search, String.format("部门编码“%s”", code));
	}
	
	@ApiMethod(value="职位名称是否已存在", params = {@ApiField("职位名称"), @ApiField("排除的职位id")}, returns = @ApiField("是否存在，true：已存在，false：不存在"))
	public boolean existsByName(String name, Long excludeId) {
		SystemJobSearchDo search=new SystemJobSearchDo();
		search.setName(name);
		this.addWhereNotEquals(search, "t.id_", excludeId, null);
		return this.findCount(search)>0;
	}
	
	@ApiMethod(value="根据职位名称查询职位信息", params = {@ApiField("职位名称")}, returns = @ApiField("职位信息"))
	public SystemJobDataDo findByName(String name) {
		SystemJobSearchDo search=new SystemJobSearchDo();
		search.setName(name);
		return this.findFirst(search, String.format("部门名称“%s”", name));
	}
	
	@ApiMethod(value="插入数据前设置默认值", params = {@ApiField("插入的数据")})
    public void setInsertDefault(SystemJobInsertDo insert) {
		insert.setStatus((byte)1);
		if(ObjectUtils.isEmpty(insert.getSort())) {
			SystemJobSearchDo search=new SystemJobSearchDo();
			insert.setSort(this.findCount(search)+1);
		}
    }
	
	@ApiMethod(value="获取最大排序值（数据库最大值+1）", returns = @ApiField("最大排序值"))
    public int findMaxSort() {
		SystemJobSearchDo search=new SystemJobSearchDo();
		return this.findCount(search)+1;
    }
	
	@ApiMethod(value="根据职位编码获取职位名称", params = {@ApiField("职位编码列表")}, returns = @ApiField("职位名称，多个用‘,’隔开"))
	public String findNamesByCodes(List<String> codeList) {
		SystemJobSearchDo search=new SystemJobSearchDo();
		this.addSelectColumn(search, "name_", "name", false);
		this.addWhereInList(search, "code_", codeList);
		this.addOrderBy(search, "sort_", Sql.ORDER_BY_ASC);
		List<SystemJobDataDo> list=this.findList(search);
		if(ObjectUtils.isNotEmpty(list)) {
			return StringUtils.join(list.stream().map(j -> j.getName()).collect(Collectors.toList()), ",");
		}
		return null;
	}
	
	@ApiMethod(value="根据部门编码获取职位名称", params = {@ApiField("部门编码")}, returns = @ApiField("职位名称，多个用‘,’隔开"))
	public String findNamesByDepartmentCode(String departmentCode) {
		SystemJobSearchDo search=new SystemJobSearchDo();
		this.addSelectColumn(search, "name_", "name", false);
		this.addWhereIn(search, "code_", Sql.format(systemJobSql.findJobCodeByDepartmentCodeSql(), Sql.sqlValue(departmentCode)));
		this.addOrderBy(search, "sort_", Sql.ORDER_BY_ASC);
		List<SystemJobDataDo> list=this.findList(search);
		if(ObjectUtils.isNotEmpty(list)) {
			return StringUtils.join(list.stream().map(j -> j.getName()).collect(Collectors.toList()), ",");
		}
		return null;
	}
	
	@ApiMethod(value="根据部门编码获取职位编码列表", params = {@ApiField("部门编码")}, returns = @ApiField("职位编码列表"))
	public List<String> findCodesByDepartmentCode(String departmentCode) {
		SystemJobSearchDo search=new SystemJobSearchDo();
		this.addSelectColumn(search, "code_", "code", false);
		this.addWhereIn(search, "code_", Sql.format(systemJobSql.findJobCodeByDepartmentCodeSql(), Sql.sqlValue(departmentCode)));
		this.addOrderBy(search, "sort_", Sql.ORDER_BY_ASC);
		List<SystemJobDataDo> list=this.findList(search);
		if(ObjectUtils.isNotEmpty(list)) {
			return list.stream().map(j -> j.getCode()).collect(Collectors.toList());
		}
		return null;
	}
}
