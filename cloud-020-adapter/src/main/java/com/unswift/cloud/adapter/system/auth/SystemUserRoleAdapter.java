package com.unswift.cloud.adapter.system.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.user.role.SystemUserRoleExtendMapper;
import com.unswift.cloud.mapper.system.user.role.SystemUserRoleMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.user.role.SystemUserRoleDataDo;
import com.unswift.cloud.pojo.dao.system.user.role.SystemUserRoleDeleteDo;
import com.unswift.cloud.pojo.dao.system.user.role.SystemUserRoleSearchDo;
import com.unswift.cloud.pojo.dao.system.user.role.SystemUserRoleSingleDo;
import com.unswift.cloud.sql.system.user.role.SystemUserRoleSql;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="会员角色公共服务-此bean可以被任何Service引用", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemUserRoleAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("会员角色数据库操作对象")
	private SystemUserRoleMapper systemUserRoleMapper;
	
	@Autowired
	@ApiField("会员角色数据库扩展操作对象")
	private SystemUserRoleExtendMapper systemUserRoleExtendMapper;
	
	
	@Autowired
	@ApiField("会员角色自定义sql")
	private SystemUserRoleSql systemUserRoleSql;
	
	@ApiMethod(value="获取会员角色数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemUserRoleMapper getMapper(){
		return systemUserRoleMapper;
	}
	
	@ApiMethod(value="获取会员角色模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemUserRole";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemUserRoleDataDo findById(Long id){
		return this.findSingle(new SystemUserRoleSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemUserRoleDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemUserRoleSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemUserRoleDataDo findFirst(SystemUserRoleSearchDo search, String multipleException){
		List<SystemUserRoleDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="角色id是否存在关联", params={@ApiField("角色id")}, returns=@ApiField("是否存在{true：存在关联，false：不存在关联"))
	public boolean existsByRoleId(Long roleId){
		SystemUserRoleSearchDo search=new SystemUserRoleSearchDo();
		search.setRoleId(roleId);
		int count=this.findCount(search);
		return count>0;
	}
	
	@ApiMethod(value="根据用户id删除对应用户角色", params = {@ApiField("用户id"), @ApiField("权限类型{occupant：使用者，manager：管理者}")}, returns = @ApiField("删除数量"))
	public int deleteByUserId(long userId, String authorityType) {
		SystemUserRoleDeleteDo deleteDo=new SystemUserRoleDeleteDo();
		this.addWhereEquals(deleteDo, "user_id_", userId, null);
		this.addWhereEquals(deleteDo, "authority_type_", authorityType, null);
		return this.delete(deleteDo);
	}
	
	@ApiMethod(value="根据用户id查询用户角色", params={@ApiField("用户id"), @ApiField("权限类型{occupant：使用者，manager：管理者}")}, returns=@ApiField("符合条件的权限列表"))
	public List<SystemUserRoleDataDo> findByUserId(Long userId, String authorityType){
		SystemUserRoleSearchDo search=new SystemUserRoleSearchDo();
		search.setUserId(userId);
		search.setAuthorityType(authorityType);
		Sql sql=Sql.createSql();
		sql.addSelect(systemUserRoleSql.findRoleNameSql(), "roleName");
		search.setSql(sql);
		return this.findList(search);
	}
}
