package com.unswift.cloud.adapter.system.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.annotation.api.ApiVersionAdd;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.api.version.SystemApiVersionExtendMapper;
import com.unswift.cloud.mapper.system.api.version.SystemApiVersionMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.api.constructor.SystemApiConstructorInsertBatchItemDo;
import com.unswift.cloud.pojo.dao.system.api.field.SystemApiFieldInsertBatchItemDo;
import com.unswift.cloud.pojo.dao.system.api.method.SystemApiMethodInsertBatchItemDo;
import com.unswift.cloud.pojo.dao.system.api.version.SystemApiVersionDataDo;
import com.unswift.cloud.pojo.dao.system.api.version.SystemApiVersionDeleteDo;
import com.unswift.cloud.pojo.dao.system.api.version.SystemApiVersionInsertDo;
import com.unswift.cloud.pojo.dao.system.api.version.SystemApiVersionSearchDo;
import com.unswift.cloud.pojo.dao.system.api.version.SystemApiVersionSingleDo;
import com.unswift.utils.DateUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="类版本api公共服务-此bean可以被任何Service引用", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiVersionAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("类版本api数据库操作对象")
	private SystemApiVersionMapper systemApiVersionMapper;
	
	@Autowired
	@ApiField("类版本api数据库扩展操作对象")
	private SystemApiVersionExtendMapper systemApiVersionExtendMapper;
	
	@ApiMethod(value="获取类版本api数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemApiVersionMapper getMapper(){
		return systemApiVersionMapper;
	}
	
	@ApiMethod(value="获取类版本api模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemApiVersion";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemApiVersionDataDo findById(Long id){
		return this.findSingle(new SystemApiVersionSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemApiVersionDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemApiVersionSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemApiVersionDataDo findFirst(SystemApiVersionSearchDo search, String multipleException){
		List<SystemApiVersionDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="解析Api版本注解", params = {@ApiField("Api版本注解"), @ApiField("关联id"), @ApiField("关联的表")})
	public void handleVersionsAnno(ApiVersionAdd[] versions, long relationId, String relationTable) {
		SystemApiVersionDeleteDo delete=new SystemApiVersionDeleteDo();
        Sql sql=Sql.createSql();
        sql.addWhere(Sql.LOGIC_AND, "relation_id_", Sql.COMPARE_EQUAL, relationId);
        sql.addWhere(Sql.LOGIC_AND, "relation_table_", Sql.COMPARE_EQUAL, Sql.sqlValue(relationTable));
        delete.setSql(sql);
        this.deleteReal(delete);
		
		if(ObjectUtils.isNotEmpty(versions)) {
			SystemApiVersionInsertDo insert;
			int sort=1;
			for (ApiVersionAdd version : versions) {
				insert=new SystemApiVersionInsertDo();
				insert.setRelationId(relationId);
				insert.setRelationTable(relationTable);
				insert.setComment(version.value());
				insert.setAuthor(version.author());
				insert.setDate(DateUtils.parse(version.date(), "yyyy-MM-dd"));
				insert.setVersion(version.version());
				insert.setSort(sort);
				sort++;
				this.save(insert, false);
			}
		}
	}
	
	@ApiMethod(value="根据字段列表解析Api版本注解", params = {@ApiField("字段列表")})
	public void handleVersionsAnno(List<SystemApiFieldInsertBatchItemDo> fieldList) {
		if(ObjectUtils.isNotEmpty(fieldList)) {
			SystemApiVersionInsertDo insert;
			int sort;
			ApiField fieldAnno;
			for (SystemApiFieldInsertBatchItemDo field : fieldList) {
				fieldAnno=field.getFieldAnno();
				if(ObjectUtils.isEmpty(fieldAnno.versionAppend())) {
					continue;
				}
				sort=1;
				for (ApiVersionAdd version : fieldAnno.versionAppend()) {
					insert=new SystemApiVersionInsertDo();
					insert.setRelationId(field.getId());
					insert.setRelationTable("system_api_field_");
					insert.setComment(version.value());
					insert.setAuthor(version.author());
					insert.setDate(DateUtils.parse(version.date(), "yyyy-MM-dd"));
					insert.setVersion(version.version());
					insert.setSort(sort);
					sort++;
					this.save(insert, false);
				}
			}
		}
	}
	
	@ApiMethod(value="根据构造函数列表解析Api版本注解", params = {@ApiField("字段列表")})
	public void handleConstructorVersionsAnno(List<SystemApiConstructorInsertBatchItemDo> constructorList) {
		if(ObjectUtils.isNotEmpty(constructorList)) {
			SystemApiVersionInsertDo insert;
			int sort;
			ApiConstructor constructorAnno;
			for (SystemApiConstructorInsertBatchItemDo constructor : constructorList) {
				constructorAnno=constructor.getConstructorAnno();
				if(ObjectUtils.isEmpty(constructorAnno.versionAppend())) {
					continue;
				}
				sort=1;
				for (ApiVersionAdd version : constructorAnno.versionAppend()) {
					insert=new SystemApiVersionInsertDo();
					insert.setRelationId(constructor.getId());
					insert.setRelationTable("system_api_constructor_");
					insert.setComment(version.value());
					insert.setAuthor(version.author());
					insert.setDate(DateUtils.parse(version.date(), "yyyy-MM-dd"));
					insert.setVersion(version.version());
					insert.setSort(sort);
					sort++;
					this.save(insert, false);
				}
			}
		}
	}
	
	@ApiMethod(value="根据方法列表解析Api版本注解", params = {@ApiField("字段列表")})
	public void handleMethodVersionsAnno(List<SystemApiMethodInsertBatchItemDo> methodList) {
		if(ObjectUtils.isNotEmpty(methodList)) {
			SystemApiVersionInsertDo insert;
			int sort;
			ApiMethod methodAnno;
			for (SystemApiMethodInsertBatchItemDo method : methodList) {
				methodAnno=method.getMethodAnno();
				if(ObjectUtils.isEmpty(methodAnno.versionAppend())) {
					continue;
				}
				sort=1;
				for (ApiVersionAdd version : methodAnno.versionAppend()) {
					insert=new SystemApiVersionInsertDo();
					insert.setRelationId(method.getId());
					insert.setRelationTable("system_api_method_");
					insert.setComment(version.value());
					insert.setAuthor(version.author());
					insert.setDate(DateUtils.parse(version.date(), "yyyy-MM-dd"));
					insert.setVersion(version.version());
					insert.setSort(sort);
					sort++;
					this.save(insert, false);
				}
			}
		}
	}
	
	@ApiMethod(value="根据类Api数据id删除字段、构造函数、方法的版本信息", params = @ApiField("类API数据id"))
	public void deleteByClassId(long apiClassId) {
		//删除字段版本
		SystemApiVersionDeleteDo delete=new SystemApiVersionDeleteDo();
        Sql sql=Sql.createSql();
        Sql searchSql=Sql.createSql();
        searchSql.addSelect("id_");
        searchSql.addFrom("system_api_field_");
        searchSql.addWhere(Sql.LOGIC_AND, "relation_id_", Sql.COMPARE_EQUAL, apiClassId);
        searchSql.addWhere(Sql.LOGIC_AND, "relation_table_", Sql.COMPARE_EQUAL, Sql.sqlValue("system_api_class_"));
        sql.addWhere(Sql.LOGIC_AND, "relation_id_", Sql.COMPARE_IN, searchSql.toSql());
        sql.addWhere(Sql.LOGIC_AND, "relation_table_", Sql.COMPARE_EQUAL, Sql.sqlValue("system_api_field_"));
        delete.setSql(sql);
        this.deleteReal(delete);
        //删除构造函数版本
        sql=Sql.createSql();
        searchSql=Sql.createSql();
        searchSql.addSelect("id_");
        searchSql.addFrom("system_api_constructor_");
        searchSql.addWhere(Sql.LOGIC_AND, "api_class_id_", Sql.COMPARE_EQUAL, apiClassId);
        sql.addWhere(Sql.LOGIC_AND, "relation_id_", Sql.COMPARE_IN, searchSql.toSql());
        sql.addWhere(Sql.LOGIC_AND, "relation_table_", Sql.COMPARE_EQUAL, Sql.sqlValue("system_api_constructor_"));
        delete.setSql(sql);
        this.deleteReal(delete);
        //删除方法版本
        sql=Sql.createSql();
        searchSql=Sql.createSql();
        searchSql.addSelect("id_");
        searchSql.addFrom("system_api_method_");
        searchSql.addWhere(Sql.LOGIC_AND, "api_class_id_", Sql.COMPARE_EQUAL, apiClassId);
        sql.addWhere(Sql.LOGIC_AND, "relation_id_", Sql.COMPARE_IN, searchSql.toSql());
        sql.addWhere(Sql.LOGIC_AND, "relation_table_", Sql.COMPARE_EQUAL, Sql.sqlValue("system_api_method_"));
        delete.setSql(sql);
        this.deleteReal(delete);
	}
}
