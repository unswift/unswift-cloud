package com.unswift.cloud.adapter.system.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.role.input.SystemRoleInputExtendMapper;
import com.unswift.cloud.mapper.system.role.input.SystemRoleInputMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.role.input.SystemRoleInputDataDo;
import com.unswift.cloud.pojo.dao.system.role.input.SystemRoleInputDeleteDo;
import com.unswift.cloud.pojo.dao.system.role.input.SystemRoleInputSearchDo;
import com.unswift.cloud.pojo.dao.system.role.input.SystemRoleInputSingleDo;
import com.unswift.cloud.sql.system.role.input.SystemRoleInputSql;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="系统角色入参公共服务-此bean可以被任何Service引用", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemRoleInputAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("系统角色入参数据库操作对象")
	private SystemRoleInputMapper systemRoleInputMapper;
	
	@Autowired
	@ApiField("系统角色入参数据库扩展操作对象")
	private SystemRoleInputExtendMapper systemRoleInputExtendMapper;
	
	@Autowired
	@ApiField("系统角色入参自定义sql")
	private SystemRoleInputSql systemRoleInputSql;
	
	@ApiMethod(value="获取系统角色入参数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemRoleInputMapper getMapper(){
		return systemRoleInputMapper;
	}
	
	@ApiMethod(value="获取系统角色入参模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemRoleInput";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemRoleInputDataDo findById(Long id){
		return this.findSingle(new SystemRoleInputSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemRoleInputDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemRoleInputSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemRoleInputDataDo findFirst(SystemRoleInputSearchDo search, String multipleException){
		List<SystemRoleInputDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="根据角色id和资源id查询匹配的入参id列表", params = {@ApiField("角色id"), @ApiField("资源id"), @ApiField("是否排序，true：排序，false：不排序")}, returns = @ApiField("角色入参列表"))
	public List<SystemRoleInputDataDo> findInputIdByRoleIdAndResourceId(long roleId, long resourceId, boolean isSort){
		SystemRoleInputSearchDo search=new SystemRoleInputSearchDo();
		search.setRoleId(roleId);
		search.setResourceId(resourceId);
		Sql sql=Sql.createSql();
		sql.addSelect("input_id_", "inputId");
		sql.addSelect(systemRoleInputSql.findRoleInputNameSql(), "inputName");
		sql.setBaseSelect(false);
		if(isSort) {
			sql.addOrderBy("sort_", Sql.ORDER_BY_ASC);
		}
		search.setSql(sql);
		List<SystemRoleInputDataDo> list = systemRoleInputMapper.findList(search);
		return list;
	}
	
	@ApiMethod(value="根据角色id和资源id删除对应角色入参", params = {@ApiField("角色id"), @ApiField("资源id")}, returns = @ApiField("删除数量"))
	public int deleteByRoleIdAndResourceId(long roleId, long resourceId) {
		SystemRoleInputDeleteDo deleteDo=new SystemRoleInputDeleteDo();
		Sql sql=Sql.createSql();
		sql.addWhere(Sql.LOGIC_AND, "role_id_", Sql.COMPARE_EQUAL, Sql.sqlValue(roleId));
		sql.addWhere(Sql.LOGIC_AND, "resource_id_", Sql.COMPARE_EQUAL, Sql.sqlValue(resourceId));
		deleteDo.setSql(sql);
		return this.delete(deleteDo);
	}
	
	@ApiMethod(value="根据角色id删除对应角色入参", params = {@ApiField("角色id")}, returns = @ApiField("删除数量"))
	public int deleteByRoleId(long roleId) {
		SystemRoleInputDeleteDo deleteDo=new SystemRoleInputDeleteDo();
		Sql sql=Sql.createSql();
		sql.addWhere(Sql.LOGIC_AND, "role_id_", Sql.COMPARE_EQUAL, Sql.sqlValue(roleId));
		deleteDo.setSql(sql);
		return this.delete(deleteDo);
	}
}
