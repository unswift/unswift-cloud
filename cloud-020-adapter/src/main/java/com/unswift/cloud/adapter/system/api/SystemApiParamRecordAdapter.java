package com.unswift.cloud.adapter.system.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.api.param.record.SystemApiParamRecordExtendMapper;
import com.unswift.cloud.mapper.system.api.param.record.SystemApiParamRecordMapper;
import com.unswift.cloud.pojo.dao.system.api.param.record.SystemApiParamRecordDataDo;
import com.unswift.cloud.pojo.dao.system.api.param.record.SystemApiParamRecordSearchDo;
import com.unswift.cloud.pojo.dao.system.api.param.record.SystemApiParamRecordSingleDo;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="api接口参数执行记录公共服务-此bean可以被任何Service引用", author="unswift", date="2023-09-16", version="1.0.0")
public class SystemApiParamRecordAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("api接口参数执行记录数据库操作对象")
	private SystemApiParamRecordMapper systemApiParamRecordMapper;
	
	@Autowired
	@ApiField("api接口参数执行记录数据库扩展操作对象")
	private SystemApiParamRecordExtendMapper systemApiParamRecordExtendMapper;
	
	@ApiMethod(value="获取api接口参数执行记录数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemApiParamRecordMapper getMapper(){
		return systemApiParamRecordMapper;
	}
	
	@ApiMethod(value="获取api接口参数执行记录模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemApiParamRecord";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemApiParamRecordDataDo findById(Long id){
		return this.findSingle(new SystemApiParamRecordSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemApiParamRecordDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemApiParamRecordSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemApiParamRecordDataDo findFirst(SystemApiParamRecordSearchDo search, String multipleException){
		List<SystemApiParamRecordDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
}
