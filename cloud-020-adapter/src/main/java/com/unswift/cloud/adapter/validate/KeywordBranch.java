package com.unswift.cloud.adapter.validate;

import java.util.ArrayList;
import java.util.List;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.cho.system.form.validate.SystemFormValidateCho;
import com.unswift.utils.ObjectUtils;

@ApiEntity(value="表单验证关键词分支，存储关键词的执行线路，firstResult决定总语句是否执行，lastResult决定最后分支执行结果", author="unswift", date="2023-06-06", version="1.0.0")
public class KeywordBranch {

	@ApiField("代码关键词")
	private String keyword;
	@ApiField("代码层级")
	private Integer codeLevel;
	@ApiField("关键词当前执行结果，if，case，foreach语句的结果")
	private Boolean firstResult;
	@ApiField("最后一次执行结果，elseif、else、when then语句执行结果")
	private Boolean lastResult;
	@ApiField("关键词代码列表")
	private List<SystemFormValidateCho> codeBodyList;
	
	@ApiConstructor("默认构造")
	public KeywordBranch() {
		
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("代码层级"), @ApiField("关键词当前执行结果，if，case，foreach语句的结果"), @ApiField("最后一次执行结果，elseif、else、when then语句执行结果")})
	public KeywordBranch(String keyword, Integer codeLevel, Boolean lastResult) {
		super();
		this.keyword = keyword;
		this.codeLevel = codeLevel;
		this.firstResult = lastResult;
		this.lastResult = lastResult;
	}

	@ApiConstructor(value="带参构造", params={@ApiField("代码层级"), @ApiField("关键词当前执行结果，if，case，foreach语句的结果"), @ApiField("最后一次执行结果，elseif、else、when then语句执行结果"), @ApiField("关键词代码列表")})
	public KeywordBranch(String keyword, Integer codeLevel, Boolean lastResult,	List<SystemFormValidateCho> codeBodyList) {
		super();
		this.keyword=keyword;
		this.codeLevel = codeLevel;
		this.firstResult = lastResult;
		this.lastResult = lastResult;
		this.codeBodyList = codeBodyList;
	}

	public Integer getCodeLevel() {
		return codeLevel;
	}
	public void setCodeLevel(Integer codeLevel) {
		this.codeLevel = codeLevel;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Boolean getFirstResult() {
		return firstResult;
	}
	public void setFirstResult(Boolean firstResult) {
		this.firstResult = firstResult;
	}
	public Boolean getLastResult() {
		return lastResult;
	}

	public void setLastResult(Boolean lastResult, boolean isUpdateFirst) {
		this.lastResult = lastResult;
		if(lastResult && isUpdateFirst){
			this.firstResult = lastResult;
		}
	}
	public List<SystemFormValidateCho> getCodeBodyList() {
		return codeBodyList;
	}
	public void setCodeBodyList(List<SystemFormValidateCho> codeBodyList) {
		this.codeBodyList = codeBodyList;
	}
	public void addCodeBody(SystemFormValidateCho codeBody) {
		if(ObjectUtils.isNull(codeBodyList)){
			codeBodyList=new ArrayList<SystemFormValidateCho>();
		}
		codeBodyList.add(codeBody);
	}
}
