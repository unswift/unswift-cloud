package com.unswift.cloud.adapter.validate;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

@Api(value="表单验证基础配置key枚举", author="unswift", date="2023-06-09", version="1.0.0")
public enum ValidateConfigEnum {
	OPERATIONAL_SCALE("operationalScale", "默认运算精度（保留的小数位数）"),
	OPERATIONAL_ROUNDING_MODE("operationalRoundingMode", "默认运算精度舍入模式（可查阅BigDecimal舍入模式）{0、进位，1：舍去，2：正进负舍，3：正舍负入,4：四舍五入，5：五舍六入，6：左奇进偶舍，7、断言}"),
	;
	
	@ApiField("配置键")
	private String key;
	@ApiField("配置键描述")
	private String describe;
	
	private ValidateConfigEnum(String key, String describe) {
		this.key = key;
		this.describe = describe;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
}
