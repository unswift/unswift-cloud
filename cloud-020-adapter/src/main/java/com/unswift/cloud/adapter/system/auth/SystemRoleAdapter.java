package com.unswift.cloud.adapter.system.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.enums.system.role.AuthorityTypeEnum;
import com.unswift.cloud.mapper.system.role.SystemRoleExtendMapper;
import com.unswift.cloud.mapper.system.role.SystemRoleMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.role.SystemRoleDataDo;
import com.unswift.cloud.pojo.dao.system.role.SystemRoleInsertDo;
import com.unswift.cloud.pojo.dao.system.role.SystemRoleSearchDo;
import com.unswift.cloud.pojo.dao.system.role.SystemRoleSingleDo;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="系统角色公共服务-此bean可以被任何Service引用", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRoleAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("系统角色数据库操作对象")
	private SystemRoleMapper systemRoleMapper;
	
	@Autowired
	@ApiField("系统角色数据库扩展操作对象")
	private SystemRoleExtendMapper systemRoleExtendMapper;
	
	@ApiMethod(value="获取系统角色数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemRoleMapper getMapper(){
		return systemRoleMapper;
	}
	
	@ApiMethod(value="获取系统角色模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemRole";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemRoleDataDo findById(Long id){
		return this.findSingle(new SystemRoleSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemRoleDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemRoleSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemRoleDataDo findFirst(SystemRoleSearchDo search, String multipleException){
		List<SystemRoleDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
    @ApiMethod(value="根据用户id及权限类型枚举查询用户的角色", params={@ApiField("用户id"), @ApiField("是否管理员{1：是，0：否}"), @ApiField("权限类型枚举{occupant：使用者，manager：管理者)}")})
    public List<SystemRoleDataDo> findByUserId(Long userId, byte isAdmin, AuthorityTypeEnum authorityType){
    	SystemRoleSearchDo search=new SystemRoleSearchDo();
    	search.setStatus((byte)1);
        if(isAdmin==1) {
            search.setIsAdmin(isAdmin);
        }else {
            Sql sql=Sql.createSql();
            sql.addSelect("role_id_");
            sql.addFrom("system_user_role_");
            sql.addWhere(Sql.LOGIC_AND, "is_delete_", Sql.COMPARE_EQUAL, 0);
            sql.addWhere(Sql.LOGIC_AND, "user_id_", Sql.COMPARE_EQUAL, userId);
            sql.addWhere(Sql.LOGIC_AND, "authority_type_", Sql.COMPARE_EQUAL, Sql.sqlValue(authorityType.getKey()));
            String inSql=sql.toSql();
            sql=Sql.createSql();
            sql.addWhere(Sql.LOGIC_AND, "id_", Sql.COMPARE_IN, inSql);
            search.setSql(sql);
        }
        return this.findList(search);
	}
    
    @ApiMethod(value="插入数据前设置默认值", params = {@ApiField("插入的数据")})
    public void setInsertDefault(SystemRoleInsertDo insert) {
    	insert.setIsAdmin((byte)0);
    	insert.setStatus((byte)1);
    }
    
    @ApiMethod(value="是否存在角色编码表示的角色", params = @ApiField("角色编码"), returns = @ApiField("是否存在，true：存在，false：不存在"))
    public boolean existsByKey(String key) {
    	SystemRoleSearchDo search=new SystemRoleSearchDo();
    	search.setKey(key);
    	return this.findCount(search)>0;
    }
    
    @ApiMethod(value="是否存在角色名称表示的角色", params = {@ApiField("角色名称"), @ApiField("排除的角色id")}, returns = @ApiField("是否存在，true：存在，false：不存在"))
    public boolean existsByName(String name, Long excludeId) {
    	SystemRoleSearchDo search=new SystemRoleSearchDo();
    	search.setName(name);
    	this.addWhereNotEquals(search, "t.id_", excludeId, null);
    	return this.findCount(search)>0;
    }
    
    @ApiMethod(value="根据角色名称查询角色", params = @ApiField("角色名称"), returns = @ApiField("角色对象"))
    public SystemRoleDataDo findByName(String name) {
    	SystemRoleSearchDo search=new SystemRoleSearchDo();
    	search.setName(name);
    	return this.findFirst(search, String.format("角色名称%s", name));
    }
}
