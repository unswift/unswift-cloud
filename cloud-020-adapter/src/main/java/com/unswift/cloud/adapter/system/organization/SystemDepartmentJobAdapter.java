package com.unswift.cloud.adapter.system.organization;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.enums.tree.TreeIconEnum;
import com.unswift.cloud.mapper.system.department.job.SystemDepartmentJobExtendMapper;
import com.unswift.cloud.mapper.system.department.job.SystemDepartmentJobMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.department.SystemDepartmentDataDo;
import com.unswift.cloud.pojo.dao.system.department.job.SystemDepartmentJobDataDo;
import com.unswift.cloud.pojo.dao.system.department.job.SystemDepartmentJobSearchDo;
import com.unswift.cloud.pojo.dao.system.department.job.SystemDepartmentJobSingleDo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserDepartmentJobTreeVo;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="部门职位公共服务-此bean可以被任何Service引用", author="liyunlong", date="2024-01-22", version="1.0.0")
public class SystemDepartmentJobAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("部门职位数据库操作对象")
	private SystemDepartmentJobMapper systemDepartmentJobMapper;
	
	@Autowired
	@ApiField("部门职位数据库扩展操作对象")
	private SystemDepartmentJobExtendMapper systemDepartmentJobExtendMapper;
	
	@ApiMethod(value="获取部门职位数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemDepartmentJobMapper getMapper(){
		return systemDepartmentJobMapper;
	}
	
	@ApiMethod(value="获取部门职位模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemDepartmentJob";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemDepartmentJobDataDo findById(Long id){
		return this.findSingle(new SystemDepartmentJobSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemDepartmentJobDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemDepartmentJobSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemDepartmentJobDataDo findFirst(SystemDepartmentJobSearchDo search, String multipleException){
		List<SystemDepartmentJobDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="根据部门编码获取职位编码Map", params = {@ApiField("部门编码")}, returns = @ApiField("职位编码Map，key=职位编码，value=id"))
	public Map<String, Long> findJobCodeMapByDepartmentCode(String departmentCode) {
		SystemDepartmentJobSearchDo search=new SystemDepartmentJobSearchDo();
		search.setDepartmentCode(departmentCode);
		this.addOrderBy(search, "sort_", Sql.ORDER_BY_ASC);
		List<SystemDepartmentJobDataDo> list=this.findList(search);
		if(ObjectUtils.isNotEmpty(list)) {
			return list.stream().collect(Collectors.toMap(dj -> dj.getJobCode(), dj -> dj.getId(), (V1, V2)-> V1));
		}
		return new HashMap<String, Long>();
	}
	
	@ApiMethod(value="根据部门编码和职位编码查询", params = {@ApiField("部门编码"), @ApiField("职位编码")}, returns = @ApiField("匹配的对象"))
	public SystemDepartmentJobDataDo findByCode(String departmentCode, String jobCode) {
		SystemDepartmentJobSearchDo search=new SystemDepartmentJobSearchDo();
		search.setDepartmentCode(departmentCode);
		search.setJobCode(jobCode);
		return this.findFirst(search, String.format("部门编码‘%s’，职位编码‘%s’", departmentCode, jobCode));
	}
	
	@ApiMethod(value="将部门、岗位列表解析为树结构", params = {@ApiField("部门列表"), @ApiField("部门岗位列表"), @ApiField("父部门树"), @ApiField("选择的节点")})
    public void parseDepartmentJobToTree(List<SystemDepartmentDataDo> list, List<SystemDepartmentJobDataDo> jobList, SystemUserDepartmentJobTreeVo parent, List<List<Object>> checkedList) {
    	SystemUserDepartmentJobTreeVo treeNode;
    	for (SystemDepartmentDataDo department : list) {
			if(ObjectUtils.isEmpty(department.getParentId())) {
				continue;
			}
    		if(department.getParentId().equals(Long.parseLong(parent.getId()))) {
    			treeNode=new SystemUserDepartmentJobTreeVo(department.getId()+"", "department", department.getName());
    			treeNode.setSpread(true);
    			if(checkedList.stream().filter(s -> s.size()==1 && Long.parseLong(s.get(0).toString())==department.getId()).findAny().isPresent()) {
    				treeNode.setChecked(true);
    			}
    			parent.addChild(treeNode);
    			parseDepartmentJobToTree(list, jobList, treeNode, checkedList);
    			parseJobToTree(jobList, treeNode, checkedList);
    			
    		}
		}
    }
	
	@ApiMethod(value="将岗位列表解析为树结构", params = {@ApiField("部门岗位列表"), @ApiField("父部门树"), @ApiField("选择的节点")})
	public void parseJobToTree(List<SystemDepartmentJobDataDo> jobList, SystemUserDepartmentJobTreeVo parent, List<List<Object>> checkedList) {
		if(ObjectUtils.isEmpty(jobList)) {
			return ;
		}
		List<SystemDepartmentJobDataDo> subJobList=jobList.stream().filter(j -> j.getDepartmentId().equals(Long.parseLong(parent.getId()))).collect(Collectors.toList());
		if(ObjectUtils.isEmpty(subJobList)) {
			return ;
		}
		SystemUserDepartmentJobTreeVo treeNode;
		boolean checked;
		for (SystemDepartmentJobDataDo job : subJobList) {
			checked=checkedList.stream().filter(c -> c.size()==2 && Long.parseLong(c.get(0).toString())==job.getDepartmentId() && Long.parseLong(c.get(1).toString())==job.getJobId()).findAny().isPresent();
			treeNode=new SystemUserDepartmentJobTreeVo(job.getDepartmentId()+","+job.getJobId(), "departmentJob", job.getJobName(), false, checked);
			treeNode.setIcon(TreeIconEnum.USERS.getKey());
			treeNode.setDepartmentName(parent.getTitle());
			parent.addChild(treeNode);
		}
	}
	
	@ApiMethod(value="部门、职位id是否已存在关联", params = {@ApiField("部门id"), @ApiField("职位id")}, returns = @ApiField("是否存在，true：已存在，false：不存在"))
    public boolean existsByDepartmentIdAndJobId(Long departmentId, Long jobId) {
		SystemDepartmentJobSearchDo search=new SystemDepartmentJobSearchDo();
		this.addJoinTable(search, "system_job_", "j", "t.job_code_=j.code_ and j.is_delete_=0");
		this.addJoinTable(search, "system_department_", "d", "t.department_code_=d.code_ and d.is_delete_=0");
		this.addWhereEquals(search, "d.id_", departmentId, null);
		this.addWhereEquals(search, "j.id_", jobId, null);
		return this.findCount(search)>0;
    }
	@ApiMethod(value="部门、职位id是否已存在关联", params = {@ApiField("部门id"), @ApiField("职位id")}, returns = @ApiField("是否存在，true：已存在，false：不存在"))
	public SystemDepartmentJobDataDo findByDepartmentIdAndJobId(Long departmentId, Long jobId) {
		SystemDepartmentJobSearchDo search=new SystemDepartmentJobSearchDo();
		this.addJoinTable(search, "system_job_", "j", "t.job_code_=j.code_ and j.is_delete_=0");
		this.addJoinTable(search, "system_department_", "d", "t.department_code_=d.code_ and d.is_delete_=0");
		this.addWhereEquals(search, "d.id_", departmentId, null);
		this.addWhereEquals(search, "j.id_", jobId, null);
		return this.findFirst(search, "部门职位");
	}
}
