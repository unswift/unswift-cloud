package com.unswift.cloud.adapter.system.attach;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.attach.SystemAttachExtendMapper;
import com.unswift.cloud.mapper.system.attach.SystemAttachMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.attach.SystemAttachDataDo;
import com.unswift.cloud.pojo.dao.system.attach.SystemAttachSearchDo;
import com.unswift.cloud.pojo.dao.system.attach.SystemAttachSingleDo;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="附件公共服务-此bean可以被任何Service引用", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("附件数据库操作对象")
	private SystemAttachMapper systemAttachMapper;
	
	@Autowired
	@ApiField("附件数据库扩展操作对象")
	private SystemAttachExtendMapper systemAttachExtendMapper;
	
	@ApiMethod(value="获取附件数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemAttachMapper getMapper(){
		return systemAttachMapper;
	}
	
	@ApiMethod(value="获取附件模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemAttach";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemAttachDataDo findById(Long id){
		return this.findSingle(new SystemAttachSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemAttachDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemAttachSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemAttachDataDo findFirst(SystemAttachSearchDo search, String multipleException){
		List<SystemAttachDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="根据附件路径查询附件信息", params={@ApiField("附件路径")}, returns=@ApiField("附件信息"))
	public SystemAttachDataDo findByFileUrl(String fileUrl) {
		SystemAttachSingleDo search=new SystemAttachSingleDo();
    	Sql sql=Sql.createSql();
    	sql.addWhere(Sql.LOGIC_AND, "file_url_", Sql.COMPARE_EQUAL, Sql.sqlValue(fileUrl));
    	search.setSql(sql);
    	return this.findSingle(search);
	}
	
	@ApiMethod(value="根据模块查询模板", params={@ApiField("模块")}, returns=@ApiField("模板附件信息"))
	public List<SystemAttachDataDo> findModelListByModule(String module) {
		SystemAttachSearchDo search=new SystemAttachSearchDo();
    	search.setClassify("model");
    	search.setDataModule(module);
    	return this.findList(search);
	}
}
