package com.unswift.cloud.adapter.system.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.cache.refresh.SystemCacheRefreshExtendMapper;
import com.unswift.cloud.mapper.system.cache.refresh.SystemCacheRefreshMapper;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshDataDo;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshSearchDo;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshSingleDo;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="缓存刷新公共服务-此bean可以被任何Service引用", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemCacheRefreshAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("缓存刷新数据库操作对象")
	private SystemCacheRefreshMapper systemCacheRefreshMapper;
	
	@Autowired
	@ApiField("缓存刷新数据库扩展操作对象")
	private SystemCacheRefreshExtendMapper systemCacheRefreshExtendMapper;
	
	@ApiMethod(value="获取缓存刷新数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemCacheRefreshMapper getMapper(){
		return systemCacheRefreshMapper;
	}
	
	@ApiMethod(value="获取缓存刷新模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemCacheRefresh";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemCacheRefreshDataDo findById(Long id){
		return this.findSingle(new SystemCacheRefreshSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemCacheRefreshDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemCacheRefreshSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemCacheRefreshDataDo findFirst(SystemCacheRefreshSearchDo search, String multipleException){
		List<SystemCacheRefreshDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
}
