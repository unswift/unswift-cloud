package com.unswift.cloud.adapter.system.api;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.api.method.SystemApiMethodExtendMapper;
import com.unswift.cloud.mapper.system.api.method.SystemApiMethodMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.api.method.SystemApiMethodDataDo;
import com.unswift.cloud.pojo.dao.system.api.method.SystemApiMethodDeleteDo;
import com.unswift.cloud.pojo.dao.system.api.method.SystemApiMethodInsertBatchDo;
import com.unswift.cloud.pojo.dao.system.api.method.SystemApiMethodInsertBatchItemDo;
import com.unswift.cloud.pojo.dao.system.api.method.SystemApiMethodSearchDo;
import com.unswift.cloud.pojo.dao.system.api.method.SystemApiMethodSingleDo;
import com.unswift.utils.DateUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;
import com.unswift.utils.StringUtils;

@Component
@Api(value="类方法api公共服务-此bean可以被任何Service引用", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiMethodAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("类方法api数据库操作对象")
	private SystemApiMethodMapper systemApiMethodMapper;
	
	@Autowired
	@ApiField("类方法api数据库扩展操作对象")
	private SystemApiMethodExtendMapper systemApiMethodExtendMapper;
	
	@ApiMethod(value="获取类方法api数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemApiMethodMapper getMapper(){
		return systemApiMethodMapper;
	}
	
	@ApiMethod(value="获取类方法api模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemApiMethod";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemApiMethodDataDo findById(Long id){
		return this.findSingle(new SystemApiMethodSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemApiMethodDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemApiMethodSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemApiMethodDataDo findFirst(SystemApiMethodSearchDo search, String multipleException){
		List<SystemApiMethodDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="根据构造函数Api注解生产数据", params = {@ApiField("构造函数API注解"), @ApiField("当前类的数据id")})
	public List<SystemApiMethodInsertBatchItemDo> handleMethodAnno(Method[] methods, long apiClassId){
		SystemApiMethodDeleteDo delete=new SystemApiMethodDeleteDo();
		Sql sql=Sql.createSql();
		sql.addWhere(Sql.LOGIC_AND, "api_class_id_", Sql.COMPARE_EQUAL, apiClassId);
		delete.setSql(sql);
		this.deleteReal(delete);
		
		if(ObjectUtils.isNotEmpty(methods)) {
			List<SystemApiMethodInsertBatchItemDo> insertList=new ArrayList<SystemApiMethodInsertBatchItemDo>();
			SystemApiMethodInsertBatchItemDo insert;
			ApiMethod methodAnno;
			int sort=1;
			for (Method method : methods) {
				methodAnno=method.getAnnotation(ApiMethod.class);
				if(ObjectUtils.isEmpty(methodAnno)) {
					continue;
				}
				insert=new SystemApiMethodInsertBatchItemDo();
				insert.setApiClassId(apiClassId);
				insert.setMethodComment(StringUtils.join(methodAnno.value(), "、"));
				insert.setMethodName(method.getName());
				ApiField[] params = methodAnno.params();
				insert.setParamCount(ObjectUtils.isEmpty(params)?0:params.length);
				insert.setModifiers(Modifier.toString(method.getModifiers()));
				insert.setAuthor(methodAnno.author());
				insert.setDate(DateUtils.parse(methodAnno.date(), "yyyy-MM-dd"));
				insert.setDeprecated(ObjectUtils.isNotEmpty(method.getAnnotation(Deprecated.class)));
				insert.setSort(sort);
				insert.setMethodAnno(methodAnno);
				insert.setMethod(method);
				insertList.add(insert);
				sort++;
			}
			if(ObjectUtils.isNotEmpty(insertList)) {
				this.saveBatch(new SystemApiMethodInsertBatchDo(insertList), false);
			}
			return insertList;
		}
		return null;
	}
}
