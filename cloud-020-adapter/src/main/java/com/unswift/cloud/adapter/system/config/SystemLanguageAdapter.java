package com.unswift.cloud.adapter.system.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.language.SystemLanguageExtendMapper;
import com.unswift.cloud.mapper.system.language.SystemLanguageMapper;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageDataDo;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageSearchDo;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageSingleDo;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="系统语言公共服务-此bean可以被任何Service引用", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemLanguageAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("系统语言数据库操作对象")
	private SystemLanguageMapper systemLanguageMapper;
	
	@Autowired
	@ApiField("系统语言数据库扩展操作对象")
	private SystemLanguageExtendMapper systemLanguageExtendMapper;
	
	@ApiMethod(value="获取系统语言数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemLanguageMapper getMapper(){
		return systemLanguageMapper;
	}
	
	@ApiMethod(value="获取系统语言模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemLanguage";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemLanguageDataDo findById(Long id){
		return this.findSingle(new SystemLanguageSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemLanguageDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemLanguageSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemLanguageDataDo findFirst(SystemLanguageSearchDo search, String multipleException){
		List<SystemLanguageDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="是否存在语言简称表示的语言", params = @ApiField("语言简称"), returns = @ApiField("是否存在，true：存在，false：不存在"))
    public boolean existsByShortName(String shortName) {
    	SystemLanguageSearchDo search=new SystemLanguageSearchDo();
    	search.setShortName(shortName);
    	return this.findCount(search)>0;
    }
	
	@ApiMethod(value="是否存在语言全称表示的语言", params = @ApiField("语言全称"), returns = @ApiField("是否存在，true：存在，false：不存在"))
    public boolean existsByFullName(String fullName, Long excludeId) {
    	SystemLanguageSearchDo search=new SystemLanguageSearchDo();
    	search.setFullName(fullName);
    	this.addWhereNotEquals(search, "t.id_", excludeId, null);
    	return this.findCount(search)>0;
    }
	
	@ApiMethod(value="根据语言全称查询表示的语言", params = @ApiField("语言全称"), returns = @ApiField("数据对象"))
    public SystemLanguageDataDo findByFullName(String fullName) {
		Map<String, SystemLanguageDataDo> typeMap=validateAdapter.getAttribute("languageMap");
		if(ObjectUtils.isNull(typeMap)) {
			typeMap=new HashMap<String, SystemLanguageDataDo>();
			validateAdapter.setAttribute("languageMap", typeMap);
		}
		if(typeMap.containsKey(fullName)) {//走缓存
			return typeMap.get(fullName);
		}else {//走数据库查询
			SystemLanguageSearchDo search=new SystemLanguageSearchDo();
	    	search.setFullName(fullName);
	    	SystemLanguageDataDo language=this.findFirst(search, String.format("语言‘%s’", fullName));
	    	typeMap.put(fullName, language);//设置缓存
	    	return language;
		}
    	
    }
}
