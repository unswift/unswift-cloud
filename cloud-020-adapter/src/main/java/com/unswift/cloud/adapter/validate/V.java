package com.unswift.cloud.adapter.validate;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.utils.ObjectUtils;
import com.unswift.utils.StringUtils;

@Api(value="验证方法类", author="unswift", date="2023-06-01", version="1.0.0")
public final class V {

	@ApiMethod(value="判断对象是否为null", params=@ApiField("要判断的对象"), returns=@ApiField("是否为null，true：为null，false：不为null"))
	public boolean isNull(Object source){
		return ObjectUtils.isNull(source);
	}
	
	@ApiMethod(value="判断对象是否不为null", params=@ApiField("要判断的对象"), returns=@ApiField("是否不为null，true：不为null，false：为null"))
	public boolean isNotNull(Object source){
		return ObjectUtils.isNotNull(source);
	}
	
	@ApiMethod(value="判断对象是否为null，如果是字符串包含为空，如果是列表或map，长度必须大于0，如果是数组，则长度必须大于0", params=@ApiField("要判断的对象"), returns=@ApiField("是否为空或null，true：为null或空，false：不为null或空"))
	public boolean isEmpty(Object source){
		return ObjectUtils.isEmpty(source);
	}
	
	@ApiMethod(value="判断对象是否为null，如果是字符串包含为空，如果是列表或map，长度必须大于0，如果是数组，则长度必须大于0", params=@ApiField("要判断的对象"), returns=@ApiField("是否为空或null，true：为null或空，false：不为null或空"))
	public boolean isNotEmpty(Object source){
		return ObjectUtils.isNotEmpty(source);
	}
	
	@ApiMethod(value="判断两个对象是否相等", params={@ApiField("源对象"), @ApiField("目标对象")}, returns=@ApiField("是否相等，true：相等，false：不相等"))
	public boolean equals(Object source, Object target){
		return ObjectUtils.equals(source, target, false);
	}
	
	@ApiMethod(value="判断两个对象是否相等", params={@ApiField("源对象"), @ApiField("目标对象")}, returns=@ApiField("是否相等，true：相等，false：不相等"))
	public boolean equalsIgnoreCase(Object source, Object target){
		return ObjectUtils.equals(source, target, true);
	}
	
	@ApiMethod(value="判断一个字符串是否不满足正则表示的规则", params = {@ApiField("需要检测的字符串"), @ApiField("正则")}, returns = @ApiField("是否不满足，true：不满足，false：满足"))
	public boolean isNotAsciiChar(String source, String regex) {
		return !StringUtils.isAsciiChar(source, regex);
	}
	
	@ApiMethod(value="字符长度匹配，字符串长度在最小值和最大值时间，则返回true，否则返回false", params = {@ApiField("需要验证的字符串"), @ApiField("最小长度"), @ApiField("最大长度")}, returns = @ApiField("是否匹配，true：匹配，false：不匹配"))
	public boolean lengthMatch(String source, Integer minLength, Integer maxLength) {
		if(ObjectUtils.isEmpty(source)) {
			return true;
		}
		int length=source.length();
		if(length >= minLength && length<=maxLength) {
			return true;
		}
		return false;
	}
}
