package com.unswift.cloud.adapter.logger;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.logger.export.task.LoggerExportTaskExtendMapper;
import com.unswift.cloud.mapper.logger.export.task.LoggerExportTaskMapper;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskDataDo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskSearchDo;
import com.unswift.cloud.pojo.dao.logger.export.task.LoggerExportTaskSingleDo;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="导出任务记录表公共服务-此bean可以被任何Service引用", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerExportTaskAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("导出任务记录表数据库操作对象")
	private LoggerExportTaskMapper loggerExportTaskMapper;
	
	@Autowired
	@ApiField("导出任务记录表数据库扩展操作对象")
	private LoggerExportTaskExtendMapper loggerExportTaskExtendMapper;
	
	@ApiMethod(value="获取导出任务记录表数据库操作对象", returns=@ApiField("数据库操作对象"))
	public LoggerExportTaskMapper getMapper(){
		return loggerExportTaskMapper;
	}
	
	@ApiMethod(value="获取导出任务记录表模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "loggerExportTask";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public LoggerExportTaskDataDo findById(Long id){
		return this.findSingle(new LoggerExportTaskSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<LoggerExportTaskDataDo> findByIds(List<Long> idList){
		return this.findList(new LoggerExportTaskSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public LoggerExportTaskDataDo findFirst(LoggerExportTaskSearchDo search, String multipleException){
		List<LoggerExportTaskDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="当前用户是否存在未完结的导出任务", params={@ApiField("查询的用户id"), @ApiField("所属模块")}, returns=@ApiField("是否存在，true：存在，false：不存在"))
	public boolean existsTasking(Long userId, String module){
		LoggerExportTaskSearchDo search=new LoggerExportTaskSearchDo();
		search.setCreateUser(userId);
		search.setModule(module);
		search.setStatus((byte)0);
		this.addWhereInList(search, "status_", ObjectUtils.asList(0, 1));
		return this.findCount(search)>0;
	}
}
