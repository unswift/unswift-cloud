package com.unswift.cloud.adapter.system.auth;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.config.RequestConfig;
import com.unswift.cloud.mapper.system.resource.output.SystemResourceOutputExtendMapper;
import com.unswift.cloud.mapper.system.resource.output.SystemResourceOutputMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.resource.output.SystemResourceOutputDataDo;
import com.unswift.cloud.pojo.dao.system.resource.output.SystemResourceOutputDeleteDo;
import com.unswift.cloud.pojo.dao.system.resource.output.SystemResourceOutputSearchDo;
import com.unswift.cloud.pojo.dao.system.resource.output.SystemResourceOutputSingleDo;
import com.unswift.cloud.pojo.vo.BaseVo;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.resource.output.SystemResourceOutputAuthVo;
import com.unswift.utils.ClassUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="系统资源出参公共服务-此bean可以被任何Service引用", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemResourceOutputAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("请求配置")
	private RequestConfig requestConfig;
	
	@Autowired
	@ApiField("系统资源出参数据库操作对象")
	private SystemResourceOutputMapper systemResourceOutputMapper;
	
	@Autowired
	@ApiField("系统资源出参数据库扩展操作对象")
	private SystemResourceOutputExtendMapper systemResourceOutputExtendMapper;
	
	@ApiMethod(value="获取系统资源出参数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemResourceOutputMapper getMapper(){
		return systemResourceOutputMapper;
	}
	
	@ApiMethod(value="获取系统资源出参模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemResourceOutput";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemResourceOutputDataDo findById(Long id){
		return this.findSingle(new SystemResourceOutputSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemResourceOutputDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemResourceOutputSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemResourceOutputDataDo findFirst(SystemResourceOutputSearchDo search, String multipleException){
		List<SystemResourceOutputDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="根据资源id删除关联出参", params = {@ApiField("资源id")}, returns = @ApiField("删除数量"))
	public int deleteByResourceId(long resourceId) {
		SystemResourceOutputDeleteDo delete=new SystemResourceOutputDeleteDo();
		Sql sql=Sql.createSql();
		sql.addWhere(Sql.LOGIC_AND, "resource_id_", Sql.COMPARE_EQUAL, resourceId);
		delete.setSql(sql);
		return this.delete(delete);
	}
	
	@ApiMethod(value="根据资源id查询出参列表", params = {@ApiField("资源id")}, returns = @ApiField("出参列表"))
	public List<SystemResourceOutputDataDo> findByResourceId(long resourceId){
		SystemResourceOutputSearchDo search=new SystemResourceOutputSearchDo();
		search.setResourceId(resourceId);
		Sql sql=Sql.createSql();
		sql.addOrderBy("sort_", Sql.ORDER_BY_ASC);//排序
		search.setSql(sql);
		return this.findList(search);
	}
	
	@SuppressWarnings("unchecked")
	@ApiMethod(value="根据权限key解析请求入参", params = {@ApiField("权限key")}, returns = @ApiField("方法参数列表"))
	public List<SystemResourceOutputAuthVo> getOutputListByAuthKey(String authKey){
		List<Class<?>> controllerClassList=ClassUtils.findAnnotationList(ObjectUtils.asList(requestConfig.getAuthScanPackage().split("\\,")), ObjectUtils.asList(RestController.class));
		if(ObjectUtils.isNotEmpty(controllerClassList)) {
			Method authMethod=null;
			for (Class<?> controllerClass : controllerClassList) {
				List<Method> methodList=ClassUtils.getAnnotationMethod(controllerClass, Auth.class);
				if(ObjectUtils.isNotEmpty(methodList)) {
					Auth auth;
					for (Method method : methodList) {
						auth=method.getAnnotation(Auth.class);
						if(authKey.equals(auth.value())){
							authMethod=method;
							break;
						}
					}
				}
				if(ObjectUtils.isNotEmpty(authMethod)) {
					break;
				}
			}
			if(ObjectUtils.isNotEmpty(authMethod)) {
				List<SystemResourceOutputAuthVo> outputList=new ArrayList<SystemResourceOutputAuthVo>();
				Set<String> paramSet=new HashSet<String>();
				Class<?> returnType= authMethod.getReturnType();
				if(ObjectUtils.isNotEmpty(returnType) && returnType.equals(ResponseBody.class)) {
					ParameterizedType paramType=(ParameterizedType)authMethod.getGenericReturnType();
					Type actualTypeArguments = paramType.getActualTypeArguments()[0];
					outputList=ClassUtils.getClassFieldList(actualTypeArguments, SystemResourceOutputAuthVo.class , new String[]{"paramKey","paramComment"}, BaseVo.class, 5, paramSet);
				}
				return outputList;
			}
		}
		return null;
	}
	
}
