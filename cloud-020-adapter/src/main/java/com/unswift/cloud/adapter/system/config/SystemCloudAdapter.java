package com.unswift.cloud.adapter.system.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.cloud.SystemCloudExtendMapper;
import com.unswift.cloud.mapper.system.cloud.SystemCloudMapper;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudDataDo;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudInsertDo;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudSearchDo;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudSingleDo;
import com.unswift.cloud.sql.system.cloud.SystemCloudSql;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="系统微服务公共服务-此bean可以被任何Service引用", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("系统微服务数据库操作对象")
	private SystemCloudMapper systemCloudMapper;
	
	@Autowired
	@ApiField("系统微服务自定义sql")
	private SystemCloudSql systemCloudSql;
	
	@Autowired
	@ApiField("系统微服务数据库扩展操作对象")
	private SystemCloudExtendMapper systemCloudExtendMapper;
	
	@ApiMethod(value="获取系统微服务数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemCloudMapper getMapper(){
		return systemCloudMapper;
	}
	
	@ApiMethod(value="获取系统微服务模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemCloud";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemCloudDataDo findById(Long id){
		return this.findSingle(new SystemCloudSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemCloudDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemCloudSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemCloudDataDo findFirst(SystemCloudSearchDo search, String multipleException){
		List<SystemCloudDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="插入数据前设置默认值", params = {@ApiField("插入的数据")})
    public void setInsertDefault(SystemCloudInsertDo insert) {
		insert.setMountStatus((byte)1);
		insert.setRunningStatus((byte)0);
		if(ObjectUtils.isEmpty(insert.getSort())) {
			SystemCloudSearchDo search=new SystemCloudSearchDo();
			String maxSortSql=systemCloudSql.findMaxSortSql();
			insert.setSort(this.findMaxField(search, maxSortSql)+1);
		}
    }
}
