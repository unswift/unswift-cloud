package com.unswift.cloud.adapter.system.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.user.account.SystemUserAccountExtendMapper;
import com.unswift.cloud.mapper.system.user.account.SystemUserAccountMapper;
import com.unswift.cloud.pojo.dao.system.user.account.SystemUserAccountDataDo;
import com.unswift.cloud.pojo.dao.system.user.account.SystemUserAccountDeleteDo;
import com.unswift.cloud.pojo.dao.system.user.account.SystemUserAccountSearchDo;
import com.unswift.cloud.pojo.dao.system.user.account.SystemUserAccountSingleDo;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="会员登录账号公共服务-此bean可以被任何Service引用", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemUserAccountAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("会员登录账号数据库操作对象")
	private SystemUserAccountMapper systemUserAccountMapper;
	
	@Autowired
	@ApiField("会员登录账号数据库扩展操作对象")
	private SystemUserAccountExtendMapper systemUserAccountExtendMapper;
	
	@ApiMethod(value="获取会员登录账号数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemUserAccountMapper getMapper(){
		return systemUserAccountMapper;
	}
	
	@ApiMethod(value="获取会员登录账号模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemUserAccount";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemUserAccountDataDo findById(Long id){
		return this.findSingle(new SystemUserAccountSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemUserAccountDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemUserAccountSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemUserAccountDataDo findFirst(SystemUserAccountSearchDo search, String multipleException){
		List<SystemUserAccountDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
    @ApiMethod(value="根据用户账号查询单个对象", params=@ApiField("用户账号"), returns=@ApiField("主键匹配的对象"))
    public SystemUserAccountDataDo findByAccount(String account){
        SystemUserAccountSearchDo search=new SystemUserAccountSearchDo();
        search.setAccount(account);
        return this.findFirst(search, "用户账号");
	}
    
    @ApiMethod(value="用户账号是否已存在", params=@ApiField("用户账号"), returns=@ApiField("是否存在，true：已存在，false：不存在"))
    public boolean existsByAccount(String account){
        SystemUserAccountSearchDo search=new SystemUserAccountSearchDo();
        search.setAccount(account);
        return this.findCount(search)>0;
	}
    
    @ApiMethod(value="根据用户id删除对应用户账号", params = @ApiField("用户id"), returns = @ApiField("删除数量"))
	public int deleteByUserId(long userId) {
		SystemUserAccountDeleteDo deleteDo=new SystemUserAccountDeleteDo();
		this.addWhereEquals(deleteDo, "user_id_", userId, null);
		return this.delete(deleteDo);
	}
}
