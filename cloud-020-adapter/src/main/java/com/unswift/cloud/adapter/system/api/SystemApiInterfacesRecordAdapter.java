package com.unswift.cloud.adapter.system.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.api.interfaces.record.SystemApiInterfacesRecordExtendMapper;
import com.unswift.cloud.mapper.system.api.interfaces.record.SystemApiInterfacesRecordMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.api.interfaces.record.SystemApiInterfacesRecordDataDo;
import com.unswift.cloud.pojo.dao.system.api.interfaces.record.SystemApiInterfacesRecordDeleteDo;
import com.unswift.cloud.pojo.dao.system.api.interfaces.record.SystemApiInterfacesRecordSearchDo;
import com.unswift.cloud.pojo.dao.system.api.interfaces.record.SystemApiInterfacesRecordSingleDo;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="接口api执行记录公共服务-此bean可以被任何Service引用", author="unswift", date="2023-09-24", version="1.0.0")
public class SystemApiInterfacesRecordAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("接口api执行记录数据库操作对象")
	private SystemApiInterfacesRecordMapper systemApiInterfacesRecordMapper;
	
	@Autowired
	@ApiField("接口api执行记录数据库扩展操作对象")
	private SystemApiInterfacesRecordExtendMapper systemApiInterfacesRecordExtendMapper;
	
	@ApiMethod(value="获取接口api执行记录数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemApiInterfacesRecordMapper getMapper(){
		return systemApiInterfacesRecordMapper;
	}
	
	@ApiMethod(value="获取接口api执行记录模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemApiInterfacesRecord";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemApiInterfacesRecordDataDo findById(Long id){
		return this.findSingle(new SystemApiInterfacesRecordSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemApiInterfacesRecordDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemApiInterfacesRecordSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemApiInterfacesRecordDataDo findFirst(SystemApiInterfacesRecordSearchDo search, String multipleException){
		List<SystemApiInterfacesRecordDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
    @ApiMethod(value="根据请求接口id查询测试记录", params = {@ApiField("请求接口id")}, returns = @ApiField("最后访问的测试记录"))
    public SystemApiInterfacesRecordDataDo findLastByApiInterfaceId(long apiInterfaceId) {
        SystemApiInterfacesRecordSearchDo search=new SystemApiInterfacesRecordSearchDo();
        search.setApiInterfaceId(apiInterfaceId);
        Sql sql=Sql.createSql();
        sql.addOrderBy("create_time_", Sql.ORDER_BY_DESC);
        sql.setFirstSize(0);
        sql.setPageSize(1);
        search.setSql(sql);
        List<SystemApiInterfacesRecordDataDo> list=this.findList(search);
        if(ObjectUtils.isEmpty(list)){
            return null;
        }
        return list.get(0);
    }
	
    @ApiMethod(value="根据请求接口id删除接口访问记录", params = {@ApiField("请求接口id")}, returns = @ApiField("删除结果"))
    public int deleteByApiInterfaceId(long apiInterfaceId) {
    	SystemApiInterfacesRecordDeleteDo delete=new SystemApiInterfacesRecordDeleteDo();
    	Sql sql=Sql.createSql();
    	sql.addWhere(Sql.LOGIC_AND, "api_interface_id_", Sql.COMPARE_EQUAL, apiInterfaceId);
    	delete.setSql(sql);
    	return this.deleteReal(delete);
    }
}
