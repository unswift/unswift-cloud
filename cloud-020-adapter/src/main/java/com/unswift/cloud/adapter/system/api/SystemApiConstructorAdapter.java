package com.unswift.cloud.adapter.system.api;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseAdapter;
import com.unswift.cloud.mapper.system.api.constructor.SystemApiConstructorExtendMapper;
import com.unswift.cloud.mapper.system.api.constructor.SystemApiConstructorMapper;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.api.constructor.SystemApiConstructorDataDo;
import com.unswift.cloud.pojo.dao.system.api.constructor.SystemApiConstructorDeleteDo;
import com.unswift.cloud.pojo.dao.system.api.constructor.SystemApiConstructorInsertBatchDo;
import com.unswift.cloud.pojo.dao.system.api.constructor.SystemApiConstructorInsertBatchItemDo;
import com.unswift.cloud.pojo.dao.system.api.constructor.SystemApiConstructorSearchDo;
import com.unswift.cloud.pojo.dao.system.api.constructor.SystemApiConstructorSingleDo;
import com.unswift.utils.DateUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Component
@Api(value="类构造函数api公共服务-此bean可以被任何Service引用", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiConstructorAdapter extends BaseAdapter{
	
	@Autowired
	@ApiField("类构造函数api数据库操作对象")
	private SystemApiConstructorMapper systemApiConstructorMapper;
	
	@Autowired
	@ApiField("类构造函数api数据库扩展操作对象")
	private SystemApiConstructorExtendMapper systemApiConstructorExtendMapper;
	
	@ApiMethod(value="获取类构造函数api数据库操作对象", returns=@ApiField("数据库操作对象"))
	public SystemApiConstructorMapper getMapper(){
		return systemApiConstructorMapper;
	}
	
	@ApiMethod(value="获取类构造函数api模块名称", returns=@ApiField("模块名称"))
	public String getModule(){
		return "systemApiConstructor";
	}
	
	@ApiMethod(value="根据主键查询单个对象", params=@ApiField("主键"), returns=@ApiField("主键匹配的对象"))
	public SystemApiConstructorDataDo findById(Long id){
		return this.findSingle(new SystemApiConstructorSingleDo(id, null));
	}
	
	@ApiMethod(value="根据主键列表查询列表对象", params=@ApiField("主键列表"), returns=@ApiField("主键列表匹配的列表对象"))
	public List<SystemApiConstructorDataDo> findByIds(List<Long> idList){
		return this.findList(new SystemApiConstructorSearchDo(idList));
	}
	
	@ApiMethod(value="根据查询对象查询首个对象", params={@ApiField("查询对象"), @ApiField("查询到多个是否抛出异常，可以传入指定提示语，如果提示语为空，则不提示，总的提示与格式：根据%s查询到多条数据，只需要传递%s部分即可")}, returns=@ApiField("首个对象"))
	public SystemApiConstructorDataDo findFirst(SystemApiConstructorSearchDo search, String multipleException){
		List<SystemApiConstructorDataDo> list=this.findList(search);
		if(ObjectUtils.isEmpty(list)){
			return null;
		}
		if(ObjectUtils.isNotEmpty(multipleException)){
			ExceptionUtils.trueException(list.size()>1, "multiple.data.found", multipleException);
		}
		return list.get(0);
	}
	
	@ApiMethod(value="根据构造函数Api注解生产数据", params = {@ApiField("构造函数API注解"), @ApiField("当前类的数据id")})
	public List<SystemApiConstructorInsertBatchItemDo> handleConstructorAnno(Constructor<?>[] constructors, long apiClassId){
		SystemApiConstructorDeleteDo delete=new SystemApiConstructorDeleteDo();
		Sql sql=Sql.createSql();
		sql.addWhere(Sql.LOGIC_AND, "api_class_id_", Sql.COMPARE_EQUAL, apiClassId);
		delete.setSql(sql);
		this.deleteReal(delete);
		
		if(ObjectUtils.isNotEmpty(constructors)) {
			List<SystemApiConstructorInsertBatchItemDo> insertList=new ArrayList<SystemApiConstructorInsertBatchItemDo>();
			SystemApiConstructorInsertBatchItemDo insert;
			ApiConstructor constructorAnno;
			int sort=1;
			for (Constructor<?> constructor : constructors) {
				constructorAnno=constructor.getAnnotation(ApiConstructor.class);
				if(ObjectUtils.isEmpty(constructorAnno)) {
					continue;
				}
				insert=new SystemApiConstructorInsertBatchItemDo();
				insert.setApiClassId(apiClassId);
				insert.setComment(constructorAnno.value());
				ApiField[] params = constructorAnno.params();
				insert.setParamCount(ObjectUtils.isEmpty(params)?0:params.length);
				insert.setModifiers(Modifier.toString(constructor.getModifiers()));
				insert.setAuthor(constructorAnno.author());
				insert.setDate(DateUtils.parse(constructorAnno.date(), "yyyy-MM-dd"));
				insert.setDeprecated(ObjectUtils.isNotEmpty(constructor.getAnnotation(Deprecated.class)));
				insert.setSort(sort);
				insert.setConstructorAnno(constructorAnno);
				insert.setConstructor(constructor);
				insertList.add(insert);
				sort++;
			}
			if(ObjectUtils.isNotEmpty(insertList)) {
				this.saveBatch(new SystemApiConstructorInsertBatchDo(insertList), false);
			}
			return insertList;
		}
		return null;
	}
}
