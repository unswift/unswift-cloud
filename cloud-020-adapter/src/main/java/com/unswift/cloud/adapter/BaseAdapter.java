package com.unswift.cloud.adapter;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.mapper.BaseMapper;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.dao.BatchDo;
import com.unswift.cloud.pojo.dao.PkListDo;
import com.unswift.cloud.pojo.dao.SingleDo;
import com.unswift.cloud.pojo.po.BasePo;
import com.unswift.cloud.pojo.po.CorePo;
import com.unswift.utils.ClassUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Api(value="管理层公共操作类，架构规定，service只能调用adapter，adapter为公共业务操作块", author="unswift", date="2023-04-16", version="1.0.0")
public abstract class BaseAdapter extends BaseSearchAdapter{
	
	@ApiMethod(value="获取当前管理层对应的Mybatis Mapper", returns=@ApiField("获取当前管理层对应的Mybatis Mapper"))
	public abstract BaseMapper getMapper();

	@ApiMethod(value="将实体数据保存到数据库", params={@ApiField("实体数据"), @ApiField("是否进行验证")}, returns=@ApiField("保存结果，返回值表示插入的数量"))
	public <T extends BaseDo> int save(T insert, boolean validate){
		ExceptionUtils.empty(insert, "insert.data.not.empty");
		if(validate){
			validateAdapter.validate(getModule(), "save", insert, getUser(), this);
			validateAdapter.finish();
		}
		if(this.existsUser() && insert instanceof BasePo){
			Long id = this.getUserId();
			if(ObjectUtils.isEmpty(ClassUtils.get(insert, "createUser"))){
				ClassUtils.set(insert, "createUser", id);
			}
			if(ObjectUtils.isEmpty(ClassUtils.get(insert, "changeUser"))){
				ClassUtils.set(insert, "changeUser", id);
			}
		}
		if(insert instanceof CorePo){
			if(ObjectUtils.isEmpty(ClassUtils.get(insert, "isDelete"))) {
				ClassUtils.set(insert, "isDelete", 0);
			}
		}
		return getMapper().insert(insert);
	}
	
	@ApiMethod(value="将实体数据列表保存到数据库", params={@ApiField("实体列表数据"), @ApiField("是否进行验证")}, returns=@ApiField("保存结果，返回值表示插入的数量"))
	public <T extends BatchDo<?>> int saveBatch(T batch, boolean validate){
		ExceptionUtils.empty(batch.getList(), "insert.data.not.empty");
		if(validate){
			validateAdapter.validate(getModule(), "save", batch.getList(), getUser(), this);
			validateAdapter.finish();
		}
		if(this.existsUser()){
			Long id = this.getUserId();
			for (Object insert : batch.getList()) {
				if(insert instanceof BasePo){
					if(ObjectUtils.isEmpty(ClassUtils.get(insert, "createUser"))){
						ClassUtils.set(insert, "createUser", id);
					}
					if(ObjectUtils.isEmpty(ClassUtils.get(insert, "changeUser"))){
						ClassUtils.set(insert, "changeUser", id);
					}
				}
			}
		}
		for (Object insert : batch.getList()) {
			if(insert instanceof CorePo){
				if(ObjectUtils.isEmpty(ClassUtils.get(insert, "isDelete"))) {
					ClassUtils.set(insert, "isDelete", 0);
				}
			}
		}
		return getMapper().insertBatch(batch.getList());
	}
	
	@ApiMethod(value="将实体数据更新到数据库", params={@ApiField("实体数据"), @ApiField("是否进行验证")}, returns=@ApiField("更新结果，返回值表示更新的数量"))
	public <T extends SingleDo> int update(T update, boolean validate){
		ExceptionUtils.trueException(ObjectUtils.isEmpty(ClassUtils.get(update, "id")) && (ObjectUtils.isEmpty(update.getSql()) || ObjectUtils.isEmpty(update.getSql().getWhereList())), "update.condition.not.empty");
		if(validate){
			validateAdapter.validate(getModule(), "update", update, getUser(), this);
			validateAdapter.finish();
		}
		if(this.existsUser() && update instanceof BasePo){
			Long id = this.getUserId();
			if(ObjectUtils.isEmpty(ClassUtils.get(update, "changeUser"))){
				ClassUtils.set(update, "changeUser", id);
			}
		}
		
		return getMapper().update(update);
	}
	
	@ApiMethod(value="将实体数据更新到数据库（完整更新，空值也会更新）", params={@ApiField("实体数据"), @ApiField("是否进行验证")}, returns=@ApiField("更新结果，返回值表示更新的数量"))
	public <T extends SingleDo> int updateAuto(T update, boolean validate){
		ExceptionUtils.trueException(ObjectUtils.isEmpty(ClassUtils.get(update, "id")) && (ObjectUtils.isEmpty(update.getSql()) || ObjectUtils.isEmpty(update.getSql().getWhereList())), "update.condition.not.empty");
		ExceptionUtils.trueException(ObjectUtils.isEmpty(update.getSql()) || ObjectUtils.isEmpty(update.getSql().getSetList()) , "update.field.not.empty");
		if(validate){
			validateAdapter.validate(getModule(), "update", update, getUser(), this);
			validateAdapter.finish();
		}
		if(this.existsUser() && update instanceof BasePo){
			Long id = this.getUserId();
			if(ObjectUtils.isEmpty(ClassUtils.get(update, "changeUser"))){
				ClassUtils.set(update, "changeUser", id);
			}
		}
		return getMapper().updateAuto(update);
	}
	
	@ApiMethod(value="将实体数据列表更新到数据库", params={@ApiField("实体列表数据"), @ApiField("是否进行验证")}, returns=@ApiField("更新结果，返回值表示更新的数量"))
	public <T extends BatchDo<?>> int updateBatch(T batch, boolean validate){
		for (Object update : batch.getList()) {
			ExceptionUtils.trueException(ObjectUtils.isEmpty(ClassUtils.get(update, "id")), "update.condition.not.empty");
		}
		if(validate){
			validateAdapter.validate(getModule(), "update", batch.getList(), getUser(), this);
			validateAdapter.finish();
		}
		if(this.existsUser()){
			Long id = this.getUserId();
			for (Object update : batch.getList()) {
				if(update instanceof BasePo){
					if(ObjectUtils.isEmpty(ClassUtils.get(update, "changeUser"))){
						ClassUtils.set(update, "changeUser", id);
					}
				}
			}
		}
		return getMapper().updateBatch(batch);
	}
	
	@ApiMethod(value="根据主键id删除数据（非物理删除）", params={@ApiField("主键")}, returns=@ApiField("删除结果，返回值表示删除的数量"))
	public <T extends SingleDo> int delete(T delete){
		ExceptionUtils.trueException(ObjectUtils.isEmpty(ClassUtils.get(delete, "id")) && (ObjectUtils.isEmpty(delete.getSql()) || ObjectUtils.isEmpty(delete.getSql().getWhereList())), "delete.condition.not.empty");//id不能未空
		return getMapper().delete(delete);
	}
	
	@ApiMethod(value="根据主键id删除数据（物理删除）", params={@ApiField("主键")}, returns=@ApiField("删除结果，返回值表示删除的数量"))
	public <T extends SingleDo> int deleteReal(T delete){
		ExceptionUtils.trueException(ObjectUtils.isEmpty(ClassUtils.get(delete, "id")) && (ObjectUtils.isEmpty(delete.getSql()) || ObjectUtils.isEmpty(delete.getSql().getWhereList())), "delete.condition.not.empty");//id不能未空
		return getMapper().deleteReal(delete);
	}
	
	@ApiMethod(value="根据主键id集合删除数据（非物理删除）", params={@ApiField("主键集合")}, returns=@ApiField("删除结果，返回值表示删除的数量"))
	public <T extends PkListDo<?>> int deleteBatch(T batch){
		ExceptionUtils.empty(batch.getIdList(), "delete.condition.not.empty");//id不能未空
		return getMapper().deleteBatch(batch);
	}
	
	@ApiMethod(value="根据主键id集合删除数据（物理删除）", params={@ApiField("主键集合")}, returns=@ApiField("删除结果，返回值表示删除的数量"))
	public <T extends PkListDo<?>> int deleteRealBatch(T batch){
		ExceptionUtils.empty(batch.getIdList(), "search.pks.not.empty");//id不能未空
		return getMapper().deleteRealBatch(batch);
	}
}
