package com.unswift.cloud.adapter.validate;

import java.util.List;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.BaseSearchAdapter;
import com.unswift.cloud.core.entity.TokenUser;
import com.unswift.cloud.pojo.Pojo;

@Api(value="数据验证接口，此Service主要是对表单及实体进行数据校验", author="unswift", date="2023-04-16", version="1.0.0")
public interface IValidateAdapter {

	@ApiMethod(value="验证单个实体", params={@ApiField("所属模块"), @ApiField("具体的操作"), @ApiField("需要验证的实体对象"), @ApiField("当前用户"), @ApiField("业务操作对象")})
	<E extends Pojo> void validate(String module, String operator, E entity, TokenUser user, BaseSearchAdapter moduleComponent);

	@ApiMethod(value="验证实体列表", params={@ApiField("所属模块"), @ApiField("具体的操作"), @ApiField("需要验证的实体列表对象"), @ApiField("当前用户"), @ApiField("业务操作对象")})
	<E extends Pojo> void validate(String module, String operator, List<E> entityList, TokenUser user, BaseSearchAdapter moduleComponent);
	
	@ApiMethod(value="识别实体中的各属性是否为空", params={@ApiField("实体对象")}, returns=@ApiField("是否为空，true:为空，false:不为空"))
	<E extends Pojo> boolean entityFieldEmpty(E entity);
	
	@ApiMethod(value="设置本次验证扩展参数，用户可调用非固定参数外的参数", params = {@ApiField("参数的名称"), @ApiField("参数的值")})
	void setAttribute(String key, Object value);
	
	@ApiMethod(value="获取本次验证扩展参数，可作为本线程缓存", params = {@ApiField("参数的名称")}, returns = @ApiField("参数的值"))
	<T> T getAttribute(String key);
	
	@ApiMethod("验证结束时调用，若设置了扩展属性或者有缓存存在，则需执行此方法")
	void finish();
}
