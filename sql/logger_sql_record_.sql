/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : 127.0.0.1:3306
 Source Schema         : cloud-logger

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 06/09/2023 01:24:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for logger_sql_record_
-- ----------------------------
DROP TABLE IF EXISTS `logger_sql_record_`;
CREATE TABLE `logger_sql_record_`  (
  `id_` bigint(19) NOT NULL COMMENT '注解',
  `sql_id_` bigint(19) NOT NULL COMMENT 'sql表主键',
  `sql_script_` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'sql脚本',
  `exe_status_` tinyint(1) NULL DEFAULT NULL COMMENT '执行状态{0：初始化，1：执行成功，2：执行失败}',
  `message_` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '错误信息',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'sql执行记录' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
