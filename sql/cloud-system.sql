/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : 127.0.0.1:3306
 Source Schema         : cloud-system

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 01/09/2023 08:56:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for system_affix_
-- ----------------------------
DROP TABLE IF EXISTS `system_affix_`;
CREATE TABLE `system_affix_`  (
  `id_` bigint(19) NOT NULL COMMENT '主键',
  `data_id_` bigint(19) NULL DEFAULT NULL COMMENT '业务id',
  `data_module_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务模块',
  `name_` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件名称',
  `type_` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件类型',
  `file_path_` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件路径',
  `file_url_` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件下载路径',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_cache_refresh_
-- ----------------------------
DROP TABLE IF EXISTS `system_cache_refresh_`;
CREATE TABLE `system_cache_refresh_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `server_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属微服务',
  `host_ip_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属主机',
  `host_port_` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属端口',
  `cache_type_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '缓存类型{memory：内存缓存，redis：redis缓存}',
  `cache_key_` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '缓存key',
  `refresh_status_` tinyint(1) NULL DEFAULT NULL COMMENT '刷新状态{0：不需要刷新，1：需要刷新}',
  `refresh_bean_` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '刷新bean',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE,
  UNIQUE INDEX `idx_unique_`(`server_`, `host_ip_`, `host_port_`, `cache_key_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '缓存刷新' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_cache_refresh_
-- ----------------------------
INSERT INTO `system_cache_refresh_` VALUES (2, 'server-system', '192.168.40.6', '8010', NULL, 'messageMap', 0, 'com.unswift.cloud.cache.refresh.MessageCacheRefreshService', 0, '2023-05-05 15:53:57', '2023-08-06 12:29:10');
INSERT INTO `system_cache_refresh_` VALUES (5, 'server-speak-text', '192.168.40.6', '8020', NULL, 'messageMap', 0, 'com.unswift.cloud.cache.refresh.MessageCacheRefreshService', 0, '2023-05-06 13:18:04', '2023-08-06 12:29:15');
INSERT INTO `system_cache_refresh_` VALUES (6, 'server-brain-central', '192.168.40.6', '8100', NULL, 'messageMap', 0, 'com.unswift.cloud.cache.refresh.MessageCacheRefreshService', 0, '2023-05-06 13:32:35', '2023-08-06 12:29:17');
INSERT INTO `system_cache_refresh_` VALUES (7, 'server-speak-text', '192.168.40.6', '8110', NULL, 'messageMap', 0, 'com.unswift.cloud.cache.refresh.MessageCacheRefreshService', 0, '2023-05-07 17:29:17', '2023-08-06 12:29:19');
INSERT INTO `system_cache_refresh_` VALUES (8, 'server-brain-central', '10.10.10.6', '8100', NULL, 'messageMap', 0, 'com.unswift.cloud.cache.refresh.MessageCacheRefreshService', 0, '2023-05-14 14:41:35', '2023-08-06 12:29:22');
INSERT INTO `system_cache_refresh_` VALUES (9, 'server-speak-text', '10.10.10.6', '8110', NULL, 'messageMap', 0, 'com.unswift.cloud.cache.refresh.MessageCacheRefreshService', 0, '2023-05-14 14:41:36', '2023-08-06 12:29:24');
INSERT INTO `system_cache_refresh_` VALUES (10, 'server-login', '192.168.14.28', '8020', NULL, 'messageMap', 0, 'com.unswift.cloud.cache.refresh.MessageCacheRefreshService', 0, '2023-05-30 20:18:54', '2023-08-06 12:29:26');
INSERT INTO `system_cache_refresh_` VALUES (11, 'server-login', '192.168.40.5', '8020', NULL, 'memory', 0, 'com.unswift.cloud.cache.refresh.MessageCacheRefreshService', 0, '2023-06-13 00:31:42', '2023-08-06 12:29:27');
INSERT INTO `system_cache_refresh_` VALUES (13, 'server-login', '192.168.40.5', '8020', 'memory', 'messageMap', 0, 'com.unswift.cloud.cache.refresh.MessageCacheRefreshService', 0, '2023-06-13 00:39:09', '2023-08-06 12:29:29');
INSERT INTO `system_cache_refresh_` VALUES (14, 'server-login', '192.168.40.5', '8020', 'memory', 'formValidateMap', 0, 'com.unswift.cloud.cache.refresh.CacheRefreshService', 0, '2023-06-13 00:39:09', '2023-08-06 12:29:31');
INSERT INTO `system_cache_refresh_` VALUES (15, 'server-login', '192.168.40.5', '8020', 'memory', 'formValidateConfigMap', 0, 'com.unswift.cloud.cache.refresh.CacheRefreshService', 0, '2023-06-13 00:39:09', '2023-08-06 12:29:32');
INSERT INTO `system_cache_refresh_` VALUES (16, 'server-login', '192.168.14.28', '8020', 'memory', 'formValidateMap', 0, 'com.unswift.cloud.cache.refresh.CacheRefreshService', 0, '2023-06-25 08:28:42', '2023-08-06 12:29:34');
INSERT INTO `system_cache_refresh_` VALUES (17, 'server-login', '192.168.14.28', '8020', 'memory', 'formValidateConfigMap', 0, 'com.unswift.cloud.cache.refresh.CacheRefreshService', 0, '2023-06-25 08:28:42', '2023-08-06 12:29:36');
INSERT INTO `system_cache_refresh_` VALUES (18, 'system-server', '192.168.40.5', '8100', 'memory', 'messageMap', 0, 'com.unswift.cache.refresh.MessageCacheRefreshService', 0, '2023-08-06 12:34:56', '2023-08-06 12:34:56');
INSERT INTO `system_cache_refresh_` VALUES (19, 'system-server', '192.168.40.5', '8100', 'memory', 'formValidateMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-06 12:34:56', '2023-08-06 12:34:56');
INSERT INTO `system_cache_refresh_` VALUES (20, 'system-server', '192.168.40.5', '8100', 'memory', 'formValidateConfigMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-06 12:34:56', '2023-08-06 12:34:56');
INSERT INTO `system_cache_refresh_` VALUES (21, 'server-login', '192.168.40.5', '8030', 'memory', 'messageMap', 0, 'com.unswift.cache.refresh.MessageCacheRefreshService', 0, '2023-08-06 12:51:56', '2023-08-06 12:51:56');
INSERT INTO `system_cache_refresh_` VALUES (22, 'server-login', '192.168.40.5', '8030', 'memory', 'formValidateMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-06 12:51:56', '2023-08-06 12:51:56');
INSERT INTO `system_cache_refresh_` VALUES (23, 'server-login', '192.168.40.5', '8030', 'memory', 'formValidateConfigMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-06 12:51:56', '2023-08-06 12:51:56');
INSERT INTO `system_cache_refresh_` VALUES (24, 'server-logger', '192.168.40.5', '8005', 'memory', 'messageMap', 0, 'com.unswift.cache.refresh.MessageCacheRefreshService', 0, '2023-08-06 13:06:48', '2023-08-06 13:06:48');
INSERT INTO `system_cache_refresh_` VALUES (25, 'server-logger', '192.168.40.5', '8005', 'memory', 'formValidateMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-06 13:06:48', '2023-08-06 13:06:48');
INSERT INTO `system_cache_refresh_` VALUES (26, 'server-logger', '192.168.40.5', '8005', 'memory', 'formValidateConfigMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-06 13:06:48', '2023-08-06 13:06:48');
INSERT INTO `system_cache_refresh_` VALUES (27, 'server-login', '127.0.0.1', '8030', 'memory', 'messageMap', 0, 'com.unswift.cache.refresh.MessageCacheRefreshService', 0, '2023-08-11 23:12:42', '2023-08-11 23:12:42');
INSERT INTO `system_cache_refresh_` VALUES (28, 'server-login', '127.0.0.1', '8030', 'memory', 'formValidateMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-11 23:12:43', '2023-08-11 23:12:43');
INSERT INTO `system_cache_refresh_` VALUES (29, 'server-login', '127.0.0.1', '8030', 'memory', 'formValidateConfigMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-11 23:12:43', '2023-08-11 23:12:43');
INSERT INTO `system_cache_refresh_` VALUES (30, 'server-system', '192.168.40.5', '8100', 'memory', 'messageMap', 0, 'com.unswift.cache.refresh.MessageCacheRefreshService', 0, '2023-08-13 15:04:25', '2023-08-13 15:04:25');
INSERT INTO `system_cache_refresh_` VALUES (31, 'server-system', '192.168.40.5', '8100', 'memory', 'formValidateMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-13 15:04:25', '2023-08-13 15:04:25');
INSERT INTO `system_cache_refresh_` VALUES (32, 'server-system', '192.168.40.5', '8100', 'memory', 'formValidateConfigMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-13 15:04:25', '2023-08-13 15:04:25');
INSERT INTO `system_cache_refresh_` VALUES (33, 'server-login', '192.168.40.11', '8030', 'memory', 'messageMap', 0, 'com.unswift.cache.refresh.MessageCacheRefreshService', 0, '2023-08-26 17:37:00', '2023-08-26 17:37:00');
INSERT INTO `system_cache_refresh_` VALUES (34, 'server-login', '192.168.40.11', '8030', 'memory', 'formValidateMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-26 17:37:00', '2023-08-26 17:37:00');
INSERT INTO `system_cache_refresh_` VALUES (35, 'server-login', '192.168.40.11', '8030', 'memory', 'formValidateConfigMap', 0, 'com.unswift.cache.refresh.CacheRefreshService', 0, '2023-08-26 17:37:00', '2023-08-26 17:37:00');

-- ----------------------------
-- Table structure for system_dictionary_
-- ----------------------------
DROP TABLE IF EXISTS `system_dictionary_`;
CREATE TABLE `system_dictionary_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型',
  `type_name_` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型名称',
  `key_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '健',
  `value_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值',
  `extend_01_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段1',
  `extend_02_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段2',
  `extend_03_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段3',
  `extend_04_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段4',
  `extend_05_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展字段5',
  `describe_` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `parent_id_` bigint(20) NULL DEFAULT NULL COMMENT '父id',
  `parent_ids_` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '父id集合',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE,
  INDEX `idx_type_`(`type_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数据字典' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_dictionary_
-- ----------------------------
INSERT INTO `system_dictionary_` VALUES (1, 'authorityType', '权限级别', 'owner', '所有者', NULL, NULL, NULL, NULL, NULL, '表示我的权限', NULL, ',1,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-05-25 19:48:06');
INSERT INTO `system_dictionary_` VALUES (2, 'authorityType', '权限级别', 'manager', '管理者', NULL, NULL, NULL, NULL, NULL, '表示我创建的权限', NULL, ',2,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-05-25 19:48:57');
INSERT INTO `system_dictionary_` VALUES (3, 'requestValidateCodeType', '请求验证代码类型', 'if', '如果', NULL, NULL, NULL, NULL, NULL, '条件判断语句，如：if ... else if ... else ... end if', NULL, ',3,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-05-31 19:56:16');
INSERT INTO `system_dictionary_` VALUES (4, 'requestValidateCodeType', '请求验证代码类型', 'else if', '否则如果', NULL, NULL, NULL, NULL, NULL, '条件判断语句，如：if ... else if ... else ... end if', NULL, ',4,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-05-31 19:56:20');
INSERT INTO `system_dictionary_` VALUES (5, 'requestValidateCodeType', '请求验证代码类型', 'else', '否则', NULL, NULL, NULL, NULL, NULL, '条件判断语句，如：if ... else if ... else ... end if', NULL, ',5,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-05-31 19:56:22');
INSERT INTO `system_dictionary_` VALUES (6, 'requestValidateCodeType', '请求验证代码类型', 'end if', '如果结束', NULL, NULL, NULL, NULL, NULL, '条件判断语句，如：if ... else if ... else ... end if', NULL, ',6,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-05-31 19:56:24');
INSERT INTO `system_dictionary_` VALUES (7, 'requestValidateCodeType', '请求验证代码类型', 'case', 'case', NULL, NULL, NULL, NULL, NULL, '当xx=xx时，那么xxx，如：case ... when ... then ... else ... end case', NULL, ',7,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-06-02 22:13:16');
INSERT INTO `system_dictionary_` VALUES (8, 'requestValidateCodeType', '请求验证代码类型', 'when then', '当xx时，那么', NULL, NULL, NULL, NULL, NULL, '当xx=xx时，那么xxx，如：case ... when ... then ... else ... end case', NULL, ',8,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-06-03 11:08:11');
INSERT INTO `system_dictionary_` VALUES (9, 'requestValidateCodeType', '请求验证代码类型', 'end case', 'case结束', NULL, NULL, NULL, NULL, NULL, '当xx=xx时，那么xxx，如：case ... when ... then ... else ... end case', NULL, ',10,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-06-03 11:08:26');
INSERT INTO `system_dictionary_` VALUES (10, 'requestValidateCodeType', '请求验证代码类型', 'foreach', 'for循环', NULL, NULL, NULL, NULL, NULL, '循环一个数组、列表等集合', NULL, ',11,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-06-03 11:08:30');
INSERT INTO `system_dictionary_` VALUES (11, 'requestValidateCodeType', '请求验证代码类型', 'continue', '结束本次循环', NULL, NULL, NULL, NULL, NULL, '结束本次循环', NULL, ',11,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-06-04 09:41:51');
INSERT INTO `system_dictionary_` VALUES (12, 'requestValidateCodeType', '请求验证代码类型', 'break', '跳出循环', NULL, NULL, NULL, NULL, NULL, '结束循环', NULL, ',11,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-06-04 09:41:37');
INSERT INTO `system_dictionary_` VALUES (13, 'requestValidateCodeType', '请求验证代码类型', 'end foreach', 'for循环结束', NULL, NULL, NULL, NULL, NULL, '循环一个数组、列表等集合结束', NULL, ',11,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-06-04 09:40:19');
INSERT INTO `system_dictionary_` VALUES (14, 'requestValidateCodeType', '请求验证代码类型', 'var', '命名变量', NULL, NULL, NULL, NULL, NULL, '定义临时变量，如：var a=1', NULL, ',11,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-06-04 09:40:15');
INSERT INTO `system_dictionary_` VALUES (15, 'requestValidateCodeType', '请求验证代码类型', 'tips', '提示语句', NULL, NULL, NULL, NULL, NULL, '如果满足条件则提示，否则不提示', NULL, ',12,', 0, NULL, '2023-05-25 19:48:02', NULL, '2023-06-04 09:40:12');

-- ----------------------------
-- Table structure for system_form_validate_
-- ----------------------------
DROP TABLE IF EXISTS `system_form_validate_`;
CREATE TABLE `system_form_validate_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `module_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属模块',
  `operator_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属操作',
  `keyword_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '验证类型{if：如果，else if：否则如果，else：否则，end if：如果结束，case：当，when then：等于某个值的时候，那么，end case：case结束，foreach：循环，end foreach：循环结束，break：跳出循环，continue：跳出本次循环，var：变量命名，tips：提示语句}',
  `code_` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '代码',
  `message_` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表达式为真时提示消息',
  `message_args_` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '消息的动态参数',
  `code_line_` int(11) NOT NULL COMMENT '代码顺序',
  `describe_` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '代码描述',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE,
  INDEX `idx_module_`(`module_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统表单验证' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_form_validate_
-- ----------------------------
INSERT INTO `system_form_validate_` VALUES (1, 'login', '', 'tips', 'v.isEmpty(entity.account)', 'field.empty', '\'用户名\'', 1, NULL, 0, NULL, '2023-05-31 20:20:40', NULL, '2023-06-25 15:02:01');
INSERT INTO `system_form_validate_` VALUES (2, 'login', '', 'tips', 'v.isEmpty(entity.password)', 'field.empty', '\'密码\'', 2, NULL, 0, NULL, '2023-05-31 20:20:40', NULL, '2023-08-27 20:53:29');
INSERT INTO `system_form_validate_` VALUES (8, 'login', '', 'tips', 'v.isEmpty(entity.sessionCode)', 'validate.code.error', NULL, 8, NULL, 0, NULL, '2023-05-31 20:20:40', NULL, '2023-08-27 20:53:52');
INSERT INTO `system_form_validate_` VALUES (9, 'login', '', 'tips', 'v.equals(entity.validateCode,entity.sessionCode)', 'validate.code.error', NULL, 9, NULL, 0, NULL, '2023-05-31 20:20:40', NULL, '2023-08-27 20:53:41');

-- ----------------------------
-- Table structure for system_form_validate_config_
-- ----------------------------
DROP TABLE IF EXISTS `system_form_validate_config_`;
CREATE TABLE `system_form_validate_config_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `key_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属模块',
  `value_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属操作',
  `describe_` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '代码描述',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE,
  INDEX `idx_module_`(`key_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统表单验证基础配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_form_validate_config_
-- ----------------------------
INSERT INTO `system_form_validate_config_` VALUES (1, 'operationalScale', '2', '默认运算精度（保留的小数位数）', 0, NULL, '2023-06-09 08:45:50', NULL, '2023-06-09 09:20:35');
INSERT INTO `system_form_validate_config_` VALUES (2, 'operationalRoundingMode', '4', '默认运算精度舍入模式（可查阅BigDecimal舍入模式）{0、进位，1：舍去，2：正进负舍，3：正舍负入,4：四舍五入，5：五舍六入，6：左奇进偶舍，7、断言}', 0, NULL, '2023-06-09 09:10:19', NULL, '2023-06-09 09:20:23');

-- ----------------------------
-- Table structure for system_message_
-- ----------------------------
DROP TABLE IF EXISTS `system_message_`;
CREATE TABLE `system_message_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `server_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属应用服务器',
  `key_` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息key',
  `language_` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '语言',
  `code_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息code',
  `message_` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息',
  `args_order_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '消息动态参数排序',
  `exception_class_` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '异常类',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `is_evolvable_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否可进化{0：不可进化，1：可进化}',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE,
  UNIQUE INDEX `idx_unique_`(`language_`, `code_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1403 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统消息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_message_
-- ----------------------------
INSERT INTO `system_message_` VALUES (1, '*', 'field.empty', 'CN', '10000000', '%s不能为空', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:30:57');
INSERT INTO `system_message_` VALUES (2, '*', 'field.empty', 'EN', '10000000', '%s cannot be empty', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:30:59');
INSERT INTO `system_message_` VALUES (3, '*', 'field.empty', 'HK', '10000000', '%s不能為空', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:04');
INSERT INTO `system_message_` VALUES (10, '*', 'form.validate.syntax.error', 'CN', '10000010', '表单验证语法错误，%s', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (11, '*', 'form.validate.syntax.error', 'EN', '10000010', 'Form validation syntax error,%s', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (12, '*', 'form.validate.syntax.error', 'HK', '10000010', '表單驗證語法錯誤，%s', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (20, '*', 'data.not.found', 'CN', '10000020', '不能找到‘%s’数据', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (21, '*', 'data.not.found', 'EN', '10000020', '\'%s\' data not found', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (22, '*', 'data.not.found', 'HK', '10000020', '無法找到’%s‘數據', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (30, '*', 'unknown.error', 'CN', '10000030', '未知错误：%s', NULL, 'com.unswift.cloud.exception.UnknownException', 0, 0, '2023-08-13 08:53:05', '2023-08-13 12:31:09');
INSERT INTO `system_message_` VALUES (31, '*', 'unknown.error', 'EN', '10000030', 'Unknown error: %s', NULL, 'com.unswift.cloud.exception.UnknownException', 0, 0, '2023-08-13 08:53:05', '2023-08-13 12:31:09');
INSERT INTO `system_message_` VALUES (32, '*', 'unknown.error', 'HK', '10000030', '未知錯誤：%s', NULL, 'com.unswift.cloud.exception.UnknownException', 0, 0, '2023-08-13 08:53:05', '2023-08-13 12:31:09');
INSERT INTO `system_message_` VALUES (1000, 'server-login', 'validate.code.error', 'CN', '10001000', '验证码输入有误', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (1001, 'server-login', 'validate.code.error', 'EN', '10001000', 'Verification code input error', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (1002, 'server-login', 'validate.code.error', 'HK', '10001000', '驗證碼輸入有誤', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (1010, 'server-login', 'incorrect.username.or.password', 'CN', '10001010', '用户名或密码有误', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (1011, 'server-login', 'incorrect.username.or.password', 'EN', '10001010', 'Incorrect username or password', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (1012, 'server-login', 'incorrect.username.or.password', 'HK', '10001010', '用戶名或密碼有誤', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-05-30 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (1020, 'server-login', 'no.permissions.have.been.assigned.yet', 'CN', '10001020', '尚未分配权限', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-06-25 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (1021, 'server-login', 'no.permissions.have.been.assigned.yet', 'EN', '10001020', 'No permissions have been assigned yet', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-06-25 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (1022, 'server-login', 'no.permissions.have.been.assigned.yet', 'HK', '10001020', '尚未分配許可權', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-06-25 08:53:05', '2023-08-06 12:31:09');
INSERT INTO `system_message_` VALUES (1030, 'server-login', 'verification.code.error', 'CN', '10001030', '验证码错误', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-08-26 08:53:05', '2023-08-26 12:31:09');
INSERT INTO `system_message_` VALUES (1031, 'server-login', 'verification.code.error', 'EN', '10001030', 'Verification code error', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-08-25 08:53:05', '2023-08-26 12:31:09');
INSERT INTO `system_message_` VALUES (1032, 'server-login', 'verification.code.error', 'HK', '10001030', '驗證碼錯誤', NULL, 'com.unswift.cloud.exception.BusinessException', 0, 0, '2023-08-26 08:53:05', '2023-08-26 12:31:09');
INSERT INTO `system_message_` VALUES (1200, 'server-export', 'export.business.exception', 'CN', '10001200', '导出逻辑错误，消息：%s', NULL, 'com.unswift.cloud.exception.ExportException', 0, 0, '2023-07-14 08:53:05', '2023-08-13 22:58:58');
INSERT INTO `system_message_` VALUES (1201, 'server-export', 'export.business.exception', 'EN', '10001200', 'Export logic error, message: %s', NULL, 'com.unswift.cloud.exception.ExportException', 0, 0, '2023-07-14 08:53:05', '2023-08-13 22:59:01');
INSERT INTO `system_message_` VALUES (1202, 'server-export', 'export.business.exception', 'HK', '10001200', '匯出邏輯錯誤，消息：%s', NULL, 'com.unswift.cloud.exception.ExportException', 0, 0, '2023-07-14 08:53:05', '2023-08-13 22:59:01');
INSERT INTO `system_message_` VALUES (1210, 'server-export', 'export.stop.task', 'CN', '10001210', '客户停止任务', NULL, 'com.unswift.cloud.exception.ExportException', 0, 0, '2023-08-14 08:53:05', '2023-08-13 22:59:01');
INSERT INTO `system_message_` VALUES (1211, 'server-export', 'export.stop.task', 'EN', '10001210', 'Customer Stop Task', NULL, 'com.unswift.cloud.exception.ExportException', 0, 0, '2023-08-14 08:53:05', '2023-08-13 22:59:01');
INSERT INTO `system_message_` VALUES (1212, 'server-export', 'export.stop.task', 'HK', '10001210', '客戶停止任務', NULL, 'com.unswift.cloud.exception.ExportException', 0, 0, '2023-08-14 08:53:05', '2023-08-13 22:59:01');
INSERT INTO `system_message_` VALUES (1400, 'server-gateway', 'invalid.request', 'CN', '10001400', '无效请求', NULL, 'com.unswift.cloud.exception.SignException', 0, 0, '2023-08-14 08:53:05', '2023-08-26 23:08:13');
INSERT INTO `system_message_` VALUES (1401, 'server-gateway', 'invalid.request', 'EN', '10001400', 'invalid request', NULL, 'com.unswift.cloud.exception.SignException', 0, 0, '2023-08-14 08:53:05', '2023-08-26 23:08:15');
INSERT INTO `system_message_` VALUES (1402, 'server-gateway', 'invalid.request', 'HK', '10001400', '無效請求', NULL, 'com.unswift.cloud.exception.SignException', 0, 0, '2023-08-14 08:53:05', '2023-08-26 23:08:18');

-- ----------------------------
-- Table structure for system_resource_
-- ----------------------------
DROP TABLE IF EXISTS `system_resource_`;
CREATE TABLE `system_resource_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源类型{menu:菜单，request:请求}',
  `auth_key_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源权限key',
  `name_` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源名称',
  `url_` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源url',
  `icon_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源图标',
  `parent_id_` bigint(20) NULL DEFAULT NULL COMMENT '资源所属父',
  `is_admin_` tinyint(1) NULL DEFAULT NULL COMMENT '是否管理员功能{1：是，0：否}',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统资源' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_resource_
-- ----------------------------
INSERT INTO `system_resource_` VALUES (1, 'platform', 'managerSystem', '后台管理系统', '', '', -1, 1, 0, 1, '2023-07-03 17:12:22', 1, '2023-07-05 12:19:50');

-- ----------------------------
-- Table structure for system_role_
-- ----------------------------
DROP TABLE IF EXISTS `system_role_`;
CREATE TABLE `system_role_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色状态{}',
  `key_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色key',
  `name_` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `status_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '角色状态{1：启用，2：停用}',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_role_
-- ----------------------------
INSERT INTO `system_role_` VALUES (1, 'administrator', '系统管理员', 1, 0, NULL, '2023-05-25 19:34:42', NULL, '2023-06-16 12:10:46');

-- ----------------------------
-- Table structure for system_role_resource_
-- ----------------------------
DROP TABLE IF EXISTS `system_role_resource_`;
CREATE TABLE `system_role_resource_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id_` bigint(20) NOT NULL COMMENT '所属角色',
  `resource_id_` bigint(20) NOT NULL COMMENT '所属资源id',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色资源' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_timer_
-- ----------------------------
DROP TABLE IF EXISTS `system_timer_`;
CREATE TABLE `system_timer_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name_` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '定时器名称',
  `timer_key_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '定时器key',
  `running_type_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '运行类型{cron:指定时间点，delay:固定时间段延迟执行}',
  `running_cycle_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '运行周期',
  `execute_bean_` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行bean',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `is_evolvable_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否可进化{0：不可进化，1：可进化}',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE,
  UNIQUE INDEX `idx_name_unique_`(`name_`) USING BTREE,
  UNIQUE INDEX `idx_timer_key_`(`timer_key_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统定时器' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_timer_
-- ----------------------------
INSERT INTO `system_timer_` VALUES (1, '系统消息缓存刷新', 'message.cache.timer', 'delay', '24*60*60*1000', 'com.unswift.cloud.timer.system.SystemMessageTimer', 0, 0, '2023-05-05 19:42:16', '2023-08-06 12:27:40');

-- ----------------------------
-- Table structure for system_user_
-- ----------------------------
DROP TABLE IF EXISTS `system_user_`;
CREATE TABLE `system_user_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `nickname_` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `name_` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `password_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会员密码',
  `id_card_type_` int(11) NULL DEFAULT NULL COMMENT '证件类型{1：居民身份证，2：军官证，3：港澳台通行证，4：护照，999：其它...}',
  `id_card_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '证件号码（MD5）',
  `id_card_show_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '证件号显示，中间带*号',
  `birthday_` date NULL DEFAULT NULL COMMENT '出生日期{yyyy-MM-dd}',
  `sex_` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别{X：女，Y：男}',
  `change_password_time_` datetime(0) NULL DEFAULT NULL COMMENT '最后修改密码时间',
  `freeze_status_` tinyint(1) NULL DEFAULT NULL COMMENT '冻结状态{0：未冻结，1：已冻结}',
  `status_` tinyint(1) NULL DEFAULT NULL COMMENT '当前用户状态{1：有效，2：失效}',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_user_
-- ----------------------------
INSERT INTO `system_user_` VALUES (1, '系统管理员', NULL, 'E10ADC3949BA59ABBE56E057F20F883E', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2023-06-25 16:09:15', NULL, '2023-06-25 16:10:39');

-- ----------------------------
-- Table structure for system_user_account_
-- ----------------------------
DROP TABLE IF EXISTS `system_user_account_`;
CREATE TABLE `system_user_account_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id_` bigint(20) NOT NULL COMMENT '所属会员id',
  `account_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录账号',
  `type_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '账号类型{mobile：手机号，mail：邮箱...}',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE,
  UNIQUE INDEX `idx_account_`(`account_`) USING BTREE,
  INDEX `idx_member_id_`(`user_id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员登录账号' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_user_account_
-- ----------------------------
INSERT INTO `system_user_account_` VALUES (1, 1, 'admin', '', 0, NULL, '2023-05-06 00:33:39', NULL, '2023-06-25 16:11:04');

-- ----------------------------
-- Table structure for system_user_role_
-- ----------------------------
DROP TABLE IF EXISTS `system_user_role_`;
CREATE TABLE `system_user_role_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id_` bigint(20) NOT NULL COMMENT '会员id',
  `role_id_` bigint(20) NOT NULL COMMENT '角色id',
  `authority_type_` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限类型{owner：所有者，manager：管理者}',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE,
  UNIQUE INDEX `idx_search_id_`(`user_id_`, `role_id_`, `authority_type_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_user_role_
-- ----------------------------
INSERT INTO `system_user_role_` VALUES (1, 1, 1, 'owner', 0, NULL, '2023-06-25 17:36:10', NULL, '2023-06-25 17:36:12');

SET FOREIGN_KEY_CHECKS = 1;
