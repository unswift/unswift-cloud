/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : 127.0.0.1:3306
 Source Schema         : cloud-system

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 06/09/2023 01:24:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for system_api_class_
-- ----------------------------
DROP TABLE IF EXISTS `system_api_class_`;
CREATE TABLE `system_api_class_`  (
  `id_` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `class_type_` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类类型{interface：接口, enum：枚举,class：类，entity：实体}',
  `modifiers_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修饰符{public：公共的，private：私有的...}',
  `class_comment_` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '类描述',
  `class_name_` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类名称',
  `author_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `date_` date NULL DEFAULT NULL COMMENT '日期',
  `version_` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本',
  `deprecated_` bit(1) NULL DEFAULT NULL COMMENT '已过期{0：未过期，1：已过期}',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '类api' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_api_constructor_
-- ----------------------------
DROP TABLE IF EXISTS `system_api_constructor_`;
CREATE TABLE `system_api_constructor_`  (
  `id_` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `api_class_id_` bigint(19) NULL DEFAULT NULL COMMENT '类API id',
  `comment_` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '类描述',
  `modifiers_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修饰符{public：公共的，private：私有的...}',
  `param_count_` int(3) NULL DEFAULT NULL COMMENT '参数数量',
  `author_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `date_` date NULL DEFAULT NULL COMMENT '日期',
  `deprecated_` bit(1) NULL DEFAULT NULL COMMENT '已过期{0：未过期，1：已过期}',
  `sort_` int(3) NULL DEFAULT NULL COMMENT '顺序',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '类构造函数api' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_api_field_
-- ----------------------------
DROP TABLE IF EXISTS `system_api_field_`;
CREATE TABLE `system_api_field_`  (
  `id_` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `relation_id_` bigint(19) NOT NULL COMMENT '关联 id',
  `relation_table_` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联表',
  `field_comment_` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '类属性描述',
  `field_name_` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类属性名称',
  `field_type_` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类属性类型',
  `modifiers_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修饰符{public：公共的，private：私有的...}',
  `author_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `rule_` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则',
  `required_` bit(1) NULL DEFAULT NULL COMMENT '必须的',
  `date_` date NULL DEFAULT NULL COMMENT '日期',
  `deprecated_` bit(1) NULL DEFAULT NULL COMMENT '已过期{0：未过期，1：已过期}',
  `sort_` int(5) NULL DEFAULT NULL COMMENT '顺序',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '类字段api' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_api_interfaces_
-- ----------------------------
DROP TABLE IF EXISTS `system_api_interfaces_`;
CREATE TABLE `system_api_interfaces_`  (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `server_` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属微服务',
  `type_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型{catalog：目录，interface：接口}',
  `name_` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '接口名称',
  `path_` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '接口路径',
  `method_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式{GET、POST、PUT、DELETE}',
  `open_level_` int(1) NULL DEFAULT NULL COMMENT '公开等级{0-9，0表示完全公开，9表示完全不公开}',
  `deprecated_` bit(1) NULL DEFAULT NULL COMMENT '已过期{0：未过期，1：已过期}',
  `sort_` int(5) NULL DEFAULT NULL COMMENT '顺序',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '接口api' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_api_method_
-- ----------------------------
DROP TABLE IF EXISTS `system_api_method_`;
CREATE TABLE `system_api_method_`  (
  `id_` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `api_class_id_` bigint(19) NULL DEFAULT NULL COMMENT '类API id',
  `method_comment_` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '类描述',
  `method_name_` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法名称',
  `modifiers_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修饰符{public：公共的，private：私有的...}',
  `param_count_` int(3) NULL DEFAULT NULL COMMENT '参数数量',
  `author_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `date_` date NULL DEFAULT NULL COMMENT '日期',
  `returns_comment_` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '返回字段描述',
  `returns_type_` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返回字段类类型',
  `deprecated_` bit(1) NULL DEFAULT NULL COMMENT '已过期{0：未过期，1：已过期}',
  `sort_` int(5) NULL DEFAULT NULL COMMENT '顺序',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '类方法api' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_api_param_
-- ----------------------------
DROP TABLE IF EXISTS `system_api_param_`;
CREATE TABLE `system_api_param_`  (
  `id_` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `api_interface_id_` bigint(19) NULL DEFAULT NULL COMMENT '接口api id',
  `param_category_` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性分类{path：path参数，param：url参数，form：表单参数，body：body参数，response：回调参数}',
  `param_key_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性名称',
  `param_comment_` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性描述',
  `param_type_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性类型{string,int,long,byte,char,json,file}',
  `rule_` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则',
  `required_` bit(1) NULL DEFAULT NULL COMMENT '必须的',
  `deprecated_` bit(1) NULL DEFAULT NULL COMMENT '已过期{0：未过期，1：已过期}',
  `parent_id_` bigint(19) NULL DEFAULT NULL COMMENT '父属性id',
  `sort_` int(5) NULL DEFAULT NULL COMMENT '顺序',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '接口参数api' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_api_version_
-- ----------------------------
DROP TABLE IF EXISTS `system_api_version_`;
CREATE TABLE `system_api_version_`  (
  `id_` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `relation_id_` bigint(19) NULL DEFAULT NULL COMMENT '关联 id',
  `relation_table_` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联表',
  `comment_` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '版本描述',
  `author_` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `date_` date NULL DEFAULT NULL COMMENT '日期',
  `version_` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本',
  `sort_` int(5) NULL DEFAULT NULL COMMENT '排序',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '类版本api' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for system_sql_
-- ----------------------------
DROP TABLE IF EXISTS `system_sql_`;
CREATE TABLE `system_sql_`  (
  `id_` bigint(19) NOT NULL COMMENT '主键',
  `online_time_` date NULL DEFAULT NULL COMMENT '上线时间',
  `sql_file_` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'sql文件路径',
  `exe_status_` tinyint(1) NULL DEFAULT NULL COMMENT '执行状态{0：未执行，1：执行中，2：执行完成}',
  `exe_result_` tinyint(1) NULL DEFAULT NULL COMMENT '执行结果{0：执行失败，1：执行成功}',
  `exe_message_` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '执行结果消息',
  `exe_progress_` int(3) NULL DEFAULT NULL COMMENT '执行进度',
  `sql_count_` int(5) NULL DEFAULT NULL COMMENT '执行总sql数量',
  `is_delete_` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除{0：未删除，1：已删除}',
  `create_user_` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_time_` datetime(0) NOT NULL COMMENT '创建时间',
  `change_user_` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
  `change_time_` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'sql上线脚本' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
