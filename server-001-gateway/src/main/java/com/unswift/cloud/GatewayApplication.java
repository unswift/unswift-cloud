package com.unswift.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.utils.ExceptionUtils;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@Api(value="网关服务器启动类", author="unswift", date="2023-04-09", version="1.0.0")
public class GatewayApplication {
	
	@ApiMethod(value="网关服务器启动方法", params=@ApiField("启动参数"))
	public static void main(String[] args) {
		try {
			SpringApplication.run(GatewayApplication.class, args);
		} catch (Exception e) {
			e.printStackTrace();
			throw ExceptionUtils.exception("server.start.exception", e, e.getMessage());
		}
		
	}
}
