package com.unswift.cloud.response;

import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.mso.logger.api.request.LoggerApiRequestQueueMso;

@Api(value="宇燕回调-继承spring的回调对象")
public class UnswiftResponse extends ServerHttpResponseDecorator{

	@ApiField("接口请求日志对象")
	private LoggerApiRequestQueueMso apiLogger;
	
	@ApiConstructor(value="带参构造", params = @ApiField("源response"))
	public UnswiftResponse(ServerHttpResponse delegate) {
		super(delegate);
	}

	@ApiConstructor(value="带参构造", params = {@ApiField("源response"), @ApiField("接口请求日志对象")})
	public UnswiftResponse(ServerHttpResponse delegate, LoggerApiRequestQueueMso apiLogger) {
		super(delegate);
		this.apiLogger=apiLogger;
	}
	
	public LoggerApiRequestQueueMso getApiLogger() {
		return apiLogger;
	}
}
