(function(){
	layui.use(['tree'], function(){
		$.layTree=layui.tree;
	});
	$.tree={
		/**
		 * 图标映射，可配置每个节点的图标，每个图标类型分有孩子时显示的图标和没孩子时显示的图标
		 */
		iconMap: {
			'organization': {folder: '<i class="layui-icon layui-icon-share tree-node-icon"></i>', node: '<i class="layui-icon layui-icon-share tree-node-icon"></i>'},
			'authorize': {folder: '<i class="layui-icon layui-icon-auz tree-node-icon"></i>', node: '<i class="layui-icon layui-icon-auz tree-node-icon"></i>'},
			'user': {folder: '<i class="layui-icon layui-icon-username tree-node-icon"></i>', node: '<i class="layui-icon layui-icon-username tree-node-icon"></i>'},
			'users': {folder: '<i class="layui-icon layui-icon-user tree-node-icon"></i>', node: '<i class="layui-icon layui-icon-user tree-node-icon"></i>'}
		},
		initView: function(treeViewId, isDialog){
			var treeView=$("#"+treeViewId);
			var tabs=isDialog?treeView.parent():treeView.parent().parent().parent();
			var totalHeight=tabs.height();
			if(isDialog){
				treeView.height(totalHeight-3);
			}else{
				var tabTitleHeight=tabs.children(":first").height();
				treeView.height(totalHeight-tabTitleHeight-3);
			}
		},
		startMove: function(move, event){
			move=$(move);
			event=event||window.event;
			var startX=event.clientX;
			var endX,subX;
			var treeNode=move.parent();
			var left=move.prev();
			var right=move.next();
			var initWidth=left.width(), width;
			treeNode.on("mousemove", function(mouseEvent){
				mouseEvent=mouseEvent||window.event;
				endX=mouseEvent.clientX;
				subX=endX-startX;
				width=initWidth+subX;
				left.width(width);
				move.css("left", width);
				right.css("left", width+3);
			});
			treeNode.on("mouseup", function(mouseEvent){
				console.log(this);
				$(this).off("mousemove");
				$(this).off("mouseup");
				mouseEvent=mouseEvent||window.event;
				endX=mouseEvent.clientX;
				subX=endX-startX;
				width=initWidth+subX;
				left.width(width);
				move.css("left", width);
				right.css("left", width+2);
			});
		},
		loadTree: function(treeId, treeData, nodeClick){
			if(typeof treeData=='array'){
				if(treeData[0].icon){
					for(var i=0, length=treeData.length; i<length; i++){
						var icon=$.tree.iconMap[treeData.icon];
						if(icon){
							treeData[i].title=(treeData[i].children?icon.folder:icon.node)+treeData[i].title;
							$.tree.parseChild(treeData[i], icon);
						}
					}
				}
			}else{
				if(treeData.icon){
					var icon=$.tree.iconMap[treeData.icon];
					if(icon){
						treeData.title=(treeData.children?icon.folder:icon.node)+treeData.title;
						$.tree.parseChild(treeData, icon);
					}
				}
			}
			var treeEvent=new TreeNodeEvent(treeId, treeData.isClickStyle, nodeClick);
			$.layTree.render({
				id: treeId,
				elem: '#'+treeId,
				showCheckbox: treeData.checkbox||false,
				data: typeof treeData=='array'?treeData:[treeData],
				showLine: false,  // 是否开启连接线
				onlyIconControl: true,
				click: treeEvent.click
			});
		},
		parseChild: function(node, icon){
			var children=node.children;
			if(children){
				for(var i=0, length=children.length; i < length; i++){
					icon=children[i].icon?$.tree.iconMap[children[i].icon]:icon;
					if(icon){
						children[i].title=(children[i].children?icon.folder:icon.node)+children[i].title;
						$.tree.parseChild(children[i], icon);
					}
				}
			}
		},
		getDataChecked: function(treeData){
			var checkedList=[];
			if(typeof treeData=='array'){
				if(treeData[0].icon){
					for(var i=0, length=treeData.length; i<length; i++){
						if(treeData[i].checked){
							checkedList.push(treeData[i])
						}
						$.tree.getChildDataChecked(treeData[i], checkedList);
					}
				}
			}else{
				if(treeData.checked){
					checkedList.push(treeData, checkedList)
				}
				$.tree.getChildDataChecked(treeData, checkedList);
			}
			return checkedList;
		},
		getChildDataChecked: function(node, checkedList){
			var children=node.children;
			if(children){
				for(var i=0, length=children.length; i < length; i++){
					if(children[i].checked){
						checkedList.push(children[i])
					}
					$.tree.getChildDataChecked(children[i], checkedList);
				}
			}
		},
		getChecked: function(treeId){
			return $.layTree.getChecked(treeId);
		},
		getCheckedList: function(treeId){
			var checkTreeData=$.layTree.getChecked(treeId);
			var nodeList=[];
			if(checkTreeData.length>0){
				for(var i=0, length=checkTreeData.length; i< length; i++){
					nodeList.push({id: checkTreeData[i].id,title: checkTreeData[i].title});
					if(checkTreeData[i].children && checkTreeData[i].children.length>0){
						$.tree.setCheckedChildrenList(checkTreeData[i].children, nodeList);
					}
				}
			}
			return nodeList;
		},
		setCheckedChildrenList: function(children, nodeList){
			for(var i=0, length=children.length; i< length; i++){
				nodeList.push({id: children[i].id,title: children[i].title});
				if(children[i].children && children[i].children.length>0){
					$.tree.setCheckedChildrenList(children[i].children, nodeList);
				}
			}
		},
		getCheckedIdList: function(treeId){
			var checkTreeData=$.layTree.getChecked(treeId);
			var ids=[];
			if(checkTreeData.length>0){
				for(var i=0, length=checkTreeData.length; i< length; i++){
					ids.push(checkTreeData[i].id);
					if(checkTreeData[i].children && checkTreeData[i].children.length>0){
						$.tree.setCheckedChildrenIdList(checkTreeData[i].children, ids);
					}
				}
			}
			return ids;
		},
		setCheckedChildrenIdList: function(children, idList){
			for(var i=0, length=children.length; i< length; i++){
				idList.push(children[i].id);
				if(children[i].children && children[i].children.length>0){
					$.tree.setCheckedChildrenIdList(children[i].children, idList);
				}
			}
		}
	};
	function TreeNodeEvent(treeId, isClickStyle, nodeClick){
		this.treeId=treeId;
		this.isClickStyle=isClickStyle;
		this.nodeClick=nodeClick;
		var that=this;
		this.click=function(node){
			if(that.isClickStyle){
				if(that.currNode){
					if(that.currNode.data.id!=node.data.id){
						that.currNode.elem.find(".layui-tree-entry:eq(0)").removeClass('checked');
						node.elem.find(".layui-tree-entry:eq(0)").addClass('checked');
					}
				}else{
					node.elem.find(".layui-tree-entry:eq(0)").addClass('checked');
				}
				that.currNode=node;
			}
			that.nodeClick(node);
		};
	}
})();