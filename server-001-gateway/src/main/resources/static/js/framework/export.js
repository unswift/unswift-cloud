(function(module){
	module.export={
		handle: function(title, exportUrl, searchData, stopUrl){
			module.export.stopUrl=stopUrl;
			$.get('/pages/admin/module/export.html', function(html){
				$.layer.open({
					title: title,
					type: 1,
					area: ['600px', '200px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: false, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["停止" ,"关闭"],
					yes: module.export.stop,
					btn2: module.export.cancel
				});
				$.post(exportUrl, searchData, function(res){
					module.export.taskId=res.id;
					module.export.progress=0;
					module.export.polling=true;
					window.setTimeout(module.export.getProgress, 1000);
				}, 'json', false);
			});
		},
		stop: function(layerIndex){
			if(module.export.progress<100){
				$.post(module.export.stopUrl, {id: module.export.taskId}, function(res){
					$.success("停止成功");
					module.export.polling=false;
				}, 'json');
			}else{
				$.warn("导出已完成，不能再停止导出");
			}
		},
		cancel: function(layerId){
			if(module.export.progress<100){
				$.layer.confirm("关闭后，可在导出记录中查看，确定要关闭此次导出吗？", {btn:['确定','取消']}, function(confirmId){
					$.layer.close(confirmId);
					$.layer.close(layerId);
					module.export.polling=false;
				}, function(){
					
				});
			}else{
				$.layer.close(layerId);
				module.export.polling=false;
			}
			return false;
		},
		getProgress: function(){
			if(!module.export.polling){
				return false;
			}
			$.post('/imexport/loggerExportTask/findProgress',{id: module.export.taskId}, function(res){
				$.element.progress('export-progress', res.progress+'%');
				module.export.progress=res.progress;
				if(res.status==2 && res.result==1){//导出完成
					$("#export-info").addClass("color-green").html("导出完成，共导出："+res.exportCount+"行数据");
					$("#export-file").html("<a href=\""+res.exportFileUrl+"\" target=\"_block\">"+res.exportFileName+"</a>");
					window.open(res.exportFileUrl);
				}else if(res.status==2){
					$("#export-info").addClass("color-red").html(res.errorMessage);
				}else if(module.export.polling){
					window.setTimeout(module.export.getProgress, 1000);
				}
			}, 'json')
		}
	}
})($.module);