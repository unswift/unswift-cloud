(function(module){
	module.import={
		/**
		 *	导入入口方法，导入流程为：上传附件->执行导入请求->解析附件->导入数据->导入完成
		 * @param 标题，决定导入弹出框的标题
		 * @param 导入模块，决定导入所使用的模板
		 * @param 允许导入的附件后缀，多个用|隔开
		 * @param 执行导入请求的url
		 * @param 停止导入的url
		 * @param 导入完成执行的方法
		 * @param 导入传递的外部参数，在“执行导入请求”时传递
		 */
		handle: function(title, moduleName, attachTypes, importUrl, stopUrl, back, extendsData){
			module.import.progress=0;
			module.import.title=title;
			module.import.moduleName=moduleName;
			module.import.attachTypes=attachTypes;
			module.import.importUrl=importUrl;
			module.import.stopUrl=stopUrl;
			module.import.back=back;
			module.import.extendsData=extendsData;
			module.import.polling=true;
			$.get('/pages/admin/module/import.html', function(html){
				$.layer.open({
					title: title,
					type: 1,
					area: ['600px', '250px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: false, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["停止" ,"关闭"],
					yes: module.import.stop,
					btn2: module.import.cancel,
					success: module.import.load
				});
			});
		},
		stop: function(layerIndex){
			if(module.import.progress<5){
				$.error("任务尚未开始，无需停止");
				module.import.polling=false;
			}else if(module.import.progress<100){
				$.post(module.import.stopUrl, {id: module.import.taskId}, function(res){
					$.success("停止成功");
					module.import.polling=false;
				}, 'json');
			}else{
				$.warn("导出已完成，不能再停止导入");
			}
		},
		cancel: function(layerId){
			if(module.import.progress<100 && module.import.progress>0){
				$.layer.confirm("关闭后，可在导入记录中查看，确定要关闭此次导入吗？", {btn:['确定','取消']}, function(confirmId){
					$.layer.close(confirmId);
					$.layer.close(layerId);
					module.import.polling=false;
				}, function(){
					
				});
			}else{
				$.layer.close(layerId);
				module.import.polling=false;
			}
			return false;
		},
		load: function(){
			$.ajax({
				url: '/attach/systemAttach/findModelAttach',
				type: 'post',
				data: $.toJson({dataModule: module.import.moduleName}),
				dataType: 'json',
				async: false,
				success: function(data){
					if(!data){
						$.error("无法找到导入模板");
						return false;
					}
					$("#import-model-file").html("<a href=\""+data.fileUrl+"\" target=\"_block\">"+data.name+"</a>");
				}
			});
			$.upload.render({
				elem: '#import-file', //绑定元素
				url: '/attach/systemAttach/upload', //上传接口
				accept: 'file',
				headers: {"upload": true},
				data: {classify: "import", module: module.import.moduleName, saveTime: 0},
				exts: module.import.attachTypes,
				done: module.import.importData,
				error: function(error) {
					$.error(error);
				}
			});
		},
		importData: function(res){
			module.import.progress=5;
			if(!module.import.polling){
				return false;
			}
			$.element.progress('import-progress', module.import.progress+'%');
			$("#import-info").removeClass("color-green").removeClass("color-red");
			$("#import-info").html("附件上传完成，准备导入");
			var fileUrl=res.body.dataList[0].fileUrl;
			var data=module.import.extendsData||{};
			data.attachId=res.body.dataList[0].id;
			data.attachUrl=fileUrl.substring(fileUrl.lastIndexOf('/')+1);
			$.post(module.import.importUrl, data, function(res){
				module.import.taskId=res.id;
				window.setTimeout(module.import.getProgress, 1000);
			}, 'json');
		},
		getProgress: function(){
			if(!module.import.polling){
				return false;
			}
			$.post('/imexport/loggerImportTask/findProgress',{id: module.import.taskId}, function(res){
				$.element.progress('import-progress', res.progress+'%');
				module.import.progress=res.progress;
				if(res.status==2 && res.result==1){//导出完成
					$("#import-info").addClass("color-green").html("导入完成，共导入："+res.importCount+"行数据");
					console.log(module.import.back);
					if(module.import.back){
						module.import.back();
					}
				}else if(res.status==2){
					$("#import-info").addClass("color-red").html(res.errorMessage);
				}else if(module.import.polling){
					window.setTimeout(module.import.getProgress, 1000);
				}
			}, 'json')
		}
	}
})($.module);