(function(){
	$.config={
		domains: [
			{
				server: window.location.protocol+"//127.0.0.1",
				routes:["/login", "/system", "/imexport", "/attach", "/logger", "/sqlexe"]
			}
		],
		language: 'ZH_CN',
		website: 'M',
		version: '1.0.0'
	};
	$.module={};
	$.initModule=function(){
		$.module={};
		$.resize=[];
	};
	$.removeModule=function(moduleId){
		delete $.module[moduleId];
		if($.resize){
			var resize=[];
			for(var i=0, length=$.resize.length; i<length; i++){
				if($.resize[i].module!=moduleId){
					resize.push($.resize[i]);
				}
			}
			$.resize=resize;
		}
	}
})();