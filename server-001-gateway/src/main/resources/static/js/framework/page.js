(function(module){
	module.page={
		initView(pageId){
			var page=$("#"+pageId);
			var tabs=page.parent().parent().parent();
			var totalHeight=tabs.height();
			var tabTitleHeight=tabs.children(":first").height();
			page.height(totalHeight-tabTitleHeight-3);
		}
	};
})($.module);