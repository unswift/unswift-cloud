function talkSubmit(){
	var talkContent=$("#talk-content");
	var content=talkContent.val();
	if(!content){
		alert("请输入内容");
		return false;
	}
	var talkRecord=$("#talk-record");
	talkRecord.append($("<div class=\"talk-speak-line talk-speak-right\"><span class=\"talk-speak\">"+content.replace(/\n/g,'<br>')+"</span></div>"));
	talkContent.val("");
	$.post("/speak-text/speak",content,function(data){
		if(data.code!='0'){
			talkRecord.append($("<div class=\"talk-speak-line\"><span class=\"talk-error\">"+data.message+"</span></div>"))
		}else{
			talkRecord.append($("<div class=\"talk-speak-line\"><span class=\"talk-answer\">"+data.body+"</span></div>"))
		}
	},"json");
}