(function(){
	$.assembly={
		data: {},//缓存
		/**
		 * 将自定义url数据加载到页面checkbox中
		 * 缓存说明：在刷新界面后缓存消失
		 * @param { 参数对象
		 *		url: 读取数据的url(必填)
		 *		method: url的请求方式(选填)，默认post
		 *		data: url参数(选填),默认{}
		 *		keys: {(必填)
		 *			value: 页面checkbox标签value属性的值(必填)
		 *			label: 页面checkbox标签显示值(必填)
		 *		},
		 *		formId: 表单id(必填)
		 *		layFilter: 复选框存放的页面对象的lay-filter属性值(必填)
		 *		checkboxName: 复选框名称(必填)
		 *		multiple: true|false 复选框多选(选填)，默认true
		 *		cacheKey: 缓存key(必填)
		 * }
		 */
		loadCheckbox: function(config){
			config.multiple=typeof config.multiple=='undefined'?true:config.multiple;
			var dataList=$.assembly.data[config.cacheKey];
			if(dataList){
				appendCheckbox(config.formId, config.layFilter, dataList, config.checkboxName, config.multiple, config.keys);
			}else{
				$.ajax({
					url: config.url,
					type: config.method||'post',
					data: $.toJson(config.data||{}),
					dataType: 'json',
					async: false,
					success: function(data){
						var dataList=data.dataList;
						if(dataList && dataList.length>0){
							$.assembly.data[config.cacheKey]=dataList;
							appendCheckbox(config.formId, config.layFilter, dataList, config.checkboxName, config.multiple, config.keys);
						}
					}
				});
			}
		},
		/**
		 * 将自定义url数据加载到页面select中
		 * 缓存说明：在刷新界面后缓存消失
		 * @param { 参数对象
		 *		url: 读取数据的url(必填)
		 *		method: url的请求方式(选填)，默认post
		 *		data: url参数(选填),默认{}
		 *		keys: {(必填)
		 *			value: 页面checkbox标签value属性的值(必填)
		 *			label: 页面checkbox标签显示值(必填)
		 *		},
		 *		formId: 表单id(必填)
		 *		layFilter: 复选框存放的页面对象的lay-filter属性值(必填)
		 *		cacheKey: 缓存key(必填)
		 * }
		 */
		loadSelect: function(config){
			var dataList=$.assembly.data[config.cacheKey];
			if(dataList){
				appendSelect(config.formId, config.layFilter, dataList, config.keys);
			}else{
				$.ajax({
					url: config.url,
					type: config.method||'post',
					data: $.toJson(config.data||{}),
					dataType: 'json',
					async: false,
					success: function(data){
						var dataList=data.dataList;
						if(dataList && dataList.length>0){
							$.assembly.data[config.cacheKey]=dataList;
							appendSelect(config.formId, config.layFilter, dataList, config.keys);
						}
					}
				});
			}
		}
	}
	function appendCheckbox(formId, layFilter, dataList, checkboxName, multiple, keys){
		var checkboxs=$("#"+formId+" [lay-filter='"+layFilter+"']");
		for(var i=0, length=dataList.length; i < length; i++){
			checkboxs.append('<input type="checkbox" name="'+checkboxName+'" value="'+dataList[i][keys.value]+'" title="'+dataList[i][keys.label]+'" multiple="'+multiple+'" formid="'+formId+'" lay-filter="'+formId+'-'+checkboxName+'">')
		}
		if(!multiple){
			$.form.on('checkbox('+formId+'-'+checkboxName+')', function(data){
				var checkbox=data.elem;
				if(checkbox.checked){
					var value=checkbox.value;
					var name=checkbox.name;
					var formId=checkbox.getAttribute('formid');
					var change=false;
					$("#"+formId+" [name='"+name+"']").each(function(){
						if(value!=this.value && this.checked){
							this.checked=false;
							change=true;
						}
					});
					$.form.render('checkbox', formId);
				}
			});
		}
	}
	function appendSelect(formId, layFilter, dataList, keys){
		var select=$("#"+formId+" [lay-filter='"+layFilter+"']");
		for(var i=0, length=dataList.length; i < length; i++){
			select.append('<option value="'+dataList[i][keys.value]+'">'+dataList[i][keys.label]+'</option>')
		}
	}
})();