(function(){
	$.auth={
		init: function(htmlObject){
			htmlObject.find("[auth]").each(function(){
				var auth=$(this);
				var authKey=auth.attr("auth");
				if(!$.auth.getByKey(authKey)){
					auth.remove();
				}
			});
		},
		getByKey: function(authKey){
			var resourceList=$.config.user.resourceList;
			for(var i=0, length=resourceList.length; i<length; i++){
				if(resourceList[i].authKey==authKey){
					return resourceList[i];
				}
			}
			return null;
		},
		getById: function(id){
			var resourceList=$.config.user.resourceList;
			for(var i=0, length=resourceList.length; i<length; i++){
				if(resourceList[i].id==id){
					return resourceList[i];
				}
			}
			return null;
		},
		getChilds: function(parentId, authType){
			var resourceList=$.config.user.resourceList;
			var children=[];
			for(var i=0, length=resourceList.length; i<length; i++){
				if(resourceList[i].parentId==parentId && resourceList[i].type==authType){
					children.push(resourceList[i]);
				}
			}
			return children;
		}
	}
})();