(function(module){
	module.logger={
		look: function(module, dataId){
			$.get('/pages/admin/module/logger.html',function(html){
				$.layer.open({
					title: '操作日志',
					type: 1,
					area: ['80%', '600px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["关闭"],
					success: function(){
						$("#commonLoggerSearchForm [name='module']").val(module);
						$("#commonLoggerSearchForm [name='dataId']").val(dataId);
						$.module.logger.search();
					}
				});
			});
		},
		tableCols: [function(){
			var cols=[//列表标题
				{ field: 'operatorTypeName', title: '操作类型', width: 90, align: "center"},
				{ field: 'operatorContent', title: '操作内容'},
				{ field: 'createUserName', title: '操作人', width: 120},
				{ field: 'createTime', title: '操作时间', width: 170, align: "center"}
			];
			return cols;
		}()],
		search: function(){
			var search=$.formToData('commonLoggerSearchForm');
			$.table.render({
				elem: '#commonLoggerTable',
				url: '/logger/loggerOperator/findPageList', // 此处为静态模拟数据，实际使用时需换成真实接口
				method: 'post',
				contentType: 'application/json',
				request: {
					pageName: 'currPage',
					limitName: 'pageSize'
				},
				parseData: function(res) { // res 即为原始返回的数据
					return {
						"code": res.code, // 解析接口状态
						"msg": res.message, // 解析提示文本
						"count": res.body.totalSize, // 解析数据长度
						"data": res.body.dataList // 解析数据列表
					};
				},
				where: search,
				cols: module.logger.tableCols,
				page: true,
				height: '430',
				done: function(){
					
				}
			});
		},
		clear: function(){
			$("#commonLoggerSearchForm [name='operatorContent']").val('');
			$("#commonLoggerSearchForm [name='operatorType']").val('');
		}
	};
})($.module);