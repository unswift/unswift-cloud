(function(){
	$.dictionary={
		data: {},//缓存
		/**
		 * 将数组字典中的值输出到页面select标签中
		 * 缓存说明：在刷新界面后缓存消失
		 * @param formId 表单id
		 * @param layFilter 绑定的select标签的lay-filter属性
		 * @param dictionaryType 数据字典类型
		 * @param emptyOption 是否显示请选择(创建空的选项)
		 * @param extend01 根据扩展字段01查询的值，没有则不作为查询条件
		 */
		loadSelect: function(formId, layFilter, dictionaryType, emptyOption, extend01){
			var dictionaryList=$.dictionary.data[dictionaryType];
			if(dictionaryList){
				appendSelect(formId, layFilter, dictionaryList, emptyOption);
			}else{
				var data={type:dictionaryType};
				if(extend01){
					data.extend01=extend01;
				}
				$.ajax({
					url: '/system/systemDictionary/findTypeList',
					type: 'post',
					data: $.toJson(data),
					dataType: 'json',
					async: false,
					success: function(data){
						var dictionaryList=data.dataList;
						if(dictionaryList && dictionaryList.length>0){
							$.dictionary.data[dictionaryType]=dictionaryList;
							appendSelect(formId, layFilter, dictionaryList, emptyOption);
						}
					}
				});
			}
		},
		/**
		 * 将数据字典中的值输出到页面radio标签中
		 * 缓存说明：在刷新界面后缓存消失
		 * @param formId 表单id
		 * @param layFilter 页面radio组件的父标识的lay-filter属性
		 * @param dictionaryType 数据字典类型
		 * @param radioName 页面radio组件的name属性值
		 * @param extend01 根据扩展字段01查询的值，没有则不作为查询条件
		 */
		loadRadio: function(formId, layFilter, dictionaryType, radioName, extend01){
			var dictionaryList=$.dictionary.data[dictionaryType];
			if(dictionaryList){
				appendRadio(formId, layFilter, dictionaryList, radioName);
			}else{
				var data={type:dictionaryType};
				if(extend01){
					data.extend01=extend01;
				}
				$.ajax({
					url: '/system/systemDictionary/findTypeList',
					type: 'post',
					data: $.toJson(data),
					dataType: 'json',
					async: false,
					success: function(data){
						var dictionaryList=data.dataList;
						if(dictionaryList && dictionaryList.length>0){
							$.dictionary.data[dictionaryType]=dictionaryList;
							appendRadio(formId, layFilter, dictionaryList, radioName);
						}
					}
				});
			}
		},
		/**
		 * 将数据字典中的值输出到页面图标标签中
		 * 缓存说明：在刷新界面后缓存消失
		 * @param formId 表单id
		 * @param layFilter 页面radio组件的父标识的lay-filter属性
		 * @param dictionaryType 数据字典类型
		 * @param radioName 页面radio组件的name属性值
		 * @param defaultValue 图标标签的默认值
		 * @param extend01 根据扩展字段01查询的值，没有则不作为查询条件
		 */
		loadRadioIcon: function(formId, layFilter, dictionaryType, radioName, defaultValue, extend01){
			var dictionaryList=$.dictionary.data[dictionaryType];
			if(dictionaryList){
				appendRadioIcon(formId, layFilter, dictionaryList, radioName, defaultValue);
			}else{
				$.ajax({
					url: '/system/systemDictionary/findTypeList',
					type: 'post',
					data: $.toJson({type:dictionaryType}),
					dataType: 'json',
					async: false,
					success: function(data){
						var dictionaryList=data.dataList;
						if(dictionaryList && dictionaryList.length>0){
							$.dictionary.data[dictionaryType]=dictionaryList;
							appendRadioIcon(formId, layFilter, dictionaryList, radioName, defaultValue);
						}
					}
				});
			}
		},
		radioIconClick: function(radioIcon, formId, radioName){
			radioIcon=$(radioIcon);
			$("#"+formId+" [name='"+radioName+"']").val(radioIcon.attr('key'));
			radioIcon.parent().children().removeClass("checked");
			radioIcon.addClass("checked");
		}
	};
	function appendSelect(formId, layFilter, dataList, emptyOption){
		var select=$("#"+formId+" [lay-filter='"+layFilter+"']");
		if(emptyOption){
			select.append('<option value="">请选择</option>')
		}
		for(var i=0, length=dataList.length; i < length; i++){
			select.append('<option value="'+dataList[i].key+'">'+dataList[i].value+'</option>')
		}
	}
	function appendRadio(formId, layFilter, dataList, radioName){
		var radios=$("#"+formId+" [lay-filter='"+layFilter+"']");
		for(var i=0, length=dataList.length; i < length; i++){
			radios.append('<input type="radio" name="'+radioName+'" value="'+dataList[i].key+'" title="'+dataList[i].value+'">')
		}
	}
	function appendRadioIcon(formId, layFilter, dataList, radioName, defaultValue){
		var radioView=$("#"+formId+" [lay-filter='"+layFilter+"']");
		var radio=$('<div class="radio-icon"></div>')
		radioView.append(radio);
		radio.append('<input type="hidden" name="'+radioName+'"'+(defaultValue?' value="'+defaultValue+'"':"")+'>');
		for(var i=0, length=dataList.length; i < length; i++){
			radio.append('<span key="'+dataList[i].key+'" onclick="$.dictionary.radioIconClick(this, \''+formId+'\', \''+radioName+'\')">'+dataList[i].value+'</span>')
		}
		if(defaultValue){
			console.log(radio.find("[key='"+defaultValue+"']"));
			radio.find("[key='"+defaultValue+"']").addClass("checked");
		}
	}
})();