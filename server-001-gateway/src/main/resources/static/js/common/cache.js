(function(){
	$.cache={
		data: {},
		put: function(key, value){
			$.cache.data[key]=value;
		},
		get: function(key){
			return $.cache.data[key];
		},
		loadAjax: function(cacheKey, url, method, data, backParamName){
			var cacheData=$.cache.data[cacheKey];
			if(cacheData){
				return cacheData;
			}
			$.ajax({
				url: url,
				type: method,
				data: data?$.toJson(data):null,
				dataType: 'json',
				async: false,
				success: function(data){
					if(backParamName){
						cacheData=data[backParamName];
					}else{
						cacheData=data.dataList;
					}
					$.cache.data[cacheKey]=cacheData;
				}
			});
			return cacheData;
		}
	}
})();