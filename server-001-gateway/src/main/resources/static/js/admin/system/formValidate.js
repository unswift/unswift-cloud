(function(module){
	$.module.page.initView('formValidateList');
	var menuId=$.parseHrefParam('menuId');
	module[menuId]={
		tableCols: [function(){
			var cols=[//列表标题
				{ field: 'module', title: '模块'},
				{ field: 'operator', title: '操作'},
				{ field: 'keyword', title: '验证类型'},
				{ field: 'code', title: '代码'},
				{ field: 'message', title: '提示消息'},
				{ field: 'messageArgs', title: '消息参数'},
				{ field: 'codeLine', title: '验证顺序'},
				{ field: 'describe', title: '代码描述', width: 200},
				{ title: '状态', width:80, templet: '#formValidateListStatusToolbar'},
				{ fixed: 'right', title:'操作', width: 138, minWidth: 138, toolbar: '#formValidateListRowToolbar'}
			];
			var local = layui.data('formValidateListTable'); // 获取对应的本地记录
			layui.each(cols, function(index, item) {
				if (item.field in local) {
					item.hide = local[item.field];
				}
			});
			return cols;
		}()],
		search: function(){
			module[menuId].selectRow=null;
			var search=$.formToData('formValidateListSearchForm');
			$.table.render({
				elem: '#formValidateListTable',
				toolbar: '#formValidateListToolbar',
				defaultToolbar: ['filter', {title: '导出',layEvent: 'auto-export',icon: 'layui-icon-export'}, 'print'],
				url: '/system/systemFormValidate/findPageList', // 此处为静态模拟数据，实际使用时需换成真实接口
				method: 'post',
				contentType: 'application/json',
				request: {
					pageName: 'currPage',
					limitName: 'pageSize'
				},
				parseData: function(res) { // res 即为原始返回的数据
					return {
						"code": res.code, // 解析接口状态
						"msg": res.message, // 解析提示文本
						"count": res.body.totalSize, // 解析数据长度
						"data": res.body.dataList // 解析数据列表
					};
				},
				where: search,
				cols: module[menuId].tableCols,
				page: true,
				limit: 15,
				limits: [10, 15, 30, 50, 100, 200, 500, 1000],
				height: 'full-162',
				done: function(){
					$.auth.init($("#formValidateList"));
					module[menuId].loadEvent(this);
				}
			});
		},
		reload: function(){
			$.table.reload("formValidateListTable", {}, true);
		},
		loadEvent: function(that){
			$.table.on('row(formValidateListTable)', function(obj) {
				var data = obj.data; // 获取当前行数据
				obj.setRowChecked({
					type: 'radio' // radio 单选模式；checkbox 复选模式
				});
				module[menuId].selectRow=data;
				$("#formValidateList .layui-table-tool .layui-btn").each(function(){
					var button=$(this);
					if(button.hasClass('layui-btn-disabled')){
						button.removeClass('layui-btn-disabled');
					}
				});
			});
			$.table.on('toolbar(formValidateListTable)',function(obj){
				if(obj.event === 'add'){
					module[menuId].add();
				}else if(obj.event === 'edit'){
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					module[menuId].edit(module[menuId].selectRow.id);
				}else if(obj.event === 'delete'){
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					module[menuId].delete(module[menuId].selectRow.id);
				}else if(obj.event === 'auto-export'){
					module[menuId].export();
				}
			});
			$.table.on('tool(formValidateListTable)', function(obj){
				var data = obj.data; // 获得当前行数据
			    if(obj.event === 'edit'){
					module[menuId].edit(data.id);
				}else if(obj.event === 'delete'){
					module[menuId].delete(data.id);
				}
			});
			$.form.on('switch(formValidateStatus)', function(obj) {
				module[menuId].status(obj.value, obj.elem.checked);
			});
			that.elem.next().on('mousedown', 'input[lay-filter="LAY_TABLE_TOOL_COLS"]+', function() {
				var input = $(this).prev()[0];
				layui.data('formValidateListTable', {
					key: input.name,
					value: input.checked
				});
			});
		},
		add: function(){
			$.get('/pages/admin/system/formValidate/formValidateView.html', function(html){
				$.layer.open({
					title: '新建配置项',
					type: 1,
					area: ['900px', '660px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["确定", "取消"],
					yes: module[menuId].create
				});
			});
		},
		formRules: {
			key: [{ required: true, message: "键不能为空"}],
			value: [{ required: true, message: "值不能为空"}]
		},
		create: function(layerId){
			if(!$.valiForm('formValidateViewForm', module.currModule.formRules)){
				return false;
			}
			var data=$.formToData('formValidateViewForm');
			$.post('/system/systemFormValidate/create', data, function(res){
				$.success("操作成功");
				module[menuId].reload();
				$.layer.close(layerId);
			}, 'json');
		},
		edit: function(id){
			$.get('/pages/admin/system/formValidate/formValidateView.html', function(html){
				$.layer.open({
					title: '编辑配置项',
					type: 1,
					area: ['900px', '660px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["确定", "取消"],
					yes: module[menuId].update,
					success: function(){
						$.loadFormAjax('#formValidateViewForm', 'POST', '/system/systemFormValidate/view', $.toJson({id: id}));
					}
				});
			});
		},
		update: function(layerId){
			if(!$.valiForm('formValidateViewForm', module.currModule.formRules)){
				return false;
			}
			var data=$.formToData('formValidateViewForm');
			$.post('/system/systemFormValidate/update', data, function(res){
				$.success("操作成功");
				module[menuId].reload();
				$.layer.close(layerId);
			}, 'json');
		},
		status: function(id, status){
			$.post('/system/systemFormValidate/updateStatus',{id: id, status: status?1:0}, function(data){
				$.success("操作成功");
				module[menuId].reload();
			},'json');
		},
		export: function(){
			$.module.export.handle('配置项导出', '/imexport/systemFormValidate/export', $.formToData('formValidateListSearchForm'), '/imexport/systemFormValidate/stop');
		},
		delete: function(id){
			$.layer.confirm("确定要删除此数据吗？", {btn:['确定','取消']}, function(){
				$.post('/system/systemFormValidate/delete', {id: id}, function(data){
					$.success("操作成功");
					module[menuId].reload();
				}, 'json');
			}, function(){
				
			});
		}
	}
	$.module.currModule=$.module[menuId];
	module[menuId].search();
})($.module);