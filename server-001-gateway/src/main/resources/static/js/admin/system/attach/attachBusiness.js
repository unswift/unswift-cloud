(function(module){
	$.module.page.initView('attachBusinessList');
	$.laydate.render({elem: "#attachBusinessCreateTime", type: "datetime", range: ['#attachBusinessCreateStartTime', '#attachBusinessCreateEndTime']});
	$.dictionary.loadSelect('attachBusinessListSearchForm', 'attachBusinessSearchDataModule', 'module', true);
	$.form.render(null, 'attachBusinessListSearchForm');
	var menuId=$.parseHrefParam('menuId');
	module[menuId]={
		tableCols: [function(){
			var cols=[//列表标题
				{ field: 'dataId', title: '业务id', width: 120},
				{ field: 'dataModuleName', title: '业务模块', width: 100},
				{ field: 'name', title: '附件名称', width: 150},
				{ field: 'type', title: '附件类型'},
				{ field: 'fileSizeShow', title: '附件大小', width: 90},
				{ field: 'fileUrl', title: '下载路径'},
				{ field: 'isExistsName', title: '附件是否存在', width: 120},
				{ field: 'createUserName', title: '创建人', width: 100},
				{ field: 'createTime', title: '创建时间', width: 170},
				{ fixed: 'right', title:'操作', width: 80, minWidth: 80, toolbar: '#attachBusinessListRowToolbar'}
			];
			var local = layui.data('attachBusinessListTable'); // 获取对应的本地记录
			layui.each(cols, function(index, item) {
				if (item.field in local) {
					item.hide = local[item.field];
				}
			});
			return cols;
		}()],
		search: function(){
			module[menuId].selectRow=null;
			var search=$.formToData('attachBusinessListSearchForm');
			search.classifyList=['business'];
			$.table.render({
				elem: '#attachBusinessListTable',
				toolbar: '#attachBusinessListToolbar',
				defaultToolbar: ['filter', 'print'],
				url: '/attach/systemAttach/findPageList', // 此处为静态模拟数据，实际使用时需换成真实接口
				method: 'post',
				contentType: 'application/json',
				request: {
					pageName: 'currPage',
					limitName: 'pageSize'
				},
				parseData: function(res) { // res 即为原始返回的数据
					return {
						"code": res.code, // 解析接口状态
						"msg": res.message, // 解析提示文本
						"count": res.body.totalSize, // 解析数据长度
						"data": res.body.dataList // 解析数据列表
					};
				},
				where: search,
				cols: module[menuId].tableCols,
				page: true,
				limit: 15,
				limits: [10, 15, 30, 50, 100, 200, 500, 1000],
				height: 'full-220',
				done: function(){
					$.auth.init($("#attachBusinessList"));
					module[menuId].loadEvent(this);
				}
			});
		},
		reload: function(){
			$.table.reload("attachBusinessListTable", {}, true);
		},
		loadEvent: function(that){
			$.table.on('row(attachBusinessListTable)', function(obj) {
				var data = obj.data; // 获取当前行数据
				obj.setRowChecked({
					type: 'radio' // radio 单选模式；checkbox 复选模式
				});
				module[menuId].selectRow=data;
			});
			$.table.on('toolbar(attachBusinessListTable)',function(obj){
				if(obj.event === 'add'){//增加
					module[menuId].add();
				}else if(obj.event === 'download'){//编辑
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					module[menuId].download(module[menuId].selectRow.fileUrl);
				}
			});
			$.table.on('tool(attachBusinessListTable)', function(obj){
				var data = obj.data; // 获得当前行数据
			    if(obj.event === 'download'){
					module[menuId].download(data.fileUrl);
				}
			});
			that.elem.next().on('mousedown', 'input[lay-filter="LAY_TABLE_TOOL_COLS"]+', function() {
				var input = $(this).prev()[0];
				layui.data('attachBusinessListTable', {
					key: input.name,
					value: input.checked
				});
			});
		},
		add: function(){
			$.get('/pages/admin/system/attach/model/attachBusinessView.html', function(html){
				module[menuId].addLayerId=$.layer.open({
					title: '新建模板',
					type: 1,
					area: ['700px', '400px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["关闭"],
					yes: module[menuId].close,
					success: module[menuId].initUpload
				});
			});
		},
		formRules: {
			dataModule: [
				{ required: true, message: "所属模块不能为空"}
			]
		},
		initUpload: function(layerId){
			$.dictionary.loadSelect('attachBusinessViewForm', 'attachBusinessDataModule', 'module', true);
			$.form.render(null, 'attachBusinessViewForm');
			var data={classify: "model", saveTime: -1};
			$.upload.render({
				elem: '#attachBusiness-file', //绑定元素
				url: '/attach/systemAttach/upload', //上传接口
				accept: 'file',
				headers: {"upload": true},
				data: data,
				exts: 'xls|xlsx',
				before: function(obj){
					if(!$.valiForm('attachBusinessViewForm', module.currModule.formRules)){
						return false;
					}
					data.module=$("#attachBusinessViewForm [name='dataModule']").val();
					return true;
				},
				done: module[menuId].upload,
				error: function(error) {
					$.error(error);
				}
			});
		},
		upload: function(){
			$.success("操作成功");
			module[menuId].reload();
			$.layer.close(module[menuId].addLayerId);
		},
		close: function(layerId){
			$.layer.close(layerId);
		},
		clear: function(){
			$("#attachBusinessListSearchForm [name='name']").val('');
		},
		download: function(fileUrl){
			window.open(fileUrl);
		}
	};
	$.module.currModule=$.module[menuId];
	module[menuId].search();
})($.module);