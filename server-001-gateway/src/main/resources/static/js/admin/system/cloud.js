(function(module){
	$.module.page.initView('cloudList');
	var menuId=$.parseHrefParam('menuId');
	module[menuId]={
		tableCols: [function(){
			var cols=[//列表标题
				{ field: 'code', title: 'jar包名称'},
				{ field: 'name', title: '中文名称'},
				{ field: 'gatewayPath', title: '网关路径'},
				{ title: '挂载状态', width:85, templet: '#cloudListMmountStatusToolbar'},
				{ field: 'runningStatusName', title: '运行状态'},
				{ fixed: 'right', title:'操作', width: 138, minWidth: 138, toolbar: '#cloudListRowToolbar'}
			];
			var local = layui.data('cloudListTable'); // 获取对应的本地记录
			layui.each(cols, function(index, item) {
				if (item.field in local) {
					item.hide = local[item.field];
				}
			});
			return cols;
		}()],
		search: function(){
			module[menuId].selectRow=null;
			var search=$.formToData('cloudListSearchForm');
			$.table.render({
				elem: '#cloudListTable',
				toolbar: '#cloudListToolbar',
				defaultToolbar: ['filter', {title: '导出',layEvent: 'auto-export',icon: 'layui-icon-export'}, 'print'],
				url: '/system/systemCloud/findPageList', // 此处为静态模拟数据，实际使用时需换成真实接口
				method: 'post',
				contentType: 'application/json',
				request: {
					pageName: 'currPage',
					limitName: 'pageSize'
				},
				parseData: function(res) { // res 即为原始返回的数据
					return {
						"code": res.code, // 解析接口状态
						"msg": res.message, // 解析提示文本
						"count": res.body.totalSize, // 解析数据长度
						"data": res.body.dataList // 解析数据列表
					};
				},
				where: search,
				cols: module[menuId].tableCols,
				page: true,
				limit: 15,
				limits: [10, 15, 30, 50, 100, 200, 500, 1000],
				height: 'full-162',
				done: function(){
					$.auth.init($("#cloudList"));
					module[menuId].loadEvent(this);
				}
			});
		},
		reload: function(){
			$.table.reload("cloudListTable", {}, true);
		},
		loadEvent: function(that){
			$.table.on('row(cloudListTable)', function(obj) {
				var data = obj.data; // 获取当前行数据
				obj.setRowChecked({
					type: 'radio' // radio 单选模式；checkbox 复选模式
				});
				module[menuId].selectRow=data;
				$("#cloudList .layui-table-tool .layui-btn").each(function(){
					var button=$(this);
					if(button.hasClass('layui-btn-disabled')){
						button.removeClass('layui-btn-disabled');
					}
				});
			});
			$.table.on('toolbar(cloudListTable)',function(obj){
				if(obj.event === 'add'){//增加
					module[menuId].add();
				}else if(obj.event === 'import'){
					module[menuId].import();
				}else if(obj.event === 'edit'){//编辑
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					if(module[menuId].selectRow.isAdmin){
						$.error("系统微服务不能进行编辑操作");
						return false;
					}
					module[menuId].edit(module[menuId].selectRow.id);
				}else if(obj.event === 'authorize'){//授权
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					if(module[menuId].selectRow.isAdmin){
						$.error("系统微服务不能进行授权操作");
						return false;
					}
					module[menuId].authorize(module[menuId].selectRow.id);
				}else if(obj.event === 'delete'){
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					if(module[menuId].selectRow.isAdmin){
						$.error("系统微服务不能进行删除操作");
						return false;
					}
					module[menuId].delete(module[menuId].selectRow.id);
				}else if(obj.event === 'auto-export'){
					module[menuId].export();
				}else if(obj.event === 'lookLogger'){
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					module[menuId].lookLogger(module[menuId].selectRow.id);
				}
			});
			$.table.on('tool(cloudListTable)', function(obj){
				var data = obj.data; // 获得当前行数据
			    if(obj.event === 'edit'){
					module[menuId].edit(data.id);
				}else if(obj.event === 'authorize'){
					module[menuId].authorize(data.id);
				}else if(obj.event === 'delete'){
					module[menuId].delete(data.id);
				}
			});
			$.form.on('switch(cloudMountStatus)', function(obj) {
				module[menuId].mountStatus(obj.value, obj.elem.checked);
			});
			that.elem.next().on('mousedown', 'input[lay-filter="LAY_TABLE_TOOL_COLS"]+', function() {
				var input = $(this).prev()[0];
				layui.data('cloudListTable', {
					key: input.name,
					value: input.checked
				});
			});
		},
		export: function(){
			$.module.export.handle('微服务导出', '/imexport/systemCloud/export', $.formToData('cloudListSearchForm'), '/imexport/systemCloud/export/stop');
		},
		import: function(){
			$.module.import.handle(
				'微服务导入', 
				'systemCloud', 
				'xls|xlsx', 
				'/imexport/systemCloud/import', 
				'/imexport/systemCloud/import/stop',
				module[menuId].reload
			);
		},
		add: function(){
			$.get('/pages/admin/system/cloud/cloudView.html', function(html){
				$.layer.open({
					title: '新建微服务',
					type: 1,
					area: ['800px', '500px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["确定", "取消"],
					yes: module[menuId].create
				});
			});
		},
		formRules: {
			key: [{ required: true, message: "jar包名称不能为空"},{ regexp: /[0-9a-zA-Z_\\-]+/, message: "jar包名称只能输入字母、数字、下划线或-"}],
			name: [{ required: true, message: "中文名称不能为空"}]
		},
		create: function(layerId){
			if(!$.valiForm('cloudViewForm', module.currModule.formRules)){
				return false;
			}
			var data=$.formToData('cloudViewForm');
			$.post('/system/systemCloud/create', data, function(res){
				$.success("操作成功");
				module[menuId].reload();
				$.layer.close(layerId);
			}, 'json');
		},
		edit: function(id){
			$.get('/pages/admin/system/cloud/cloudView.html', function(html){
				$.layer.open({
					title: '编辑微服务',
					type: 1,
					area: ['700px', '450px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["确定", "取消"],
					yes: module[menuId].update,
					success: function(){
						$.loadFormAjax('#cloudViewForm', 'POST', '/system/systemCloud/view', $.toJson({id: id}), function(){
							$("#cloudViewForm [update='disabled']").attr("disabled",true);
						});
					}
				});
			});
		},
		update: function(layerId){
			if(!$.valiForm('cloudViewForm', module.currModule.formRules)){
				return false;
			}
			var data=$.formToData('cloudViewForm');
			$.post('/system/systemCloud/update', data, function(res){
				$.success("操作成功");
				module[menuId].reload();
				$.layer.close(layerId);
			}, 'json');
		},
		delete: function(id){
			$.layer.confirm("确定要删除此数据吗？", {btn:['确定','取消']}, function(){
				$.post('/system/systemCloud/delete', {id: id}, function(data){
					$.success("操作成功");
					module[menuId].reload();
				}, 'json');
			}, function(){
				
			});
		},
		mountStatus: function(id, status){
			$.post('/system/systemCloud/updateMountStatus',{id: id, mountStatus: status?1:0}, function(data){
				$.success("操作成功");
				module[menuId].reload();
			},'json');
		},
		clear: function(){
			$("#cloudListSearchForm [name='name']").val('');
		},
		lookLogger: function(id){
			$.module.logger.look('systemCloud', id);
		}
	};
	$.module.currModule=$.module[menuId];
	module[menuId].search();
})($.module);