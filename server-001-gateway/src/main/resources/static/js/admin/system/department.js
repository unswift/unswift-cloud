(function(module){
	//初始化树布局
	$.tree.initView('department-tree-view');
	//树的menu菜单id
	var menuId=$.parseHrefParam('menuId');
	//窗口变化时执行的方法
	$.resize.push({module:menuId, method: $.tree.initView, args: 'department-tree-view'});
	//资源树对象
	module[menuId]={
		loadTree:	function(name, spreadId){
			var search={};
			if(name){
				search.name=name;
			}
			if(spreadId){
				search.spreadId=spreadId;
			}
			$.post('/system/systemDepartment/findTreeList', search, function(data){
				$.tree.loadTree('department-tree', data, function(node){
					module[menuId].currNode=node;
					var id=node.data.id;
					module[menuId].toolbarShow('treeNodeClient', id);
					module[menuId].departmentView(id);
				});
				if(spreadId){
					module[menuId].toolbarShow('treeNodeClient', spreadId);
					module[menuId].departmentView(spreadId);
				}
			}, 'json');
		},
		treeSearch: function(search, event){
			if(event.keyCode==13){
				module[menuId].loadTree(search.value);
			}
		},
		toolbarShow: function(event, nodeId){
			var buttons=$("#department-tree-view .unswift-tree-toolbar .layui-btn");
			if(event=='treeNodeClient'){
				var button, type;
				buttons.each(function(){
					button=$(this);
					type=button.attr("type");
					if(nodeId==-1){
						if(type=='submit' || type=='addBrother' || type=='departmentUserList' || type=='delete'){
							if(!button.hasClass("layui-btn-disabled")){
								button.addClass("layui-btn-disabled");
							}
						}else if(button.hasClass("layui-btn-disabled")){
							button.removeClass("layui-btn-disabled");
						}
					}else if(button.hasClass("layui-btn-disabled")){
						button.removeClass("layui-btn-disabled");
					}
				});
			}else if(event=='addNext' || event=='addBrother'){
				buttons.each(function(){
					button=$(this);
					type=button.attr("type");
					if(type=='addNext' || type=='addBrother' || type=='departmentUserList' || type=='delete'){
						if(!button.hasClass("layui-btn-disabled")){
							button.addClass("layui-btn-disabled");
						}
					}else if(button.hasClass("layui-btn-disabled")){
						button.removeClass("layui-btn-disabled");
					}
				});
			}
		},
		export: function(){
			var searchValue=$("#department-tree-view [name='tree-search']").val();
			var data={};
			if(searchValue){
				data.name=searchValue;
			}
			$.module.export.handle(
				'部门导出', 
				'/imexport/systemDepartment/export', 
				data, 
				'/imexport/systemDepartment/export/stop'
			);
		},
		import: function(){
			$.module.import.handle(
				'部门导入', 
				'systemDepartment', 
				'xls|xlsx', 
				'/imexport/systemDepartment/import', 
				'/imexport/systemDepartment/import/stop',
				module[menuId].loadTree
			);
		},
		departmentView: function(id){
			$.get('/pages/admin/system/department/departmentView.html',function(html){
				var body=$("#department-tree-body");
				body.html(html);
				$("#departmentViewForm [create='false']").show();
				$.auth.init(body);
				$.dictionary.loadSelect('departmentViewForm', 'departmentType', 'departmentType');
				$.assembly.loadCheckbox({
					url: '/system/systemJob/findList', 
					method: "post", 
					data: {}, 
					keys: {
						value: 'code',
						label: 'name'
					},
					formId: 'departmentViewForm',
					layFilter: 'departmentJobs',
					checkboxName: 'jobList',
					multiple: true,
					cacheKey: 'jobList'
				});
				$.loadFormAjax('#departmentViewForm', 'POST', '/system/systemDepartment/view', $.toJson({id: id}), function(data, form){
					if(id==-1){
						$("#departmentViewForm [rootedit=false]").attr("disabled", true);
					}
					$("#departmentViewForm [update=false]").attr("disabled", true);
					$.form.render(null, 'departmentViewForm');
				});
			});
		},
		formRules:	{
			code	:	[{ required: true, message: "部门编码不能为空"}, { regexp: /[0-9a-zA-Z.]+/, message: "部门编码只能输入字母和数字"}],
			name	:	[{ required: true, message: "部门名称不能为空"}],
			type	:	[{ required: true, message: "部门类型不能为空"}]
		},
		handleUpdate: function (){
			if(!$.valiForm('departmentViewForm', module.currModule.formRules)){
				return false;
			}
			var data=$.formToData('departmentViewForm', ['jobList']);
			if(data.id){
				$.post('/system/systemDepartment/update', data, function(res){
					$.success("操作成功");
					module[menuId].loadTree($("#department-tree-view [name='tree-search']").val(), module[menuId].currNode.data.id);
				}, 'json');
			}else{
				$.post('/system/systemDepartment/create', data, function(res){
					$.success("操作成功");
					module[menuId].loadTree($("#department-tree-view [name='tree-search']").val(), module[menuId].currNode.data.id);
				}, 'json');
			}
		},
		addChild: function(){
			$.post('/system/systemDepartment/view',{id:module[menuId].currNode.data.id}, function(data){
				var parentId=module[menuId].currNode.data.id;
				var form={
					parentId: parentId==-1?null:parentId,
					parentName: data?data.name:"部门树",
					id: '',
					name:'',
					code: '',
					type: !data?'headOffice':(data.type=='headOffice'?'branchOffices':'department'),
					sort: '',
					describe: ''
				};
				$.loadFormData('#departmentViewForm', form, function(data, form){
					$("#departmentViewForm [rootedit=false]").attr("disabled", false);
					$.form.render(null, 'departmentViewForm');
					module[menuId].toolbarShow('addNext', module[menuId].currNode.data.id);
				});
			}, 'json');
		},
		addBrother: function(){
			$.post('/system/systemDepartment/view',{id:module[menuId].currNode.data.id}, function(data){
				var form={
					id: '',
					name:'',
					code: '',
					type: data.type,
					sort: '',
					describe: ''
				};
				$.loadFormData('#departmentViewForm', form, function(data, form){
					$("#departmentViewForm [rootedit=false]").attr("disabled", false);
					$.form.render(null, 'departmentViewForm');
					module[menuId].toolbarShow('addBrother', module[menuId].currNode.data.id);
				});
			}, 'json');
		},
		deleteThis: function(){
			$.layer.confirm("确定要删除此数据吗？", {btn:['确定','取消']}, function(){
				$.post('/system/systemDepartment/delete', {id:module[menuId].currNode.data.id}, function(){
					$.success("操作成功");
					module[menuId].currNode=null;
					module[menuId].loadTree($("#department-tree-view [name='tree-search']").val());
				}, 'json');
			}, function(){
				
			});
		},
		userList: function(){
			var nodeId=module[menuId].currNode.data.id;
			$.get('/pages/admin/system/department/userList.html',function(html){
				var height=660;
				$.layer.open({
					title: '用户列表',
					type: 1,
					area: ['90%', height+'px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["关闭"],
					yes: module[menuId].closeUserList,
					success: function(){
						$.auth.init($("#departmentUserListSearchForm"));
						module[menuId].userTableHeight=height-170;
						$("#departmentUserListSearchForm [name='departmentId']").val(nodeId);
						module[menuId].userSearch();
					}
				});
			});
		},
		closeUserList: function(layerId){
			$.layer.close(layerId);
		},
		userTableCols: [function(){
			var cols=[//列表标题
				{ field: 'account', title: '登录账号'},
				{ field: 'phone', title: '手机号'},
				{ field: 'email', title: '电子邮箱'},
				{ field: 'nickname', title: '昵称'},
				{ field: 'name', title: '姓名'},
				{ field: 'idCardTypeName', title: '证件类型'},
				{ field: 'idCardShow', title: '证件号'},
				{ field: 'roleNames', title: '角色'},
				{ title: '状态', width:85, templet: '#departmentUserListStatusToolbar'}
			];
			var local = layui.data('departmentUserListTable'); // 获取对应的本地记录
			layui.each(cols, function(index, item) {
				if (item.field in local) {
					item.hide = local[item.field];
				}
			});
			return cols;
		}()],
		userSearch: function(){
			module[menuId].selectRow=null;
			var search=$.formToData('departmentUserListSearchForm');
			$.table.render({
				elem: '#departmentUserListTable',
				url: '/system/systemDepartment/user/findPageList', // 此处为静态模拟数据，实际使用时需换成真实接口
				method: 'post',
				contentType: 'application/json',
				request: {
					pageName: 'currPage',
					limitName: 'pageSize'
				},
				parseData: function(res) { // res 即为原始返回的数据
					return {
						"code": res.code, // 解析接口状态
						"msg": res.message, // 解析提示文本
						"count": res.body.totalSize, // 解析数据长度
						"data": res.body.dataList // 解析数据列表
					};
				},
				where: search,
				cols: module[menuId].userTableCols,
				page: true,
				limit: 15,
				limits: [10, 15, 30, 50, 100, 200, 500, 1000],
				height: module[menuId].userTableHeight,
				done: function(){
					module[menuId].userLoadEvent(this);
				}
			});
		},
		userLoadEvent: function(){
			
		},
		userReload: function(){
			$.table.reload("departmentUserListTable", {}, true);
		},
		userClear: function(){
			var data={account:''};
			$.loadFormData("#departmentUserListSearchForm", data);
		},
		userImport: function(){
			$.module.import.handle(
				'用户导入', 
				'systemDepartmentUser', 
				'xls|xlsx', 
				'/imexport/systemDepartment/user/import', 
				'/imexport/systemDepartment/user/import/stop',
				module[menuId].userSearch,
				{departmentId: module[menuId].currNode.data.id}
			);
		}
	}
	$.auth.init($("#department-tree-view"));
	module[menuId].loadTree();//初始化
	module.currModule=module[menuId];
})($.module);