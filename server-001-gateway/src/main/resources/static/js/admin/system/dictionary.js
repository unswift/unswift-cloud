(function(module){
	$.module.page.initView('dictionaryTypeList');
	var menuId=$.parseHrefParam('menuId');
	module[menuId]={
		tableCols: [function(){
			var cols=[//列表标题
				{ field: 'code', title: '类型编码'},
				{ field: 'name', title: '类型名称'},
				{ field: 'describe', title: '描述', width: 200},
				{ field: 'createUserName', title: '创建人', width: 150},
				{ field: 'createTime', title: '创建时间', width: 170},
				{ fixed: 'right', title:'操作', width: 198, minWidth: 198, toolbar: '#dictionaryTypeListRowToolbar'}
			];
			var local = layui.data('dictionaryTypeListTable'); // 获取对应的本地记录
			layui.each(cols, function(index, item) {
				if (item.field in local) {
					item.hide = local[item.field];
				}
			});
			return cols;
		}()],
		search: function(){
			module[menuId].selectRow=null;
			var search=$.formToData('dictionaryTypeListSearchForm');
			$.table.render({
				elem: '#dictionaryTypeListTable',
				toolbar: '#dictionaryTypeListToolbar',
				defaultToolbar: ['filter', {title: '导出',layEvent: 'auto-export',icon: 'layui-icon-export'}, 'print'],
				url: '/system/systemDictionaryType/findPageList', // 此处为静态模拟数据，实际使用时需换成真实接口
				method: 'post',
				contentType: 'application/json',
				request: {
					pageName: 'currPage',
					limitName: 'pageSize'
				},
				parseData: function(res) { // res 即为原始返回的数据
					return {
						"code": res.code, // 解析接口状态
						"msg": res.message, // 解析提示文本
						"count": res.body.totalSize, // 解析数据长度
						"data": res.body.dataList // 解析数据列表
					};
				},
				where: search,
				cols: module[menuId].tableCols,
				page: true,
				limit: 15,
				limits: [10, 15, 30, 50, 100, 200, 500, 1000],
				height: 'full-162',
				done: function(){
					$.auth.init($("#dictionaryTypeList"));
					module[menuId].loadEvent(this);
				}
			});
		},
		reload: function(){
			$.table.reload("dictionaryTypeListTable", {}, true);
		},
		loadEvent: function(that){
			$.table.on('row(dictionaryTypeListTable)', function(obj) {
				var data = obj.data; // 获取当前行数据
				obj.setRowChecked({
					type: 'radio' // radio 单选模式；checkbox 复选模式
				});
				module[menuId].selectRow=data;
				$("#dictionaryTypeList .layui-table-tool .layui-btn").each(function(){
					var button=$(this);
					if(button.hasClass('layui-btn-disabled')){
						button.removeClass('layui-btn-disabled');
					}
				});
			});
			$.table.on('toolbar(dictionaryTypeListTable)',function(obj){
				if(obj.event === 'add'){
					module[menuId].add();
				}else if(obj.event === 'import'){
					module[menuId].import();
				}else if(obj.event === 'edit'){
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					module[menuId].edit(module[menuId].selectRow.id);
				}else if(obj.event === 'delete'){
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					module[menuId].delete(module[menuId].selectRow.id);
				}else if(obj.event === 'auto-export'){
					module[menuId].export();
				}else if(obj.event === 'dictionary-tree'){
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					module[menuId].dictionaryTree(module[menuId].selectRow.code);
				}
			});
			$.table.on('tool(dictionaryTypeListTable)', function(obj){
				var data = obj.data; // 获得当前行数据
			    if(obj.event === 'edit'){
					module[menuId].edit(data.id);
				}else if(obj.event === 'delete'){
					module[menuId].delete(data.id);
				}else if(obj.event === 'dictionary-tree'){
					module[menuId].dictionaryTree(data.code);
				}
			});
			that.elem.next().on('mousedown', 'input[lay-filter="LAY_TABLE_TOOL_COLS"]+', function() {
				var input = $(this).prev()[0];
				layui.data('dictionaryTypeListTable', {
					key: input.name,
					value: input.checked
				});
			});
		},
		add: function(){
			$.get('/pages/admin/system/dictionary/dictionaryTypeView.html', function(html){
				$.layer.open({
					title: '新建数据字典类型',
					type: 1,
					area: ['700px', '400px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["确定", "取消"],
					yes: module[menuId].create
				});
			});
		},
		formRules: {
			code: [{ required: true, message: "类型编码不能为空"},{ regexp: /[0-9a-zA-Z]+/, message: "类型编码只能输入字母和数字"}],
			name: [{ required: true, message: "类型名称不能为空"}]
		},
		create: function(layerId){
			if(!$.valiForm('dictionaryTypeViewForm', module.currModule.formRules)){
				return false;
			}
			var data=$.formToData('dictionaryTypeViewForm');
			$.post('/system/systemDictionaryType/create', data, function(res){
				$.success("操作成功");
				module[menuId].reload();
				$.layer.close(layerId);
			}, 'json');
		},
		edit: function(id){
			$.get('/pages/admin/system/dictionary/dictionaryTypeView.html', function(html){
				$.layer.open({
					title: '编辑数据字典类型',
					type: 1,
					area: ['700px', '400px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["确定", "取消"],
					yes: module[menuId].update,
					success: function(){
						$.loadFormAjax('#dictionaryTypeViewForm', 'POST', '/system/systemDictionaryType/view', $.toJson({id: id}), function(){
							$("#dictionaryTypeViewForm [update='disabled']").attr("disabled",true);
						});
					}
				});
			});
		},
		update: function(layerId){
			if(!$.valiForm('dictionaryTypeViewForm', module.currModule.formRules)){
				return false;
			}
			var data=$.formToData('dictionaryTypeViewForm');
			$.post('/system/systemDictionaryType/update', data, function(res){
				$.success("操作成功");
				module[menuId].reload();
				$.layer.close(layerId);
			}, 'json');
		},
		export: function(){
			$.module.export.handle('数据字典类型导出', '/imexport/systemDictionaryType/export', $.formToData('dictionaryTypeListSearchForm'), '/imexport/systemDictionaryType/stop');
		},
		import: function(){
			$.module.import.handle(
				'数据字典类型导入', 
				'systemDictionaryType', 
				'xls|xlsx', 
				'/imexport/systemDictionaryType/import', 
				'/imexport/systemDictionaryType/import/stop',
				module[menuId].reload
			);
		},
		delete: function(id){
			$.layer.confirm("确定要删除此数据吗？", {btn:['确定','取消']}, function(){
				$.post('/system/systemDictionaryType/delete', {id: id}, function(data){
					$.success("操作成功");
					module[menuId].reload();
				}, 'json');
			}, function(){
				
			});
		},
		dictionaryTree: function(typeCode){
			$.get('/pages/admin/system/dictionary/dictionaryTree.html', function(html){
				$.layer.open({
					title: '编辑数据字典子项',
					type: 1,
					area: ['900px', '600px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["关闭"],
					yes: module[menuId].closeTree,
					success: function(){
						$.auth.init($("#dictionary-tree-view"));
						module[menuId].selectTypeCode=typeCode;
						module[menuId].loadTree();
					}
				});
			});
		},
		closeTree: function(layerId){
			$.layer.close(layerId);
		},
		loadTree:	function(value, spreadId){
			var search={type:module[menuId].selectTypeCode};
			if(value){
				search.value=value;
			}
			if(spreadId){
				search.spreadId=spreadId;
			}
			$.post('/system/systemDictionary/findTreeList', search, function(data){
				$.tree.loadTree('dictionary-tree', data, function(node){
					module[menuId].currNode=node;
					var id=node.data.id;
					module[menuId].toolbarShow('treeNodeClient', id);
					module[menuId].dictionaryView(id);
				});
				if(spreadId){
					module[menuId].toolbarShow('treeNodeClient', spreadId);
					module[menuId].dictionaryView(spreadId);
				}
			}, 'json');
		},
		treeSearch: function(search, event){
			if(event.keyCode==13){
				module[menuId].loadTree(search.value);
			}
		},
		toolbarShow: function(event, nodeId){
			var buttons=$("#dictionary-tree-view .unswift-tree-toolbar .layui-btn");
			if(event=='treeNodeClient'){
				var button, type;
				buttons.each(function(){
					button=$(this);
					type=button.attr("type");
					if(nodeId==-1){
						if(type=='submit' || type=='addBrother' || type=='dictionaryUserList' || type=='delete'){
							if(!button.hasClass("layui-btn-disabled")){
								button.addClass("layui-btn-disabled");
							}
						}else if(button.hasClass("layui-btn-disabled")){
							button.removeClass("layui-btn-disabled");
						}
					}else if(button.hasClass("layui-btn-disabled")){
						button.removeClass("layui-btn-disabled");
					}
				});
			}else if(event=='addNext' || event=='addBrother'){
				buttons.each(function(){
					button=$(this);
					type=button.attr("type");
					if(type=='addNext' || type=='addBrother' || type=='dictionaryUserList' || type=='delete'){
						if(!button.hasClass("layui-btn-disabled")){
							button.addClass("layui-btn-disabled");
						}
					}else if(button.hasClass("layui-btn-disabled")){
						button.removeClass("layui-btn-disabled");
					}
				});
			}
		},
		exportDictionary: function(){
			var searchValue=$("#dictionary-tree-view [name='tree-search']").val();
			var data={type:module[menuId].selectTypeCode};
			if(searchValue){
				data.value=searchValue;
			}
			$.module.export.handle(
				'数据字典项导出', 
				'/imexport/systemDictionary/export', 
				data, 
				'/imexport/systemDictionary/export/stop'
			);
		},
		importDictionary: function(){
			$.module.import.handle(
				'数据字典项导入', 
				'systemDictionary', 
				'xls|xlsx', 
				'/imexport/systemDictionary/import', 
				'/imexport/systemDictionary/import/stop',
				module[menuId].loadTree,
				{type:module[menuId].selectTypeCode}
			);
		},
		dictionaryView: function(id){
			$.get('/pages/admin/system/dictionary/dictionaryView.html',function(html){
				var body=$("#dictionary-tree-body");
				body.html(html);
				$("#dictionaryViewForm [create='false']").show();
				$.auth.init(body);
				$.assembly.loadSelect({
					url: "/system/systemLanguage/findList",
					method: "post",
					data: {"activeStatus":1},
					keys: {
						value: "shortName",
						label: "fullName"
					},
					formId: "dictionaryViewForm",
					layFilter: "dictionaryLanguage",
					cacheKey: "languageList"
				});
				$.loadFormAjax('#dictionaryViewForm', 'POST', '/system/systemDictionary/view', $.toJson({id: id}), function(data, form){
					if(id==-1){
						$("#dictionaryViewForm [rootedit=false]").attr("disabled", true);
					}
					$("#dictionaryViewForm [update=false]").attr("disabled", true);
					$.form.render(null, 'dictionaryViewForm');
				});
			});
		},
		dictionaryFormRules:	{
			key	:	[{ required: true, message: "数据字典键不能为空"}, { regexp: /[0-9a-zA-Z.]+/, message: "数据字典键只能输入字母和数字"}],
			value	:	[{ required: true, message: "数据字典值不能为空"}]
		},
		dictionaryHandleUpdate: function (){
			if(!$.valiForm('dictionaryViewForm', module.currModule.dictionaryFormRules)){
				return false;
			}
			var data=$.formToData('dictionaryViewForm', ['jobList']);
			if(data.id){
				$.post('/system/systemDictionary/update', data, function(res){
					$.success("操作成功");
					module[menuId].loadTree($("#dictionary-tree-view [name='tree-search']").val(), module[menuId].currNode.data.id);
				}, 'json');
			}else{
				$.post('/system/systemDictionary/create', data, function(res){
					$.success("操作成功");
					module[menuId].loadTree($("#dictionary-tree-view [name='tree-search']").val(), module[menuId].currNode.data.id);
				}, 'json');
			}
		},
		addChild: function(){
			$.post('/system/systemDictionary/view',{id:module[menuId].currNode.data.id}, function(data){
				var parentId=module[menuId].currNode.data.id;
				var type=module[menuId].currNode.data.type;
				var typeName=module[menuId].currNode.data.typeName;
				var form={
					parentId: parentId==-1?null:parentId,
					parentName: data?data.name:"",
					id: '',
					key: '',
					value:'',
					type: type,
					typeName: typeName,
					sort: '',
					describe: ''
				};
				$.assembly.loadCheckbox({
					url: "/system/systemLanguage/findList",
					method: "post",
					data: {"activeStatus":1},
					keys: {
						value: "shortName",
						label: "fullName"
					},
					formId: "dictionaryViewForm",
					layFilter: "languageListView",
					checkboxName: "languageList",
					multiple: true,
					cacheKey: "languageList"
				});
				$.loadFormData('#dictionaryViewForm', form, function(data, form){
					$("#dictionaryViewForm [rootedit=false]").attr("disabled", false);
					$.form.render(null, 'dictionaryViewForm');
					module[menuId].toolbarShow('addNext', module[menuId].currNode.data.id);
				});
			}, 'json');
		},
		addBrother: function(){
			$.post('/system/systemDictionary/view',{id:module[menuId].currNode.data.id}, function(data){
				var type=module[menuId].currNode.data.type;
				var typeName=module[menuId].currNode.data.typeName;
				var form={
					id: '',
					key:'',
					value: '',
					type: type,
					typeName: typeName,
					sort: '',
					describe: ''
				};
				$.loadFormData('#dictionaryViewForm', form, function(data, form){
					$("#dictionaryViewForm [rootedit=false]").attr("disabled", false);
					$.form.render(null, 'dictionaryViewForm');
					module[menuId].toolbarShow('addBrother', module[menuId].currNode.data.id);
				});
			}, 'json');
		},
		deleteThis: function(){
			$.layer.confirm("确定要删除此数据吗？", {btn:['确定','取消']}, function(){
				$.post('/system/systemDictionary/delete', {id:module[menuId].currNode.data.id}, function(){
					$.success("操作成功");
					module[menuId].currNode=null;
					module[menuId].loadTree($("#dictionary-tree-view [name='tree-search']").val());
				}, 'json');
			}, function(){
				
			});
		},
	}
	$.module.currModule=$.module[menuId];
	module[menuId].search();
})($.module);