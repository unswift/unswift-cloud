(function(module){
	$.module.page.initView('languageList');
	var menuId=$.parseHrefParam('menuId');
	module[menuId]={
		tableCols: [function(){
			var cols=[//列表标题
				{ field: 'shortName', title: '简称'},
				{ field: 'fullName', title: '全称'},
				{ field: 'describe', title: '描述', width: 200},
				{ title: '激活', width:85, templet: '#languageListStatusToolbar'},
				{ fixed: 'right', title:'操作', width: 138, minWidth: 138, toolbar: '#languageListRowToolbar'}
			];
			var local = layui.data('languageListTable'); // 获取对应的本地记录
			layui.each(cols, function(index, item) {
				if (item.field in local) {
					item.hide = local[item.field];
				}
			});
			return cols;
		}()],
		search: function(){
			module[menuId].selectRow=null;
			var search=$.formToData('languageListSearchForm');
			$.table.render({
				elem: '#languageListTable',
				toolbar: '#languageListToolbar',
				defaultToolbar: ['filter', {title: '导出',layEvent: 'auto-export',icon: 'layui-icon-export'}, 'print'],
				url: '/system/systemLanguage/findPageList', // 此处为静态模拟数据，实际使用时需换成真实接口
				method: 'post',
				contentType: 'application/json',
				request: {
					pageName: 'currPage',
					limitName: 'pageSize'
				},
				parseData: function(res) { // res 即为原始返回的数据
					return {
						"code": res.code, // 解析接口状态
						"msg": res.message, // 解析提示文本
						"count": res.body.totalSize, // 解析数据长度
						"data": res.body.dataList // 解析数据列表
					};
				},
				where: search,
				cols: module[menuId].tableCols,
				page: true,
				limit: 15,
				limits: [10, 15, 30, 50, 100, 200, 500, 1000],
				height: 'full-162',
				done: function(){
					$.auth.init($("#languageList"));
					module[menuId].loadEvent(this);
				}
			});
		},
		reload: function(){
			$.table.reload("languageListTable", {}, true);
		},
		loadEvent: function(that){
			$.table.on('row(languageListTable)', function(obj) {
				var data = obj.data; // 获取当前行数据
				obj.setRowChecked({
					type: 'radio' // radio 单选模式；checkbox 复选模式
				});
				module[menuId].selectRow=data;
				$("#languageList .layui-table-tool .layui-btn").each(function(){
					var button=$(this);
					if(button.hasClass('layui-btn-disabled')){
						button.removeClass('layui-btn-disabled');
					}
				});
			});
			$.table.on('toolbar(languageListTable)',function(obj){
				if(obj.event === 'add'){
					module[menuId].add();
				}else if(obj.event === 'import'){
					module[menuId].import();
				}else if(obj.event === 'edit'){
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					module[menuId].edit(module[menuId].selectRow.id);
				}else if(obj.event === 'delete'){
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					module[menuId].delete(module[menuId].selectRow.id);
				}else if(obj.event === 'auto-export'){
					module[menuId].export();
				}
			});
			$.table.on('tool(languageListTable)', function(obj){
				var data = obj.data; // 获得当前行数据
			    if(obj.event === 'edit'){
					module[menuId].edit(data.id);
				}else if(obj.event === 'delete'){
					module[menuId].delete(data.id);
				}
			});
			$.form.on('switch(languageActiveStatus)', function(obj) {
				module[menuId].status(obj.value, obj.elem.checked);
			});
			that.elem.next().on('mousedown', 'input[lay-filter="LAY_TABLE_TOOL_COLS"]+', function() {
				var input = $(this).prev()[0];
				layui.data('languageListTable', {
					key: input.name,
					value: input.checked
				});
			});
		},
		add: function(){
			$.get('/pages/admin/system/language/languageView.html', function(html){
				$.layer.open({
					title: '新建系统语言',
					type: 1,
					area: ['700px', '400px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["确定", "取消"],
					yes: module[menuId].create
				});
			});
		},
		formRules: {
			shortName: [{ required: true, message: "语言简称不能为空"},{ regexp: /[A-Z_]+/, message: "语言简称只能由大写字母和_组成"}],
			fullName: [{ required: true, message: "语言全称不能为空"}]
		},
		create: function(layerId){
			if(!$.valiForm('languageViewForm', module.currModule.formRules)){
				return false;
			}
			var data=$.formToData('languageViewForm');
			$.post('/system/systemLanguage/create', data, function(res){
				$.success("操作成功");
				module[menuId].reload();
				$.layer.close(layerId);
			}, 'json');
		},
		edit: function(id){
			$.get('/pages/admin/system/language/languageView.html', function(html){
				$.layer.open({
					title: '编辑系统语言',
					type: 1,
					area: ['700px', '400px'],
					shade: 0.6, // 遮罩透明度
					shadeClose: true, // 点击遮罩区域，关闭弹层
					content: html,
					btn: ["确定", "取消"],
					yes: module[menuId].update,
					success: function(){
						$.loadFormAjax('#languageViewForm', 'POST', '/system/systemLanguage/view', $.toJson({id: id}), function(){
							$("#languageViewForm [update='disabled']").attr("disabled",true);
						});
					}
				});
			});
		},
		update: function(layerId){
			if(!$.valiForm('languageViewForm', module.currModule.formRules)){
				return false;
			}
			var data=$.formToData('languageViewForm');
			$.post('/system/systemLanguage/update', data, function(res){
				$.success("操作成功");
				module[menuId].reload();
				$.layer.close(layerId);
			}, 'json');
		},
		export: function(){
			$.module.export.handle('系统语言导出', '/imexport/systemLanguage/export', $.formToData('languageListSearchForm'), '/imexport/systemLanguage/stop');
		},
		import: function(){
			$.module.import.handle(
				'系统语言导入', 
				'systemLanguage', 
				'xls|xlsx', 
				'/imexport/systemLanguage/import', 
				'/imexport/systemLanguage/import/stop',
				module[menuId].reload
			);
		},
		status: function(id, status){
			$.post('/system/systemLanguage/updateStatus',{id: id, activeStatus: status?1:0}, function(data){
				$.success("操作成功");
				module[menuId].reload();
			},'json');
		},
		delete: function(id){
			$.layer.confirm("确定要删除此数据吗？", {btn:['确定','取消']}, function(){
				$.post('/system/systemLanguage/delete', {id: id}, function(data){
					$.success("操作成功");
					module[menuId].reload();
				}, 'json');
			}, function(){
				
			});
		}
	}
	$.module.currModule=$.module[menuId];
	module[menuId].search();
})($.module);