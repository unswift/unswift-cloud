(function(module){
	$.module.page.initView('attachTempList');
	$.laydate.render({elem: "#attachTempCreateTime", type: "datetime", range: ['#attachTempCreateStartTime', '#attachTempCreateEndTime']});
	$.dictionary.loadSelect('attachTempListSearchForm', 'attachTempSearchClassify', 'attachClassify', true, "temp");
	$.dictionary.loadSelect('attachTempListSearchForm', 'attachTempSearchDataModule', 'module', true);
	$.form.render(null, 'attachTempListSearchForm');
	var menuId=$.parseHrefParam('menuId');
	module[menuId]={
		tableCols: [function(){
			var cols=[//列表标题
				{ field: 'dataId', title: '业务id', width: 120},
				{ field: 'classifyName', title: '所属分类', width: 100},
				{ field: 'dataModuleName', title: '业务模块', width: 100},
				{ field: 'name', title: '附件名称', width: 150},
				{ field: 'type', title: '附件类型'},
				{ field: 'fileSizeShow', title: '附件大小', width: 90},
				{ field: 'fileUrl', title: '下载路径'},
				{ field: 'isExistsName', title: '附件是否存在', width: 120},
				{ field: 'createUserName', title: '创建人', width: 100},
				{ field: 'createTime', title: '创建时间', width: 170},
				{ fixed: 'right', title:'操作', width: 80, minWidth: 80, toolbar: '#attachTempListRowToolbar'}
			];
			var local = layui.data('attachTempListTable'); // 获取对应的本地记录
			layui.each(cols, function(index, item) {
				if (item.field in local) {
					item.hide = local[item.field];
				}
			});
			return cols;
		}()],
		search: function(){
			module[menuId].selectRow=null;
			var search=$.formToData('attachTempListSearchForm');
			search.classifyList=['import','export'];
			$.table.render({
				elem: '#attachTempListTable',
				toolbar: '#attachTempListToolbar',
				defaultToolbar: ['filter', 'print'],
				url: '/attach/systemAttach/findPageList', // 此处为静态模拟数据，实际使用时需换成真实接口
				method: 'post',
				contentType: 'application/json',
				request: {
					pageName: 'currPage',
					limitName: 'pageSize'
				},
				parseData: function(res) { // res 即为原始返回的数据
					return {
						"code": res.code, // 解析接口状态
						"msg": res.message, // 解析提示文本
						"count": res.body.totalSize, // 解析数据长度
						"data": res.body.dataList // 解析数据列表
					};
				},
				where: search,
				cols: module[menuId].tableCols,
				page: true,
				limit: 15,
				limits: [10, 15, 30, 50, 100, 200, 500, 1000],
				height: 'full-220',
				done: function(){
					$.auth.init($("#attachTempList"));
					module[menuId].loadEvent(this);
				}
			});
		},
		reload: function(){
			$.table.reload("attachTempListTable", {}, true);
		},
		loadEvent: function(that){
			$.table.on('row(attachTempListTable)', function(obj) {
				var data = obj.data; // 获取当前行数据
				obj.setRowChecked({
					type: 'radio' // radio 单选模式；checkbox 复选模式
				});
				module[menuId].selectRow=data;
			});
			$.table.on('toolbar(attachTempListTable)',function(obj){
				if(obj.event === 'download'){//编辑
					if(!module[menuId].selectRow){
						$.error("未选定行");
						return false;
					}
					module[menuId].download(module[menuId].selectRow.fileUrl);
				}
			});
			$.table.on('tool(attachTempListTable)', function(obj){
				var data = obj.data; // 获得当前行数据
			    if(obj.event === 'download'){
					module[menuId].download(data.fileUrl);
				}
			});
			that.elem.next().on('mousedown', 'input[lay-filter="LAY_TABLE_TOOL_COLS"]+', function() {
				var input = $(this).prev()[0];
				layui.data('attachTempListTable', {
					key: input.name,
					value: input.checked
				});
			});
		},
		clear: function(){
			$("#attachTempListSearchForm [name='name']").val('');
		},
		download: function(fileUrl){
			window.open(fileUrl);
		}
	};
	$.module.currModule=$.module[menuId];
	module[menuId].search();
})($.module);