(function(module){
	$.module.page.initView('loginRecordList');
	$.laydate.render({elem: "#loginRecordCreateTime", type: "datetime", range: ['#loginRecordCreateStartTime', '#loginRecordCreateEndTime']});
	$.form.render(null, 'loginRecordListSearchForm');
	var menuId=$.parseHrefParam('menuId');
	module[menuId]={
		tableCols: [function(){
			var cols=[//列表标题
				{ field: 'account', title: '登录账号'},
				{ field: 'loginTime', title: '登录时间'},
				{ field: 'loginResultName', title: '登录结果'},
				{ field: 'errorCode', title: '错误编码'}
			];
			var local = layui.data('loginRecordListTable'); // 获取对应的本地记录
			layui.each(cols, function(index, item) {
				if (item.field in local) {
					item.hide = local[item.field];
				}
			});
			return cols;
		}()],
		search: function(){
			module[menuId].selectRow=null;
			var search=$.formToData('loginRecordListSearchForm');
			search.classifyList=['business'];
			$.table.render({
				elem: '#loginRecordListTable',
				toolbar: '#loginRecordListToolbar',
				defaultToolbar: ['filter', {title: '导出',layEvent: 'auto-export',icon: 'layui-icon-export'}, 'print'],
				url: '/logger/loggerLoginRecord/findPageList', // 此处为静态模拟数据，实际使用时需换成真实接口
				method: 'post',
				contentType: 'application/json',
				request: {
					pageName: 'currPage',
					limitName: 'pageSize'
				},
				parseData: function(res) { // res 即为原始返回的数据
					return {
						"code": res.code, // 解析接口状态
						"msg": res.message, // 解析提示文本
						"count": res.body.totalSize, // 解析数据长度
						"data": res.body.dataList // 解析数据列表
					};
				},
				where: search,
				cols: module[menuId].tableCols,
				page: true,
				limit: 15,
				limits: [10, 15, 30, 50, 100, 200, 500, 1000],
				height: 'full-220',
				done: function(){
					$.auth.init($("#loginRecordList"));
					module[menuId].loadEvent(this);
				}
			});
		},
		reload: function(){
			$.table.reload("loginRecordListTable", {}, true);
		},
		loadEvent: function(that){
			$.table.on('row(loginRecordListTable)', function(obj) {
				var data = obj.data; // 获取当前行数据
				obj.setRowChecked({
					type: 'radio' // radio 单选模式；checkbox 复选模式
				});
				module[menuId].selectRow=data;
			});
			$.table.on('toolbar(loginRecordListTable)',function(obj){
				if(obj.event === 'auto-export'){
					module[menuId].export();
				}
			});
			$.table.on('tool(loginRecordListTable)', function(obj){
				
			});
			that.elem.next().on('mousedown', 'input[lay-filter="LAY_TABLE_TOOL_COLS"]+', function() {
				var input = $(this).prev()[0];
				layui.data('loginRecordListTable', {
					key: input.name,
					value: input.checked
				});
			});
		},
		export: function(){
			$.module.export.handle('登录日志导出', '/imexport/loggerLoginRecord/export', $.formToData('loginRecordListSearchForm'), '/imexport/loggerLoginRecord/export/stop');
		}
	};
	$.module.currModule=$.module[menuId];
	module[menuId].search();
})($.module);