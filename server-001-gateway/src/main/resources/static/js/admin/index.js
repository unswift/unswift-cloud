(function(module){
	module.index={
		homePage: '/pages/admin/home.html',
		
		init: function(){
			$.get(homePage, function(html){
				$("#home").html(html);
			});
			$.post('/login/getUserInfo',function(data){
				$.config.user=data;
				$("#user-info").html(data.nickname);
				module.index.initMenu();
			},'json');
			$.element.on('tab(main-tabs)', function(tab){
				var currTab=tab.elem;
				var layId=currTab.find(".layui-tab-title .layui-this").attr('lay-id');
				$.module.currModule=$.module[layId];
				$.setHrefParam("menuId", layId);
			});
		},
		initMenu: function(){
			var root=$.auth.getByKey('managerPlatform');
			if(root){
				var headerMenuId=$.parseHrefParam('headerMenuId');
				if(headerMenuId){
					var menuResource=$.auth.getById(headerMenuId);
					if(!menuResource){
						headerMenuId=undefined;
					}
				}
				var children=$.auth.getChilds(root.id, 'folder'), menuChildren, menuChildren2;
				if(!headerMenuId && children && children.length>0){
					headerMenuId=children[0].id;
				}
				var menuId=$.parseHrefParam('menuId');
				var parentFolderId="home";
				if(menuId){
					var menuResource=$.auth.getById(menuId);
					if(!menuResource){
						menuId=undefined;
					}else{
						parentFolderId=menuResource.parentId;
					}
				}
				var html='', menuHtml='';
				menuHtml+='<li class="layui-nav-item'+(parentFolderId=='home'?'':' layui-nav-itemed')+'" dataid="home" title="首页" url="'+homePage+'">';
				menuHtml+='<a class="" href="javascript:;"><i class="layui-icon layui-icon-home menu-icon font16 vertical-align-center"></i>首页</a>';
				menuHtml+="</li>";
				for(var i=0, length=children.length; i<length; i++){//顶部菜单
					html+='<li class="layui-nav-item layui-hide-xs'+(headerMenuId==children[i].id?' layui-this':'')+'" dataid="'+children[i].id+'">';
					html+='<a href="javascript:;">';
					if(children[i].icon){
						html+='<i class="layui-icon '+children[i].icon+' menu-icon font16 vertical-align-center"></i>';
					}
					html+=children[i].name+'</a></li>';
					menuChildren=$.auth.getChilds(children[i].id, 'folder');
					if(menuChildren){
						for(var j=0, length2=menuChildren.length; j < length2; j++){//左侧父菜单
							menuHtml+='<li class="layui-nav-item'+(parentFolderId==menuChildren[j].id?' layui-nav-itemed':'')+'" dataid="'+menuChildren[j].id+'" parentid="'+menuChildren[j].parentId+'"'+(menuChildren[j].parentId==headerMenuId?'':' style="display:none;"')+'>';
							menuHtml+='<a class="" href="javascript:;">';
							if(menuChildren[j].icon){
								menuHtml+='<i class="layui-icon '+menuChildren[j].icon+' menu-icon font16 vertical-align-center"></i>';
							}
							menuHtml+=menuChildren[j].name+'</a>';
							menuChildren2=$.auth.getChilds(menuChildren[j].id, 'menu');
							if(menuChildren2){
								menuHtml+='<dl class="layui-nav-child">';
								for(var k=0, length3=menuChildren2.length; k < length3; k++){//左侧子菜单
									menuHtml+='<dd dataid="'+menuChildren2[k].id+'" title="'+menuChildren2[k].name+'" url="'+menuChildren2[k].url+'"'+(menuId==menuChildren2[k].id?' class="layui-this"':'')+'>';
									menuHtml+='<a href="javascript:;">';
									if(menuChildren2[k].icon){
										menuHtml+='<i class="layui-icon '+menuChildren2[k].icon+' menu-icon font16 vertical-align-center"></i>';
									}
									menuHtml+=menuChildren2[k].name+'</a></dd>';
								}
								menuHtml+='</dl>';
							}
							menuHtml+="</li>";
						}
					}
				}
				$("#header-menu").append($(html));
				$("#left-menu").append($(menuHtml));
				$.element.render('nav', 'header-menu');
				$.element.render('nav', 'left-menu');
				$.element.on('nav(header-menu)', $.module.index.clientHeaderMenu);
				$.element.on('nav(left-menu)', $.module.index.clientLeftMenu);
				if(parentFolderId!='home' && menuId){
					var menu=$("#left-menu [dataid='"+menuId+"']");
					module.index.clientLeftMenu(menu.children(":eq(0)"));
				}
			}
		},
		logout: function(){
			$.post("/login/logout", function(){
				window.location.href="/pages/admin/login.html";
			},'json');
		},
		clientHeaderMenu: function(menu){
			menu=menu.parent();
			var parentId=menu.attr('dataid');
			$.setHrefParam("headerMenuId", parentId);
			var children=$("#left-menu").children();
			var childMenu;
			for(var i=0, length=children.length; i<length; i++){
				childMenu=children.eq(i);
				if(childMenu.attr('dataid')=='home'){
					continue;
				}
				if(childMenu.attr('parentid')==parentId){
					childMenu.show();
				}else{
					childMenu.hide();
				}
			}
		},
		clientLeftMenu: function(menu){
			menu=menu.parent();
			var menuid=menu.attr('dataid');
			var url=menu.attr('url');
			if(url){
				$.removeModule(menuid);
				$.element.tabDelete("main-tabs", menuid);
				var title=menu.attr('title');
				$.get(url, function(html){
					$.setHrefParam("menuId", menuid);
					$.element.tabAdd('main-tabs', {
						id: menuid,
						title: title,
						content: html,
						change: true
					});
				});
			}
		}
	}
})($.module);