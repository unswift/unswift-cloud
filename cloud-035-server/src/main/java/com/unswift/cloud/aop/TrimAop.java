package com.unswift.cloud.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.annotation.trim.WantTrim;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.pojo.Pojo;
import com.unswift.utils.ObjectUtils;

@Aspect
@Component
@Order(Ordered.HIGHEST_PRECEDENCE+1)
@Api(value="去空处理AOP", author = "unswift", date = "2024-04-14", version = "1.0.0")
public class TrimAop {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@ApiMethod("去空的切面")
	@Pointcut("@annotation(com.unswift.cloud.annotation.trim.TrimHandle)")
	public void entryTrimPoint() {
	}

	@ApiMethod(value="去空锁切面扩展", params = {@ApiField("切面的方法封装"), @ApiField("去空注解")})
	@Around("entryTrimPoint() && @annotation(trim)")
	public Object around(ProceedingJoinPoint point, TrimHandle trim) throws Throwable {
		try {
			Object[] args = point.getArgs();
			if(ObjectUtils.isNotEmpty(args)) {
				ObjectUtils.trim(args, Pojo.class, trim.value().equals(WantTrim.class));//去空处理
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
		try {
			return point.proceed();
		} catch (Exception e) {
			throw e;
		}
	}
	
}
