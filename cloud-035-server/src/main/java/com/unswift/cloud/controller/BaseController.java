package com.unswift.cloud.controller;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.core.CommonOperator;

@Api(value="controller基类，所有的controller类必须基础此类", author = "unswift", date = "2024-04-16", version = "1.0.0")
public abstract class BaseController extends CommonOperator{
	
}
