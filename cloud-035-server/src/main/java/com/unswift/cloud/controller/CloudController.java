package com.unswift.cloud.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.TestVo;

@Api(value="微服务公共controller，提供一些公共操作", author = "unswift", date = "2024-04-16", version = "1.0.0")
public class CloudController extends BaseController{

	@Active
	@RequestMapping(value="/testing", method=RequestMethod.GET)
	@ApiMethod(value="测试服务器状态", returns=@ApiField("测试回调信息"))
	public ResponseBody<TestVo> testing(){
		return ResponseBody.success(new TestVo("服务正常"));
	}
	
}
