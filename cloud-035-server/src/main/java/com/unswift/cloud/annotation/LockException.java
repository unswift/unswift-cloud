package com.unswift.cloud.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Api(value="异常锁，当前被注解的方法如果有程序再执行，则抛出异常", author = "unswift")
public @interface LockException {

	@ApiMethod(value="锁前缀")
	String prefix() default "";
	
	@ApiMethod(value="数据锁的唯一标识，如果不设置，则表示被注解的方法单通道，如果返回的数组大于1，则表示多字段组合锁定"
			+ "规则：<br/>如果不获取参数，表示是字符串，如：unique=all，<br/>"
			+ "如果要获取第一个参数，使用$开头，如：unique=$.id，<br/>"
			+ "如果第一参数是列表，要获取列表某行的值，可使用$[]，如：unique=$[1].id，<br/>"
			+ "如果要获取非第一个参数，可使用[]，如：unique=[1].id，<br/>"
			+ "如果要获取当前用户id，可使用currUserId", returns = @ApiField("数据锁的唯一标识"))
	String[] unique() default "";
	
	@ApiMethod(value="异常锁消息的内容code，必须配置", returns = @ApiField("消息的内容code"))
	String messageCode();
	
	@ApiMethod(value="异常锁消息的内容参数", returns = @ApiField("消息的内容参数"))
	String[] messageArgs() default {};
	
	@ApiMethod(value="锁方式{redis：redis锁，memory：内存锁}，如果未配置，则使用nacos配置")
	String method() default "";
}
