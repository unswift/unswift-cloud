package com.unswift.cloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.Pojo;
import com.unswift.cloud.pojo.mso.logger.operator.LoggerOperatorQueueMso;
import com.unswift.cloud.rabbit.RabbitConstant;
import com.unswift.cloud.rabbit.RabbitQueue;
import com.unswift.cloud.utils.LoggerUtils;

@Service
@Api(value="操作日志服务", author = "unswift", date = "2024-01-10", version = "1.0.0")
public class LoggerService implements RabbitConstant{

	@Autowired
	@ApiField("Rabbit Mq发送消息服务")
	private RabbitQueue rabbitQueue;
	
	public <E extends Pojo> void writer(long dataId, String module, OperatorTypeEnum operatorType, String token, E entity) {
		LoggerOperatorQueueMso loggerMso=new LoggerOperatorQueueMso();
		loggerMso.setModule(module);
		loggerMso.setOperatorType(operatorType.getKey());
		loggerMso.setToken(token);
		loggerMso.setDataId(dataId);
		loggerMso.setOperatorContent(LoggerUtils.toLoggerString(entity));
		rabbitQueue.sendMessage(LOGGER_OPERATOR_EXCHANGE_NAME, LOGGER_OPERATOR_ROUTE_KEY, loggerMso);//发送导出队列
	}
	
}
