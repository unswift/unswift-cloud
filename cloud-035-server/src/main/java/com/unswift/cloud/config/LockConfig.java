package com.unswift.cloud.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "unswift.cache.lock")
@ApiEntity(value="缓存锁相关yml配置实体", author = "unswift", date = "2023-09-08", version = "1.0.0")
public class LockConfig {

	@ApiField("锁超时时间")
	private long timeout;
	
	@ApiField("锁方式")
	private String method;
}
