package com.unswift.cloud.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.feign.config.FeignConfiguration;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachDownloadPathDto;
import com.unswift.cloud.pojo.dto.system.attach.SystemAttachListDto;
import com.unswift.cloud.pojo.vo.ListVo;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachDownloadPathVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachDownloadVo;
import com.unswift.cloud.pojo.vo.system.attach.SystemAttachListVo;

@Component
@FeignClient(value = "server-attach", configuration = FeignConfiguration.class)
@Api(
		value="附件服务器feign，其它服务器访问系统服务器，则通过此接口访问，宇燕平台不推荐使用feign调用，保证每个微服务的独立性，可通过Adapter模块实现相关业务", 
		author="unswift", 
		date="2023-04-16", 
		version="1.0.0"
)
public interface AttachFeign {

	@RequestMapping(value="/systemAttach/findList", method=RequestMethod.POST)
	@ApiMethod(value="查询附件列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	ResponseBody<ListVo<SystemAttachListVo>> findAttachList(@RequestBody SystemAttachListDto searchDto);
	
	@RequestMapping(value="/systemAttach/getDownloadPath", method=RequestMethod.POST)
	@ApiMethod(value="根据业务id获取下载路径", params=@ApiField("业务id Dto对象"), returns=@ApiField("路径对象"))
	ResponseBody<SystemAttachDownloadPathVo> getAttachDownloadPath(@RequestBody SystemAttachDownloadPathDto searchDto);
	
	@RequestMapping(value="/systemAttach/downloadByte/{fileUrlName}", method=RequestMethod.GET)
    @ApiMethod(value="下载附件-返回byte数组", params=@ApiField("附件路径"))
    ResponseBody<SystemAttachDownloadVo> downloadByte(@PathVariable("fileUrlName")String fileUrlName);
}
