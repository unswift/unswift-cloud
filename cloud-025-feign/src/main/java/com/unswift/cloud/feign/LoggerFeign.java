package com.unswift.cloud.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

import com.unswift.annotation.api.Api;
import com.unswift.cloud.feign.config.FeignConfiguration;

@Component
@FeignClient(value = "server-logger", configuration = FeignConfiguration.class)
@Api(
		value="日志服务器feign，其它服务器访问系统服务器，则通过此接口访问，宇燕平台不推荐使用feign调用，保证每个微服务的独立性，可通过Adapter模块实现相关业务", 
		author="unswift", 
		date="2023-04-16", 
		version="1.0.0"
)
public interface LoggerFeign {

}
