package com.unswift.cloud.enums.imports.task;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

@Api(value="导出任务事件枚举", author = "unswift", date = "2024-01-09", version = "1.0.0")
public enum ImportTaskEventEnum {
	START("start", "开始导入"),
	STOP("stop", "停止导入"),
	;
	
	@ApiField("导入事件key")
	private String key;
	@ApiField("导入事件描述")
	private String value;
	private ImportTaskEventEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
