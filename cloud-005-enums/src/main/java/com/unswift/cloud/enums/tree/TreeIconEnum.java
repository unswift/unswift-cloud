package com.unswift.cloud.enums.tree;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

@Api(value="树节点图标映射", author = "unswift", date = "2024-02-28", version = "1.0.0")
public enum TreeIconEnum {
	ORGANIZATION("organization", "组织机构图标"),
	AUTHORIZE("authorize", "授权图标"),
	USER("user", "用户图标（单）"),
	USERS("users", "用户图标（多）"),
	;
	
	@ApiField("导出事件key")
	private String key;
	@ApiField("导出事件描述")
	private String value;
	private TreeIconEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
