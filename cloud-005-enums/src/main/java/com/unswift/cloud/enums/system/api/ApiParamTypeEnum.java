package com.unswift.cloud.enums.system.api;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.utils.ExceptionUtils;

@Api(value="api参数类型映射", author = "unswift", date = "2023-09-07", version = "1.0.0")
public enum ApiParamTypeEnum {
	STRING(String.class, "varchar"),
	BOOLEAN(boolean.class, "boolean"),
	BOOLEAN2(Boolean.class, "boolean"),
	CHAR(char.class, "char"),
	CHARACTER(Character.class, "char"),
	BYTE(byte.class, "int"),
	BYTE2(Byte.class, "int"),
	SHORT(short.class, "int"),
	SHORT2(Short.class, "int"),
	INT(int.class, "int"),
	INTEGER(Integer.class, "int"),
	LONG(long.class, "int"),
	LONG2(Long.class, "int"),
	FLOAT(float.class, "double"),
	FLOAT2(Float.class, "double"),
	DOUBLE(double.class, "double"),
	DOUBLE2(Double.class, "double"),
	DECIMAL(BigDecimal.class, "double"),
	NUMBER(Number.class, "number"),
	DATETIME(Date.class, "datetime"),
	LIST(Iterable.class, "array"),
	ARRAY(Array.class, "array"),
	MAP(Map.class, "object"),
	MULTIPARTFILE(MultipartFile.class, "file"),
	OBJECT(Object.class, "object"),
	;
	
	
	@ApiField("参数java类型")
	private Class<?> key;
	@ApiField("参数展示类型")
	private String value;
	private ApiParamTypeEnum(Class<?> key, String value) {
		this.key = key;
		this.value = value;
	}
	public Class<?> getKey() {
		return key;
	}
	public void setKey(Class<?> key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public static String match(Class<?> type) {
		if(type.isArray()) {
			return ARRAY.value;
		}
		for (ApiParamTypeEnum paramType : values()) {
			if(paramType.getKey().isAssignableFrom(type)) {
				return paramType.value;
			}
		}
		throw ExceptionUtils.message("unable.to.match.parameter.type", type.getName());
	}
}
