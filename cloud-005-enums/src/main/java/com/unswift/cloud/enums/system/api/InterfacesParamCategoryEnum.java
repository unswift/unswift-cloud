package com.unswift.cloud.enums.system.api;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

@Api(value="接口参数分类枚举", author = "unswift", date = "2023-09-10", version = "1.0.0")
public enum InterfacesParamCategoryEnum {
	PATH("path", "请求路径参数"),
	URL("param", "url参数"),
	FORM("form", "表单参数"),
	BODY("body", "body参数"),
	RETURN("return", "返回参数"),
	;
	
	@ApiField("参数分类key")
	private String key;
	@ApiField("参数分类描述")
	private String describe;
	private InterfacesParamCategoryEnum(String key, String describe) {
		this.key = key;
		this.describe = describe;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	
}
