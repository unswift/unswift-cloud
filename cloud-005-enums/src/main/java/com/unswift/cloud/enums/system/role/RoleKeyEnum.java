package com.unswift.cloud.enums.system.role;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

@Api(value="角色key的特殊枚举", author = "unswift", date = "2023-08-08", version = "1.0.0")
public enum RoleKeyEnum {
	ADMINISTRATOR("administrator", "系统管理员"),
	;
	
	@ApiField("角色key")
	private String key;
	@ApiField("角色key描述")
	private String describe;
	private RoleKeyEnum(String key, String describe) {
		this.key = key;
		this.describe = describe;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	
}
