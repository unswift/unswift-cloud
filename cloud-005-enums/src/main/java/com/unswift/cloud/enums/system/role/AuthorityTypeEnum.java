package com.unswift.cloud.enums.system.role;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

@Api(value="会员-角色的权限类型，分所有者和管理者，所有者是当前人拥有的权限，管理者是当前人创建的权限", author="unswift", date="2023-04-16", version="1.0.0")
public enum AuthorityTypeEnum {
	OCCUPANT("occupant", "使用者，分配给我的权限"),
	MANAGER("manager", "管理者，指我创建的权限"),
	;
	
	@ApiField("权限类型key")
	private String key;
	@ApiField("权限类型描述")
	private String value;
	private AuthorityTypeEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
