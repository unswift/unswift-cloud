package com.unswift.cloud.enums.export.task;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;

@Api(value="导出任务事件枚举", author = "unswift", date = "2023-08-08", version = "1.0.0")
public enum ExportTaskEventEnum {
	START("start", "开始导出"),
	STOP("stop", "停止导出"),
	;
	
	@ApiField("导出事件key")
	private String key;
	@ApiField("导出事件描述")
	private String value;
	private ExportTaskEventEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
