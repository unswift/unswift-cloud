package com.unswift.cloud.exception;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.exception.CoreException;

@SuppressWarnings("serial")
@Api(value="数据源异常类", author="unswift", date="2023-04-08", version="1.0.0")
public class DataSourceException extends CoreException {
	
	@ApiConstructor(value="数据源异常类构造", params={@ApiField("异常编码"), @ApiField("异常消息")})
	public DataSourceException(String code, String message) {
		super(code, message);
	}

	@ApiConstructor(value="数据源异常类构造", params={@ApiField("异常编码"), @ApiField("异常消息"), @ApiField("异常")})
	public DataSourceException(String code, String message, Throwable e) {
		super(code, message, e);
	}

}
