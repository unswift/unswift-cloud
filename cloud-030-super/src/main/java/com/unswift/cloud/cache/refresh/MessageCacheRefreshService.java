package com.unswift.cloud.cache.refresh;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.config.SystemMessageAdapter;
import com.unswift.cloud.cache.CacheEnum;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.message.SystemMessageDataDo;
import com.unswift.cloud.pojo.dao.system.message.SystemMessageSearchDo;
import com.unswift.cloud.pojo.po.system.SystemMessage;
import com.unswift.core.entity.Message;
import com.unswift.utils.ClassUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.JsonUtils;
import com.unswift.utils.ObjectUtils;

import jakarta.annotation.PostConstruct;

@Service
@Api(value="消息缓存刷新服务", author="unswift", date="2023-05-05", version="1.0.0")
public class MessageCacheRefreshService implements ICacheRefreshService{

	@ApiField("日志对象")
	private final Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	@ApiField("系统消息")
	private SystemMessageAdapter systemMessageAdapter;
	
	@ApiField("spring cloud应用名称")
	@Value("${spring.application.name}")
	private String serverName;
	
	@PostConstruct
	@ApiMethod(value="内存缓存组件初始化")
	public void messageInit(){
		this.refresh(CacheEnum.SYSTEM_MESSAGE);
	}
	
	@Override
	@ApiMethod(value="刷新缓存实现方法", params=@ApiField("缓存枚举"))
	@SuppressWarnings("unchecked")
	public synchronized void refresh(CacheEnum cacheEnum) {
		SystemMessageSearchDo search=new SystemMessageSearchDo();
		Sql sql=Sql.createSql();
		sql.addWhereAnd(true, false, "server_", Sql.COMPARE_EQUAL, Sql.sqlValue(serverName, null));
		sql.addWhereOr(false, true, "server_", Sql.COMPARE_EQUAL, Sql.sqlValue("*", null));
		search.setSql(sql);
		List<SystemMessageDataDo> messageList = systemMessageAdapter.findList(search);
		List<Message> cacheMessageList=new ArrayList<Message>();
		if(ObjectUtils.isNotEmpty(messageList)){
			Message cacheMessage;
			for (SystemMessage message : messageList) {
				cacheMessage=new Message();
				cacheMessage.setKey(message.getKey());
				cacheMessage.setLanguage(message.getLanguage());
				cacheMessage.setCode(message.getCode());
				cacheMessage.setMessage(message.getMessage());
				if(ObjectUtils.isNotEmpty(message.getArgsOrder())){
					cacheMessage.setArgsOrder(JsonUtils.toJava(message.getArgsOrder(), List.class, Integer.class));
				}
				cacheMessage.setException(ClassUtils.forName(message.getExceptionClass()));
				cacheMessageList.add(cacheMessage);
			}
			ExceptionUtils.loadingMessageList(cacheMessageList);
			logger.error(CacheEnum.SYSTEM_MESSAGE.getKey()+"缓存刷新");
		}
	}

}
