package com.unswift.cloud.pojo.po.logger;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.Date;
import com.unswift.cloud.pojo.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="登录记录映射实体", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecord extends BasePo{
	
	@ApiField("登录账号")
	private String account;
	
	@ApiField("登录时间")
	private Date loginTime;
	
	@ApiField("登录状态{0：登录失败，1：登录成功}")
	private Byte loginResult;
	
	@ApiField("登录错误码system_message_表的code")
	private String errorCode;
	
}
