package com.unswift.cloud.pojo.po.system;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统语言映射实体", author="liyunlong", date="2024-02-10", version="1.0.0")
public class SystemLanguage extends BasePo{
	
	@ApiField("简称")
	private String shortName;
	
	@ApiField("全称")
	private String fullName;
	
	@ApiField("描述")
	private String describe;
	
	@ApiField("激活状态{0：未激活，1：已激活}")
	private Byte activeStatus;
	
}
