package com.unswift.cloud.pojo.dto.system.role;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色出参更新Dto实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemRoleAuthorizeInputUpdateBto extends BaseDto{
	
	@ApiField("入参id")
	private Long inputId;
	
	@ApiField("入参名称")
	private Long inputName;
	
}
