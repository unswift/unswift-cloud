package com.unswift.cloud.pojo.dao.system.dictionary.type;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemDictionaryType;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典类型查询结果实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypeDataDo extends SystemDictionaryType implements BaseDo{
	
	@ApiField("创建人姓名")
	private String createUserName;
}
