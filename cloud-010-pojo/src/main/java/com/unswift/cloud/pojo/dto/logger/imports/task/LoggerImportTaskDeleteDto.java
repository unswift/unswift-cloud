package com.unswift.cloud.pojo.dto.logger.imports.task;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导入任务记录表删除Dto实体", author="liyunlong", date="2024-01-09", version="1.0.0")
public class LoggerImportTaskDeleteDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
}
