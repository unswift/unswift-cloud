package com.unswift.cloud.pojo.vo.system.role;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色分页查询返回实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRolePageVo extends BaseVo{
	
	@ApiField("角色状态{}")
	private Long id;
	
	@ApiField("角色key")
	private String key;
	
	@ApiField("角色名称")
	private String name;
	
	@ApiField("角色状态{1：启用，2：停用}")
	private Byte status;
	
	@ApiField("是否管理员功能{1：是，0：否}")
	private Byte isAdmin;
	
	@ApiField("创建人姓名-所有者")
	private String createUserName;
	
	@ApiField("变更时间")
	@JsonFormat(timezone="GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date changeTime;
}
