package com.unswift.cloud.pojo.bo.system.resource.output;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.bo.BaseBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源入参根据权限key查询业务实体", author="liyunlong", date="2023-11-24", version="1.0.0")
public class SystemResourceOutputAuthBo extends BaseBo{

	@ApiField("资源权限key")
	private String authKey;
	
}
