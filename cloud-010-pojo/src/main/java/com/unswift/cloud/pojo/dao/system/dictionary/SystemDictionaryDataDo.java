package com.unswift.cloud.pojo.dao.system.dictionary;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.po.system.SystemDictionary;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典查询结果实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryDataDo extends SystemDictionary implements BaseDo{
    
	@ApiField("父资源名称")
    private String parentName;
	
    @ApiField("类型")
	private String typeName;
}
