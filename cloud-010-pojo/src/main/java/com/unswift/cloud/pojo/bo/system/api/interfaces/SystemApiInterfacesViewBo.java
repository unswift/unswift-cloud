package com.unswift.cloud.pojo.bo.system.api.interfaces;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api详情业务实体", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiInterfacesViewBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;

	@ApiConstructor("默认构造")
	public SystemApiInterfacesViewBo() {
		super();
	}
	
	@ApiConstructor(value="带参构造", params = @ApiField("主键"))
	public SystemApiInterfacesViewBo(Long id) {
		super();
		this.id = id;
	}



	
}
