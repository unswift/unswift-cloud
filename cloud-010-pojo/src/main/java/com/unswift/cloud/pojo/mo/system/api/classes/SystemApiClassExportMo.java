package com.unswift.cloud.pojo.mo.system.api.classes;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("类api")
@ApiEntity(value="类api导出模板实体", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiClassExportMo extends BaseMo{
	
	@ApiField("类类型{interface：接口, enum：枚举,class：类，entity：实体}")
	@ExcelColumn("类类型{interface：接口, enum：枚举,class：类，entity：实体}")
	private String classType;
	
	@ApiField("修饰符{public：公共的，private：私有的...}")
	@ExcelColumn("修饰符{public：公共的，private：私有的...}")
	private String modifiers;
	
	@ApiField("类描述")
	@ExcelColumn("类描述")
	private String classComment;
	
	@ApiField("类名称")
	@ExcelColumn("类名称")
	private String className;
	
	@ApiField("作者")
	@ExcelColumn("作者")
	private String author;
	
	@ApiField("日期")
	@ExcelColumn(value="日期", format="yyyy-MM-dd")
	private Date date;
	
	@ApiField("版本")
	@ExcelColumn("版本")
	private String version;
	
	@ApiField("已过期{0：未过期，1：已过期}")
	@ExcelColumn("已过期{0：未过期，1：已过期}")
	private Boolean deprecated;
	
}
