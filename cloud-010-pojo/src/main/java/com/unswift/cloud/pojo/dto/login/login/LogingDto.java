package com.unswift.cloud.pojo.dto.login.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="登录接口登录Dto实体", author="unswift", date="2023-05-31", version="1.0.0")
public class LogingDto extends BaseDto{

	@ApiField(value="登录账号", testExample = "admin")
	private String account;
	
	@ApiField(value="密码", testExample = "E10ADC3949BA59ABBE56E057F20F883E")
	private String password;
	
	@ApiField(value="验证码", testExample = "1234")
	private String validateCode;

}
