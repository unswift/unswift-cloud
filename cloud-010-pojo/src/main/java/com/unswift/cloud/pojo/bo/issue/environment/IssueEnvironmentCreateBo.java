package com.unswift.cloud.pojo.bo.issue.environment;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="发布环境新建业务实体", author="liyunlong", date="2024-04-16", version="1.0.0")
public class IssueEnvironmentCreateBo extends BaseBo{
	
	@ApiField("环境编码")
	private String code;
	
	@ApiField("环境名称")
	private String name;
	
}
