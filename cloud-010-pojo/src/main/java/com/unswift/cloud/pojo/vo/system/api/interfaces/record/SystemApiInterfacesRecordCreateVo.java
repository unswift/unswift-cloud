package com.unswift.cloud.pojo.vo.system.api.interfaces.record;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api执行记录新建返回实体", author="unswift", date="2023-09-24", version="1.0.0")
public class SystemApiInterfacesRecordCreateVo extends BaseVo{
	
	@ApiField("新建数据id")
	private Long id;
	@ApiField("执行结果")
	private Integer result;
	
	@ApiConstructor("默认构造")
	public SystemApiInterfacesRecordCreateVo(){
		
	}
	
	@ApiConstructor(value="默认构造", params=@ApiField("执行结果"))
	public SystemApiInterfacesRecordCreateVo(Integer result){
		this.result=result;
	}
}
