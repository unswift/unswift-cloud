package com.unswift.cloud.pojo.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="测试接口返回实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class TestVo extends BaseVo{

	@ApiConstructor("默认构造")
	public TestVo() {
		
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("消息")})
	public TestVo(String message) {
		this.message=message;
	}
	
	@ApiField("返回消息")
	private String message;
	
}
