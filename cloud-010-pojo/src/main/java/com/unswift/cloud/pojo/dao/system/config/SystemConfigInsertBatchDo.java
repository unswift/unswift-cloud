package com.unswift.cloud.pojo.dao.system.config;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统配置批量插入封装实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigInsertBatchDo implements BatchDo<SystemConfigInsertBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemConfigInsertBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemConfigInsertBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量插入列表")})
	public SystemConfigInsertBatchDo(List<SystemConfigInsertBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
