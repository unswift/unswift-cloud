package com.unswift.cloud.pojo.dao.system.api.interfaces;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemApiInterfaces;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api插入记录实体", author="unswift", date="2023-09-09", version="1.0.0")
public class SystemApiInterfacesInsertDo extends SystemApiInterfaces implements BaseDo{
	
	
}
