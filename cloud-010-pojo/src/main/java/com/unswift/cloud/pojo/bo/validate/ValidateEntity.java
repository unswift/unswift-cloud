package com.unswift.cloud.pojo.bo.validate;

import java.util.HashMap;
import java.util.Map;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.core.entity.TokenUser;
import com.unswift.utils.ObjectUtils;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@ApiEntity(value="验证实体", author="unswift", date="2023-05-31", version="1.0.0")
public class ValidateEntity<T> {

	@ApiField("表单实体")
	private T entity;
	
	@ApiField("请求对象")
	private HttpServletRequest request;
	
	@ApiField("请求回调对象")
	private HttpServletResponse response;
	
	@ApiField("当前会话对象")
	private HttpSession session;
	
	@ApiField("登录用户")
	private TokenUser user;
	
	@ApiField("基本参数配置")
	private Map<String, String> config;
	
	@ApiField("扩展属性")
	private Map<String, Object> attribute;

	public T getEntity() {
		return entity;
	}

	public void setEntity(T entity) {
		this.entity = entity;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public TokenUser getUser() {
		return user;
	}

	public void setUser(TokenUser user) {
		this.user = user;
	}

	public Map<String, String> getConfig() {
		return config;
	}
	
	@SuppressWarnings("unchecked")
	public <V> V getConfig(String key){
		if(ObjectUtils.isEmpty(config)){
			return null;
		}
		return (V)config.get(key);
	}

	public void setConfig(Map<String, String> config) {
		this.config = config;
	}

	public Map<String, Object> getAttribute() {
		return attribute;
	}

	public void setAttribute(Map<String, Object> attribute) {
		this.attribute = attribute;
	}
	
	@SuppressWarnings("unchecked")
	public <O> O getAttribute(String key) {
		if(ObjectUtils.isNull(key)){
			return null;
		}
		return (O)attribute.get(key);
	}
	
	public void addAttribute(String key, Object value) {
		if(ObjectUtils.isNull(this.attribute)){
			this.attribute=new HashMap<String, Object>();
		}
		this.attribute.put(key, value);
	}
	
	public void addAttributes(Map<String, Object> attribute) {
		if(ObjectUtils.isNull(this.attribute)){
			this.attribute=new HashMap<String, Object>();
		}
		if(ObjectUtils.isNotEmpty(attribute)) {
			this.attribute.putAll(attribute);
		}
	}
}
