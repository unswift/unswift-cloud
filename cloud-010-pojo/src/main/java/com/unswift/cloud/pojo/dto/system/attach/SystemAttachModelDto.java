package com.unswift.cloud.pojo.dto.system.attach;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件模板查询Dto实体", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachModelDto extends BaseDto{
	
	@ApiField("业务模块")
	private String dataModule;
	
	@ApiConstructor("默认构造")
	public SystemAttachModelDto(){
		
	}

}
