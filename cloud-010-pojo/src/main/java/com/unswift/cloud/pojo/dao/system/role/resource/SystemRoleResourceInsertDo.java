package com.unswift.cloud.pojo.dao.system.role.resource;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemRoleResource;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="角色资源插入记录实体", author="liyunlong", date="2023-12-02", version="1.0.0")
public class SystemRoleResourceInsertDo extends SystemRoleResource implements BaseDo{
	
	
}
