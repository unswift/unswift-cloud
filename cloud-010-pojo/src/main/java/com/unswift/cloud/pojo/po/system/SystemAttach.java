package com.unswift.cloud.pojo.po.system;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.po.SortPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件映射实体", author="liyunlong", date="2024-01-21", version="1.0.0")
public class SystemAttach extends SortPo{
	
	@ApiField("业务id")
	private Long dataId;
	
	@ApiField("业务模块")
	private String dataModule;
	
	@ApiField("附件名称")
	private String name;
	
	@ApiField("附件分类")
	private String classify;
	
	@ApiField("附件类型")
	private String type;
	
	@ApiField("文件大小")
	private Long fileSize;
	
	@ApiField("附件路径")
	private String filePath;
	
	@ApiField("附件下载路径")
	private String fileUrl;
	
	@ApiField("保存时间（天）")
	private Integer saveTime;
	
	@ApiField("附件是否存在{0：已删除，1：存在}")
	private Byte isExists;
	
}
