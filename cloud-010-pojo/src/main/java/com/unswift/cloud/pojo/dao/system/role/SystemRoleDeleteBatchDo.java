package com.unswift.cloud.pojo.dao.system.role;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.PkListDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色多条记录删除实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRoleDeleteBatchDo implements PkListDo<Long>{
	
	@ApiField("角色状态{}集合")
	private List<Long> idList;
	
	@ApiConstructor("默认构造")
	public SystemRoleDeleteBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("删除的主键集合")})
	public SystemRoleDeleteBatchDo(List<Long> idList){
		super();
		this.idList=idList;
	}

}
