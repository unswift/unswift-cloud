package com.unswift.cloud.pojo.dao.system.user;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="会员批量插入封装实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemUserInsertBatchDo implements BatchDo<SystemUserInsertBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemUserInsertBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemUserInsertBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量插入列表")})
	public SystemUserInsertBatchDo(List<SystemUserInsertBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
