package com.unswift.cloud.pojo.dao.issue.environment;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.issue.IssueEnvironment;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="发布环境批量插入记录实体", author="liyunlong", date="2024-04-16", version="1.0.0")
public class IssueEnvironmentInsertBatchItemDo extends IssueEnvironment implements BaseDo{
	
	
}
