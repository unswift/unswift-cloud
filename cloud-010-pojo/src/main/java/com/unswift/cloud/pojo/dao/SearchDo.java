package com.unswift.cloud.pojo.dao;

import java.util.Date;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@ApiEntity(value="数据库访问层查询实体（接口），宇燕微服务架构规定，do为系统的数据库实体层，所有的mapper层操作必须使用此层实体", author="unswift", date="2023-06-13", version="1.0.0")
public interface SearchDo extends AutoSqlDo,PkListDo<Long>{

	@ApiMethod(value="查询条件-获取创建时间-开始", returns=@ApiField("创建时间-开始"))
	public Date getCreateStartTime();

	@ApiMethod(value="查询条件-设置创建时间-开始", params=@ApiField("创建时间-开始"))
	public void setCreateStartTime(Date createStartTime);

	@ApiMethod(value="查询条件-获取创建时间-结束", returns=@ApiField("创建时间-结束"))
	public Date getCreateEndTime();

	@ApiMethod(value="查询条件-设置创建时间-结束", params=@ApiField("创建时间-结束"))
	public void setCreateEndTime(Date createEndTime);
	
	@ApiMethod(value="查询条件-获取变更时间-开始", returns=@ApiField("变更时间-开始"))
	public Date getChangeStartTime();

	@ApiMethod(value="查询条件-设置变更时间-开始", params=@ApiField("变更时间-开始"))
	public void setChangeStartTime(Date changeStartTime);

	@ApiMethod(value="查询条件-获取变更时间-结束", returns=@ApiField("变更时间-结束"))
	public Date getChangeEndTime();

	@ApiMethod(value="查询条件-设置变更时间-结束", params=@ApiField("变更时间-结束"))
	public void setChangeEndTime(Date changeEndTime);
	
}
