package com.unswift.cloud.pojo.vo.system.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统配置导出返回实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigExportVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("执行结果")
	private Integer result;
	
	@ApiConstructor("默认构造")
	public SystemConfigExportVo(){
		
	}
	
	@ApiConstructor(value="默认构造", params={@ApiField("主键"), @ApiField("执行结果")})
	public SystemConfigExportVo(Long id, Integer result){
		this.id=id;
		this.result=result;
	}
}
