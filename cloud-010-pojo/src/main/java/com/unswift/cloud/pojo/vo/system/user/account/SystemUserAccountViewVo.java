package com.unswift.cloud.pojo.vo.system.user.account;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="会员登录账号详情返回实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemUserAccountViewVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("所属会员id")
	private Long userId;
	
	@ApiField("登录账号")
	private String account;
	
	@ApiField("账号类型{mobile：手机号，mail：邮箱...}")
	private String type;
	
}
