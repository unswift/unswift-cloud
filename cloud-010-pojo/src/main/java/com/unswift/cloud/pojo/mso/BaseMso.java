package com.unswift.cloud.pojo.mso;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.IBaseMso;
import com.unswift.cloud.pojo.Pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="消息发送实体基类(Message Sending Object)", author = "unswift", date = "2023-12-12", version = "1.0.0")
public abstract class BaseMso implements Pojo, IBaseMso{

	@ApiField("业务id")
	private Long businessId;
	
	@ApiField("系统语言")
	private String language;
	
	@ApiField("登录用户token")
	private String token;
	
}
