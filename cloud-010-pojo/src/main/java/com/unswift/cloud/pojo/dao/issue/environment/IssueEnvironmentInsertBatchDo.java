package com.unswift.cloud.pojo.dao.issue.environment;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="发布环境批量插入封装实体", author="liyunlong", date="2024-04-16", version="1.0.0")
public class IssueEnvironmentInsertBatchDo implements BatchDo<IssueEnvironmentInsertBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<IssueEnvironmentInsertBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public IssueEnvironmentInsertBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量插入列表")})
	public IssueEnvironmentInsertBatchDo(List<IssueEnvironmentInsertBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
