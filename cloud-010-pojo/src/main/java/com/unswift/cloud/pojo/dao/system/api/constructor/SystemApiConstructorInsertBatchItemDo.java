package com.unswift.cloud.pojo.dao.system.api.constructor;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import java.lang.reflect.Constructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemApiConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类构造函数api批量插入记录实体", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiConstructorInsertBatchItemDo extends SystemApiConstructor implements BaseDo{
	
	@ApiField("API构造函数注解对象")
	private ApiConstructor constructorAnno;
	
	@ApiField("构造函数反射对象")
	private Constructor<?> constructor;
}
