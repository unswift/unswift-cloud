package com.unswift.cloud.pojo.dto;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.cloud.pojo.Pojo;

@SuppressWarnings("serial")
@ApiEntity(value="Dto基础实体类", author="unswift", date="2023-06-13", version="1.0.0")
public abstract class BaseDto implements Pojo{
	
}
