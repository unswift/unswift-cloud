package com.unswift.cloud.pojo.mo.system.cloud.host;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;

import com.unswift.cloud.pojo.mo.BaseMo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("微服务主机")
@ApiEntity(value="微服务主机导出模板实体", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudHostExportMo extends BaseMo{
	
	@ApiField("微服务id")
	@ExcelColumn("微服务id")
	private Long cloudId;
	
	@ApiField("主机")
	@ExcelColumn("主机")
	private String host;
	
	@ApiField("端口")
	@ExcelColumn("端口")
	private Integer port;
	
	@ApiField("运行状态{0：未启动，1：运行正常，2：运行异常}")
	@ExcelColumn("运行状态{0：未启动，1：运行正常，2：运行异常}")
	private Byte runningStatus;
	
}
