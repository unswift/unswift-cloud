package com.unswift.cloud.pojo.dao.logger.login.record;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="登录记录批量更新封装实体", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordUpdateBatchDo implements BatchDo<LoggerLoginRecordUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<LoggerLoginRecordUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public LoggerLoginRecordUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public LoggerLoginRecordUpdateBatchDo(List<LoggerLoginRecordUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
