package com.unswift.cloud.pojo.mo.system.timer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("系统定时器")
@ApiEntity(value="系统定时器导出模板实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemTimerExportMo extends BaseMo{
	
	@ApiField("定时器名称")
	@ExcelColumn("定时器名称")
	private String name;
	
	@ApiField("定时器key")
	@ExcelColumn("定时器key")
	private String timerKey;
	
	@ApiField("运行类型{cron:指定时间点，delay:固定时间段延迟执行}")
	@ExcelColumn("运行类型{cron:指定时间点，delay:固定时间段延迟执行}")
	private String runningType;
	
	@ApiField("运行周期")
	@ExcelColumn("运行周期")
	private String runningCycle;
	
	@ApiField("执行bean")
	@ExcelColumn("执行bean")
	private String executeBean;
	
	@ApiField("是否可进化{0：不可进化，1：可进化}")
	@ExcelColumn("是否可进化{0：不可进化，1：可进化}")
	private Byte isEvolvable;
	
}
