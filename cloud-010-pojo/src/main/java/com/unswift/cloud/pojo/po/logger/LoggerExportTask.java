package com.unswift.cloud.pojo.po.logger;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.json.Decimal2Serializer;
import com.unswift.cloud.pojo.po.BasePo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导出任务记录表映射实体", author="liyunlong", date="2024-01-10", version="1.0.0")
public class LoggerExportTask extends BasePo{
	
	@ApiField("名称")
	private String name;
	
	@ApiField("所属模块")
	private String module;
	
	@ApiField("导出文档类型{excel}")
	private String documentType;
	
	@ApiField("导出状态{0:等待导出,1:导出中,2:导出完成}")
	private Byte status;
	
	@ApiField("导出开始时间")
	private Date startTime;
	
	@ApiField("导出结束时间")
	private Date endTime;
	
	@ApiField("导出业务Bean路径")
	private String exportBeanClass;
	
	@ApiField("导出模板路径")
	private String exportModelClass;
	
	@ApiField("导出数据的查询条件")
	private String exportCondition;
	
	@ApiField("导出结果{0：失败，1：成功}")
	private Byte result;
	
	@ApiField("错误消息")
	private String errorMessage;
	
	@ApiField("执行消息，如果有异常，则记录异常")
	private String errorStack;
	
	@ApiField("导出进度")
	@JsonSerialize(using=Decimal2Serializer.class)
	private BigDecimal progress;
	
	@ApiField("导出数量")
	private Long exportCount;
	
}
