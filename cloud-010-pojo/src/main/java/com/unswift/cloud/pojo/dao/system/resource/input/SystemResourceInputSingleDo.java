package com.unswift.cloud.pojo.dao.system.resource.input;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.SingleDo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源入参单条数据查询实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemResourceInputSingleDo implements SingleDo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("查询的sql对象")
	private Sql sql;
	
	@ApiField("是否删除{0：未删除，1：已删除}")
	private Integer isDelete;
	
	@ApiConstructor("默认构造")
	public SystemResourceInputSingleDo(){
		super();
		this.setIsDelete(0);
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("主键"), @ApiField("自定义sql")})
	public SystemResourceInputSingleDo(Long id, Sql sql){
		super();
		this.id=id;
		this.sql=sql;
		this.setIsDelete(0);
	}
	
}
