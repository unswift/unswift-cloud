package com.unswift.cloud.pojo.mo.system.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("系统配置")
@ApiEntity(value="系统配置导出模板实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigExportMo extends BaseMo{
	
	@ApiField("键")
	@ExcelColumn("键")
	private String key;
	
	@ApiField("值")
	@ExcelColumn("值")
	private String value;
	
	@ApiField("扩展字段1")
	@ExcelColumn("扩展字段1")
	private String extend01;
	
	@ApiField("扩展字段2")
	@ExcelColumn("扩展字段2")
	private String extend02;
	
	@ApiField("扩展字段3")
	@ExcelColumn("扩展字段3")
	private String extend03;
	
	@ApiField("扩展字段4")
	@ExcelColumn("扩展字段4")
	private String extend04;
	
	@ApiField("扩展字段5")
	@ExcelColumn("扩展字段5")
	private String extend05;
	
	@ApiField("描述")
	@ExcelColumn("描述")
	private String describe;
	
	@ApiField("状态{1：启用，2：停用}")
	@ExcelColumn(value="状态", translateType = "openStop", translateBean = "systemConfigExportService")
	private Byte status;
	
}
