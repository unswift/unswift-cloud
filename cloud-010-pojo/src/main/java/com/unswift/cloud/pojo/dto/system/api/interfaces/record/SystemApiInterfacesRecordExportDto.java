package com.unswift.cloud.pojo.dto.system.api.interfaces.record;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.PageDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api执行记录导出查询Dto实体", author="unswift", date="2023-09-16", version="1.0.0")
public class SystemApiInterfacesRecordExportDto extends PageDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("接口API id")
	private Long apiInterfaceId;
	
	@ApiField("接口路径")
	private String path;
	
	@ApiField("请求方式{GET、POST、PUT、DELETE}")
	private String method;
	
}
