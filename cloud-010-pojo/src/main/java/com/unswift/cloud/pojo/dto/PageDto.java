package com.unswift.cloud.pojo.dto;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

@SuppressWarnings("serial")
@ApiEntity(value="分页查询Dto顶层实体类", author="unswift", date="2023-06-13", version="1.0.0")
public abstract class PageDto extends BaseDto{

	@ApiField("当前页")
	private Integer currPage;
	
	@ApiField("查询条数")
	private Integer pageSize;

	public Integer getCurrPage() {
		return currPage;
	}

	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
}
