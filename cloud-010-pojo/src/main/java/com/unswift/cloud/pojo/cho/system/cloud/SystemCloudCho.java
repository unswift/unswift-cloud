package com.unswift.cloud.pojo.cho.system.cloud;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统微服务详情缓存实体", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudCho extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("微服务编码")
	private String code;
	
	@ApiField("微服务名称")
	private String name;
	
	@ApiField("网关路径")
	private String gatewayPath;
	
	@ApiField("挂载状态{0：未挂载，1：已挂载}")
	private Byte mountStatus;
	
	@ApiField("运行状态{0：未启动，1：运行正常，2：部分异常，3：全部异常}")
	private Byte runningStatus;
	
	@ApiField("描述")
	private String describe;
	
	@ApiField("顺序")
	private Integer sort;
	
}
