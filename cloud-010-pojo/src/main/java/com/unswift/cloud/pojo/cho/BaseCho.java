package com.unswift.cloud.pojo.cho;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.cloud.pojo.IBaseCho;
import com.unswift.cloud.pojo.Pojo;

@SuppressWarnings("serial")
@ApiEntity(value="缓存基础实体类", author="unswift", date="2024-03-07", version="1.0.0")
public class BaseCho implements Pojo,IBaseCho{

}
