package com.unswift.cloud.pojo.vo.system.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统配置参数名称选项返回实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigParamNameOptionVo extends BaseVo{
	
	@ApiField("选项键")
	private String key;
	
	@ApiField("选项名")
	private String value;
	
}
