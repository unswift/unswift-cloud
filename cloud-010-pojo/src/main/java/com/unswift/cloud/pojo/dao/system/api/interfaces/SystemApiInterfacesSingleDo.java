package com.unswift.cloud.pojo.dao.system.api.interfaces;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.SingleDo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api单条数据查询实体", author="unswift", date="2023-09-09", version="1.0.0")
public class SystemApiInterfacesSingleDo implements SingleDo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("查询的sql对象")
	private Sql sql;
	
	@ApiField("是否删除{0：未删除，1：已删除}")
	private Integer isDelete;
	
	@ApiConstructor("默认构造")
	public SystemApiInterfacesSingleDo(){
		super();
		this.setIsDelete(0);
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("主键"), @ApiField("自定义sql")})
	public SystemApiInterfacesSingleDo(Long id, Sql sql){
		super();
		this.id=id;
		this.sql=sql;
		this.setIsDelete(0);
	}
	
}
