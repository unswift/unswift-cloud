package com.unswift.cloud.pojo.po.system;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.po.BasePo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类api映射实体", author="unswift", date="2023-09-09", version="1.0.0")
public class SystemApiClass extends BasePo{
	
	@ApiField("所属微服务")
	private String server;
	
	@ApiField("类类型{interface：接口, enum：枚举,class：类，entity：实体}")
	private String classType;
	
	@ApiField("修饰符{public：公共的，private：私有的...}")
	private String modifiers;
	
	@ApiField("类声明")
	private String classStatement;
	
	@ApiField("类名称")
	private String className;
	
	@ApiField("类描述")
	private String classDescribe;
	
	@ApiField("作者")
	private String author;
	
	@ApiField("日期")
	private Date date;
	
	@ApiField("版本")
	private String version;
	
	@ApiField("已过期{0：未过期，1：已过期}")
	private Boolean deprecated;
	
	@ApiField("父类")
	private String superClass;
	
	@ApiField("父接口")
	private String interfaces;
	
	@ApiField("类内容签名")
	private String classSignature;
	
}
