package com.unswift.cloud.pojo.dao;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@ApiEntity(value="数据库访问层顶层实体（接口），宇燕微服务架构规定，do为系统的数据库实体层，所有的mapper层操作必须使用此层实体", author="unswift", date="2023-06-13", version="1.0.0")
public interface PkListDo<K extends Comparable<?>> extends BaseDo{

	@ApiMethod(value="获取主键集合", returns=@ApiField("主键集合"))
	List<K> getIdList();
	
	@ApiMethod(value="设置主键集合", params=@ApiField("主键集合"))
	void setIdList(List<K> idList);
}
