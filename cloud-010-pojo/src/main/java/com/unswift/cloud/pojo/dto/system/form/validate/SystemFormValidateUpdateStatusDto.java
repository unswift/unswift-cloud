package com.unswift.cloud.pojo.dto.system.form.validate;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统表单验证更新有效/无效Dto实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateUpdateStatusDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;

	@ApiField("状态{0：无效，1：有效}")
	private Byte status;
	
}
