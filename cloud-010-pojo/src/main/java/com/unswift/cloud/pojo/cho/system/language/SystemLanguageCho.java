package com.unswift.cloud.pojo.cho.system.language;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.cho.BaseCho;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统语言缓存返回实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemLanguageCho extends BaseCho{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("简称")
	private String shortName;
	
	@ApiField("全称")
	private String fullName;
	
	@ApiField("描述")
	private String describe;
	
	@ApiField("激活状态{0：未激活，1：已激活}")
	private Byte activeStatus;
}
