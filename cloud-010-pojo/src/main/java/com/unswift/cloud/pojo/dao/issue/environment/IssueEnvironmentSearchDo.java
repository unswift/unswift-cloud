package com.unswift.cloud.pojo.dao.issue.environment;

import java.util.Date;
import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.SearchDo;
import com.unswift.cloud.pojo.dao.sql.Sql;

import com.unswift.cloud.pojo.po.issue.IssueEnvironment;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="发布环境数据库查询条件实体", author="liyunlong", date="2024-04-16", version="1.0.0")
public class IssueEnvironmentSearchDo extends IssueEnvironment implements SearchDo{
	
	@ApiField("创建时间查询-开始")
	private Date createStartTime;
	
	@ApiField("创建时间查询-结束")
	private Date createEndTime;
	
	@ApiField("变更时间查询-开始")
	private Date changeStartTime;
	
	@ApiField("变更时间查询-结束")
	private Date changeEndTime;
	
	@ApiField("主键集合查询")
	private List<Long> idList;
	
	@ApiField("查询的sql对象")
	private Sql sql;
	
	@ApiConstructor("默认构造")
	public IssueEnvironmentSearchDo(){
		super();
		this.setIsDelete(0);
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("主键集合")})
	public IssueEnvironmentSearchDo(List<Long> idList){
		super();
		this.idList=idList;
		this.setIsDelete(0);
	}
	
}
