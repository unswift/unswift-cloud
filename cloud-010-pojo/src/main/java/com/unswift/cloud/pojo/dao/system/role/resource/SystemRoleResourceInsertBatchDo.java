package com.unswift.cloud.pojo.dao.system.role.resource;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="角色资源批量插入封装实体", author="liyunlong", date="2023-12-02", version="1.0.0")
public class SystemRoleResourceInsertBatchDo implements BatchDo<SystemRoleResourceInsertBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemRoleResourceInsertBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemRoleResourceInsertBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量插入列表")})
	public SystemRoleResourceInsertBatchDo(List<SystemRoleResourceInsertBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
