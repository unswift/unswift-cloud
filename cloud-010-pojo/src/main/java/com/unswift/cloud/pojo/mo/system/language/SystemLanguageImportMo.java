package com.unswift.cloud.pojo.mo.system.language;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.ImportMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("系统语言")
@ApiEntity(value="系统语言导入模板实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemLanguageImportMo extends ImportMo{
	
	@ApiField("简称")
	@ExcelColumn(value="语言简称", required = true, unique = true)
	private String shortName;
	
	@ApiField("全称")
	@ExcelColumn(value="语言全称", required = true, unique = true)
	private String fullName;
	
	@ApiField("描述")
	@ExcelColumn("描述")
	private String describe;
	
}
