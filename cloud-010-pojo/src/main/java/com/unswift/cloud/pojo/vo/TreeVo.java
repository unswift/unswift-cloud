package com.unswift.cloud.pojo.vo;

import java.util.ArrayList;
import java.util.List;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.utils.ObjectUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings({ "serial", "rawtypes" })
@ApiEntity(value="Vo树实体类", author="unswift", date="2023-09-09", version="1.0.0")
public abstract class TreeVo<T extends TreeVo, K extends Comparable> extends BaseVo{

	@ApiField("唯一值")
	private K id;
	
	@ApiField("显示内容")
	private String title;

	@ApiField("节点是否展开")
	private Boolean spread;
	
	@ApiField("节点是否选中")
	private Boolean checked;
	
	@ApiField("节点图标类型，可参阅TreeIconEnum类及tree.js，如果所有树的图标都一样，则只需要设置树根节点的icon即可")
	private String icon;
	
	@ApiField("孩子列表")
	private List<T> children;

	@ApiConstructor("默认构造")
	public TreeVo() {
		super();
	}

	@ApiConstructor(value="带参构造", params = {@ApiField("唯一值"), @ApiField("显示内容")})
	public TreeVo(K id, String title) {
		super();
		this.id = id;
		this.title = title;
		this.spread = false;
		this.checked = false;
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("唯一值"), @ApiField("显示内容")})
	public TreeVo(K id, String title, boolean spread, boolean checked) {
		super();
		this.id = id;
		this.title = title;
		this.spread = spread;
		this.checked = checked;
	}
	
	public void addChild(T t) {
		if(ObjectUtils.isNull(children)) {
			this.children=new ArrayList<T>();
		}
		this.children.add(t);
	}
}
