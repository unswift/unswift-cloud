package com.unswift.cloud.pojo.po.system;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.po.SortPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门职位映射实体", author="liyunlong", date="2024-01-22", version="1.0.0")
public class SystemDepartmentJob extends SortPo{
	
	@ApiField("部门code")
	private String departmentCode;
	
	@ApiField("职位code")
	private String jobCode;
	
}
