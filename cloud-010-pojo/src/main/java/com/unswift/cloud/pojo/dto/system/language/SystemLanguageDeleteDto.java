package com.unswift.cloud.pojo.dto.system.language;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统语言删除Dto实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemLanguageDeleteDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
}
