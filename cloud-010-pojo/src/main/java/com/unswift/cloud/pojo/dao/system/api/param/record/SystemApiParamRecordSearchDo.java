package com.unswift.cloud.pojo.dao.system.api.param.record;

import java.util.Date;
import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.SearchDo;
import com.unswift.cloud.pojo.dao.sql.Sql;

import com.unswift.cloud.pojo.po.system.SystemApiParamRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="api接口参数执行记录数据库查询条件实体", author="unswift", date="2023-09-16", version="1.0.0")
public class SystemApiParamRecordSearchDo extends SystemApiParamRecord implements SearchDo{
	
	@ApiField("创建时间查询-开始")
	private Date createStartTime;
	
	@ApiField("创建时间查询-结束")
	private Date createEndTime;
	
	@ApiField("变更时间查询-开始")
	private Date changeStartTime;
	
	@ApiField("变更时间查询-结束")
	private Date changeEndTime;
	
	@ApiField("主键集合查询")
	private List<Long> idList;
	
	@ApiField("查询的sql对象")
	private Sql sql;
	
	@ApiConstructor("默认构造")
	public SystemApiParamRecordSearchDo(){
		super();
		this.setIsDelete(0);
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("主键集合")})
	public SystemApiParamRecordSearchDo(List<Long> idList){
		super();
		this.idList=idList;
		this.setIsDelete(0);
	}
	
}
