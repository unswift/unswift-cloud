package com.unswift.cloud.pojo.dto.system.role;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色授权树查询Dto实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemRoleAuthorizeUpdateDto extends BaseDto{
	
	@ApiField("资源id")
	private Long resourceId;
	
	@ApiField("资源")
	private String resourceName;
	
	@ApiField("是否重新设置入参和出参")
	private Boolean resetParam;
	
	@ApiField("入参控制")
	private Boolean openInput;
	
	@ApiField("入参控制")
	private String openInputName;
	
	@ApiField("入参列表")
	private List<SystemRoleAuthorizeInputUpdateBto> inputList;
	
	@ApiField("出参控制")
	private Boolean openOutput;
	
	@ApiField("出参控制")
	private String openOutputName;
	
	@ApiField("出参列表")
	private List<SystemRoleAuthorizeOutputUpdateBto> outputList;
}
