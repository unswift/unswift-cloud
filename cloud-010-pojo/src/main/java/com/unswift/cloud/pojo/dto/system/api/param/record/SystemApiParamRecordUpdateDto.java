package com.unswift.cloud.pojo.dto.system.api.param.record;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="api接口参数执行记录更新Dto实体", author="unswift", date="2023-09-16", version="1.0.0")
public class SystemApiParamRecordUpdateDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("接口api id")
	private Long apiInterfaceId;
	
	@ApiField("接口参数id")
	private Long apiParamId;
	
	@ApiField("接口api执行记录id")
	private Long apiInterfaceRecordId;
    @ApiField("批次")
    private Integer batchNo;
	
	@ApiField("访问者id")
	private String sessionId;
	
	@ApiField("请求示例")
	private String requestExample;
	
}
