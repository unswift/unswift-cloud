package com.unswift.cloud.pojo.mo.logger.login.record;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;

import com.unswift.cloud.pojo.mo.BaseMo;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("登录记录")
@ApiEntity(value="登录记录导出模板实体", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordExportMo extends BaseMo{
	
	@ApiField("登录账号")
	@ExcelColumn("登录账号")
	private String account;
	
	@ApiField("登录时间")
	@ExcelColumn(value="登录时间", format = "yyyy-MM-dd HH:mm:ss")
	private Date loginTime;
	
	@ApiField("登录状态{0：登录失败，1：登录成功}")
	@ExcelColumn(value="登录状态", translateBean = "loggerLoginRecordExportService", translateType = "successFail")
	private Byte loginResult;
	
	@ApiField("登录错误码system_message_表的code")
	@ExcelColumn("登录错误码")
	private String errorCode;
	
}
