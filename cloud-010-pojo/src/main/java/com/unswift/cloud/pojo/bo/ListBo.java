package com.unswift.cloud.pojo.bo;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@ApiEntity(value="列表Bo顶层实现")
public class ListBo<E> extends BaseBo{

	@ApiField("传递的数据列表")
	private List<E> list;
	
}
