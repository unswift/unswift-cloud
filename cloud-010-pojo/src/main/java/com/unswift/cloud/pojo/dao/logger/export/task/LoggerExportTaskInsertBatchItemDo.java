package com.unswift.cloud.pojo.dao.logger.export.task;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.logger.LoggerExportTask;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导出任务记录表批量插入记录实体", author="liyunlong", date="2024-01-10", version="1.0.0")
public class LoggerExportTaskInsertBatchItemDo extends LoggerExportTask implements BaseDo{
	
	
}
