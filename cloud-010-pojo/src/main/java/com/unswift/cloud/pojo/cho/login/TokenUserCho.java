package com.unswift.cloud.pojo.cho.login;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.core.entity.TokenUser;
import com.unswift.cloud.pojo.cho.BaseCho;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="用户Token实体", author="unswift", date="2023-06-15", version="1.0.0")
public class TokenUserCho extends BaseCho implements TokenUser{

	@ApiField("用户id")
	private Long id;
	
	@ApiField("用户token")
	private String token;
	
	@ApiField("登录账户")
	private String account;
	
	@ApiField("昵称")
	private String nickname;
	
	@ApiField("头像")
	private String headSculpturePath;
	
	@ApiField("用户角色")
	private List<TokenRoleCho> roleList;
	
	@ApiField("拥有资源权限")
	private List<TokenResourceCho> resourceList;
	
	@ApiField("是否管理员{1：是，0：否}")
	private Byte isAdmin;
}
