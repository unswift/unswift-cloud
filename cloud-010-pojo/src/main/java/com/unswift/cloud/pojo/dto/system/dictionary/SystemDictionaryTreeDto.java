package com.unswift.cloud.pojo.dto.system.dictionary;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统部门树查询Dto实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemDictionaryTreeDto extends BaseDto{
	
	@ApiField("数据字典类型")
	private String type;
	
	@ApiField("数据字典查询条件键/值")
	private String value;
	
	@ApiField("展开的部门id")
	private Long spreadId;
}
