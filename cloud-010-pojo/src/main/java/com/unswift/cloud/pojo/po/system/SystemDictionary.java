package com.unswift.cloud.pojo.po.system;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.po.SortPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典映射实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionary extends SortPo{
	
	@ApiField("类型")
	private String type;
	
	@ApiField("语言")
	private String language;
	
	@ApiField("健")
	private String key;
	
	@ApiField("值")
	private String value;
	
	@ApiField("扩展字段1")
	private String extend01;
	
	@ApiField("扩展字段2")
	private String extend02;
	
	@ApiField("扩展字段3")
	private String extend03;
	
	@ApiField("扩展字段4")
	private String extend04;
	
	@ApiField("扩展字段5")
	private String extend05;
	
	@ApiField("描述")
	private String describe;
	
	@ApiField("父id")
	private Long parentId;
	
	@ApiField("父路径")
	private String parentPath;
	
	@ApiField("状态{0：停用，1：启用}")
	private Byte status;

}
