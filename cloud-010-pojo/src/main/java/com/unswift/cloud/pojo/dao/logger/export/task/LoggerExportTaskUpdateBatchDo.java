package com.unswift.cloud.pojo.dao.logger.export.task;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导出任务记录表批量更新封装实体", author="liyunlong", date="2024-01-10", version="1.0.0")
public class LoggerExportTaskUpdateBatchDo implements BatchDo<LoggerExportTaskUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<LoggerExportTaskUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public LoggerExportTaskUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public LoggerExportTaskUpdateBatchDo(List<LoggerExportTaskUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
