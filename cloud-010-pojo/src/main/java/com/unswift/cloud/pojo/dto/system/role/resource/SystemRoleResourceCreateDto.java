package com.unswift.cloud.pojo.dto.system.role.resource;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="角色资源新建Dto实体", author="liyunlong", date="2023-12-02", version="1.0.0")
public class SystemRoleResourceCreateDto extends BaseDto{
	
	@ApiField("所属角色")
	private Long roleId;
	
	@ApiField("所属资源id")
	private Long resourceId;
	
	@ApiField("开启入参控制")
	private Boolean openInput;
	
	@ApiField("开启出参控制")
	private Boolean openOutput;
	
}
