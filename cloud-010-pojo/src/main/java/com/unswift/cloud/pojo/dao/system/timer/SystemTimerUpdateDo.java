package com.unswift.cloud.pojo.dao.system.timer;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.SingleDo;
import com.unswift.cloud.pojo.dao.sql.Sql;

import com.unswift.cloud.pojo.po.system.SystemTimer;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统定时器更新单条记录实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemTimerUpdateDo extends SystemTimer implements SingleDo{
	
	@ApiField("查询的sql对象")
	private Sql sql;

}
