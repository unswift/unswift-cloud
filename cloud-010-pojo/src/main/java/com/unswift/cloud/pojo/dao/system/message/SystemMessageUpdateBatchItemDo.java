package com.unswift.cloud.pojo.dao.system.message;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemMessage;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统消息批量更新记录实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemMessageUpdateBatchItemDo extends SystemMessage implements BaseDo{
	
	
}
