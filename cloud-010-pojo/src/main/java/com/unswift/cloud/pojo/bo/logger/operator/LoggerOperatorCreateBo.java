package com.unswift.cloud.pojo.bo.logger.operator;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="操作日志新建业务实体", author="liyunlong", date="2023-12-12", version="1.0.0")
public class LoggerOperatorCreateBo extends BaseBo{
	
	@ApiField("数据id")
	private Long dataId;
	
	@ApiField("所属模块")
	private String module;
	
	@ApiField("操作类型")
	private String operatorType;
	
	@ApiField("操作内容")
	private String operatorContent;
	
}
