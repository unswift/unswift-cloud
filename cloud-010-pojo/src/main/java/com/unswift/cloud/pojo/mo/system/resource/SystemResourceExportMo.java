package com.unswift.cloud.pojo.mo.system.resource;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("系统资源")
@ApiEntity(value="系统资源导出模板实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemResourceExportMo extends BaseMo{
	
	@ApiField("资源类型{menu:菜单，request:请求}")
	@ExcelColumn(value="资源类型", translateBean = "systemResourceExportService", translateType = "resourceType")
	private String type;
	
	@ApiField("资源权限key")
	@ExcelColumn("权限key")
	private String authKey;
	
	@ApiField("资源名称")
	@ExcelColumn("资源名称")
	private String name;
	
	@ApiField("资源url")
	@ExcelColumn("资源url")
	private String url;
	
	@ApiField("父资源")
	@ExcelColumn("父资源")
	private String parentName;
	
}
