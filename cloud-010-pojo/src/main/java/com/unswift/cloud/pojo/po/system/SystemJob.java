package com.unswift.cloud.pojo.po.system;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.po.SortPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="职位映射实体", author="liyunlong", date="2024-01-18", version="1.0.0")
public class SystemJob extends SortPo{
	
	@ApiField("编码")
	private String code;
	
	@ApiField("名称")
	private String name;
	
	@ApiField("职责描述")
	private String describe;
	
	@ApiField("状态{0：禁用，1：启用}")
	private Byte status;
	
}
