package com.unswift.cloud.pojo.dao.system.form.validate.config;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统表单验证基础配置批量插入封装实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateConfigInsertBatchDo implements BatchDo<SystemFormValidateConfigInsertBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemFormValidateConfigInsertBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemFormValidateConfigInsertBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量插入列表")})
	public SystemFormValidateConfigInsertBatchDo(List<SystemFormValidateConfigInsertBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
