package com.unswift.cloud.pojo.dto.system.message;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统消息停止导出Dto实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemMessageExportStopDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
}
