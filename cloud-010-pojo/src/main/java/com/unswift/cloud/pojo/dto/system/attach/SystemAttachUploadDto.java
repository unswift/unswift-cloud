package com.unswift.cloud.pojo.dto.system.attach;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件上传Dto实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemAttachUploadDto extends BaseDto{
	
	@ApiField("业务id")
	private Long dataId;
	
	@ApiField("附件所属分类")
	private String classify;
	
	@ApiField("附件所属模块")
	private String module;
	
	@ApiField("附件所属模块")
	private Boolean fileNameEncode;
	
	@ApiField("保存时间（天）")
	private Integer saveTime;
	
	@ApiField("附件实体")
	private MultipartFile[] file;
	
}
