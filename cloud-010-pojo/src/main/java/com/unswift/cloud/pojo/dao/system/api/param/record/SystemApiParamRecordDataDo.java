package com.unswift.cloud.pojo.dao.system.api.param.record;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemApiParamRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="api接口参数执行记录查询结果实体", author="unswift", date="2023-09-16", version="1.0.0")
public class SystemApiParamRecordDataDo extends SystemApiParamRecord implements BaseDo{
	
	
}
