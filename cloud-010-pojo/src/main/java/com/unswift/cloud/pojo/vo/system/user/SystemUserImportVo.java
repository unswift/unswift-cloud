package com.unswift.cloud.pojo.vo.system.user;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="会员导入返回实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemUserImportVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("执行结果")
	private Integer result;
	
	@ApiConstructor("默认构造")
	public SystemUserImportVo(){
		
	}
	
	@ApiConstructor(value="带参构造", params= {@ApiField("任务id"), @ApiField("执行结果")})
	public SystemUserImportVo(Long id, Integer result){
		this.id=id;
		this.result=result;
	}
}
