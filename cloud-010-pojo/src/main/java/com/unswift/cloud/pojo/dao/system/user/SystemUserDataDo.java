package com.unswift.cloud.pojo.dao.system.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.po.system.SystemUser;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="会员查询结果实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemUserDataDo extends SystemUser implements BaseDo{
	
	@ApiField("账号")
	private String account;
	
	@ApiField("手机号")
	private String phone;
	
	@ApiField("邮箱")
	private String email;
	
	@ApiField("拥有角色")
	private String roleNames;
	
	@ApiField("部门/职位id集合")
	private String departmentJobIds;
	
	@ApiField("部门/职位名称集合")
	private String departmentJobNames;
}
