package com.unswift.cloud.pojo.bo.system.resource;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.PageBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源分页查询业务实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemResourcePageBo extends PageBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("资源类型{menu:菜单，request:请求}")
	private String type;
	
	@ApiField("资源权限key")
	private String authKey;
	
	@ApiField("资源名称")
	private String name;
	
	@ApiField("资源url")
	private String url;
	
	@ApiField("资源图标")
	private String icon;
	
	@ApiField("所属父资源")
	private Long parentId;
	
	@ApiField("父资源path")
	private String parentPath;
	
	@ApiField("是否管理员功能{1：是，0：否}")
	private Byte isAdmin;
	
}
