package com.unswift.cloud.pojo.po.system;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.po.BasePo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色出参映射实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemRoleOutput extends BasePo{
	
	@ApiField("角色id")
	private Long roleId;
	
	@ApiField("资源id")
	private Long resourceId;
	
	@ApiField("出参id")
	private Long outputId;
	
}
