package com.unswift.cloud.pojo.po.system;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.po.SortPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门职位用户映射实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentJobUser extends SortPo{
	
	@ApiField("部门职位id")
	private Long departmentJobId;
	
	@ApiField("部门用户id")
	private Long departmentUserId;
	
}
