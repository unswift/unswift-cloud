package com.unswift.cloud.pojo.vo.system.api.classes;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类api获取进度返回实体", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiClassProgressVo extends BaseVo{
	
	@ApiField("进度")
	private Integer progress;
	
	@ApiField("任务错误消息")
	private String errorMessage;
	
	@ApiConstructor("默认构造")
	public SystemApiClassProgressVo(){
		
	}
	
	@ApiConstructor(value="默认构造", params= {@ApiField("执行结果"), @ApiField("任务错误消息")})
	public SystemApiClassProgressVo(Integer progress, String errorMessage){
		this.progress=progress;
		this.errorMessage=errorMessage;
	}
}
