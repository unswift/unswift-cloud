package com.unswift.cloud.pojo.vo.system.attach;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件上传返回实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemAttachUploadVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("业务id")
	private Long dataId;
	
	@ApiField("业务模块")
	private String dataModule;
	
	@ApiField("附件名称")
	private String name;
	
	@ApiField("附件类型")
	private String type;
	
	@ApiField("附件下载路径")
	private String fileUrl;
	
}
