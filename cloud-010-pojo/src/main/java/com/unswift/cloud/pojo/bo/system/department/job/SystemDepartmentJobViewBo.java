package com.unswift.cloud.pojo.bo.system.department.job;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门职位详情业务实体", author="liyunlong", date="2024-01-22", version="1.0.0")
public class SystemDepartmentJobViewBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
}
