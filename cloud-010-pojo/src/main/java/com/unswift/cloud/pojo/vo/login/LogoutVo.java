package com.unswift.cloud.pojo.vo.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="登录返回实体", author="unswift", date="2023-07-03", version="1.0.0")
public class LogoutVo extends BaseVo{

	@ApiField("执行结果")
	private Integer result;
	
	@ApiConstructor("默认构造")
	public LogoutVo() {
		
	}
	
	@ApiConstructor(value="带参构造", params = @ApiField("登出的执行结果"))
	public LogoutVo(Integer result) {
		super();
		this.result = result;
	}
	
}
