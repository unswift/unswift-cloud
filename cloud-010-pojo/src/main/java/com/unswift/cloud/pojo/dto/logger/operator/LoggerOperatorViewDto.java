package com.unswift.cloud.pojo.dto.logger.operator;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="操作日志详情Dto实体", author="liyunlong", date="2023-12-12", version="1.0.0")
public class LoggerOperatorViewDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
}
