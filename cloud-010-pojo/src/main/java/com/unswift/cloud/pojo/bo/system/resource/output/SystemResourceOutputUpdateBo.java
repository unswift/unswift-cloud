package com.unswift.cloud.pojo.bo.system.resource.output;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源出参更新业务实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemResourceOutputUpdateBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("资源id")
	private Long resourceId;
	
	@ApiField("字段名")
	private String fieldName;
	
	@ApiField("字段描述")
	private String fieldComment;
	
}
