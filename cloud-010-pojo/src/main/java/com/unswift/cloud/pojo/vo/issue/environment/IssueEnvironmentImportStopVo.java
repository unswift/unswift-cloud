package com.unswift.cloud.pojo.vo.issue.environment;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="发布环境停止导入实体", author="liyunlong", date="2024-04-16", version="1.0.0")
public class IssueEnvironmentImportStopVo extends BaseVo{
	
	@ApiField("执行结果")
	private Integer result;
	
	@ApiConstructor("默认构造")
	public IssueEnvironmentImportStopVo(){
		
	}
	
	@ApiConstructor(value="默认构造", params=@ApiField("执行结果"))
	public IssueEnvironmentImportStopVo(Integer result){
		this.result=result;
	}
}
