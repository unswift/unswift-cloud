package com.unswift.cloud.pojo.dao.system.department.user;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemDepartmentUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门用户查询结果实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentUserDataDo extends SystemDepartmentUser implements BaseDo{
	
	
}
