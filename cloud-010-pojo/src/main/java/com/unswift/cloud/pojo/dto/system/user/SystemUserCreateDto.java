package com.unswift.cloud.pojo.dto.system.user;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="会员新建Dto实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemUserCreateDto extends BaseDto{
	
	@ApiField("登录账号")
	private String account;
	
	@ApiField("手机号")
	private String phone;
	
	@ApiField("电子邮箱")
	private String email;
	
	@ApiField("昵称")
	private String nickname;
	
	@ApiField("姓名")
	private String name;
	
	@ApiField("会员密码")
	private String password;
	
	@ApiField("证件类型{1：居民身份证，2：军官证，3：港澳台通行证，4：护照，999：其它...}")
	private Integer idCardType;
	
	@ApiField("证件号码（MD5）")
	private String idCard;
	
	@ApiField("证件号显示，中间带*号")
	private String idCardShow;
	
	@ApiField("出生日期{yyyy-MM-dd}")
	@JsonFormat(pattern="yyyy-MM-dd", timezone="GMT+8")
	private Date birthday;
	
	@ApiField("性别{X：女，Y：男}")
	private String sex;
	
	@ApiField("部门/职位id集合")
	private String departmentJobIds;
	
	@ApiField("部门/职位名称集合")
	private String departmentJobNames;	
	
}
