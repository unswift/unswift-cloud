package com.unswift.cloud.pojo.dao.logger.timer.record;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.logger.LoggerTimerRecord;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="定时器执行记录插入记录实体", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerTimerRecordInsertDo extends LoggerTimerRecord implements BaseDo{
	
	
}
