package com.unswift.cloud.pojo.dto.system.role;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色新建Dto实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRoleCreateDto extends BaseDto{
	
	@ApiField("角色key")
	private String key;
	
	@ApiField("角色名称")
	private String name;
	
	@ApiField("角色状态{1：启用，2：停用}")
	private Byte status;
	
	@ApiField("是否管理员功能{1：是，0：否}")
	private Byte isAdmin;
	
}
