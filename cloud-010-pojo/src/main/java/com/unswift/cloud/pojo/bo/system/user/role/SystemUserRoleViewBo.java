package com.unswift.cloud.pojo.bo.system.user.role;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="会员角色详情业务实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemUserRoleViewBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
}
