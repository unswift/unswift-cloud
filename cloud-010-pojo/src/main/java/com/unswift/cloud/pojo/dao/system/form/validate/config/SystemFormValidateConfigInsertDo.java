package com.unswift.cloud.pojo.dao.system.form.validate.config;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemFormValidateConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统表单验证基础配置插入记录实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateConfigInsertDo extends SystemFormValidateConfig implements BaseDo{
	
	
}
