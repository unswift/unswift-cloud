package com.unswift.cloud.pojo.dao;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@Api(value="查询单一实体的查询条件的抽象实体", author="unswift", date="2023-07-17", version="1.0.0")
public interface SingleDo extends AutoSqlDo{

	@ApiMethod(value="获取主键值", returns=@ApiField("主键值"))
	Long getId();
	
	@ApiMethod(value="设置主键值", params=@ApiField("主键值"))
	void setId(Long id);
}
