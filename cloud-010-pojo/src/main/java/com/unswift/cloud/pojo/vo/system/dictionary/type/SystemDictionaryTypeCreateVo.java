package com.unswift.cloud.pojo.vo.system.dictionary.type;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典类型新建返回实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypeCreateVo extends BaseVo{
	
	@ApiField("新建数据id")
	private Long id;
	@ApiField("执行结果")
	private Integer result;
	
	@ApiConstructor("默认构造")
	public SystemDictionaryTypeCreateVo(){
		
	}
	
	@ApiConstructor(value="默认构造", params=@ApiField("执行结果"))
	public SystemDictionaryTypeCreateVo(Integer result){
		this.result=result;
	}
}
