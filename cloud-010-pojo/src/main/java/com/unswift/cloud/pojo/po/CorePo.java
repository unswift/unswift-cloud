package com.unswift.cloud.pojo.po;

import java.util.Date;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.Pojo;

@SuppressWarnings("serial")
@ApiEntity(value="核心实体类", author="unswift", date="2023-04-06", version="1.0.0")
public abstract class CorePo implements Pojo{

	@ApiField("主键")
	private Long id;
	
	@ApiField("是否删除{0：未删除，1：已删除}")
	private Integer isDelete;
	
	@ApiField("创建时间")
	private Date createTime;
	
	@ApiField("变更时间")
	private Date changeTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getChangeTime() {
		return changeTime;
	}

	public void setChangeTime(Date changeTime) {
		this.changeTime = changeTime;
	}
	
}
