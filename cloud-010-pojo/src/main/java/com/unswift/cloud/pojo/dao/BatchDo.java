package com.unswift.cloud.pojo.dao;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@ApiEntity(value="数据库访问层顶层实体（接口），宇燕微服务架构规定，do为系统的数据库实体层，所有的mapper层操作必须使用此层实体", author="unswift", date="2023-06-13", version="1.0.0")
public interface BatchDo<T extends BaseDo> extends BaseDo{

	@ApiMethod(value="获取更新数据集合", returns=@ApiField("更新数据集合"))
	List<T> getList();
	
	@ApiMethod(value="设置更新数据集合", params=@ApiField("更新数据集合"))
	void setList(List<T> list);
}
