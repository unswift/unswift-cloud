package com.unswift.cloud.pojo.dao.system.api.constructor;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemApiConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类构造函数api批量更新记录实体", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiConstructorUpdateBatchItemDo extends SystemApiConstructor implements BaseDo{
	
	
}
