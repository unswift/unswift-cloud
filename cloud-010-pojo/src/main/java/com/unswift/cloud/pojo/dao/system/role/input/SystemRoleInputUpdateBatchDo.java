package com.unswift.cloud.pojo.dao.system.role.input;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色出参批量更新封装实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemRoleInputUpdateBatchDo implements BatchDo<SystemRoleInputUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemRoleInputUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemRoleInputUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public SystemRoleInputUpdateBatchDo(List<SystemRoleInputUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
