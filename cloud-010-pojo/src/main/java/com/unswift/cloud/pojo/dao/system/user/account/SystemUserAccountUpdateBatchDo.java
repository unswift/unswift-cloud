package com.unswift.cloud.pojo.dao.system.user.account;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="会员登录账号批量更新封装实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemUserAccountUpdateBatchDo implements BatchDo<SystemUserAccountUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemUserAccountUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemUserAccountUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public SystemUserAccountUpdateBatchDo(List<SystemUserAccountUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
