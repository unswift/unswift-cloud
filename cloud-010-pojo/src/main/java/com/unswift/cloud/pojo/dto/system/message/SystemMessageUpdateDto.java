package com.unswift.cloud.pojo.dto.system.message;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统消息更新Dto实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemMessageUpdateDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("所属应用服务器")
	private String server;
	
	@ApiField("消息key")
	private String key;
	
	@ApiField("语言")
	private String language;
	
	@ApiField("消息code")
	private String code;
	
	@ApiField("消息")
	private String message;
	
	@ApiField("消息动态参数排序")
	private String argsOrder;
	
	@ApiField("异常类")
	private String exceptionClass;
	
	@ApiField("是否可进化{0：不可进化，1：可进化}")
	private Byte isEvolvable;
	
}
