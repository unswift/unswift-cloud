package com.unswift.cloud.pojo.dto.system.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统用户部门职位Dto实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemUserDepartmentJobTreeDto extends BaseDto{
	
	@ApiField("被选中的部门/职位(json)，格式[[部门id,职位id]...]")
	private String checkedJson;
	
}
