package com.unswift.cloud.pojo.mo.system.dictionary;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.mo.ImportMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("数据字典")
@ApiEntity(value="数据字典导入模板实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryImportMo extends ImportMo{
	
	@Compare
	@ApiField("类型")
	@ExcelColumn(value="类型", required = true)
	private String typeName;
	
	@Compare
	@ApiField("语言")
	@ExcelColumn(value="语言", required = true)
	private String language;
	
	@Compare
	@ApiField("健")
	@ExcelColumn(value="健", required = true)
	private String key;
	
	@Compare
	@ApiField("值")
	@ExcelColumn(value="值", required = true)
	private String value;
	
	@ApiField("扩展字段1")
	@ExcelColumn("扩展1")
	private String extend01;
	
	@ApiField("扩展字段2")
	@ExcelColumn("扩展2")
	private String extend02;
	
	@ApiField("扩展字段3")
	@ExcelColumn("扩展3")
	private String extend03;
	
	@ApiField("扩展字段4")
	@ExcelColumn("扩展4")
	private String extend04;
	
	@ApiField("扩展字段5")
	@ExcelColumn("扩展5")
	private String extend05;
	
	@ApiField("描述")
	@ExcelColumn("描述")
	private String describe;
	
	@ApiField("父id")
	@ExcelColumn("父项key路径（AA/BB/CC）")
	private String parentKeyPath;
	
	@ApiField("父项是否存在在Excel中")
	private Boolean parentNameExistsExcel;
	
	@ApiField("状态{0：停用，1：启用}")
	@ExcelColumn(value="状态", translateBean = "systemDictionaryExportService", translateType = "openStop")
	private Byte status;

}
