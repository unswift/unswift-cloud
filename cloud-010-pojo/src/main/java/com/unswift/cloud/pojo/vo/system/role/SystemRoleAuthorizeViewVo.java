package com.unswift.cloud.pojo.vo.system.role;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源详情返回实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemRoleAuthorizeViewVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("资源类型{menu:菜单，request:请求}")
	private String type;
	
	@ApiField("资源权限key")
	private String authKey;
	
	@ApiField("资源名称")
	private String name;
	
	@ApiField("开启入参权限")
	private Boolean openInput;
	
	@ApiField("入参列表")
	private List<SystemRoleAuthorizeInputViewVo> inputList;
	
	@ApiField("开启出参权限")
	private Boolean openOutput;
	
	@ApiField("出参列表")
	private List<SystemRoleAuthorizeOutputViewVo> outputList;
	
}
