package com.unswift.cloud.pojo.dao.logger.login.record;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.po.logger.LoggerLoginRecord;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="登录记录插入记录实体", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordInsertDo extends LoggerLoginRecord implements BaseDo{
	
	public LoggerLoginRecordInsertDo() {
		
	}
	
	public LoggerLoginRecordInsertDo(String account,Byte loginResult, String errorCode) {
		this.setAccount(account);
		this.setLoginResult(loginResult);
		this.setLoginTime(new Date());
		this.setErrorCode(errorCode);
		this.setCreateUser(-1L);
		this.setChangeUser(-1L);
	}
}
