package com.unswift.cloud.pojo.dao.system.api.constructor;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.SingleDo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类构造函数api单条记录删除实体", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiConstructorDeleteDo implements SingleDo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("查询的sql对象")
	private Sql sql;
	
	@ApiConstructor("默认构造")
	public SystemApiConstructorDeleteDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("删除的主键"), @ApiField("自定义sql")})
	public SystemApiConstructorDeleteDo(Long id, Sql sql){
		super();
		this.id=id;
		this.sql=sql;
	}
	
}
