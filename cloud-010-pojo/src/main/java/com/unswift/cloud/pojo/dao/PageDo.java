package com.unswift.cloud.pojo.dao;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@ApiEntity(value="分页查询mapper层顶层实体类", author="unswift", date="2023-06-13", version="1.0.0")
public interface PageDo extends SearchDo{
	
	@ApiMethod(value="查询条件-获取分页-开始记录数", returns=@ApiField("分页-开始记录数"))
	Integer getFirstSize();

	@ApiMethod(value="查询条件-设置分页-开始记录数", params=@ApiField("分页-开始记录数"))
	void setFirstSize(Integer firstSize);

	@ApiMethod(value="查询条件-获取分页-页大小", returns=@ApiField("分页-页大小"))
	Integer getPageSize();

	@ApiMethod(value="查询条件-设置分页-页大小", params=@ApiField("分页-页大小"))
	void setPageSize(Integer pageSize);

	@ApiMethod(value="查询条件-获取分页-当前页", returns=@ApiField("分页-当前页"))
	Integer getCurrPage();

	@ApiMethod(value="查询条件-设置分页-当前页", params=@ApiField("分页-当前页"))
	void setCurrPage(Integer currPage);

}

