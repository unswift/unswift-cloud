package com.unswift.cloud.pojo.vo.system.department;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.TreeVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统部门树返回实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemDepartmentTreeVo extends TreeVo<SystemDepartmentTreeVo, Long>{
	
	@ApiConstructor("默认构造")
	public SystemDepartmentTreeVo() {
		super();
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("唯一值"), @ApiField("显示内容")})
	public SystemDepartmentTreeVo(Long id, String title) {
		super(id, title);
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("唯一值"), @ApiField("显示内容"), @ApiField("节点是否展开"), @ApiField("节点是否选中")})
	public SystemDepartmentTreeVo(Long id, String title, boolean spread, boolean checked) {
		super(id, title, spread, checked);
	}
}
