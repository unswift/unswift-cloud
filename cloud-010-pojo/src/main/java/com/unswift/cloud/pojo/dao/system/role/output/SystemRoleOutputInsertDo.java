package com.unswift.cloud.pojo.dao.system.role.output;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemRoleOutput;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色出参插入记录实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemRoleOutputInsertDo extends SystemRoleOutput implements BaseDo{
	
	
}
