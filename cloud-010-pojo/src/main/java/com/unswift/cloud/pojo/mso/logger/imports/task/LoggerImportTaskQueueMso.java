package com.unswift.cloud.pojo.mso.logger.imports.task;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.enums.imports.task.ImportTaskEventEnum;
import com.unswift.cloud.pojo.mso.BaseMso;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导入任务记录表发送队列实体", author="unswift", date="2023-08-12", version="1.0.0")
public class LoggerImportTaskQueueMso extends BaseMso{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("事件")
	private String event;
	
	@ApiConstructor(value="带参构造", params = {@ApiField("导入任务id"), @ApiField("事件{start：开始导入，stop：停止导入}"), @ApiField("当前用户token"), @ApiField("当前语言")})
	public LoggerImportTaskQueueMso(Long id, ImportTaskEventEnum event, String token, String language) {
		super();
		this.id = id;
		this.event = event.getKey();
		this.setToken(token);
		this.setLanguage(language);
	}
	
}
