package com.unswift.cloud.pojo.dao.system.resource.input;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源入参批量更新封装实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemResourceInputUpdateBatchDo implements BatchDo<SystemResourceInputUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemResourceInputUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemResourceInputUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public SystemResourceInputUpdateBatchDo(List<SystemResourceInputUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
