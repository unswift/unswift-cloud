package com.unswift.cloud.pojo.mo.system.cloud;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;

import com.unswift.cloud.pojo.mo.BaseMo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("系统微服务")
@ApiEntity(value="系统微服务导出模板实体", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudExportMo extends BaseMo{
	
	@ApiField("微服务编码")
	@ExcelColumn("微服务编码")
	private String code;
	
	@ApiField("微服务名称")
	@ExcelColumn("微服务名称")
	private String name;
	
	@ApiField("网关路径")
	@ExcelColumn("网关路径")
	private String gatewayPath;
	
	@ApiField("挂载状态{0：未挂载，1：已挂载}")
	@ExcelColumn(value="挂载状态", translateBean = "systemCloudExportService", translateType = "mountStatus")
	private Byte mountStatus;
	
	@ApiField("运行状态{0：未启动，1：运行正常，2：部分异常，3：全部异常}")
	@ExcelColumn(value="运行状态", translateBean = "systemCloudExportService", translateType = "runningStatus")
	private Byte runningStatus;
	
	@ApiField("描述")
	@ExcelColumn("描述")
	private String describe;
	
	@ApiField("顺序")
	@ExcelColumn("顺序")
	private Integer sort;
	
}
