package com.unswift.cloud.pojo.dto.system.role;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色停止导出Dto实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRoleExportStopDto extends BaseDto{
	
	@ApiField("角色状态{}")
	private Long id;
	
}
