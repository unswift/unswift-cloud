package com.unswift.cloud.pojo.dao.sql;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

@SuppressWarnings("serial")
@ApiEntity(value="sql update set语句", author = "unswift", date = "2024-04-14", version = "1.0.0")
public class SqlSet implements SqlKeyword{

	@ApiConstructor("默认构造")
	public SqlSet() {
		
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("更新的字段"), @ApiField("更新的字段值")})
	public SqlSet(String field, String value) {
		super();
		this.field = field;
		this.value = value;
	}
	
	@ApiField("set的字段")
	private String field;
	
	@ApiField("set的value")
	private String value;
	
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toSql() {
		return field+SPACE+COMPARE_EQUAL+SPACE+value;
	}
}
