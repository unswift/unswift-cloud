package com.unswift.cloud.pojo.dao.system.role;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemRole;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色批量插入记录实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRoleInsertBatchItemDo extends SystemRole implements BaseDo{
	
	
}
