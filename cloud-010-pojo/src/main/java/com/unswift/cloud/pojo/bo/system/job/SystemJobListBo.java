package com.unswift.cloud.pojo.bo.system.job;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.cloud.pojo.bo.PageBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="职位列表查询业务实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemJobListBo extends PageBo{
	
	
}
