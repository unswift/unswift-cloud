package com.unswift.cloud.pojo.vo.login;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@ApiEntity(value="验证码返回实体", author="unswift", date="2023-08-26", version="1.0.0")
public class ValidateCodeVo extends BaseVo{

	@ApiField("验证码key")
	private String key;
	@ApiField("验证码base64值")
	private String base64Value;
	
	@ApiConstructor("默认构造")
	public ValidateCodeVo() {
		super();
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("验证码的key"), @ApiField("验证码的base64值")})
	public ValidateCodeVo(String key, String base64Value) {
		super();
		this.key = key;
		this.base64Value = base64Value;
	}
}
