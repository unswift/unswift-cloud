package com.unswift.cloud.pojo.dao.system.api.interfaces;

import java.lang.reflect.Method;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.po.system.SystemApiInterfaces;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api批量插入记录实体", author="unswift", date="2023-09-09", version="1.0.0")
public class SystemApiInterfacesInsertBatchItemDo extends SystemApiInterfaces implements BaseDo{
    
	@ApiField("API接口方法注解对象")
    private ApiMethod methodAnno;
    
	@ApiField("API接口方法反射对象")
    private Method methodClass;
	
	@ApiField("内容是否变更")
    private Boolean isChange;
}
