package com.unswift.cloud.pojo.bo.logger.login.record;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="登录记录停止导出业务实体", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordExportStopBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
}
