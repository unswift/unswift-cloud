package com.unswift.cloud.pojo.vo.system.resource.output;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源入参权限返回实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemResourceOutputAuthVo extends BaseVo{
	
	@ApiField("属性key")
	private String paramKey;
	
	@ApiField("属性描述")
	private String paramComment;

	@ApiConstructor("默认构造")
	public SystemResourceOutputAuthVo() {
		
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("参数名称"), @ApiField("参数描述")})
	public SystemResourceOutputAuthVo(String paramKey, String paramComment) {
		super();
		this.paramKey = paramKey;
		this.paramComment = paramComment;
	}
	
	
}
