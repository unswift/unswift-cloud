package com.unswift.cloud.pojo.dto.logger.sql.record;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="sql执行记录详情Dto实体", author="unswift", date="2023-09-05", version="1.0.0")
public class LoggerSqlRecordViewDto extends BaseDto{
	
	@ApiField("注解")
	private Long id;
	
}
