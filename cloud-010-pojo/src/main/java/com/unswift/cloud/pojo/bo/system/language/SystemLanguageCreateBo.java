package com.unswift.cloud.pojo.bo.system.language;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统语言新建业务实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemLanguageCreateBo extends BaseBo{
	
	@Compare
	@ApiField("简称")
	private String shortName;
	
	@Compare
	@ApiField("全称")
	private String fullName;
	
	@ApiField("描述")
	private String describe;
	
}
