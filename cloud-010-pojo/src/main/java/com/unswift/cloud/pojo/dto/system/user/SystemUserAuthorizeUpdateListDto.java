package com.unswift.cloud.pojo.dto.system.user;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.ListDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色授权树查询Dto实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemUserAuthorizeUpdateListDto extends ListDto<SystemUserAuthorizeUpdateDto>{
	
	@ApiField("用户id")
	private Long userId;
	
}
