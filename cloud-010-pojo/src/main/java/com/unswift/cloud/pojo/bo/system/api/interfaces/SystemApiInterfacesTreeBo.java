package com.unswift.cloud.pojo.bo.system.api.interfaces;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.PageBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api分页查询业务实体", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiInterfacesTreeBo extends PageBo{
	
	@ApiField("所属微服务")
	private String server;
	
	@ApiField("关键词")
	private String keyword;
	
	@ApiField("选中节点的id")
	private Long selectNodeId;
}
