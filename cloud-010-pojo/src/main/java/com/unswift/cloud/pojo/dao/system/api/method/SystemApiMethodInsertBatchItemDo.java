package com.unswift.cloud.pojo.dao.system.api.method;

import java.lang.reflect.Method;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.po.system.SystemApiMethod;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类方法api批量插入记录实体", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiMethodInsertBatchItemDo extends SystemApiMethod implements BaseDo{
	
	
	@ApiField("API方法注解对象")
	private ApiMethod methodAnno;
	
	@ApiField("方法反射对象")
	private Method method;
	
}
