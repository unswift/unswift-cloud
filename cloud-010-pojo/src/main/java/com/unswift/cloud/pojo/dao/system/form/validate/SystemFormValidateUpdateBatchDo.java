package com.unswift.cloud.pojo.dao.system.form.validate;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统表单验证批量更新封装实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateUpdateBatchDo implements BatchDo<SystemFormValidateUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemFormValidateUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemFormValidateUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public SystemFormValidateUpdateBatchDo(List<SystemFormValidateUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
