package com.unswift.cloud.pojo.dto.system.api.interfaces;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.PageDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api分页查询Dto实体", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiInterfacesPageDto extends PageDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("所属微服务")
	private String server;
	
	@ApiField("类型{catalog：目录，interface：接口}")
	private String type;
	
	@ApiField("接口名称")
	private String name;
	
	@ApiField("接口路径")
	private String path;
	
	@ApiField("请求方式{GET、POST、PUT、DELETE}")
	private String method;
	
	@ApiField("公开等级{0-9，0表示完全公开，9表示完全不公开}")
	private Integer openLevel;
	
	@ApiField("已过期{0：未过期，1：已过期}")
	private Boolean deprecated;
	
}
