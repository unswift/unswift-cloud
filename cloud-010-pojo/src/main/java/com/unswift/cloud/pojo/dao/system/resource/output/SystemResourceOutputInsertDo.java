package com.unswift.cloud.pojo.dao.system.resource.output;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemResourceOutput;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源出参插入记录实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemResourceOutputInsertDo extends SystemResourceOutput implements BaseDo{
	
	
}
