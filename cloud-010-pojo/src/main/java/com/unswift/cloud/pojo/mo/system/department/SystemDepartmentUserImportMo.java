package com.unswift.cloud.pojo.mo.system.department;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.mo.ImportMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("会员")
@ApiEntity(value="部门会员导入模板实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemDepartmentUserImportMo extends ImportMo{
	
	@Compare
	@ApiField("账号")
	@ExcelColumn(value="登录账号", required = true, unique = true)
	private String account;
	
	@ApiField("手机号")
	@ExcelColumn(value="手机号码", unique = true)
	private String phone;
	
	@ApiField("邮箱")
	@ExcelColumn(value="电子邮箱", unique = true)
	private String email;
	
	@ApiField("登录密码")
	@ExcelColumn(value="登录密码", required = true)
	private String password;
	
	@Compare
	@ApiField("昵称")
	@ExcelColumn(value="昵称", required = true)
	private String nickname;
	
	@ApiField("姓名")
	@ExcelColumn("姓名")
	private String name;
	
	@ApiField("证件类型{1：居民身份证，2：军官证，3：港澳台通行证，4：护照，999：其它...}")
	@ExcelColumn(value="证件类型")
	private String idCardTypeName;
	
	@ApiField("证件号显示，中间带*号")
	@ExcelColumn("证件号码")
	private String idCardShow;
	
	@ApiField("出生日期{yyyy-MM-dd}")
	@ExcelColumn(value="出生日期")
	private Date birthday;
	
	@ApiField("性别{X：女，Y：男}")
	@ExcelColumn(value="性别")
	private String sex;
	
	@ApiField("拥有角色")
	@ExcelColumn(value="拥有角色", required = true)
	private String roleNames;
	
	@ApiField("拥有角色数组-非excel字段")
	private String[] roleNameArray;
	
	@ApiField("职位")
	@ExcelColumn(value="职位", required = true)
	private String jobNames;
}
