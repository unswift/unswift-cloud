package com.unswift.cloud.pojo.dao.system.dictionary.type;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.SingleDo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典类型单条记录删除实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypeDeleteDo implements SingleDo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("查询的sql对象")
	private Sql sql;
	
	@ApiConstructor("默认构造")
	public SystemDictionaryTypeDeleteDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("删除的主键"), @ApiField("自定义sql")})
	public SystemDictionaryTypeDeleteDo(Long id, Sql sql){
		super();
		this.id=id;
		this.sql=sql;
	}
	
}
