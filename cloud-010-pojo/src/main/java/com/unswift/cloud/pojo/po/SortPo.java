package com.unswift.cloud.pojo.po;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

@SuppressWarnings("serial")
@ApiEntity(value="具有排序属性的基类", author="unswift", date="2023-04-22", version="1.0.0")
public abstract class SortPo extends BasePo{

	@ApiField("顺序")
	private Integer sort;

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
}
