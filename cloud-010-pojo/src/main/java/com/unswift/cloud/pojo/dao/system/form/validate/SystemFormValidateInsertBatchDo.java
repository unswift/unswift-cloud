package com.unswift.cloud.pojo.dao.system.form.validate;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统表单验证批量插入封装实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateInsertBatchDo implements BatchDo<SystemFormValidateInsertBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemFormValidateInsertBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemFormValidateInsertBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量插入列表")})
	public SystemFormValidateInsertBatchDo(List<SystemFormValidateInsertBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
