package com.unswift.cloud.pojo.bo.system.role.resource;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.bo.BaseBo;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleAuthorizeInputUpdateBo;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleAuthorizeOutputUpdateBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="角色资源更新业务实体", author="liyunlong", date="2023-12-02", version="1.0.0")
public class SystemRoleResourceUpdateBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("所属角色")
	private Long roleId;
	
	@ApiField("所属资源id")
	private Long resourceId;
	
	@Compare
	@ApiField("名称")
	private String resourceName;
	
	@ApiField("是否重新设置入参和出参")
	private Boolean resetParam;
	
	@ApiField("开启入参控制")
	private Boolean openInput;
	
	@Compare
	@ApiField("入参控制")
	private String openInputName;
	
	@ApiField("入参")
	@Compare(pkFieldName = "inputId")
	private List<SystemRoleAuthorizeInputUpdateBo> inputList;
	
	@ApiField("开启出参控制")
	private Boolean openOutput;
	
	@Compare
	@ApiField("出参控制")
	private String openOutputName;
	
	@ApiField("出参")
	@Compare(pkFieldName = "inputId")
	private List<SystemRoleAuthorizeOutputUpdateBo> outputList;
}
