package com.unswift.cloud.pojo.bo.system.form.validate;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统表单验证更新有效/无效业务实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateUpdateStatusBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;

	@Compare(translateType = "")
	@ApiField("状态")
	private Byte status;
	
}
