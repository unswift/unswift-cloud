package com.unswift.cloud.pojo.dao.system.department.job;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门职位批量更新封装实体", author="liyunlong", date="2024-01-22", version="1.0.0")
public class SystemDepartmentJobUpdateBatchDo implements BatchDo<SystemDepartmentJobUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemDepartmentJobUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemDepartmentJobUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public SystemDepartmentJobUpdateBatchDo(List<SystemDepartmentJobUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
