package com.unswift.cloud.pojo.dao.system.user.account;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemUserAccount;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="会员登录账号插入记录实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemUserAccountInsertDo extends SystemUserAccount implements BaseDo{
	
	@ApiConstructor("默认构造")
	public SystemUserAccountInsertDo() {
		
	}
	
	@ApiConstructor(value="带参构造")
	public SystemUserAccountInsertDo(Long userId, String account, String type) {
		this.setUserId(userId);
		this.setAccount(account);
		this.setType(type);
	}
}
