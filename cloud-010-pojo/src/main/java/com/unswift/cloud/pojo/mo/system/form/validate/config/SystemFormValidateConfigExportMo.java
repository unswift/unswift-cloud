package com.unswift.cloud.pojo.mo.system.form.validate.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("系统表单验证基础配置")
@ApiEntity(value="系统表单验证基础配置导出模板实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateConfigExportMo extends BaseMo{
	
	@ApiField("所属模块")
	@ExcelColumn("所属模块")
	private String key;
	
	@ApiField("所属操作")
	@ExcelColumn("所属操作")
	private String value;
	
	@ApiField("代码描述")
	@ExcelColumn("代码描述")
	private String describe;
	
}
