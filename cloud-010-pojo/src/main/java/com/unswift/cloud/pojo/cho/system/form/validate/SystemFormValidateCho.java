package com.unswift.cloud.pojo.cho.system.form.validate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.cho.BaseCho;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统表单验证详情返回实体", author="liyunlong", date="2023-07-19", version="1.0.0")
public class SystemFormValidateCho extends BaseCho{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("所属模块")
	private String module;
	
	@ApiField("所属操作")
	private String operator;
	
	@ApiField("验证类型{if：如果，else if：否则如果，else：否则，end if：如果结束，case：当，when then：等于某个值的时候，那么，end case：case结束，foreach：循环，end foreach：循环结束，break：跳出循环，continue：跳出本次循环，var：变量命名，tips：提示语句}")
	private String keyword;
	
	@ApiField("代码")
	private String code;
	
	@ApiField("表达式为真时提示消息")
	private String message;
	
	@ApiField("消息的动态参数")
	private String messageArgs;
	
	@ApiField("代码顺序")
	private Integer codeLine;
	
	@ApiField("代码描述")
	private String describe;
	
}
