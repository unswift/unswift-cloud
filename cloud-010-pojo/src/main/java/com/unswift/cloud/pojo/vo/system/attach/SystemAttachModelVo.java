package com.unswift.cloud.pojo.vo.system.attach;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件详情返回实体", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachModelVo extends BaseVo{
	
	@ApiField("附件下载路径")
	private String fileUrl;
	
	@ApiField("附件名称")
	private String name;
}
