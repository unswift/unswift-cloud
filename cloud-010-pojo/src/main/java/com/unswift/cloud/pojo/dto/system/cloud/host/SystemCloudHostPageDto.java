package com.unswift.cloud.pojo.dto.system.cloud.host;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.PageDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="微服务主机分页查询Dto实体", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudHostPageDto extends PageDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("微服务id")
	private Long cloudId;
	
	@ApiField("主机")
	private String host;
	
	@ApiField("端口")
	private Integer port;
	
	@ApiField("运行状态{0：未启动，1：运行正常，2：运行异常}")
	private Byte runningStatus;
	
}
