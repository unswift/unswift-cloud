package com.unswift.cloud.pojo.vo.system.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统授权角色返回实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemUserAuthorizeRoleVo extends BaseVo{
	
	@ApiField("角色id")
	private Long id;
	
	@ApiField("角色名称")
	private String name;
	
	@ApiField("节点是否选中")
	private Boolean checked;
}
