package com.unswift.cloud.pojo.dao.logger.sql.record;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.PkListDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="sql执行记录多条记录删除实体", author="unswift", date="2023-09-05", version="1.0.0")
public class LoggerSqlRecordDeleteBatchDo implements PkListDo<Long>{
	
	@ApiField("注解集合")
	private List<Long> idList;
	
	@ApiConstructor("默认构造")
	public LoggerSqlRecordDeleteBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("删除的主键集合")})
	public LoggerSqlRecordDeleteBatchDo(List<Long> idList){
		super();
		this.idList=idList;
	}

}
