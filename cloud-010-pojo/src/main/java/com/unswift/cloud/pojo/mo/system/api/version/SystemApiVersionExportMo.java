package com.unswift.cloud.pojo.mo.system.api.version;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("类版本api")
@ApiEntity(value="类版本api导出模板实体", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiVersionExportMo extends BaseMo{
	
	@ApiField("关联 id")
	@ExcelColumn("关联 id")
	private Long relationId;
	
	@ApiField("关联表")
	@ExcelColumn("关联表")
	private String relationTable;
	
	@ApiField("版本描述")
	@ExcelColumn("版本描述")
	private String classComment;
	
	@ApiField("作者")
	@ExcelColumn("作者")
	private String author;
	
	@ApiField("日期")
	@ExcelColumn(value="日期", format="yyyy-MM-dd")
	private Date date;
	
	@ApiField("版本")
	@ExcelColumn("版本")
	private String version;
	
}
