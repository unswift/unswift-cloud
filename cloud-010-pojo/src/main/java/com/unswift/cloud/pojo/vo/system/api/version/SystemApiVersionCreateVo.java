package com.unswift.cloud.pojo.vo.system.api.version;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类版本api新建返回实体", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiVersionCreateVo extends BaseVo{
	
	@ApiField("执行结果")
	private Integer result;
	
	@ApiConstructor("默认构造")
	public SystemApiVersionCreateVo(){
		
	}
	
	@ApiConstructor(value="默认构造", params=@ApiField("执行结果"))
	public SystemApiVersionCreateVo(Integer result){
		this.result=result;
	}
}
