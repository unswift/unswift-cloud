package com.unswift.cloud.pojo.bo.logger.export.task;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.bo.BaseBo;

@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导出任务记录表导出业务实体", author="unswift", date="2023-08-06", version="1.0.0")
public class LoggerExportTaskExportBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("名称")
	private String name;
	
	@ApiField("导出文档类型{excel}")
	private String documentType;
	
	@ApiField("导出状态{0:等待导出,1:导出中,2:导出完成}")
	private Byte status;
	
	@ApiField("导出开始时间")
	private Date startTime;
	
	@ApiField("导出结束时间")
	private Date endTime;
	
	@ApiField("导出业务Bean路径")
	private String exportBeanClass;
	
	@ApiField("导出模板路径")
	private String exportModelClass;
	
	@ApiField("导出数据的查询条件")
	private String exportCondition;
	
	@ApiField("导出结果{0：失败，1：成功}")
	private Byte result;
	
	@ApiField("错误消息")
	private String errorMessage;
	
	@ApiField("执行消息，如果有异常，则记录异常")
	private String errorStack;
	
	public Long getId(){
		return this.id;
	}
	public void setId(Long id){
		this.id=id;
	}
	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getDocumentType(){
		return this.documentType;
	}
	public void setDocumentType(String documentType){
		this.documentType=documentType;
	}
	public Byte getStatus(){
		return this.status;
	}
	public void setStatus(Byte status){
		this.status=status;
	}
	public Date getStartTime(){
		return this.startTime;
	}
	public void setStartTime(Date startTime){
		this.startTime=startTime;
	}
	public Date getEndTime(){
		return this.endTime;
	}
	public void setEndTime(Date endTime){
		this.endTime=endTime;
	}
	public String getExportBeanClass(){
		return this.exportBeanClass;
	}
	public void setExportBeanClass(String exportBeanClass){
		this.exportBeanClass=exportBeanClass;
	}
	public String getExportModelClass(){
		return this.exportModelClass;
	}
	public void setExportModelClass(String exportModelClass){
		this.exportModelClass=exportModelClass;
	}
	public String getExportCondition(){
		return this.exportCondition;
	}
	public void setExportCondition(String exportCondition){
		this.exportCondition=exportCondition;
	}
	public Byte getResult(){
		return this.result;
	}
	public void setResult(Byte result){
		this.result=result;
	}
	public String getErrorMessage(){
		return this.errorMessage;
	}
	public void setErrorMessage(String errorMessage){
		this.errorMessage=errorMessage;
	}
	public String getErrorStack(){
		return this.errorStack;
	}
	public void setErrorStack(String errorStack){
		this.errorStack=errorStack;
	}
}
