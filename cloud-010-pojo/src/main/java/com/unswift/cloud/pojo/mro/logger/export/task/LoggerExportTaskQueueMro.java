package com.unswift.cloud.pojo.mro.logger.export.task;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.enums.export.task.ExportTaskEventEnum;
import com.unswift.cloud.pojo.mro.BaseMro;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导出任务记录表队列接收实体", author="unswift", date="2023-08-12", version="1.0.0")
public class LoggerExportTaskQueueMro extends BaseMro{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("事件")
	private String event;
	
	@ApiConstructor("默认构造")
	public LoggerExportTaskQueueMro() {
		
	}
	
	public LoggerExportTaskQueueMro(Long id, ExportTaskEventEnum event, String token, String language) {
		super();
		this.id = id;
		this.event = event.getKey();
		this.setToken(token);
		this.setLanguage(language);
	}
	
}
