package com.unswift.cloud.pojo.bo.system.role.input;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.PageBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色出参分页查询业务实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemRoleInputPageBo extends PageBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("角色id")
	private Long roleId;
	
	@ApiField("资源id")
	private Long resourceId;
	
	@ApiField("入参id")
	private Long inputId;
	
}
