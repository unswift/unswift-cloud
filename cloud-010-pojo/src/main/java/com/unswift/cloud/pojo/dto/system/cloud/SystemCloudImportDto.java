package com.unswift.cloud.pojo.dto.system.cloud;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.PageDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统微服务导入查询Dto实体", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudImportDto extends PageDto{
	
	@ApiField("附件id")
	private Long attachId;
	
	@ApiField("附件路径")
	private String attachUrl;
}
