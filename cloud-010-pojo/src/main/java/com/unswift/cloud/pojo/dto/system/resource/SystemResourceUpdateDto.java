package com.unswift.cloud.pojo.dto.system.resource;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import com.unswift.cloud.pojo.dto.system.resource.input.SystemResourceInputUpdateDto;
import com.unswift.cloud.pojo.dto.system.resource.output.SystemResourceOutputUpdateDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源更新Dto实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemResourceUpdateDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("资源类型{menu:菜单，request:请求}")
	private String type;
	
	@ApiField("资源权限key")
	private String authKey;
	
	@ApiField("资源名称")
	private String name;
	
	@ApiField("资源url")
	private String url;
	
	@ApiField("资源图标")
	private String icon;
	
	@ApiField("所属父资源")
	private Long parentId;
	
	@ApiField("父资源path")
	private String parentPath;
	
	@ApiField("是否管理员功能{1：是，0：否}")
	private Byte isAdmin;
	
	@ApiField("开启入参权限")
	private Boolean openInput;
	
	@ApiField("入参列表")
	private List<SystemResourceInputUpdateDto> inputList;
	
	@ApiField("开启出参权限")
	private Boolean openOutput;
	
	@ApiField("出参列表")
	private List<SystemResourceOutputUpdateDto> outputList;
	
	@ApiField("顺序")
	private Integer sort;
}
