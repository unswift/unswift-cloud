package com.unswift.cloud.pojo.dao.logger.api.request;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.logger.LoggerApiRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口请求日志批量插入记录实体", author="liyunlong", date="2023-12-08", version="1.0.0")
public class LoggerApiRequestInsertBatchItemDo extends LoggerApiRequest implements BaseDo{
	
	
}
