package com.unswift.cloud.pojo.mo.logger.timer.record;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("定时器执行记录")
@ApiEntity(value="定时器执行记录导出模板实体", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerTimerRecordExportMo extends BaseMo{
	
	@ApiField("定时器id")
	@ExcelColumn("定时器id")
	private Long timerId;
	
	@ApiField("日志类型{start：启动日志，end：结束日志，running：执行日志}")
	@ExcelColumn("日志类型{start：启动日志，end：结束日志，running：执行日志}")
	private String type;
	
	@ApiField("执行结果{0：失败，1：成功}")
	@ExcelColumn("执行结果{0：失败，1：成功}")
	private Byte result;
	
	@ApiField("执行消息，如果有异常，则记录异常")
	@ExcelColumn("执行消息，如果有异常，则记录异常")
	private String message;
	
}
