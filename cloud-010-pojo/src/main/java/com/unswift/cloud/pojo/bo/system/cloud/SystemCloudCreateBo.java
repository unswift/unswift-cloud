package com.unswift.cloud.pojo.bo.system.cloud;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统微服务新建业务实体", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudCreateBo extends BaseBo{
	
	@Compare
	@ApiField("微服务编码")
	private String code;
	
	@Compare
	@ApiField("微服务名称")
	private String name;
	
	@Compare
	@ApiField("网关路径")
	private String gatewayPath;
	
	@ApiField("描述")
	private String describe;
	
	@ApiField("顺序")
	private Integer sort;
	
}
