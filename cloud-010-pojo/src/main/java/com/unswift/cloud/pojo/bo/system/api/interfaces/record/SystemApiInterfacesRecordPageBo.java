package com.unswift.cloud.pojo.bo.system.api.interfaces.record;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.PageBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api执行记录分页查询业务实体", author="unswift", date="2023-09-24", version="1.0.0")
public class SystemApiInterfacesRecordPageBo extends PageBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("接口API id")
	private Long apiInterfaceId;
	
	@ApiField("接口路径")
	private String path;
	
	@ApiField("请求方式{GET、POST、PUT、DELETE}")
	private String method;
	
	@ApiField("请求头")
	private String headers;
	
	@ApiField("请求body")
	private String body;
	
	@ApiField("返回数据")
	private String response;
	
}
