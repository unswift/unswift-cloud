package com.unswift.cloud.pojo.dto.system.api.interfaces.record;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api执行记录新建Dto实体", author="unswift", date="2023-09-16", version="1.0.0")
public class SystemApiInterfacesRecordCreateDto extends BaseDto{
	
	@ApiField("接口API id")
	private Long apiInterfaceId;
	
	@ApiField("接口路径")
	private String path;
	
	@ApiField("请求方式{GET、POST、PUT、DELETE}")
	private String method;
	
	@ApiField("form请求参数")
	private Map<String, String> formParamMap;
	
	@ApiField("请求头")
	private String headers;
	
	@ApiField("请求body参数")
	private String body;
	
	@ApiField("请求回调参数")
	private String response;
}
