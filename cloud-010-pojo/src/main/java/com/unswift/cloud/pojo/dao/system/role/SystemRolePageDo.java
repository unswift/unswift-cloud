package com.unswift.cloud.pojo.dao.system.role;

import java.util.Date;
import java.util.List;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.PageDo;
import com.unswift.cloud.pojo.dao.sql.Sql;

import com.unswift.cloud.pojo.po.system.SystemRole;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色分页查询条件实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRolePageDo extends SystemRole implements PageDo{
	
	@ApiField("创建时间查询-开始")
	private Date createStartTime;
	
	@ApiField("创建时间查询-结束")
	private Date createEndTime;
	
	@ApiField("变更时间查询-开始")
	private Date changeStartTime;
	
	@ApiField("变更时间查询-结束")
	private Date changeEndTime;
	
	@ApiField("主键集合查询")
	private List<Long> idList;
	
	@ApiField("查询的sql对象")
	private Sql sql;
	
	@ApiField("当前页")
	private Integer firstSize;
	
	@ApiField("查询条数")
	private Integer pageSize;

	@ApiField("当前页")
	private Integer currPage;

	@ApiConstructor("默认构造-设置默认查询未删除")
	public SystemRolePageDo(){
		this.setIsDelete(0);
	}
}
