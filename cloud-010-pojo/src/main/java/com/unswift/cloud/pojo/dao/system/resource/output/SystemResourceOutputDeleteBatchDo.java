package com.unswift.cloud.pojo.dao.system.resource.output;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.PkListDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源出参多条记录删除实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemResourceOutputDeleteBatchDo implements PkListDo<Long>{
	
	@ApiField("主键集合")
	private List<Long> idList;
	
	@ApiConstructor("默认构造")
	public SystemResourceOutputDeleteBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("删除的主键集合")})
	public SystemResourceOutputDeleteBatchDo(List<Long> idList){
		super();
		this.idList=idList;
	}

}
