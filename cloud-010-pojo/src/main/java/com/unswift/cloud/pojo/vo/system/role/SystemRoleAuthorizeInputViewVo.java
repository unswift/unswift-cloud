package com.unswift.cloud.pojo.vo.system.role;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源入参详情返回实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemRoleAuthorizeInputViewVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("资源id")
	private Long resourceId;
	
	@ApiField("属性key")
	private String paramKey;
	
	@ApiField("属性描述")
	private String paramComment;
	
	@ApiField("是否选中")
	private Boolean checked;
	
}
