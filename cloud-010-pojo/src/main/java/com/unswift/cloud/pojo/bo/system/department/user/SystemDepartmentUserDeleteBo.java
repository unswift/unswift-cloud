package com.unswift.cloud.pojo.bo.system.department.user;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门用户删除业务实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentUserDeleteBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
}
