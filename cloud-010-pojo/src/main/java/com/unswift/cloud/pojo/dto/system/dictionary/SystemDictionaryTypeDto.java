package com.unswift.cloud.pojo.dto.system.dictionary;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典类型查询Dto实体", author="unswift", date="2023-10-06", version="1.0.0")
public class SystemDictionaryTypeDto extends BaseDto{
	
	@ApiField("类型")
	private String type;
	
	@ApiField("扩展1")
	private String extend01;
}
