package com.unswift.cloud.pojo.dto.system.attach;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件分页查询Dto实体", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachDownloadPathDto extends BaseDto{
	
	@ApiField("业务id")
	private Long dataId;
	
	@ApiField("业务模块")
	private String dataModule;
	
	@ApiConstructor("默认构造")
	public SystemAttachDownloadPathDto(){
		
	}

	@ApiConstructor(value="带参构造", params = {@ApiField("业务id"), @ApiField("所属模块")})
	public SystemAttachDownloadPathDto(Long dataId, String dataModule) {
		super();
		this.dataId = dataId;
		this.dataModule = dataModule;
	}
	
}
