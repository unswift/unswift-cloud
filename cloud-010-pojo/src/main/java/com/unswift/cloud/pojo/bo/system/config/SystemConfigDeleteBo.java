package com.unswift.cloud.pojo.bo.system.config;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统配置删除业务实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigDeleteBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
}
