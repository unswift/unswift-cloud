package com.unswift.cloud.pojo.dao.logger.queue;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.logger.LoggerQueue;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口请求日志查询结果实体", author="liyunlong", date="2024-01-25", version="1.0.0")
public class LoggerQueueDataDo extends LoggerQueue implements BaseDo{
	
	
}
