package com.unswift.cloud.pojo.dao.system.api.field;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.PkListDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类字段api多条记录删除实体", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiFieldDeleteBatchDo implements PkListDo<Long>{
	
	@ApiField("主键集合")
	private List<Long> idList;
	
	@ApiConstructor("默认构造")
	public SystemApiFieldDeleteBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("删除的主键集合")})
	public SystemApiFieldDeleteBatchDo(List<Long> idList){
		super();
		this.idList=idList;
	}

}
