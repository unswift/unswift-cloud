package com.unswift.cloud.pojo.mo.system.dictionary.type;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("数据字典类型")
@ApiEntity(value="数据字典类型导出模板实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypeExportMo extends BaseMo{
	
	@ApiField("类型编码")
	@ExcelColumn("类型编码")
	private String code;
	
	@ApiField("类型名称")
	@ExcelColumn("类型名称")
	private String name;
	
	@ApiField("类型描述")
	@ExcelColumn("类型描述")
	private String describe;

	@ApiField("状态{0：停用，1：启用}")
	@ExcelColumn(value="状态", translateBean = "systemDictionaryExportService", translateType = "openStop")
	private Byte status;
	
	@ApiField("创建人姓名")
	@ExcelColumn("创建人")
	private String createUserName;
	
	@ApiField("创建时间")
	@ExcelColumn(value="创建时间", format = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
}
