package com.unswift.cloud.pojo.bo.system.user.role;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.bo.ListBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="用户角色更新业务实体", author="liyunlong", date="2023-12-02", version="1.0.0")
public class SystemUserRoleUpdateListBo extends ListBo<SystemUserRoleUpdateBo>{
	
	@ApiField("所属用户")
	private Long userId;
	
	@ApiField("角色集合")
	@Compare(value="角色", pkFieldName = "roleId")
	private List<SystemUserRoleUpdateBo> list;
	
	@ApiConstructor("默认构造")
	public SystemUserRoleUpdateListBo() {
		
	}

	@ApiConstructor(value="带参构造", params = {@ApiField("资源列表")})
	public SystemUserRoleUpdateListBo(List<SystemUserRoleUpdateBo> list) {
		super();
		this.setList(list);
	}
	
	
}
