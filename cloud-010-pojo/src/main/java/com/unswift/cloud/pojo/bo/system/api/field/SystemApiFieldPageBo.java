package com.unswift.cloud.pojo.bo.system.api.field;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.bo.PageBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类字段api分页查询业务实体", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiFieldPageBo extends PageBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("关联 id")
	private Long relationId;
	
	@ApiField("关联表")
	private String relationTable;
	
	@ApiField("场景{field：字段，param：参数，return：返回}")
	private String scene;
	
	@ApiField("类属性描述")
	private String fieldComment;
	
	@ApiField("类属性名称")
	private String fieldName;
	
	@ApiField("类属性类型")
	private String fieldType;
	
	@ApiField("修饰符{public：公共的，private：私有的...}")
	private String modifiers;
	
	@ApiField("作者")
	private String author;
	
	@ApiField("规则")
	private String rule;
	
	@ApiField("必须的")
	private Boolean required;
	
	@ApiField("日期")
	private Date date;
	
	@ApiField("已过期{0：未过期，1：已过期}")
	private Boolean deprecated;
	
}
