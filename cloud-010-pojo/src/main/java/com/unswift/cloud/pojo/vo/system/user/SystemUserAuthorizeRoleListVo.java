package com.unswift.cloud.pojo.vo.system.user;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.ListVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统授权角色列表返回实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemUserAuthorizeRoleListVo extends ListVo<SystemUserAuthorizeRoleVo>{
	
	@ApiConstructor("默认构造")
	public SystemUserAuthorizeRoleListVo() {
		
	}
	
	@ApiConstructor(value="带参构造", params = @ApiField("角色列表"))
	public SystemUserAuthorizeRoleListVo(List<SystemUserAuthorizeRoleVo> dataList) {
		this.setDataList(dataList);
	}
}
