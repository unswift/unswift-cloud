package com.unswift.cloud.pojo.po.logger;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口请求日志映射实体", author="liyunlong", date="2023-12-08", version="1.0.0")
public class LoggerApiRequest extends BasePo{
	
	@ApiField("请求路径")
	private String path;
	
	@ApiField("接口名称")
	private String name;
	
	@ApiField("请求头")
	private String headers;
	
	@ApiField("请求参数")
	private String params;
	
	@ApiField("请求body")
	private String bodys;
	
	@ApiField("返回数据")
	private String response;
	
}
