package com.unswift.cloud.pojo.dto.system.language;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="用户更新状态Dto实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemLanguageUpdateStatusDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("激活状态{0：未激活，1：已激活}")
	private Byte activeStatus;
}
