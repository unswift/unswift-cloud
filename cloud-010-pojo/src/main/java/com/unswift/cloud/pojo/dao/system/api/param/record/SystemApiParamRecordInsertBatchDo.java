package com.unswift.cloud.pojo.dao.system.api.param.record;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="api接口参数执行记录批量插入封装实体", author="unswift", date="2023-09-16", version="1.0.0")
public class SystemApiParamRecordInsertBatchDo implements BatchDo<SystemApiParamRecordInsertBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemApiParamRecordInsertBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemApiParamRecordInsertBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量插入列表")})
	public SystemApiParamRecordInsertBatchDo(List<SystemApiParamRecordInsertBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
