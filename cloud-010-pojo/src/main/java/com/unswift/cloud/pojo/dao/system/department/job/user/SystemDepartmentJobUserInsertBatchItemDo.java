package com.unswift.cloud.pojo.dao.system.department.job.user;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemDepartmentJobUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门职位用户批量插入记录实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentJobUserInsertBatchItemDo extends SystemDepartmentJobUser implements BaseDo{
	
	
}
