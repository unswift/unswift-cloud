package com.unswift.cloud.pojo.vo.system.api.interfaces;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;
import com.unswift.cloud.pojo.vo.system.api.param.SystemApiParamViewVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api详情返回实体", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiInterfacesViewVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("所属微服务")
	private String server;
	
	@ApiField("类型{catalog：目录，interface：接口}")
	private String type;
	
	@ApiField("接口名称")
	private String name;
	
	@ApiField("接口路径")
	private String path;
	
	@ApiField("测试接口路径")
	private String testPath;
	
	@ApiField("请求方式{GET、POST、PUT、DELETE}")
	private String method;
	
	@ApiField("请求头")
	private String headers;
	
	@ApiField("公开等级{0-9，0表示完全公开，9表示完全不公开}")
	private Integer openLevel;
	
	@ApiField("已过期{0：未过期，1：已过期}")
	private Boolean deprecated;
	
	@ApiField("父接口名称")
	private String parentName;
	
	@ApiField("请求接口路径参数")
	private List<SystemApiParamViewVo> pathParamList;
	
	@ApiField("请求接口URL参数")
	private List<SystemApiParamViewVo> urlParamList;
	
	@ApiField("请求接口表单参数")
	private List<SystemApiParamViewVo> formParamList;
	
	@ApiField("请求接口body参数(Json格式)")
	private String bodyParamJson;
	
	@ApiField("请求接口返回参数(Json格式)")
	private String returnParamJson;
	
	@ApiField("请求接口body测试参数(Json格式)")
	private String testBodyParamJson;
	
	@ApiField("请求接口返回测试参数(Json格式)")
	private String testReturnParamJson;
}
