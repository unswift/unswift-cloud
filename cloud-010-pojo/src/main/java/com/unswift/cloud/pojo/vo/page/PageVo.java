package com.unswift.cloud.pojo.vo.page;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.vo.BaseVo;
import com.unswift.utils.ObjectUtils;

@JsonInclude(Include.NON_NULL)
@Api(value="分页对象", author="unswift", date="2023-04-08", version="1.0.0")
public class PageVo<E> extends BaseVo implements IPageVo<E>{

	@ApiField("序列化id")
	private static final long serialVersionUID = -7303798307968223442L;
	
	@ApiField("当前页")
	private Integer currPage;
	@ApiField("最后记录数")
	private Integer lastSize;
	@ApiField("总记录数")
	private Integer totalSize;
	@ApiField("总页数")
	private Integer totalPage;
	@ApiField("查询开始记录数")
	private Integer firstSize;
	@ApiField("查询条数")
	private Integer pageSize;
	@ApiField("查询结果数据列表")
	private List<E> dataList;
	
	@ApiConstructor("默认构造")
	public PageVo() {
		this.currPage=1;
		this.pageSize=10;
		this.firstSize=0;
		this.lastSize=10;
	}
	@ApiConstructor(value="带参构造", params={@ApiField("当前页"), @ApiField("每页大小")})
	public PageVo(Integer currPage, Integer pageSize) {
		this.currPage=currPage;
		this.pageSize=pageSize;
		this.firstSize=(this.currPage-1)*this.pageSize;
		this.lastSize=this.currPage*this.pageSize;
	}
	
	@ApiMethod(value="获取当前页", returns=@ApiField("当前页"))
	public Integer getCurrPage() {
		return currPage;
	}
	
	@ApiMethod(value="设置当前页", params=@ApiField("当前页"))
	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
		if(ObjectUtils.isNotNull(this.pageSize)){
			this.firstSize=(this.currPage-1)*this.pageSize;
			this.lastSize=this.currPage*this.pageSize;
		}
	}
	
	@ApiMethod(value="设置页大小", returns=@ApiField("页大小"))
	public Integer getPageSize() {
		return pageSize;
	}
	
	@ApiMethod(value="获取页大小", params=@ApiField("页大小"))
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
		if(ObjectUtils.isNotNull(this.currPage)){
			this.firstSize=(this.currPage-1)*this.pageSize;
			this.lastSize=this.currPage*this.pageSize;
		}
	}
	
	@ApiMethod(value="获取开始记录数", returns=@ApiField("开始记录数"))
	public Integer getFirstSize() {
		return firstSize;
	}
	
	@ApiMethod(value="设置开始记录数", params=@ApiField("开始记录数"))
	public void setFirstSize(Integer firstSize) {
		this.firstSize = firstSize;
	}
	
	@ApiMethod(value="获取结束记录数，如果是第一页，每页10条，则lastSize=10", returns=@ApiField("结束记录数"))
	public Integer getLastSize() {
		return lastSize;
	}
	
	@ApiMethod(value="设置结束记录数，如果是第一页，每页10条，则lastSize=10", params=@ApiField("结束记录数"))
	public void setLastSize(Integer lastSize) {
		this.lastSize = lastSize;
	}
	
	@ApiMethod(value="获取总条数", returns=@ApiField("总条数"))
	public Integer getTotalSize() {
		return totalSize;
	}
	
	@ApiMethod(value="设置总条数", params=@ApiField("总条数"))
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
		if(ObjectUtils.isNotNull(this.pageSize)){
			this.totalPage=this.totalSize%this.pageSize==0?this.totalSize/this.pageSize:this.totalSize/this.pageSize+1;
		}
	}
	
	@ApiMethod(value="获取总页数", returns=@ApiField("总页数"))
	public Integer getTotalPage() {
		return totalPage;
	}
	
	@ApiMethod(value="设置总页数", params=@ApiField("总页数"))
	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}
	
	@ApiMethod(value="获取当前页数据列表", returns=@ApiField("数据列表"))
	public List<E> getDataList() {
		return dataList;
	}
	
	@ApiMethod(value="设置当前页数据列表", params=@ApiField("数据列表"))
	public void setDataList(List<E> dataList) {
		this.dataList = dataList;
	}
	
	@ApiMethod(value="循环当前分页的数据", params=@ApiField("处理行数据"))
	public void forEach(PageRowHandler<E> rowHandler) {
		if(ObjectUtils.isNotEmpty(dataList)) {
			int index=0;
			for (E e : dataList) {
				rowHandler.handler(e, index);
				index++;
			}
		}
	}
	
	@Api("分页数据行处理")
	public abstract static class PageRowHandler<E>{
		
		@ApiMethod(value="处理行数据方法", params= {@ApiField("行数据对象"), @ApiField("行数据下标，从0开始")})
		public void handler(E rowData, int index) {}
	}
}
