package com.unswift.cloud.pojo.dto.system.job;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="职位更新Dto实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemJobUpdateDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("名称")
	private String name;
	
	@ApiField("职责描述")
	private String describe;
	
	@ApiField("顺序")
	private Integer sort;
	
}
