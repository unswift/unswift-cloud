package com.unswift.cloud.pojo.bo.system.attach;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.PageBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件分页查询业务实体", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachPageBo extends PageBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("业务id")
	private Long dataId;
	
	@ApiField("业务模块")
	private String dataModule;
	
	@ApiField("附件分类")
	private String classify;
	
	@ApiField("附件分类列表")
	private List<String> classifyList;
	
	@ApiField("附件名称")
	private String name;
	
	@ApiField("附件类型")
	private String type;
	
	@ApiField("文件大小")
	private Long fileSize;
	
	@ApiField("附件路径")
	private String filePath;
	
	@ApiField("附件下载路径")
	private String fileUrl;
	
	@ApiField("创建时间查询-开始")
	private Date createStartTime;
	
	@ApiField("创建时间查询-结束")
	private Date createEndTime;
}
