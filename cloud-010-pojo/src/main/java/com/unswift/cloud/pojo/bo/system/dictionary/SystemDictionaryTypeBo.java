package com.unswift.cloud.pojo.bo.system.dictionary;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.bo.BaseBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典查询业务实体", author="unswift", date="2023-10-06", version="1.0.0")
public class SystemDictionaryTypeBo extends BaseBo{
	
	@ApiField("类型")
	private String type;
	
	@ApiField("扩展1")
	private String extend01;
}
