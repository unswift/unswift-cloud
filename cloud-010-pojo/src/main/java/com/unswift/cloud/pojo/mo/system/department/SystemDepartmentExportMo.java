package com.unswift.cloud.pojo.mo.system.department;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("部门")
@ApiEntity(value="职位导出模板实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentExportMo extends BaseMo{
	
	@ApiField("编码")
	@ExcelColumn("编码")
	private String code;
	
	@ApiField("名称")
	@ExcelColumn("名称")
	private String name;
	
	@ApiField("部门描述")
	@ExcelColumn("部门描述")
	private String describe;
	
	@ApiField("父部门")
	@ExcelColumn("父部门")
	private String parentName;
	
	@ExcelColumn(value="部门类型", translateBean = "systemDepartmentExportService", translateType = "departmentType")
	@ApiField("部门类型{headOffice：总公司，branchOffices：分公司，subsidiaries：子公司，department：部门}")
	private String type;
	
	@ApiField("顺序")
	@ExcelColumn("顺序")
	private Integer sort;
	
	@ApiField("创建人姓名")
	@ExcelColumn("创建人")
	private String createUserName;
	
	@ApiField("创建时间")
	@ExcelColumn(value="创建时间", format = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
}
