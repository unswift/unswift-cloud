package com.unswift.cloud.pojo.dao.system.config;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemConfig;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统配置批量插入记录实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigInsertBatchItemDo extends SystemConfig implements BaseDo{
	
	
}
