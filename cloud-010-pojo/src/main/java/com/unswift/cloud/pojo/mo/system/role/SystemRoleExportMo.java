package com.unswift.cloud.pojo.mo.system.role;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("系统角色")
@ApiEntity(value="系统角色导出模板实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRoleExportMo extends BaseMo{
	
	@ApiField("角色key")
	@ExcelColumn("角色编码")
	private String key;
	
	@ApiField("角色名称")
	@ExcelColumn("角色名称")
	private String name;
	
	@ApiField("角色状态{1：启用，2：停用}")
	@ExcelColumn(value="角色状态", translateType = "openStop", translateBean = "systemRoleExportService")
	private Byte status;
	
	@ApiField("创建人姓名")
	@ExcelColumn("所有者")
	private String createUserName;
	
	@ApiField("更新时间")
	@ExcelColumn(value="更新时间", format = "yyyy-MM-dd HH:mm:ss")
	private Date changeTime;
}
