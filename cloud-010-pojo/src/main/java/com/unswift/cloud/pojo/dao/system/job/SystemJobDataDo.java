package com.unswift.cloud.pojo.dao.system.job;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemJob;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="职位查询结果实体", author="liyunlong", date="2024-01-18", version="1.0.0")
public class SystemJobDataDo extends SystemJob implements BaseDo{
	
	@ApiField("创建人姓名")
	private String createUserName;

}
