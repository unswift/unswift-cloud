package com.unswift.cloud.pojo.mo.system.cloud;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;

import com.unswift.cloud.pojo.mo.ImportMo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("系统微服务")
@ApiEntity(value="系统微服务导入模板实体", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudImportMo extends ImportMo{
	
	@ApiField("微服务编码")
	@ExcelColumn("jar包名称")
	private String code;
	
	@ApiField("微服务名称")
	@ExcelColumn("中文名称")
	private String name;
	
	@ApiField("网关路径")
	@ExcelColumn("网关路径")
	private String gatewayPath;
	
	@ApiField("描述")
	@ExcelColumn("描述")
	private String describe;
	
}
