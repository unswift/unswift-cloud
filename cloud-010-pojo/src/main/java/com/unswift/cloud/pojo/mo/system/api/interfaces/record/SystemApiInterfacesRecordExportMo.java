package com.unswift.cloud.pojo.mo.system.api.interfaces.record;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("接口api执行记录")
@ApiEntity(value="接口api执行记录导出模板实体", author="unswift", date="2023-09-24", version="1.0.0")
public class SystemApiInterfacesRecordExportMo extends BaseMo{
	
	@ApiField("接口API id")
	@ExcelColumn("接口API id")
	private Long apiInterfaceId;
	
	@ApiField("接口路径")
	@ExcelColumn("接口路径")
	private String path;
	
	@ApiField("请求方式{GET、POST、PUT、DELETE}")
	@ExcelColumn("请求方式{GET、POST、PUT、DELETE}")
	private String method;
	
	@ApiField("请求头")
	@ExcelColumn("请求头")
	private String headers;
	
	@ApiField("请求body")
	@ExcelColumn("请求body")
	private String body;
	
	@ApiField("返回数据")
	@ExcelColumn("返回数据")
	private String response;
	
}
