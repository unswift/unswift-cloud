package com.unswift.cloud.pojo.dto.system.config;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统配置更新Dto实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigUpdateDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("键")
	private String key;
	
	@ApiField("值")
	private String value;
	
	@ApiField("扩展字段1")
	private String extend01;
	
	@ApiField("扩展字段2")
	private String extend02;
	
	@ApiField("扩展字段3")
	private String extend03;
	
	@ApiField("扩展字段4")
	private String extend04;
	
	@ApiField("扩展字段5")
	private String extend05;
	
	@ApiField("描述")
	private String describe;
	
}
