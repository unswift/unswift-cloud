package com.unswift.cloud.pojo.bo.system.department;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.bo.BaseBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统部门树查询业务实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemDepartmentTreeBo extends BaseBo{
	
	@ApiField("部门名称")
	private String name;
	
	@ApiField("展开的部门id")
	private Long spreadId;
}
