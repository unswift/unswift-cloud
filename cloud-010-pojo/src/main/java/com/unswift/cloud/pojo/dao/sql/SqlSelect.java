package com.unswift.cloud.pojo.dao.sql;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.utils.ObjectUtils;

@SuppressWarnings("serial")
@ApiEntity(value="sql select语句", author = "unswift", date = "2024-04-14", version = "1.0.0")
public class SqlSelect implements SqlKeyword{

	@ApiField("查询的字段")
	private String field;
	@ApiField("别名")
	private String alias;
	public SqlSelect(String field, String alias) {
		super();
		this.field = field;
		this.alias = alias;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	@Override
	@ApiMethod(value="将当前select对象转换为sql语句", returns=@ApiField("sql语句"))
	public String toSql() {
		return field+(ObjectUtils.isEmpty(alias)?"":SPACE+alias);
	}
}
