package com.unswift.cloud.pojo.po.system;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.po.SortPo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口参数api映射实体", author="unswift", date="2023-09-10", version="1.0.0")
public class SystemApiParam extends SortPo{
	
	@ApiField("接口api id")
	private Long apiInterfaceId;
	
	@ApiField("属性分类{param：url参数,form：表单参数,body：body参数，return：回调参数}")
	private String paramCategory;
	
	@ApiField("属性名称")
	private String paramKey;
	
	@ApiField("属性描述")
	private String paramComment;
	
	@ApiField("属性类型{string,int,long,byte,char,json,file}")
	private String paramType;
	
	@ApiField("规则")
	private String rule;
	
	@ApiField("必须的")
	private Boolean required;
	
	@ApiField("已过期{0：未过期，1：已过期}")
	private Boolean deprecated;
	
	@ApiField("测试示例")
	private String testExample;
	
	@ApiField("父属性id")
	private Long parentId;
	
}
