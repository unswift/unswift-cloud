package com.unswift.cloud.pojo.mo.system.dictionary;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("数据字典")
@ApiEntity(value="数据字典导出模板实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryExportMo extends BaseMo{
	
	@ApiField("类型")
	private String type;
	
	@ApiField("类型")
	@ExcelColumn("类型")
	private String typeName;
	
	@ApiField("语言")
	@ExcelColumn("语言")
	private String language;
	
	@ApiField("健")
	@ExcelColumn("健")
	private String key;
	
	@ApiField("值")
	@ExcelColumn("值")
	private String value;
	
	@ApiField("扩展字段1")
	@ExcelColumn("扩展1")
	private String extend01;
	
	@ApiField("扩展字段2")
	@ExcelColumn("扩展2")
	private String extend02;
	
	@ApiField("扩展字段3")
	@ExcelColumn("扩展3")
	private String extend03;
	
	@ApiField("扩展字段4")
	@ExcelColumn("扩展4")
	private String extend04;
	
	@ApiField("扩展字段5")
	@ExcelColumn("扩展5")
	private String extend05;
	
	@ApiField("描述")
	@ExcelColumn("描述")
	private String describe;
	
	@ApiField("父项")
	@ExcelColumn("父项")
	private String parentName;
	
	@ApiField("顺序")
	@ExcelColumn("顺序")
	private Integer sort;
	
}
