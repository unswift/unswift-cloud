package com.unswift.cloud.pojo.mo.system.dictionary.type;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.ImportMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("数据字典类型")
@ApiEntity(value="数据字典类型导入模板实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypeImportMo extends ImportMo{
	
	@ApiField("类型编码")
	@ExcelColumn(value="类型编码", required = true, unique = true)
	private String code;
	
	@ApiField("类型名称")
	@ExcelColumn(value="类型名称", required = true, unique = true)
	private String name;
	
	@ApiField("类型描述")
	@ExcelColumn("类型描述")
	private String describe;

}
