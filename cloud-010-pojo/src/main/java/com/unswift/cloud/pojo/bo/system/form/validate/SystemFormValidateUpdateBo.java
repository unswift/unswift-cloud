package com.unswift.cloud.pojo.bo.system.form.validate;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统表单验证更新业务实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateUpdateBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;

	@ApiField("所属微服务")
	private String server;
	
	@ApiField("所属模块")
	private String module;
	
	@Compare
	@ApiField("所属操作")
	private String operator;
	
	@Compare
	@ApiField("验证类型{if：如果，else if：否则如果，else：否则，end if：如果结束，case：当，when then：等于某个值的时候，那么，end case：case结束，foreach：循环，end foreach：循环结束，break：跳出循环，continue：跳出本次循环，var：变量命名，tips：提示语句}")
	private String keyword;
	
	@Compare
	@ApiField("代码")
	private String code;
	
	@Compare
	@ApiField("表达式为真时提示消息")
	private String message;
	
	@Compare
	@ApiField("消息的动态参数")
	private String messageArgs;
	
	@Compare
	@ApiField("代码顺序")
	private Integer codeLine;
	
	@Compare
	@ApiField("代码描述")
	private String describe;
	
}
