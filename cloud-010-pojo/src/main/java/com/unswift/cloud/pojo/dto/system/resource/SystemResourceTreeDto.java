package com.unswift.cloud.pojo.dto.system.resource;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源树查询Dto实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemResourceTreeDto extends BaseDto{
	
	@ApiField("资源名称")
	private String name;
	
	@ApiField("展开的资源id")
	private Long spreadId;
}
