package com.unswift.cloud.pojo.bo.system.job;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.PageBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="职位分页查询业务实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemJobPageBo extends PageBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("编码")
	private String code;
	
	@ApiField("名称")
	private String name;
	
	@ApiField("职责描述")
	private String describe;
	
	@ApiField("顺序")
	private Integer sort;
	
}
