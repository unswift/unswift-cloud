package com.unswift.cloud.pojo.bo.system.cloud;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="微服务更新状态业务实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemCloudUpdateStatusBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@Compare(translateType = "mountStatus")
	@ApiField("挂载状态")
	private Byte mountStatus;
	
}
