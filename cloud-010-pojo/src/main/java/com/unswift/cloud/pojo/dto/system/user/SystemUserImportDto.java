package com.unswift.cloud.pojo.dto.system.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="会员导入Dto实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemUserImportDto extends BaseDto{
	
	@ApiField("附件id")
	private Long attachId;
	
	@ApiField("附件路径")
	private String attachUrl;
}
