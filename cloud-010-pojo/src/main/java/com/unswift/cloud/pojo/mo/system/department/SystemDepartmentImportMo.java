package com.unswift.cloud.pojo.mo.system.department;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.mo.ImportMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("部门")
@ApiEntity(value="部门导入模板实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentImportMo extends ImportMo{
	
	@Compare
	@ExcelColumn(value="部门类型", required = true)
	@ApiField("部门类型{headOffice：总公司，branchOffices：分公司，subsidiaries：子公司，department：部门}")
	private String type;
	
	@Compare
	@ApiField("编码")
	@ExcelColumn(value="编码", unique = true, required = true)
	private String code;
	
	@Compare
	@ApiField("名称")
	@ExcelColumn(value="名称", required = true)
	private String name;
	
	@ApiField("部门描述")
	@ExcelColumn("部门描述")
	private String describe;
	
	@ApiField("父部门路径（AA/BB/CC）")
	@ExcelColumn("父部门路径（AA/BB/CC）")
	private String parentNamePath;
	
	@ApiField("父部门路径存在本表格")
	private Boolean parentNamePathExistsExcel;
}
