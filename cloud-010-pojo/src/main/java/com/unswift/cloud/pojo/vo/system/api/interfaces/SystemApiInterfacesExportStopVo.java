package com.unswift.cloud.pojo.vo.system.api.interfaces;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api停止导出实体", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiInterfacesExportStopVo extends BaseVo{
	
	@ApiField("执行结果")
	private Integer result;
	
	@ApiConstructor("默认构造")
	public SystemApiInterfacesExportStopVo(){
		
	}
	
	@ApiConstructor(value="默认构造", params=@ApiField("执行结果"))
	public SystemApiInterfacesExportStopVo(Integer result){
		this.result=result;
	}
}
