package com.unswift.cloud.pojo.dao.system.department;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.po.system.SystemDepartment;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门查询结果实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentDataDo extends SystemDepartment implements BaseDo{
	
	@ApiField("部门类型")
	private String typeName;
	
	@ApiField("父部门")
	private String parentName;
	
	@ApiField("创建人姓名")
	private String createUserName;
}
