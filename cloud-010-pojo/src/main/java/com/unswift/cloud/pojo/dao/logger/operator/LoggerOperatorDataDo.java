package com.unswift.cloud.pojo.dao.logger.operator;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.logger.LoggerOperator;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="操作日志查询结果实体", author="liyunlong", date="2023-12-12", version="1.0.0")
public class LoggerOperatorDataDo extends LoggerOperator implements BaseDo{
	
	
}
