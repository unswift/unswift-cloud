package com.unswift.cloud.pojo.vo.system.job;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="职位分页查询返回实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemJobPageVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("编码")
	private String code;
	
	@ApiField("名称")
	private String name;
	
	@ApiField("职责描述")
	private String describe;
	
	@ApiField("状态{0：禁用，1：启用}")
	private Byte status;
	
	@ApiField("顺序")
	private Integer sort;
	
	@ApiField("创建人姓名")
	private String createUserName;
	
	@ApiField("创建时间")
	@JsonFormat(timezone="GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
}
