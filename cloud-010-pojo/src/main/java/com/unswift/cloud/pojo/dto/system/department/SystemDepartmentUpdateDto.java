package com.unswift.cloud.pojo.dto.system.department;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门更新Dto实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentUpdateDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("类型{headOffice：总公司，branchOffices：分公司，subsidiaries：子公司，department：部门}")
	private String type;
	
	@ApiField("部门编码")
	private String code;
	
	@ApiField("部门名称")
	private String name;
	
	@ApiField("部门描述")
	private String describe;
	
	@ApiField("父部门id")
	private Long parentId;
	
	@ApiField("父部门id路径")
	private String parentPath;
	
	@ApiField("顺序")
	private Integer sort;
	
	@ApiField("职位编码列表")
	private List<String> jobList;
}
