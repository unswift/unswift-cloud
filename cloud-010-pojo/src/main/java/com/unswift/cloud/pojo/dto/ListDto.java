package com.unswift.cloud.pojo.dto;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@ApiEntity(value="列表Dto顶层实现")
public class ListDto<E> extends BaseDto{

	@ApiField("传递的数据列表")
	private List<E> list;
	
}
