package com.unswift.cloud.pojo.dao.system.resource;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemResource;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源查询结果实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemResourceDataDo extends SystemResource implements BaseDo{
	
	@ApiField("父资源名称")
	private String parentName;
	
}
