package com.unswift.cloud.pojo.bo.system.config;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统配置更新业务实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigUpdateBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@Compare
	@ApiField("键")
	private String key;
	
	@Compare
	@ApiField("值")
	private String value;
	
	@Compare
	@ApiField("扩展字段1")
	private String extend01;
	
	@Compare
	@ApiField("扩展字段2")
	private String extend02;
	
	@Compare
	@ApiField("扩展字段3")
	private String extend03;
	
	@Compare
	@ApiField("扩展字段4")
	private String extend04;
	
	@Compare
	@ApiField("扩展字段5")
	private String extend05;
	
	@Compare
	@ApiField("描述")
	private String describe;
	
}
