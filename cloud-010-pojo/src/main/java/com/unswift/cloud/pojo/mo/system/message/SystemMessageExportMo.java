package com.unswift.cloud.pojo.mo.system.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("系统消息")
@ApiEntity(value="系统消息导出模板实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemMessageExportMo extends BaseMo{
	
	@ApiField("所属应用服务器")
	@ExcelColumn("所属应用服务器")
	private String server;
	
	@ApiField("消息key")
	@ExcelColumn("消息key")
	private String key;
	
	@ApiField("语言")
	@ExcelColumn("语言")
	private String language;
	
	@ApiField("消息code")
	@ExcelColumn("消息code")
	private String code;
	
	@ApiField("消息")
	@ExcelColumn("消息")
	private String message;
	
	@ApiField("消息动态参数排序")
	@ExcelColumn("消息动态参数排序")
	private String argsOrder;
	
	@ApiField("异常类")
	@ExcelColumn("异常类")
	private String exceptionClass;
	
	@ApiField("是否可进化{0：不可进化，1：可进化}")
	@ExcelColumn("是否可进化{0：不可进化，1：可进化}")
	private Byte isEvolvable;
	
}
