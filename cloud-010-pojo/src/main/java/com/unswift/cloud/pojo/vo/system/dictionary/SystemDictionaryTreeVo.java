package com.unswift.cloud.pojo.vo.system.dictionary;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.TreeVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统数据字典树返回实体", author="unswift", date="2023-10-05", version="1.0.0")
public class SystemDictionaryTreeVo extends TreeVo<SystemDictionaryTreeVo, Long>{
	
	@ApiField("类型")
	private String type;
	
	@ApiField("类型名称")
	private String typeName;
	
	@ApiConstructor("默认构造")
	public SystemDictionaryTreeVo() {
		super();
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("唯一值"), @ApiField("显示内容")})
	public SystemDictionaryTreeVo(Long id, String title, String type, String typeName) {
		super(id, title);
		this.type=type;
		this.typeName=typeName;
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("唯一值"), @ApiField("显示内容"), @ApiField("节点是否展开"), @ApiField("节点是否选中")})
	public SystemDictionaryTreeVo(Long id, String title, boolean spread, boolean checked, String type, String typeName) {
		super(id, title, spread, checked);
		this.type=type;
		this.typeName=typeName;
	}
}
