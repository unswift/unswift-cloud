package com.unswift.cloud.pojo.mo.logger.api.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("接口请求日志")
@ApiEntity(value="接口请求日志导出模板实体", author="liyunlong", date="2023-12-08", version="1.0.0")
public class LoggerApiRequestExportMo extends BaseMo{
	
	@ApiField("请求路径")
	@ExcelColumn("请求路径")
	private String path;
	
	@ApiField("接口名称")
	@ExcelColumn("接口名称")
	private String name;
	
	@ApiField("请求头")
	@ExcelColumn("请求头")
	private String headers;
	
	@ApiField("请求参数")
	@ExcelColumn("请求参数")
	private String params;
	
	@ApiField("请求body")
	@ExcelColumn("请求body")
	private String bodys;
	
	@ApiField("返回数据")
	@ExcelColumn("返回数据")
	private String response;
	
}
