package com.unswift.cloud.pojo.dao.system.attach;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件批量更新封装实体", author="liyunlong", date="2024-01-21", version="1.0.0")
public class SystemAttachUpdateBatchDo implements BatchDo<SystemAttachUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemAttachUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemAttachUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public SystemAttachUpdateBatchDo(List<SystemAttachUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
