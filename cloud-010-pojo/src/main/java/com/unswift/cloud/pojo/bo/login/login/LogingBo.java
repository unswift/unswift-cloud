package com.unswift.cloud.pojo.bo.login.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.bo.BaseBo;

@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="登录接口登录Dto实体", author="unswift", date="2023-05-31", version="1.0.0")
public class LogingBo extends BaseBo{

	@ApiField("登录账号")
	private String account;
	
	@ApiField("密码")
	private String password;
	
	@ApiField("验证码")
	private String validateCode;
	
	@ApiField("缓存验证码")
	private String sessionCode;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}

	public String getSessionCode() {
		return sessionCode;
	}

	public void setSessionCode(String sessionCode) {
		this.sessionCode = sessionCode;
	}
	
}
