package com.unswift.cloud.pojo.bo.system.timer;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统定时器导出业务实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemTimerExportBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("定时器名称")
	private String name;
	
	@ApiField("定时器key")
	private String timerKey;
	
	@ApiField("运行类型{cron:指定时间点，delay:固定时间段延迟执行}")
	private String runningType;
	
	@ApiField("运行周期")
	private String runningCycle;
	
	@ApiField("执行bean")
	private String executeBean;
	
	@ApiField("是否可进化{0：不可进化，1：可进化}")
	private Byte isEvolvable;
	
}
