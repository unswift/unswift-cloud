package com.unswift.cloud.pojo.vo.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="用户资源Token实体", author="unswift", date="2023-06-15", version="1.0.0")
public class TokenResourceVo extends BaseVo{

	@ApiField("主键")
	private Long id;
	
	@ApiField("资源类型{menu:菜单，request:请求}")
	private String type;
	
	@ApiField("资源权限key")
	private String authKey;
	
	@ApiField("资源名称")
	private String name;
	
	@ApiField("资源url")
	private String url;
	
	@ApiField("资源图标")
	private String icon;
	
	@ApiField("资源所属父")
	private Long parentId;
	
}
