package com.unswift.cloud.pojo.dao;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.cloud.pojo.Pojo;

@ApiEntity(value="数据库访问层顶层实体（接口），宇燕微服务架构规定，do为系统的数据库实体层，所有的mapper层操作必须使用此层实体", author="unswift", date="2023-06-13", version="1.0.0")
public interface BaseDo extends Pojo{

	
}
