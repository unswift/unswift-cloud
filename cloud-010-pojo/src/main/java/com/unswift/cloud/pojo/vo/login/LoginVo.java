package com.unswift.cloud.pojo.vo.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="登录返回实体", author="unswift", date="2023-07-03", version="1.0.0")
public class LoginVo extends BaseVo{

	@ApiField("登录成功后的token")
	private String token;
	
	@ApiField("登录成功后的token头")
	private String tokenHeader;
	
	@ApiField("登录token的header的key")
	private String tokenKey;
	
	@ApiField("登录token失效时间")
	private Long tokenExpire;

	@ApiConstructor("默认构造")
	public LoginVo() {
		
	}
	
	@ApiConstructor(value="带参构造", params= {@ApiField("用户token"), @ApiField("token追加的头信息"), @ApiField("token在header里的名称"), @ApiField("token失效时间")})
	public LoginVo(String token, String tokenHeader, String tokenKey, Integer tokenExpire) {
		super();
		this.token = token;
		this.tokenHeader = tokenHeader;
		this.tokenKey=tokenKey;
		this.tokenExpire=System.currentTimeMillis()+tokenExpire*1000;
	}

}
