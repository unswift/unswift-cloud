package com.unswift.cloud.pojo.mo.system.job;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.mo.ImportMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("职位")
@ApiEntity(value="职位导入模板实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemJobImportMo extends ImportMo{
	
	@Compare
	@ApiField("编码")
	@ExcelColumn(value="编码", required = true, unique = true)
	private String code;
	
	@Compare
	@ApiField("名称")
	@ExcelColumn(value="名称", required = true, unique = true)
	private String name;
	
	@ApiField("职责描述")
	@ExcelColumn(value="职责描述")
	private String describe;

}
