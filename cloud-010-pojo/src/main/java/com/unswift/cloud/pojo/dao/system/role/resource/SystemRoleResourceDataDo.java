package com.unswift.cloud.pojo.dao.system.role.resource;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemRoleResource;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="角色资源查询结果实体", author="liyunlong", date="2023-12-02", version="1.0.0")
public class SystemRoleResourceDataDo extends SystemRoleResource implements BaseDo{
	
	@ApiField("所属资源名称")
	private String resourceName;
	
	@ApiField("开启入参控制")
	private String openInputName;
	
	@ApiField("开启出参控制")
	private String openOutputName;
}
