package com.unswift.cloud.pojo.dto.system.form.validate.config;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.PageDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统表单验证基础配置分页查询Dto实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateConfigPageDto extends PageDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("所属模块")
	private String key;
	
	@ApiField("所属操作")
	private String value;
	
	@ApiField("代码描述")
	private String describe;
	
}
