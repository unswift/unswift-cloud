package com.unswift.cloud.pojo.bo.system.resource.input;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源入参新建业务实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemResourceInputCreateBo extends BaseBo{
	
	@ApiField("资源id")
	private Long resourceId;
	
	@ApiField("属性key")
	private String paramKey;
	
	@ApiField("属性描述")
	private String paramComment;
	
	@ApiField("值来源")
	private String valueSource;
	
	@ApiField("值")
	private String value;
	
}
