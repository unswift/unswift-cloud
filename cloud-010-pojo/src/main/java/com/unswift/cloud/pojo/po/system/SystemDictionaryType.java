package com.unswift.cloud.pojo.po.system;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.po.BasePo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典类型映射实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryType extends BasePo{
	
	@ApiField("类型编码")
	private String code;
	
	@ApiField("类型名称")
	private String name;
	
	@ApiField("类型描述")
	private String describe;

	@ApiField("状态{0：停用，1：启用}")
	private Byte status;
	
}
