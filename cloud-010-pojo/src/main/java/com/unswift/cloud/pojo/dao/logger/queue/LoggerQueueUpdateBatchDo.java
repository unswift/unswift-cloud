package com.unswift.cloud.pojo.dao.logger.queue;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口请求日志批量更新封装实体", author="liyunlong", date="2024-01-25", version="1.0.0")
public class LoggerQueueUpdateBatchDo implements BatchDo<LoggerQueueUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<LoggerQueueUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public LoggerQueueUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public LoggerQueueUpdateBatchDo(List<LoggerQueueUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
