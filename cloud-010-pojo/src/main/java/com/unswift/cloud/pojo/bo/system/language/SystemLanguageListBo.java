package com.unswift.cloud.pojo.bo.system.language;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.PageBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典-系统语言列表查询业务实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemLanguageListBo extends PageBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("简称")
	private String shortName;
	
	@ApiField("全称")
	private String fullName;
	
	@ApiField("描述")
	private String describe;
	
	@ApiField("激活状态")
	private Byte activeStatus;
	
}
