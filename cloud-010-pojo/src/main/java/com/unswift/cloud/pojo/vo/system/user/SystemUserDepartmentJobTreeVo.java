package com.unswift.cloud.pojo.vo.system.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.TreeVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统用户部门/职位树返回实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemUserDepartmentJobTreeVo extends TreeVo<SystemUserDepartmentJobTreeVo, String>{
	
	@ApiField("树节点类型，department：部门，departmentJob：岗位")
	private String nodeType;
	@ApiField("部门名称,如果节点是岗位时，有值")
	private String departmentName;
	
	@ApiConstructor("默认构造")
	public SystemUserDepartmentJobTreeVo() {
		super();
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("唯一值"), @ApiField("显示内容")})
	public SystemUserDepartmentJobTreeVo(String id, String nodeType, String title) {
		super(id, title);
		this.nodeType=nodeType;
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("唯一值"), @ApiField("部门id"), @ApiField("显示内容"), @ApiField("节点是否展开"), @ApiField("节点是否选中")})
	public SystemUserDepartmentJobTreeVo(String id, String nodeType, String title, boolean spread, boolean checked) {
		super(id, title, spread, checked);
		this.nodeType=nodeType;
	}
}
