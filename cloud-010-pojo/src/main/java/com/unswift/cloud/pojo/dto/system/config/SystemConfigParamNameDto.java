package com.unswift.cloud.pojo.dto.system.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统配置参数名称树Dto实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigParamNameDto extends BaseDto{

	@ApiField("所属模块")
	private String module;
	
}
