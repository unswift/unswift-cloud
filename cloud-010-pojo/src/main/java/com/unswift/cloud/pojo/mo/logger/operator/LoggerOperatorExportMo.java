package com.unswift.cloud.pojo.mo.logger.operator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("操作日志")
@ApiEntity(value="操作日志导出模板实体", author="liyunlong", date="2023-12-12", version="1.0.0")
public class LoggerOperatorExportMo extends BaseMo{
	
	@ApiField("数据id")
	@ExcelColumn("数据id")
	private Long dataId;
	
	@ApiField("所属模块")
	@ExcelColumn("所属模块")
	private String module;
	
	@ApiField("操作类型")
	@ExcelColumn("操作类型")
	private String operatorType;
	
	@ApiField("操作内容")
	@ExcelColumn("操作内容")
	private String operatorContent;
	
}
