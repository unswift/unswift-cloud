package com.unswift.cloud.pojo.dao.system.attach;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemAttach;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件批量更新记录实体", author="liyunlong", date="2024-01-21", version="1.0.0")
public class SystemAttachUpdateBatchItemDo extends SystemAttach implements BaseDo{
	
	
}
