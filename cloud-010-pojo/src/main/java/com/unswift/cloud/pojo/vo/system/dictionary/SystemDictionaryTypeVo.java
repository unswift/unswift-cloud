package com.unswift.cloud.pojo.vo.system.dictionary;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典类型查询返回实体", author="unswift", date="2023-10-06", version="1.0.0")
public class SystemDictionaryTypeVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("健")
	private String key;
	
	@ApiField("值")
	private String value;
	
	@ApiField("扩展字段1")
	private String extend01;
	
	@ApiField("扩展字段2")
	private String extend02;
	
	@ApiField("扩展字段3")
	private String extend03;
	
	@ApiField("扩展字段4")
	private String extend04;
	
	@ApiField("扩展字段5")
	private String extend05;
	
	@ApiField("描述")
	private String describe;
	
	@ApiField("父id")
	private Long parentId;
	
	@ApiField("父id集合")
	private String parentIds;
	
}
