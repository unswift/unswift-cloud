package com.unswift.cloud.pojo.vo.system.api.version;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类版本api详情返回实体", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiVersionViewVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("关联 id")
	private Long relationId;
	
	@ApiField("关联表")
	private String relationTable;
	
	@ApiField("版本描述")
	private String comment;
	
	@ApiField("作者")
	private String author;
	
	@ApiField("日期")
	@JsonFormat(pattern="yyyy-MM-dd", timezone="GMT+8")
	private Date date;
	
	@ApiField("版本")
	private String version;
	
}
