package com.unswift.cloud.pojo.dao.system.dictionary;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemDictionary;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典插入记录实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryInsertDo extends SystemDictionary implements BaseDo{
	
	
}
