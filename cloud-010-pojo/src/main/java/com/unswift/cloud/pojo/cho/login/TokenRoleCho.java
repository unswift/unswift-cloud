package com.unswift.cloud.pojo.cho.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.cho.BaseCho;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="用户角色Token实体", author="unswift", date="2023-06-15", version="1.0.0")
public class TokenRoleCho extends BaseCho{

	@ApiField("主键")
	private Long id;
	
	@ApiField("角色key")
	private String key;
	
	@ApiField("角色名称")
	private String name;

}
