package com.unswift.cloud.pojo.bo.logger.imports.task;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.bo.BaseBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导出任务记录表详情业务实体", author="unswift", date="2023-07-23", version="1.0.0")
public class LoggerImportTaskQueueBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("用户token")
	private String token;
	
	@ApiField("系统语言")
	private String language;
}
