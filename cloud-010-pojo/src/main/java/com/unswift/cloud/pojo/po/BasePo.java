package com.unswift.cloud.pojo.po;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

@SuppressWarnings("serial")
@ApiEntity(value="基础实体类", author="unswift", date="2023-04-06", version="1.0.0")
public abstract class BasePo extends CorePo{

	@ApiField("创建人")
	private Long createUser;
	
	@ApiField("变更人")
	private Long changeUser;

	public Long getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Long createUser) {
		this.createUser = createUser;
	}

	public Long getChangeUser() {
		return changeUser;
	}

	public void setChangeUser(Long changeUser) {
		this.changeUser = changeUser;
	}
	
}
