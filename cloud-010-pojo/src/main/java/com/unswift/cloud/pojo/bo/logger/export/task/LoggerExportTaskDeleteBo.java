package com.unswift.cloud.pojo.bo.logger.export.task;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导出任务记录表删除业务实体", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerExportTaskDeleteBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
}
