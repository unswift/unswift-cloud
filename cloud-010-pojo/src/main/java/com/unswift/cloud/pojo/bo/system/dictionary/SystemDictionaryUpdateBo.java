package com.unswift.cloud.pojo.bo.system.dictionary;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典更新业务实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryUpdateBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("类型")
	private String type;
	
	@ApiField("语言")
	private String language;
	
	@Compare
	@ApiField("语言")
	private String languageName;
	
	@Compare
	@ApiField("健")
	private String key;
	
	@Compare
	@ApiField("值")
	private String value;
	
	@Compare
	@ApiField("扩展字段1")
	private String extend01;
	
	@Compare
	@ApiField("扩展字段2")
	private String extend02;
	
	@Compare
	@ApiField("扩展字段3")
	private String extend03;
	
	@Compare
	@ApiField("扩展字段4")
	private String extend04;
	
	@Compare
	@ApiField("扩展字段5")
	private String extend05;
	
	@Compare
	@ApiField("描述")
	private String describe;
	
	@ApiField("父id")
	private Long parentId;
	
	@ApiField("父路径")
	private String parentPath;
	
	@Compare
	@ApiField("顺序")
	private Integer sort;

	@ApiField("状态{0：停用，1：启用}")
	private Byte status;
	
}
