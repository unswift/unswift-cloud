package com.unswift.cloud.pojo.vo.logger.imports.task;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.unswift.cloud.json.Decimal2Serializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导入任务记录表分页查询返回实体", author="liyunlong", date="2024-01-09", version="1.0.0")
public class LoggerImportTaskPageVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("名称")
	private String name;
	
	@ApiField("所属模块")
	private String module;
	
	@ApiField("导入文档类型{excel}")
	private String documentType;
	
	@ApiField("导出状态{0:等待导入,1:导入中,2:导入完成}")
	private Byte status;
	
	@ApiField("导入开始时间")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date startTime;
	
	@ApiField("导入结束时间")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date endTime;
	
	@ApiField("导入业务Bean路径")
	private String importBeanClass;
	
	@ApiField("导入模板路径")
	private String importModelClass;
	
	@ApiField("导入数据索引，附件id等")
	private String importData;
	
	@ApiField("导入结果{0：失败，1：成功}")
	private Byte result;
	
	@ApiField("错误消息")
	private String errorMessage;
	
	@ApiField("执行消息，如果有异常，则记录异常")
	private String errorStack;
	
	@ApiField("导入进度")
	@JsonSerialize(using=Decimal2Serializer.class)
	private BigDecimal progress;
	
}
