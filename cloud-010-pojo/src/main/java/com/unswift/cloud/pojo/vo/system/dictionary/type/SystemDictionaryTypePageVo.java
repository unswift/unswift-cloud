package com.unswift.cloud.pojo.vo.system.dictionary.type;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典类型分页查询返回实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypePageVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("类型编码")
	private String code;
	
	@ApiField("类型名称")
	private String name;
	
	@ApiField("类型描述")
	private String describe;

	@ApiField("状态{0：停用，1：启用}")
	private Byte status;
	
	@ApiField("创建人姓名")
	private String createUserName;
	
	@ApiField("创建时间")
	@JsonFormat(timezone="GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
}
