package com.unswift.cloud.pojo.vo.system.department.job;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门职位分页查询返回实体", author="liyunlong", date="2024-01-22", version="1.0.0")
public class SystemDepartmentJobPageVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("部门code")
	private String departmentCode;
	
	@ApiField("职位code")
	private String jobCode;
	
	@ApiField("顺序")
	private Integer sort;
	
}
