package com.unswift.cloud.pojo.dao.system.department.user;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门用户批量插入封装实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentUserInsertBatchDo implements BatchDo<SystemDepartmentUserInsertBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemDepartmentUserInsertBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemDepartmentUserInsertBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量插入列表")})
	public SystemDepartmentUserInsertBatchDo(List<SystemDepartmentUserInsertBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
