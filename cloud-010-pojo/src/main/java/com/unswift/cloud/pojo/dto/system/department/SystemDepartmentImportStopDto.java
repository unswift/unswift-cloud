package com.unswift.cloud.pojo.dto.system.department;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门停止导入Dto实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentImportStopDto extends BaseDto{
	
	@ApiField("主键")
	private Long id;
	
}
