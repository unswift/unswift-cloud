package com.unswift.cloud.pojo.dao.logger.timer.record;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="定时器执行记录批量更新封装实体", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerTimerRecordUpdateBatchDo implements BatchDo<LoggerTimerRecordUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<LoggerTimerRecordUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public LoggerTimerRecordUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public LoggerTimerRecordUpdateBatchDo(List<LoggerTimerRecordUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
