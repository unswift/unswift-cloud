package com.unswift.cloud.pojo.bo.system.role.resource;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.logger.Compare;
import com.unswift.cloud.pojo.bo.ListBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="角色资源更新业务实体", author="liyunlong", date="2023-12-02", version="1.0.0")
public class SystemRoleResourceUpdateListBo extends ListBo<SystemRoleResourceUpdateBo>{
	
	@ApiField("所属角色")
	private Long roleId;
	
	@ApiField("资源集合")
	@Compare(value="资源", pkFieldName = "resourceId")
	private List<SystemRoleResourceUpdateBo> list;
	
	@ApiConstructor("默认构造")
	public SystemRoleResourceUpdateListBo() {
		
	}

	@ApiConstructor(value="带参构造", params = {@ApiField("资源列表")})
	public SystemRoleResourceUpdateListBo(List<SystemRoleResourceUpdateBo> list) {
		super();
		this.setList(list);
	}
	
	
}
