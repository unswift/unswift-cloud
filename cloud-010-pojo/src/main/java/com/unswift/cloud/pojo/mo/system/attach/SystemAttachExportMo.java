package com.unswift.cloud.pojo.mo.system.attach;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("附件")
@ApiEntity(value="附件导出模板实体", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachExportMo extends BaseMo{
	
	@ApiField("业务id")
	@ExcelColumn("业务id")
	private Long dataId;
	
	@ApiField("业务模块")
	@ExcelColumn("业务模块")
	private String dataModule;
	
	@ApiField("附件名称")
	@ExcelColumn("附件名称")
	private String name;
	
	@ApiField("附件类型")
	@ExcelColumn("附件类型")
	private String type;
	
	@ApiField("文件大小")
	@ExcelColumn("文件大小")
	private Long fileSize;
	
	@ApiField("附件路径")
	@ExcelColumn("附件路径")
	private String filePath;
	
	@ApiField("附件下载路径")
	@ExcelColumn("附件下载路径")
	private String fileUrl;
	
}
