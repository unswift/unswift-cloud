package com.unswift.cloud.pojo.dao.logger.timer.record;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.PkListDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="定时器执行记录多条记录删除实体", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerTimerRecordDeleteBatchDo implements PkListDo<Long>{
	
	@ApiField("主键集合")
	private List<Long> idList;
	
	@ApiConstructor("默认构造")
	public LoggerTimerRecordDeleteBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("删除的主键集合")})
	public LoggerTimerRecordDeleteBatchDo(List<Long> idList){
		super();
		this.idList=idList;
	}

}
