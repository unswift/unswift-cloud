package com.unswift.cloud.pojo.po.system;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.po.SortPo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类构造函数api映射实体", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiConstructor extends SortPo{
	
	@ApiField("类API id")
	private Long apiClassId;
	
	@ApiField("类描述")
	private String comment;
	
	@ApiField("修饰符{public：公共的，private：私有的...}")
	private String modifiers;
	
	@ApiField("参数数量")
	private Integer paramCount;
	
	@ApiField("作者")
	private String author;
	
	@ApiField("日期")
	private Date date;
	
	@ApiField("已过期{0：未过期，1：已过期}")
	private Boolean deprecated;
	
}
