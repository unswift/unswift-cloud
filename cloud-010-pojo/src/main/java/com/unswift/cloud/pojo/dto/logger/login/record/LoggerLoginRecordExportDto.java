package com.unswift.cloud.pojo.dto.logger.login.record;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.PageDto;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="登录记录导出查询Dto实体", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordExportDto extends PageDto{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("登录账号")
	private String account;
	
	@ApiField("登录状态{0：登录失败，1：登录成功}")
	private Byte loginResult;
	
	@ApiField("登录错误码system_message_表的code")
	private String errorCode;
	
	@ApiField("登录时间查询-开始")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date loginStartTime;
	
	@ApiField("登录时间查询-结束")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date loginEndTime;
}
