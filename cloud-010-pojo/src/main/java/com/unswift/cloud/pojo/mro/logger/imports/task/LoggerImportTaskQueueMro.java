package com.unswift.cloud.pojo.mro.logger.imports.task;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.enums.imports.task.ImportTaskEventEnum;
import com.unswift.cloud.pojo.mro.BaseMro;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导入任务记录表队列接收实体", author="unswift", date="2024-01-09", version="1.0.0")
public class LoggerImportTaskQueueMro extends BaseMro{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("事件")
	private String event;
	
	@ApiConstructor("默认构造")
	public LoggerImportTaskQueueMro() {
		
	}
	
	public LoggerImportTaskQueueMro(Long id, ImportTaskEventEnum event, String token, String language) {
		super();
		this.id = id;
		this.event = event.getKey();
		this.setToken(token);
		this.setLanguage(language);
	}
	
}
