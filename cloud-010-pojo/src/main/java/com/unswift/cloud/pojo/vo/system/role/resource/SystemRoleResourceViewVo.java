package com.unswift.cloud.pojo.vo.system.role.resource;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="角色资源详情返回实体", author="liyunlong", date="2023-12-02", version="1.0.0")
public class SystemRoleResourceViewVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("所属角色")
	private Long roleId;
	
	@ApiField("所属资源id")
	private Long resourceId;
	
	@ApiField("开启入参控制")
	private Boolean openInput;
	
	@ApiField("开启出参控制")
	private Boolean openOutput;
	
}
