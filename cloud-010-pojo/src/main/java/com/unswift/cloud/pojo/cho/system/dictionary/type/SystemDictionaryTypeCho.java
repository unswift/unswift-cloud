package com.unswift.cloud.pojo.cho.system.dictionary.type;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.cho.BaseCho;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="数据字典类型详情返回实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypeCho extends BaseCho{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("类型编码")
	private String code;
	
	@ApiField("类型名称")
	private String name;
	
	@ApiField("类型描述")
	private String describe;
	
}
