package com.unswift.cloud.pojo.dao.system.resource;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统资源批量更新封装实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemResourceUpdateBatchDo implements BatchDo<SystemResourceUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemResourceUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemResourceUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public SystemResourceUpdateBatchDo(List<SystemResourceUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
