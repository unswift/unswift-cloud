package com.unswift.cloud.pojo.bo.logger.queue;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口请求日志导出业务实体", author="liyunlong", date="2024-01-25", version="1.0.0")
public class LoggerQueueExportBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("队列名称")
	private String queueName;
	
	@ApiField("交换机名称")
	private String exchangeName;
	
	@ApiField("路由key")
	private String routeKey;
	
	@ApiField("消息内容")
	private String message;
	
	@ApiField("状态{0：已发送，1：已消费}")
	private Byte status;
	
	@ApiField("消费结果，0：异常，1：正常")
	private Byte result;
	
	@ApiField("异常消息")
	private String errorMessage;
	
}
