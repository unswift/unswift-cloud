package com.unswift.cloud.pojo.vo.system.job;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="职位列表查询返回实体", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemJobListVo extends BaseVo{
	
	@ApiField("编码")
	private String code;
	
	@ApiField("名称")
	private String name;
	
}
