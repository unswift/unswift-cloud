package com.unswift.cloud.pojo.vo;

import java.util.List;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@ApiEntity(value="列表Vo返回实体类", author="unswift", date="2023-10-06", version="1.0.0")
public class ListVo<E extends BaseVo> extends BaseVo{

	@ApiField("数据列表")
	private List<E> dataList;
	
	@ApiConstructor("默认构造")
	public ListVo() {
		
	}
	
	@ApiConstructor(value="带参构造", params = @ApiField("数据列表"))
	public ListVo(List<E> dataList) {
		this.dataList=dataList;
	}
}
