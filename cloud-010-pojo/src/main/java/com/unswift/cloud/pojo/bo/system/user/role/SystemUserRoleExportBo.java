package com.unswift.cloud.pojo.bo.system.user.role;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="会员角色导出业务实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemUserRoleExportBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("会员id")
	private Long userId;
	
	@ApiField("角色id")
	private Long roleId;
	
	@ApiField("权限类型{owner：所有者，manager：管理者}")
	private String authorityType;
	
}
