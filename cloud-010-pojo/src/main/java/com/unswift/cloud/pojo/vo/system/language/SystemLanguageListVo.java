package com.unswift.cloud.pojo.vo.system.language;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.ListVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统语言列表返回实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemLanguageListVo extends ListVo<SystemLanguageListItemVo>{
	
	@ApiConstructor("默认构造")
	public SystemLanguageListVo() {
		
	}
	
	@ApiConstructor(value="带参构造", params = @ApiField("语言列表"))
	public SystemLanguageListVo(List<SystemLanguageListItemVo> dataList) {
		this.setDataList(dataList);
	}
}
