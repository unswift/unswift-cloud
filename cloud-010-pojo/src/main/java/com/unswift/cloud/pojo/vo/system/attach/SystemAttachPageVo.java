package com.unswift.cloud.pojo.vo.system.attach;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件分页查询返回实体", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachPageVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("业务id")
	private Long dataId;
	
	@ApiField("业务模块")
	private String dataModule;
	@ApiField("业务模块")
	private String dataModuleName;
	
	@ApiField("附件名称")
	private String name;
	
	@ApiField("附件分类")
	private String classify;
	
	@ApiField("附件分类")
	private String classifyName;
	
	@ApiField("附件类型")
	private String type;
	
	@ApiField("文件大小")
	private Long fileSize;
	
	@ApiField("文件大小")
	private String fileSizeShow;
	
	@ApiField("附件下载路径")
	private String fileUrl;
	
	@ApiField("附件是否存在{0：已删除，1：存在}")
	private Byte isExists;
	
	@ApiField("附件是否存在{0：已删除，1：存在}")
	private String isExistsName;
	
	@ApiField("创建人姓名")
	private String createUserName;
	
	@ApiField("创建时间")
	@JsonFormat(timezone="GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
}
