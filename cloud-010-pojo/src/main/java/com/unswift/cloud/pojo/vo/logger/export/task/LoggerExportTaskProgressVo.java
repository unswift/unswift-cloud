package com.unswift.cloud.pojo.vo.logger.export.task;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导出任务记录表详情返回实体", author="unswift", date="2023-08-13", version="1.0.0")
public class LoggerExportTaskProgressVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("所属模块")
	private String module;
	
	@ApiField("导出状态{0:等待导出,1:导出中,2:导出完成}")
	private Byte status;
	
	@ApiField("导出结果{0：失败，1：成功}")
	private Byte result;
	
	@ApiField("导出进度{0-100}")
	private BigDecimal progress;
	
	@ApiField("错误消息")
	private String errorMessage;
	
	@ApiField("导出文件下载路径")
	private String exportFileUrl;
	
	@ApiField("导出文件名称")
	private String exportFileName;
	
	@ApiField("导入数量")
	private Long exportCount;
}
