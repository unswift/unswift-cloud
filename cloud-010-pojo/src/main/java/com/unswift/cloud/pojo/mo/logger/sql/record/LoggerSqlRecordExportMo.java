package com.unswift.cloud.pojo.mo.logger.sql.record;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("sql执行记录")
@ApiEntity(value="sql执行记录导出模板实体", author="unswift", date="2023-09-05", version="1.0.0")
public class LoggerSqlRecordExportMo extends BaseMo{
	
	@ApiField("sql表主键")
	@ExcelColumn("sql表主键")
	private Long sqlId;
	
	@ApiField("sql脚本")
	@ExcelColumn("sql脚本")
	private String sqlScript;
	
	@ApiField("执行状态{0：初始化，1：执行成功，2：执行失败}")
	@ExcelColumn("执行状态{0：初始化，1：执行成功，2：执行失败}")
	private Byte exeStatus;
	
	@ApiField("错误信息")
	@ExcelColumn("错误信息")
	private String message;
	
}
