package com.unswift.cloud.pojo.vo.system.cache.refresh;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="缓存刷新分页查询返回实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemCacheRefreshPageVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("所属微服务")
	private String server;
	
	@ApiField("所属主机")
	private String hostIp;
	
	@ApiField("所属端口")
	private String hostPort;
	
	@ApiField("缓存类型{memory：内存缓存，redis：redis缓存}")
	private String cacheType;
	
	@ApiField("缓存key")
	private String cacheKey;
	
	@ApiField("刷新状态{0：不需要刷新，1：需要刷新}")
	private Byte refreshStatus;
	
	@ApiField("刷新bean")
	private String refreshBean;
	
}
