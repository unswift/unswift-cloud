package com.unswift.cloud.pojo.bo.logger.login.record;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="登录记录更新业务实体", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordUpdateBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("登录账号")
	private String account;
	
	@ApiField("登录时间")
	private Date loginTime;
	
	@ApiField("登录状态{0：登录失败，1：登录成功}")
	private Byte loginResult;
	
	@ApiField("登录错误码system_message_表的code")
	private String errorCode;
	
}
