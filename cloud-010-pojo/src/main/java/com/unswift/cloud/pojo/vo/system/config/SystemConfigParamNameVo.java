package com.unswift.cloud.pojo.vo.system.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统配置参数名称返回实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigParamNameVo extends BaseVo{
	
	@ApiField("参数键")
	private String key;
	
	@ApiField("参数名")
	private String value;
	
	@ApiField("值类型{text:输入框，radio:单选框，checkbox：复选框，select：下拉框，tree：树节点选择框}")
	private String valueType;
	
	@ApiField("选项数据")
	private Object optionsData;
	
	@ApiConstructor("默认构造")
	public SystemConfigParamNameVo() {
		super();
	}

	@ApiConstructor(value="带参构造", params = {@ApiField("参数键"), @ApiField("参数名"), @ApiField("值类型"), @ApiField("选项数据")})
	public SystemConfigParamNameVo(String key, String value, String valueType, Object optionsData) {
		super();
		this.key = key;
		this.value = value;
		this.valueType = valueType;
		this.optionsData = optionsData;
	}
	
}
