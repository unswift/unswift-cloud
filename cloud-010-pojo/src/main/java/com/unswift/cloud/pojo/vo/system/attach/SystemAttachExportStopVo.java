package com.unswift.cloud.pojo.vo.system.attach;

import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件停止导出实体", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachExportStopVo extends BaseVo{
	
	@ApiField("执行结果")
	private Integer result;
	
	@ApiConstructor("默认构造")
	public SystemAttachExportStopVo(){
		
	}
	
	@ApiConstructor(value="默认构造", params=@ApiField("执行结果"))
	public SystemAttachExportStopVo(Integer result){
		this.result=result;
	}
}
