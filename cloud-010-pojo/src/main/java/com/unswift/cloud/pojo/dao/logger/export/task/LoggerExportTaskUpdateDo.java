package com.unswift.cloud.pojo.dao.logger.export.task;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.SingleDo;
import com.unswift.cloud.pojo.dao.sql.Sql;

import com.unswift.cloud.pojo.po.logger.LoggerExportTask;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导出任务记录表更新单条记录实体", author="liyunlong", date="2024-01-10", version="1.0.0")
public class LoggerExportTaskUpdateDo extends LoggerExportTask implements SingleDo{
	
	@ApiField("查询的sql对象")
	private Sql sql;

}
