package com.unswift.cloud.pojo.cho.system.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.cho.BaseCho;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统配置缓存实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigCho extends BaseCho{
	
	@ApiField("键")
	private String key;
	
	@ApiField("值")
	private String value;
	
	@ApiField("扩展字段1")
	private String extend01;
	
	@ApiField("扩展字段2")
	private String extend02;
	
	@ApiField("扩展字段3")
	private String extend03;
	
	@ApiField("扩展字段4")
	private String extend04;
	
	@ApiField("扩展字段5")
	private String extend05;
	
	@ApiField("描述")
	private String describe;
	
	@ApiField("状态{1：启用，2：停用}")
	private Byte status;
	
}
