package com.unswift.cloud.pojo.dao.system.department.job;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.po.system.SystemDepartmentJob;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门职位查询结果实体", author="liyunlong", date="2024-01-22", version="1.0.0")
public class SystemDepartmentJobDataDo extends SystemDepartmentJob implements BaseDo{
	
	@ApiField("岗位id")
	private Long jobId;
	
	@ApiField("岗位名称")
	private String jobName;
	
	@ApiField("部门id")
	private Long departmentId;
}
