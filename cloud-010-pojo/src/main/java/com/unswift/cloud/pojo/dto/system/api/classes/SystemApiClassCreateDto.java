package com.unswift.cloud.pojo.dto.system.api.classes;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.BaseDto;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类api新建Dto实体", author="unswift", date="2023-09-06", version="1.0.0")
public class SystemApiClassCreateDto extends BaseDto{
	
	@ApiField("类类型{interface：接口, enum：枚举,class：类，entity：实体}")
	private String classType;
	
	@ApiField("修饰符{public：公共的，private：私有的...}")
	private String modifiers;
	
	@ApiField("类描述")
	private String classComment;
	
	@ApiField("类名称")
	private String className;
	
	@ApiField("作者")
	private String author;
	
	@ApiField("日期")
	@JsonFormat(pattern="yyyy-MM-dd", timezone="GMT+8")
	private Date date;
	
	@ApiField("版本")
	private String version;
	
	@ApiField("已过期{0：未过期，1：已过期}")
	private Boolean deprecated;
	
}
