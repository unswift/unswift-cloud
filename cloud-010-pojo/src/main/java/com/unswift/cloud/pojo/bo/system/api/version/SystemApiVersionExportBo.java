package com.unswift.cloud.pojo.bo.system.api.version;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.bo.BaseBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类版本api导出业务实体", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiVersionExportBo extends BaseBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("关联 id")
	private Long relationId;
	
	@ApiField("关联表")
	private String relationTable;
	
	@ApiField("版本描述")
	private String classComment;
	
	@ApiField("作者")
	private String author;
	
	@ApiField("日期")
	private Date date;
	
	@ApiField("版本")
	private String version;
	
}
