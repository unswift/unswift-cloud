package com.unswift.cloud.pojo.dao.sql;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.utils.ObjectUtils;

@SuppressWarnings("serial")
@ApiEntity(value="sql排序", author = "unswift", date = "2024-04-14", version = "1.0.0")
public class SqlOrderBy implements SqlKeyword{

	@ApiField("排序字段")
	private String field;
	@ApiField("顺序或者倒序{ASC:顺序，DESC:倒叙}")
	private String ascDesc;
	public SqlOrderBy(String field, String ascDesc) {
		super();
		this.field = field;
		this.ascDesc = ascDesc;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getAscDesc() {
		return ascDesc;
	}
	public void setAscDesc(String ascDesc) {
		this.ascDesc = ascDesc;
	}
	
	@Override
	@ApiMethod(value="将当前orderBy对象转换为sql语句", returns=@ApiField("sql语句"))
	public String toSql() {
		return field+ObjectUtils.init(ascDesc, ORDER_BY_ASC);
	}
}
