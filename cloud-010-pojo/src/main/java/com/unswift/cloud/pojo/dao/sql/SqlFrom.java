package com.unswift.cloud.pojo.dao.sql;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.utils.ObjectUtils;

@SuppressWarnings("serial")
@ApiEntity(value="sql from 语句", author = "unswift", date = "2024-04-14", version = "1.0.0")
public class SqlFrom implements SqlKeyword{

	@ApiField("表名称")
	private String tableName;
	@ApiField("别名")
	private String alias;
	@ApiField("表连接模式 (join on),(left join on)")
	private String joinOnType;
	@ApiField("表连接条件")
	private String joinCondition;
	public SqlFrom(String tableName, String alias, String joinOnType, String joinCondition) {
		super();
		this.tableName = tableName;
		this.alias = alias;
		this.joinOnType = joinOnType;
		this.joinCondition = joinCondition;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getJoinOnType() {
		return joinOnType;
	}
	public void setJoinOnType(String joinOnType) {
		this.joinOnType = joinOnType;
	}
	public String getJoinCondition() {
		return joinCondition;
	}
	public void setJoinCondition(String joinCondition) {
		this.joinCondition = joinCondition;
	}
	
	@Override
	@ApiMethod(value="将当前from对象转换为sql语句", returns=@ApiField("sql语句"))
	public String toSql() {
		StringBuilder sql=new StringBuilder();
		if(ObjectUtils.isNotEmpty(joinOnType)){
			sql.append(joinOnType).append(SPACE);
		}
		sql.append(tableName);
		if(ObjectUtils.isNotEmpty(alias)){
			sql.append(SPACE).append(alias);
		}
		if(ObjectUtils.isNotEmpty(joinOnType)){
			sql.append(FROM_ON);
		}
		if(ObjectUtils.isNotEmpty(joinCondition)){
			sql.append(joinCondition);
		}
		return sql.toString();
	}
}
