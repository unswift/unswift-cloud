package com.unswift.cloud.pojo.dao.sql;

import java.io.Serializable;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;

@ApiEntity(value="自定义sql常量", author="unswift", date="2023-07-17", version="1.0.0")
public interface SqlKeyword extends Serializable{

	@ApiField("sql语句中的and关键词")
	final static String LOGIC_AND="AND";
	
	@ApiField("sql语句中的or关键词")
	final static String LOGIC_OR="OR";
	
	@ApiField("sql语句中的等于号")
	final static String COMPARE_EQUAL="=";
	
	@ApiField("sql语句中的不等于号")
	final static String COMPARE_NOT_EQUAL="<>";
	
	@ApiField("sql语句中的大于号")
	final static String COMPARE_GREATER=">";
	
	@ApiField("sql语句中的大于等于号")
	final static String COMPARE_GREATER_EQUAL=">=";
	
	@ApiField("sql语句中的小于号")
	final static String COMPARE_LESS="<";
	
	@ApiField("sql语句中的小于等于号")
	final static String COMPARE_LESS_EQUAL="<=";
	
	@ApiField("sql语句中的like关键词")
	final static String COMPARE_LIKE="LIKE";
	
	@ApiField("sql语句中的not like关键词")
	final static String COMPARE_NOT_LIKE="NOT LIKE";
	
	@ApiField("sql语句中的IS号")
	final static String COMPARE_IS="IS";
	
	@ApiField("sql语句中的IS NOT号")
	final static String COMPARE_IS_NOT="IS NOT";
	
	@ApiField("sql语句中的in关键词")
	final static String COMPARE_IN="IN";
	
	@ApiField("sql语句中的exists关键词")
	final static String COMPARE_EXISTS="EXISTS";
	
	@ApiField("sql语句中的not exists关键词")
	final static String COMPARE_NOT_EXISTS="NOT EXISTS";
	
	@ApiField("sql语句中的between关键词")
	final static String COMPARE_BETWEEN="BETWEEN";
	
	@ApiField("sql语句中的order by关键词")
	final static String ORDER_BY=" ORDER BY ";
	
	@ApiField("sql语句中的group by关键词")
	final static String GROUP_BY=" GROUP BY ";
	@ApiField("sql语句中的group by关键词")
	final static String LIMIT=" LIMIT ";
	
	@ApiField("sql语句中的asc关键词")
	final static String ORDER_BY_ASC="ASC";
	
	@ApiField("sql语句中的desc关键词")
	final static String ORDER_BY_DESC="DESC";
	
	@ApiField("sql语句中的join关键词")
	final static String FROM_JOIN="JOIN";
	
	@ApiField("sql语句中的left join关键词")
	final static String FROM_LEFT_JOIN="LEFT JOIN";
	
	@ApiField("sql语句中的on关键词")
	final static String FROM_ON=" ON ";
	
	@ApiField("sql语句中的select关键词")
	final static String SELECT="SELECT ";
	
	@ApiField("sql语句中的update关键词")
	final static String UPDATE="UPDATE ";
	
	@ApiField("sql语句中的from关键词")
	final static String FROM=" FROM ";
	
	@ApiField("sql语句中的set关键词")
	final static String SET="SET ";
	
	@ApiField("sql语句中的where关键词")
	final static String WHERE=" WHERE ";
	
	@ApiField("sql语句中的空格")
	final static String SPACE=" ";
	
	@ApiField("sql语句中的关键词修饰")
	final static String KEYWORD_DECORATE="`";
	
	@ApiField("sql语句中的左括号")
	final static String LEFT_BRACKET="(";
	
	@ApiField("sql语句中的左括号")
	final static String RIGHT_BRACKET=")";
	
	@ApiField("sql语句中的,")
	final static String COMMA=",";
	
	@ApiMethod(value="将当前对象转换为sql语句", returns=@ApiField("sql语句"))
	String toSql();
}
