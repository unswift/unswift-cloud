package com.unswift.cloud.pojo.vo;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.exception.ResponseBodyException;

@SuppressWarnings("serial")
@Api(value="请求返回对象", author="unswift", date="2023-04-22", version="1.0.0")
public class ResponseBody<E extends IBaseVo> extends BusinessVo{

	@ApiMethod(value="请求成功返回数据", params=@ApiField("返回的数据"), returns=@ApiField("封装的response body对象"))
	public static <E extends IBaseVo> ResponseBody<E> success(E body){
		return new ResponseBody<E>(body);
	}
	
	@ApiMethod(value="请求失败返回", params={@ApiField("错误编码"), @ApiField("错误消息")}, returns=@ApiField("封装的response body对象"))
	public static <E extends IBaseVo> ResponseBody<E> error(String code, String message){
		return new ResponseBody<E>(code, message);
	}
	
	@ApiField("返回编码,正确：0，否则，错误")
	private String code;
	@ApiField("错误消息")
	private String message;
	@ApiField("返回数据")
	private E body;
	
	@ApiConstructor("默认构造")
	public ResponseBody() {
		
	}
	
	@ApiConstructor(value="返回数据的Response body的构造", params = @ApiField("返回数据对象"))
	private ResponseBody(E body) {
		this.body=body;
		this.code="0";
		this.message="成功";
	}
	
	@ApiConstructor(value="错误编码和消息的Response body的构造", params = {@ApiField("返回编码,正确：0，否则，错误"), @ApiField("错误消息")})
	private ResponseBody(String code, String message) {
		this.code=code;
		this.message=message;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public E getBody() {
		return body;
	}
	public E toBody() {
		if(!"0".equals(code)){
			throw new ResponseBodyException(code, message);
		}
		return body;
	}
	public void setBody(E body) {
		this.body = body;
	}
}
