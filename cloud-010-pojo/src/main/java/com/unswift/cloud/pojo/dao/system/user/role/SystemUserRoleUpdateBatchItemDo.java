package com.unswift.cloud.pojo.dao.system.user.role;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemUserRole;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="会员角色批量更新记录实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemUserRoleUpdateBatchItemDo extends SystemUserRole implements BaseDo{
	
	
}
