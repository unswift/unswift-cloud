package com.unswift.cloud.pojo.dao.system.job;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="职位批量插入封装实体", author="liyunlong", date="2024-01-18", version="1.0.0")
public class SystemJobInsertBatchDo implements BatchDo<SystemJobInsertBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemJobInsertBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemJobInsertBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量插入列表")})
	public SystemJobInsertBatchDo(List<SystemJobInsertBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
