package com.unswift.cloud.pojo.vo.logger.login.record;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="登录记录导出返回实体", author="liyunlong", date="2024-04-06", version="1.0.0")
public class LoggerLoginRecordExportVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("执行结果")
	private Integer result;
	
	@ApiConstructor("默认构造")
	public LoggerLoginRecordExportVo(){
		
	}
	
	@ApiConstructor(value="默认构造", params={@ApiField("主键"), @ApiField("执行结果")})
	public LoggerLoginRecordExportVo(Long id, Integer result){
		this.id=id;
		this.result=result;
	}
}
