package com.unswift.cloud.pojo.vo.logger.operator;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="操作日志队列实体", author="liyunlong", date="2023-12-12", version="1.0.0")
public class LoggerOperatorPageVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("数据id")
	private Long dataId;
	
	@ApiField("所属模块")
	private String module;
	
	@ApiField("操作类型")
	private String operatorType;
	@ApiField("操作类型翻译")
	private String operatorTypeName;
	
	@ApiField("操作内容")
	private String operatorContent;
	
	@ApiField("创建人")
	private Long createUser;
	
	@ApiField("创建人姓名")
	private String createUserName;
	
	@ApiField("创建时间")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date createTime;
}
