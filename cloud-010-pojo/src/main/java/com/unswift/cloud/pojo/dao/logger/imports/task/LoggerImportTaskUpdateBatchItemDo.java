package com.unswift.cloud.pojo.dao.logger.imports.task;

import com.unswift.annotation.api.ApiEntity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.logger.LoggerImportTask;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="导入任务记录表批量更新记录实体", author="liyunlong", date="2024-01-10", version="1.0.0")
public class LoggerImportTaskUpdateBatchItemDo extends LoggerImportTask implements BaseDo{
	
	
}
