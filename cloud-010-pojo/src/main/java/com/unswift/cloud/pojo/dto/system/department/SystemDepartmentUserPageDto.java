package com.unswift.cloud.pojo.dto.system.department;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.dto.PageDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门用户分页查询Dto实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemDepartmentUserPageDto extends PageDto{
	
	@ApiField("部门id")
	private Long departmentId;
	
	@ApiField("账号")
	private String account;
	
	@ApiField("创建时间查询-开始")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date createStartTime;
	
	@ApiField("创建时间查询-结束")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
	private Date createEndTime;
}
