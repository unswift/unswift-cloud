package com.unswift.cloud.pojo.vo.system.attach;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.vo.BaseVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件删除返回实体", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachDownloadVo extends BaseVo{
	
	@ApiField("附件数据")
	@ExcelColumn("附件数据")
	private byte[] attachContent;
	
	@ApiConstructor("默认构造")
	public SystemAttachDownloadVo() {
		
	}

	@ApiConstructor(value="带参构造", params = @ApiField("附件内容"))
	public SystemAttachDownloadVo(byte[] attachContent) {
		super();
		this.attachContent = attachContent;
	}
	
}
