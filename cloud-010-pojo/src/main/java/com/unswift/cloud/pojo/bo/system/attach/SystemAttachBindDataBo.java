package com.unswift.cloud.pojo.bo.system.attach;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.bo.BaseBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件绑定业务实体", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemAttachBindDataBo extends BaseBo{
	
	@ApiField("附件id集合")
	private List<Long> idList;
	
	@ApiField("业务id")
	private Long dataId;
	
}
