package com.unswift.cloud.pojo.mo.system.user;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;
import com.unswift.cloud.pojo.mo.BaseMo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("会员")
@ApiEntity(value="会员导出模板实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemUserExportMo extends BaseMo{
	
	@ApiField("账号")
	@ExcelColumn("登录账号")
	private String account;
	
	@ApiField("手机号")
	@ExcelColumn("手机号码")
	private String phone;
	
	@ApiField("邮箱")
	@ExcelColumn("电子邮箱")
	private String email;
	
	@ApiField("昵称")
	@ExcelColumn("昵称")
	private String nickname;
	
	@ApiField("姓名")
	@ExcelColumn("姓名")
	private String name;
	
	@ApiField("证件类型{1：居民身份证，2：军官证，3：港澳台通行证，4：护照，999：其它...}")
	@ExcelColumn(value="证件类型", translateBean = "systemUserExportService", translateType = "idCardType")
	private Integer idCardType;
	
	@ApiField("证件号显示，中间带*号")
	@ExcelColumn("证件号码")
	private String idCardShow;
	
	@ApiField("出生日期{yyyy-MM-dd}")
	@ExcelColumn(value="出生日期", format="yyyy-MM-dd")
	private Date birthday;
	
	@ApiField("性别{X：女，Y：男}")
	@ExcelColumn(value="性别", translateBean = "systemUserExportService", translateType = "sex")
	private String sex;
	
	@ApiField("最后修改密码时间")
	@ExcelColumn(value="最后修改密码时间", format="yyyy-MM-dd HH:mm:ss")
	private Date changePasswordTime;
	
	@ApiField("冻结状态{0：未冻结，1：已冻结}")
	@ExcelColumn(value="冻结状态", translateBean = "systemUserExportService", translateType = "freezeStatus")
	private Byte freezeStatus;
	
	@ApiField("冻结时间")
	@ExcelColumn(value="冻结时间", format="yyyy-MM-dd HH:mm:ss")
	private Date freezeTime;
	
	@ApiField("当前用户状态{1：有效，2：失效}")
	@ExcelColumn(value="用户状态", translateBean = "systemUserExportService", translateType = "openStop")
	private Byte status;
	
	@ApiField("是否管理员功能{1：是，0：否}")
	@ExcelColumn(value="管理员", translateBean = "systemUserExportService", translateType = "whetherInt")
	private Byte isAdmin;
	
	@ApiField("拥有角色")
	@ExcelColumn("拥有角色")
	private String roleNames;
}
