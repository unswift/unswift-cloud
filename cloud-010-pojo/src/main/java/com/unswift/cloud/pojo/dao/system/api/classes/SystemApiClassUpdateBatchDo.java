package com.unswift.cloud.pojo.dao.system.api.classes;

import java.util.List;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BatchDo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类api批量更新封装实体", author="unswift", date="2023-09-09", version="1.0.0")
public class SystemApiClassUpdateBatchDo implements BatchDo<SystemApiClassUpdateBatchItemDo>{
	
	@ApiField("更新数据集合")
	private List<SystemApiClassUpdateBatchItemDo> list;
	
	@ApiConstructor("默认构造")
	public SystemApiClassUpdateBatchDo(){
		super();
	}
	
	@ApiConstructor(value="带参构造", params={@ApiField("批量更新列表")})
	public SystemApiClassUpdateBatchDo(List<SystemApiClassUpdateBatchItemDo> list){
		super();
		this.list=list;
	}
	
}
