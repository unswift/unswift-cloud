package com.unswift.cloud.pojo.dao.system.role.input;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemRoleInput;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色出参查询结果实体", author="liyunlong", date="2023-11-23", version="1.0.0")
public class SystemRoleInputDataDo extends SystemRoleInput implements BaseDo{
	
	@ApiField("参数名称")
	private String inputName;
}
