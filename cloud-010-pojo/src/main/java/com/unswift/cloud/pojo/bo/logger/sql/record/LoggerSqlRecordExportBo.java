package com.unswift.cloud.pojo.bo.logger.sql.record;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="sql执行记录导出业务实体", author="unswift", date="2023-09-05", version="1.0.0")
public class LoggerSqlRecordExportBo extends BaseBo{
	
	@ApiField("注解")
	private Long id;
	
	@ApiField("sql表主键")
	private Long sqlId;
	
	@ApiField("sql脚本")
	private String sqlScript;
	
	@ApiField("执行状态{0：初始化，1：执行成功，2：执行失败}")
	private Byte exeStatus;
	
	@ApiField("错误信息")
	private String message;
	
}
