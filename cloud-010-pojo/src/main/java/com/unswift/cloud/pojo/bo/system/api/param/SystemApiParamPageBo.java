package com.unswift.cloud.pojo.bo.system.api.param;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.PageBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口参数api分页查询业务实体", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiParamPageBo extends PageBo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("接口api id")
	private Long apiInterfaceId;
	
	@ApiField("属性分类{param：url参数,form：表单参数,json：body参数，response：回调参数}")
	private String paramCategory;
	
	@ApiField("属性名称")
	private String paramKey;
	
	@ApiField("属性描述")
	private String paramComment;
	
	@ApiField("属性类型{string,int,long,byte,char,json,file}")
	private String paramType;
	
	@ApiField("规则")
	private String rule;
	
	@ApiField("必须的")
	private Boolean required;
	
	@ApiField("已过期{0：未过期，1：已过期}")
	private Boolean deprecated;
	
	@ApiField("父属性id")
	private Long parentId;
	
}
