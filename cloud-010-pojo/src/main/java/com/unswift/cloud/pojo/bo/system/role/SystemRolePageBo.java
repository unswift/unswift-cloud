package com.unswift.cloud.pojo.bo.system.role;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.PageBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色分页查询业务实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRolePageBo extends PageBo{
	
	@ApiField("角色状态{}")
	private Long id;
	
	@ApiField("角色key")
	private String key;
	
	@ApiField("角色名称")
	private String name;
	
	@ApiField("角色状态{1：启用，2：停用}")
	private Byte status;
	
	@ApiField("是否管理员功能{1：是，0：否}")
	private Byte isAdmin;
	
}
