package com.unswift.cloud.pojo.vo.system.api.interfaces;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiConstructor;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.vo.TreeVo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="接口api分页查询返回实体", author="unswift", date="2023-09-05", version="1.0.0")
public class SystemApiInterfacesTreeVo extends TreeVo<SystemApiInterfacesTreeVo, Long>{
	
	@ApiField("类型{catalog：目录，interface：接口}")
	private String type;
	
	@ApiConstructor("默认构造")
	public SystemApiInterfacesTreeVo() {
		super();
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("唯一值"), @ApiField("显示内容"), @ApiField("接口类型{catalog：目录，interface：接口}")})
	public SystemApiInterfacesTreeVo(Long id, String title, String type) {
		super(id, title);
		this.type=type;
	}
	
	@ApiConstructor(value="带参构造", params = {@ApiField("唯一值"), @ApiField("显示内容"), @ApiField("接口类型{catalog：目录，interface：接口}"), @ApiField("节点是否展开"), @ApiField("节点是否选中")})
	public SystemApiInterfacesTreeVo(Long id, String title, String type, boolean spread, boolean checked) {
		super(id, title, spread, checked);
		this.type=type;
	}
}
