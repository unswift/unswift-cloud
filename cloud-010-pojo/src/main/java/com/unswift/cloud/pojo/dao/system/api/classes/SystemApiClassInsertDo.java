package com.unswift.cloud.pojo.dao.system.api.classes;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.BaseDo;

import com.unswift.cloud.pojo.po.system.SystemApiClass;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="类api插入记录实体", author="unswift", date="2023-09-09", version="1.0.0")
public class SystemApiClassInsertDo extends SystemApiClass implements BaseDo{
    
	@ApiField("内容是否变更")
    private Boolean isChange;
	
}
