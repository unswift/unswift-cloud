package com.unswift.cloud.pojo.dao.system.department.job;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dao.SingleDo;
import com.unswift.cloud.pojo.dao.sql.Sql;

import com.unswift.cloud.pojo.po.system.SystemDepartmentJob;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="部门职位更新单条记录实体", author="liyunlong", date="2024-01-22", version="1.0.0")
public class SystemDepartmentJobUpdateDo extends SystemDepartmentJob implements SingleDo{
	
	@ApiField("查询的sql对象")
	private Sql sql;

}
