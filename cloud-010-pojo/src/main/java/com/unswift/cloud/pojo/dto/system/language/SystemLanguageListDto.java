package com.unswift.cloud.pojo.dto.system.language;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.dto.PageDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="据字典模块-系统数语言列表查询Dto实体", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemLanguageListDto extends PageDto{
	
	@ApiField("激活状态")
	private Byte activeStatus;
	
}
