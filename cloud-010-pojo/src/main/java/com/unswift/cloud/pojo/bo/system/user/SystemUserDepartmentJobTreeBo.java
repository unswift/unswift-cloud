package com.unswift.cloud.pojo.bo.system.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.bo.BaseBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统用户部门/职位树业务实体", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemUserDepartmentJobTreeBo extends BaseBo{
	
	@ApiField("被选中的部门/职位(json)，格式[[部门id,职位id]...]")
	private String checkedJson;
	
}
