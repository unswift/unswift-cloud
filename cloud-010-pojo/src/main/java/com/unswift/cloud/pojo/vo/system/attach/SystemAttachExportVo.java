package com.unswift.cloud.pojo.vo.system.attach;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiConstructor;

import com.unswift.cloud.pojo.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="附件导出返回实体", author="liyunlong", date="2023-12-07", version="1.0.0")
public class SystemAttachExportVo extends BaseVo{
	
	@ApiField("主键")
	private Long id;
	
	@ApiField("执行结果")
	private Integer result;
	
	@ApiConstructor("默认构造")
	public SystemAttachExportVo(){
		
	}
	
	@ApiConstructor(value="默认构造", params={@ApiField("主键"), @ApiField("执行结果")})
	public SystemAttachExportVo(Long id, Integer result){
		this.id=id;
		this.result=result;
	}
}
