package com.unswift.cloud.pojo.bo.system.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.bo.BaseBo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统配置参数名称树Bo实体", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigParamNameBo extends BaseBo{

	@ApiField("所属模块")
	private String module;
	
}
