package com.unswift.cloud.pojo.mo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.annotation.excel.Excel;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("部门")
@ApiEntity(value="导入实体基类", author="unswift", date="2024-01-11", version="1.0.0")
public abstract class ImportMo extends BaseMo{

	@ApiField("导入行次")
	private Integer rowNumber;
	
	@ApiField("当前语言")
	private String webLanguage;
}
