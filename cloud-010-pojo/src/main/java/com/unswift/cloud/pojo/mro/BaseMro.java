package com.unswift.cloud.pojo.mro;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.cloud.pojo.IBaseMro;
import com.unswift.cloud.pojo.Pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="消息接收实体基类(Messages Receiving Object)", author = "unswift", date = "2023-12-12", version = "1.0.0")
public class BaseMro implements Pojo, IBaseMro{

	@ApiField("业务id")
	private Long businessId;
	
	@ApiField("系统语言")
	private String language;
	
	@ApiField("登录用户token")
	private String token;
	
}
