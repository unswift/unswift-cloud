package com.unswift.cloud.pojo.dao;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.pojo.dao.sql.Sql;

@ApiEntity(value="数据库访问层查询实体（接口），宇燕微服务架构规定，do为系统的数据库实体层，所有的mapper层操作必须使用此层实体", author="unswift", date="2023-06-13", version="1.0.0")
public interface AutoSqlDo extends BaseDo{

	@ApiMethod(value="查询条件-获取自定义查询条件", returns=@ApiField("自定义查询条件"))
	Sql getSql();

	@ApiMethod(value="查询条件-设置自定义查询条件", params=@ApiField("自定义查询条件"))
	void setSql(Sql sql);
}
