package com.unswift.cloud.pojo.bo.system.role;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.cloud.pojo.bo.BaseBo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@ApiEntity(value="系统角色详情业务实体", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRoleViewBo extends BaseBo{
	
	@ApiField("角色状态{}")
	private Long id;
	
}
