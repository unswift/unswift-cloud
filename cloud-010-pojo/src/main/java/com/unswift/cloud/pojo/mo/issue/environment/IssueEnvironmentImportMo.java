package com.unswift.cloud.pojo.mo.issue.environment;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.unswift.annotation.api.ApiEntity;
import com.unswift.annotation.api.ApiField;

import com.unswift.cloud.annotation.excel.Excel;
import com.unswift.cloud.annotation.excel.ExcelColumn;

import com.unswift.cloud.pojo.mo.ImportMo;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
@Excel("发布环境")
@ApiEntity(value="发布环境导入模板实体", author="liyunlong", date="2024-04-16", version="1.0.0")
public class IssueEnvironmentImportMo extends ImportMo{
	
	@ApiField("环境编码")
	@ExcelColumn("环境编码")
	private String code;
	
	@ApiField("环境名称")
	@ExcelColumn("环境名称")
	private String name;
	
}
