package com.unswift.cloud.pojo.vo;

import com.unswift.annotation.api.ApiEntity;

@SuppressWarnings("serial")
@ApiEntity(value="Vo顶层实体类", author="unswift", date="2023-07-05", version="1.0.0")
public abstract class BusinessVo extends BaseVo{

}
