package com.unswift.cloud.timer.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.cloud.adapter.system.config.SystemCacheRefreshAdapter;
import com.unswift.cloud.cache.CacheEnum;
import com.unswift.cloud.core.CommonOperator;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshDataDo;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshInsertBatchDo;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshInsertBatchItemDo;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshPageDo;
import com.unswift.cloud.pojo.po.system.SystemCacheRefresh;
import com.unswift.cloud.timer.CloudTimerTask;
import com.unswift.utils.ObjectUtils;

@Service
public class SystemMessageTimer extends CommonOperator implements CloudTimerTask{

	@Autowired
	private SystemCacheRefreshAdapter systemCacheRefreshAdapter;
	
	@Override
	public void run() {
		SystemCacheRefreshPageDo cacheRefresh=new SystemCacheRefreshPageDo();
		cacheRefresh.setCacheKey(CacheEnum.SYSTEM_MESSAGE.getKey());
		List<SystemCacheRefreshDataDo> cacheRefreshList=systemCacheRefreshAdapter.findList(cacheRefresh);
		if(ObjectUtils.isNotEmpty(cacheRefreshList)){
			for (SystemCacheRefresh systemCacheRefresh : cacheRefreshList) {
				systemCacheRefresh.setRefreshStatus((byte)1);
			}
			SystemCacheRefreshInsertBatchDo batch=new SystemCacheRefreshInsertBatchDo();
			List<SystemCacheRefreshInsertBatchItemDo> list=this.convertPojoList(cacheRefreshList, SystemCacheRefreshInsertBatchItemDo.class);
			batch.setList(list);
			systemCacheRefreshAdapter.updateBatch(batch, false);
		}
	}

}
