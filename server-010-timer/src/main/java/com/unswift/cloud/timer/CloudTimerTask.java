package com.unswift.cloud.timer;

public interface CloudTimerTask {

	void run();
	
}
