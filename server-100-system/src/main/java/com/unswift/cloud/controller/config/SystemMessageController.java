package com.unswift.cloud.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.message.SystemMessageCreateBo;
import com.unswift.cloud.pojo.bo.system.message.SystemMessageDeleteBo;
import com.unswift.cloud.pojo.bo.system.message.SystemMessagePageBo;
import com.unswift.cloud.pojo.bo.system.message.SystemMessageUpdateBo;
import com.unswift.cloud.pojo.bo.system.message.SystemMessageViewBo;
import com.unswift.cloud.pojo.dto.system.message.SystemMessageCreateDto;
import com.unswift.cloud.pojo.dto.system.message.SystemMessageDeleteDto;
import com.unswift.cloud.pojo.dto.system.message.SystemMessagePageDto;
import com.unswift.cloud.pojo.dto.system.message.SystemMessageUpdateDto;
import com.unswift.cloud.pojo.dto.system.message.SystemMessageViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.message.SystemMessageCreateVo;
import com.unswift.cloud.pojo.vo.system.message.SystemMessageDeleteVo;
import com.unswift.cloud.pojo.vo.system.message.SystemMessagePageVo;
import com.unswift.cloud.pojo.vo.system.message.SystemMessageUpdateVo;
import com.unswift.cloud.pojo.vo.system.message.SystemMessageViewVo;
import com.unswift.cloud.service.config.SystemMessageService;

@RestController
@RequestMapping("/systemMessage")
@Api(value="系统消息对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemMessageController extends BaseController{
	
	@Autowired
	private SystemMessageService systemMessageService;
	
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="系统消息分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemMessagePageVo>> findPageList(@RequestBody SystemMessagePageDto searchDto){
		SystemMessagePageBo searchBo=this.convertPojo(searchDto, SystemMessagePageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemMessageService.findPageList(searchBo));
	}
	
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="系统消息详情", params=@ApiField("主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemMessageViewVo> view(@RequestBody SystemMessageViewDto viewDto){
		SystemMessageViewBo viewBo=this.convertPojo(viewDto, SystemMessageViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemMessageService.view(viewBo));
	}
	
	@TrimHandle
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建系统消息", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemMessageCreateVo> create(@RequestBody SystemMessageCreateDto createDto){
		SystemMessageCreateBo createBo=this.convertPojo(createDto, SystemMessageCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemMessageService.create(createBo));
	}
	
	@TrimHandle
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新系统消息", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemMessageUpdateVo> update(@RequestBody SystemMessageUpdateDto updateDto){
		SystemMessageUpdateBo updateBo=this.convertPojo(updateDto, SystemMessageUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemMessageService.update(updateBo));
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除系统消息", params=@ApiField("主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemMessageDeleteVo> delete(@RequestBody SystemMessageDeleteDto deleteDto){
		SystemMessageDeleteBo deleteBo=this.convertPojo(deleteDto, SystemMessageDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemMessageService.delete(deleteBo));
	}
}
