package com.unswift.cloud.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudCreateBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudDeleteBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudPageBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudUpdateBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudUpdateStatusBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudViewBo;
import com.unswift.cloud.pojo.dto.system.cloud.SystemCloudCreateDto;
import com.unswift.cloud.pojo.dto.system.cloud.SystemCloudDeleteDto;
import com.unswift.cloud.pojo.dto.system.cloud.SystemCloudPageDto;
import com.unswift.cloud.pojo.dto.system.cloud.SystemCloudUpdateDto;
import com.unswift.cloud.pojo.dto.system.cloud.SystemCloudUpdateStatusDto;
import com.unswift.cloud.pojo.dto.system.cloud.SystemCloudViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudCreateVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudDeleteVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudPageVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudUpdateStatusVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudUpdateVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudViewVo;
import com.unswift.cloud.service.config.SystemCloudService;

@RestController
@RequestMapping("/systemCloud")
@Api(value="系统微服务对外接口", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudController extends BaseController{
	
	@Autowired
	@ApiField("系统微服务服务")
	private SystemCloudService systemCloudService;
	
    @Active
	@Auth("system.cloud.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="系统微服务分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemCloudPageVo>> findPageList(@RequestBody SystemCloudPageDto searchDto){
		SystemCloudPageBo searchBo=this.convertPojo(searchDto, SystemCloudPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudService.findPageList(searchBo));
	}
	
    @Active
	@Auth("system.cloud.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="系统微服务详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemCloudViewVo> view(@RequestBody SystemCloudViewDto viewDto){
		SystemCloudViewBo viewBo=this.convertPojo(viewDto, SystemCloudViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudService.view(viewBo));
	}
	
    @Active
    @TrimHandle
	@Auth("system.cloud.create")
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建系统微服务", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemCloudCreateVo> create(@RequestBody SystemCloudCreateDto createDto){
		SystemCloudCreateBo createBo=this.convertPojo(createDto, SystemCloudCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudService.create(createBo));
	}
	
    @Active
    @TrimHandle
	@Auth("system.cloud.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新系统微服务", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemCloudUpdateVo> update(@RequestBody SystemCloudUpdateDto updateDto){
		SystemCloudUpdateBo updateBo=this.convertPojo(updateDto, SystemCloudUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudService.update(updateBo));
	}
    
    @Active
	@Auth("system.cloud.update")
	@RequestMapping(value="/updateMountStatus", method=RequestMethod.POST)
	@ApiMethod(value="启用/禁用职位", params=@ApiField("启用/禁用的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemCloudUpdateStatusVo> updateStatus(@RequestBody SystemCloudUpdateStatusDto updateDto){
		SystemCloudUpdateStatusBo updateBo=this.convertPojo(updateDto, SystemCloudUpdateStatusBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudService.updateMountStatus(updateBo));
	}
	
    @Active
	@Auth("system.cloud.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除系统微服务", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemCloudDeleteVo> delete(@RequestBody SystemCloudDeleteDto deleteDto){
		SystemCloudDeleteBo deleteBo=this.convertPojo(deleteDto, SystemCloudDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudService.delete(deleteBo));
	}
}
