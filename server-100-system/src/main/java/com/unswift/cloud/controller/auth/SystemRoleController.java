package com.unswift.cloud.controller.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockWait;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleAuthorizeInputUpdateBo;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleAuthorizeOutputUpdateBo;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleAuthorizeTreeBo;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleAuthorizeViewBo;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleCreateBo;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleDeleteBo;
import com.unswift.cloud.pojo.bo.system.role.SystemRolePageBo;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleUpdateBo;
import com.unswift.cloud.pojo.bo.system.role.SystemRoleViewBo;
import com.unswift.cloud.pojo.bo.system.role.resource.SystemRoleResourceUpdateBo;
import com.unswift.cloud.pojo.bo.system.role.resource.SystemRoleResourceUpdateListBo;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleAuthorizeInputUpdateBto;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleAuthorizeOutputUpdateBto;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleAuthorizeTreeDto;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleAuthorizeUpdateDto;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleAuthorizeUpdateListDto;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleAuthorizeViewDto;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleCreateDto;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleDeleteDto;
import com.unswift.cloud.pojo.dto.system.role.SystemRolePageDto;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleUpdateDto;
import com.unswift.cloud.pojo.dto.system.role.SystemRoleViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.role.SystemRoleAuthorizeTreeVo;
import com.unswift.cloud.pojo.vo.system.role.SystemRoleAuthorizeViewVo;
import com.unswift.cloud.pojo.vo.system.role.SystemRoleCreateVo;
import com.unswift.cloud.pojo.vo.system.role.SystemRoleDeleteVo;
import com.unswift.cloud.pojo.vo.system.role.SystemRolePageVo;
import com.unswift.cloud.pojo.vo.system.role.SystemRoleUpdateVo;
import com.unswift.cloud.pojo.vo.system.role.SystemRoleViewVo;
import com.unswift.cloud.pojo.vo.system.role.resource.SystemRoleResourceUpdateVo;
import com.unswift.cloud.service.auth.SystemRoleService;

@RestController
@RequestMapping("/systemRole")
@Api(value="系统角色对外接口", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemRoleController extends BaseController{
	
	@Autowired
	private SystemRoleService systemRoleService;
	
	@Active
	@Auth("system.role.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="系统角色分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemRolePageVo>> findPageList(@RequestBody SystemRolePageDto searchDto){
		SystemRolePageBo searchBo=this.convertPojo(searchDto, SystemRolePageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemRoleService.findPageList(searchBo));
	}
	
	@Active
	@Auth("system.role.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="系统角色详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemRoleViewVo> view(@RequestBody SystemRoleViewDto viewDto){
		SystemRoleViewBo viewBo=this.convertPojo(viewDto, SystemRoleViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemRoleService.view(viewBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.role.create")
	@LockWait(prefix = "role" ,unique = "all")//受角色编码唯一性影响，必须加锁
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建系统角色", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemRoleCreateVo> create(@RequestBody SystemRoleCreateDto createDto){
		SystemRoleCreateBo createBo=this.convertPojo(createDto, SystemRoleCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemRoleService.create(createBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.role.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新系统角色", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemRoleUpdateVo> update(@RequestBody SystemRoleUpdateDto updateDto){
		SystemRoleUpdateBo updateBo=this.convertPojo(updateDto, SystemRoleUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemRoleService.update(updateBo));
	}
	
	@Active
	@Auth("system.role.updateAuthorize")
	@RequestMapping(value="/updateAuthorize", method=RequestMethod.POST)
	@ApiMethod(value="更新角色授权", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemRoleResourceUpdateVo> updateAuthorize(@RequestBody SystemRoleAuthorizeUpdateListDto updateDto){
		SystemRoleResourceUpdateListBo updateBo=this.convertPojo(updateDto, SystemRoleResourceUpdateListBo.class, new PojoFieldExtend<List<SystemRoleAuthorizeUpdateDto>, List<SystemRoleResourceUpdateBo>>("list"){
			public List<SystemRoleResourceUpdateBo> customAuto(List<SystemRoleAuthorizeUpdateDto> s) {
				return convertPojoList(s, SystemRoleResourceUpdateBo.class, new PojoFieldExtend<List<SystemRoleAuthorizeInputUpdateBto>, List<SystemRoleAuthorizeInputUpdateBo>>("inputList"){
					@Override
					public List<SystemRoleAuthorizeInputUpdateBo> customAuto(List<SystemRoleAuthorizeInputUpdateBto> s) {
						return convertPojoList(s, SystemRoleAuthorizeInputUpdateBo.class);
					}
				}, new PojoFieldExtend<List<SystemRoleAuthorizeOutputUpdateBto>, List<SystemRoleAuthorizeOutputUpdateBo>>("outputList"){
					@Override
					public List<SystemRoleAuthorizeOutputUpdateBo> customAuto(List<SystemRoleAuthorizeOutputUpdateBto> s) {
						return convertPojoList(s, SystemRoleAuthorizeOutputUpdateBo.class);
					}
				}) ;
			}
		});//将Dto转换为Bo,列表为的泛型对象不同，需要进行特殊转换
		return ResponseBody.success(systemRoleService.updateAuthorize(updateBo));
	}
	
	@Active
	@Auth("system.role.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除系统角色", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemRoleDeleteVo> delete(@RequestBody SystemRoleDeleteDto deleteDto){
		SystemRoleDeleteBo deleteBo=this.convertPojo(deleteDto, SystemRoleDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemRoleService.delete(deleteBo));
	}
	
	@Active
	@Auth("system.role.findAuthorizeTree")
	@RequestMapping(value="/findAuthorizeTree", method=RequestMethod.POST)
	@ApiMethod(value="获取角色授权树", params=@ApiField("查询Dto对象"), returns=@ApiField("角色授权树-当前用户权限被选中"))
	public ResponseBody<SystemRoleAuthorizeTreeVo> findAuthorizeTree(@RequestBody SystemRoleAuthorizeTreeDto searchDto){
		SystemRoleAuthorizeTreeBo searchBo=this.convertPojo(searchDto, SystemRoleAuthorizeTreeBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemRoleService.findAuthorizeTree(searchBo));
	}
	
	@Active
	@Auth("system.role.findAuthorizeView")
	@RequestMapping(value="/findAuthorizeView", method=RequestMethod.POST)
	@ApiMethod(value="获取角色授权树", params=@ApiField("查询Dto对象"), returns=@ApiField("角色授权树-当前用户权限被选中"))
	public ResponseBody<SystemRoleAuthorizeViewVo> findAuthorizeView(@RequestBody SystemRoleAuthorizeViewDto viewDto){
		SystemRoleAuthorizeViewBo searchBo=this.convertPojo(viewDto, SystemRoleAuthorizeViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemRoleService.findAuthorizeView(searchBo));
	}
}
