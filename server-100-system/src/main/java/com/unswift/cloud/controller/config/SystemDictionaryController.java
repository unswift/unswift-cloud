package com.unswift.cloud.controller.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryCreateBo;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryDeleteBo;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryTreeBo;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryTypeBo;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryUpdateBo;
import com.unswift.cloud.pojo.bo.system.dictionary.SystemDictionaryViewBo;
import com.unswift.cloud.pojo.dto.system.dictionary.SystemDictionaryCreateDto;
import com.unswift.cloud.pojo.dto.system.dictionary.SystemDictionaryDeleteDto;
import com.unswift.cloud.pojo.dto.system.dictionary.SystemDictionaryTreeDto;
import com.unswift.cloud.pojo.dto.system.dictionary.SystemDictionaryTypeDto;
import com.unswift.cloud.pojo.dto.system.dictionary.SystemDictionaryUpdateDto;
import com.unswift.cloud.pojo.dto.system.dictionary.SystemDictionaryViewDto;
import com.unswift.cloud.pojo.vo.ListVo;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryCreateVo;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryDeleteVo;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryTreeVo;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryTypeVo;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryUpdateVo;
import com.unswift.cloud.pojo.vo.system.dictionary.SystemDictionaryViewVo;
import com.unswift.cloud.service.config.SystemDictionaryService;

@RestController
@RequestMapping("/systemDictionary")
@Api(value="数据字典对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemDictionaryController extends BaseController{
	
	@Autowired
	@ApiField("数据字典服务")
	private SystemDictionaryService systemDictionaryService;
	
	@Active
	@Auth("system.dictionary.treeList")
    @RequestMapping(value="/findTreeList", method=RequestMethod.POST)
    @ApiMethod(value="部门树列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的树数据"))
    public ResponseBody<SystemDictionaryTreeVo> findTreeList(@RequestBody SystemDictionaryTreeDto treeDto){
		SystemDictionaryTreeBo searchBo=this.convertPojo(treeDto, SystemDictionaryTreeBo.class);//将Dto转换为Bo
        return ResponseBody.success(systemDictionaryService.findTreeList(searchBo));
    }
	
	@Active
	@RequestMapping(value="/findTypeList", method=RequestMethod.POST)
	@ApiMethod(value="数据字典类型列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的类型数据"))
	public ResponseBody<ListVo<SystemDictionaryTypeVo>> findTypeList(@RequestBody SystemDictionaryTypeDto searchDto){
		SystemDictionaryTypeBo searchBo=this.convertPojo(searchDto, SystemDictionaryTypeBo.class);//将Dto转换为Bo
		List<SystemDictionaryTypeVo> typeList = systemDictionaryService.findTypeList(searchBo);
		return ResponseBody.success(new ListVo<SystemDictionaryTypeVo>(typeList));
	}
	
	@Active
	@Auth("system.dictionary.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="数据字典详情", params=@ApiField("主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemDictionaryViewVo> view(@RequestBody SystemDictionaryViewDto viewDto){
		SystemDictionaryViewBo viewBo=this.convertPojo(viewDto, SystemDictionaryViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryService.view(viewBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.dictionary.create")
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建数据字典", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemDictionaryCreateVo> create(@RequestBody SystemDictionaryCreateDto createDto){
		SystemDictionaryCreateBo createBo=this.convertPojo(createDto, SystemDictionaryCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryService.create(createBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.dictionary.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新数据字典", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemDictionaryUpdateVo> update(@RequestBody SystemDictionaryUpdateDto updateDto){
		SystemDictionaryUpdateBo updateBo=this.convertPojo(updateDto, SystemDictionaryUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryService.update(updateBo));
	}
	
	@Active
	@Auth("system.dictionary.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除数据字典", params=@ApiField("主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemDictionaryDeleteVo> delete(@RequestBody SystemDictionaryDeleteDto deleteDto){
		SystemDictionaryDeleteBo deleteBo=this.convertPojo(deleteDto, SystemDictionaryDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryService.delete(deleteBo));
	}
	
}
