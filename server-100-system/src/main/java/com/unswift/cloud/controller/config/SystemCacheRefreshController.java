package com.unswift.cloud.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshCreateBo;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshDeleteBo;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshPageBo;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshUpdateBo;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshViewBo;
import com.unswift.cloud.pojo.dto.system.cache.refresh.SystemCacheRefreshCreateDto;
import com.unswift.cloud.pojo.dto.system.cache.refresh.SystemCacheRefreshDeleteDto;
import com.unswift.cloud.pojo.dto.system.cache.refresh.SystemCacheRefreshPageDto;
import com.unswift.cloud.pojo.dto.system.cache.refresh.SystemCacheRefreshUpdateDto;
import com.unswift.cloud.pojo.dto.system.cache.refresh.SystemCacheRefreshViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshCreateVo;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshDeleteVo;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshPageVo;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshUpdateVo;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshViewVo;
import com.unswift.cloud.service.config.SystemCacheRefreshService;

@RestController
@RequestMapping("/systemCacheRefresh")
@Api(value="缓存刷新对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemCacheRefreshController extends BaseController{
	
	@Autowired
	private SystemCacheRefreshService systemCacheRefreshService;
	
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="缓存刷新分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemCacheRefreshPageVo>> findPageList(@RequestBody SystemCacheRefreshPageDto searchDto){
		SystemCacheRefreshPageBo searchBo=this.convertPojo(searchDto, SystemCacheRefreshPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCacheRefreshService.findPageList(searchBo));
	}
	
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="缓存刷新详情", params=@ApiField("主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemCacheRefreshViewVo> view(@RequestBody SystemCacheRefreshViewDto viewDto){
		SystemCacheRefreshViewBo viewBo=this.convertPojo(viewDto, SystemCacheRefreshViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCacheRefreshService.view(viewBo));
	}
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建缓存刷新", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemCacheRefreshCreateVo> create(@RequestBody SystemCacheRefreshCreateDto createDto){
		SystemCacheRefreshCreateBo createBo=this.convertPojo(createDto, SystemCacheRefreshCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCacheRefreshService.create(createBo));
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新缓存刷新", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemCacheRefreshUpdateVo> update(@RequestBody SystemCacheRefreshUpdateDto updateDto){
		SystemCacheRefreshUpdateBo updateBo=this.convertPojo(updateDto, SystemCacheRefreshUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCacheRefreshService.update(updateBo));
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除缓存刷新", params=@ApiField("主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemCacheRefreshDeleteVo> delete(@RequestBody SystemCacheRefreshDeleteDto deleteDto){
		SystemCacheRefreshDeleteBo deleteBo=this.convertPojo(deleteDto, SystemCacheRefreshDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCacheRefreshService.delete(deleteBo));
	}
}
