package com.unswift.cloud.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockWait;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigCreateBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigDeleteBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigPageBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigParamNameBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigUpdateBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigUpdateStatusBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigViewBo;
import com.unswift.cloud.pojo.dto.system.config.SystemConfigCreateDto;
import com.unswift.cloud.pojo.dto.system.config.SystemConfigDeleteDto;
import com.unswift.cloud.pojo.dto.system.config.SystemConfigPageDto;
import com.unswift.cloud.pojo.dto.system.config.SystemConfigParamNameDto;
import com.unswift.cloud.pojo.dto.system.config.SystemConfigUpdateDto;
import com.unswift.cloud.pojo.dto.system.config.SystemConfigUpdateStatusDto;
import com.unswift.cloud.pojo.dto.system.config.SystemConfigViewDto;
import com.unswift.cloud.pojo.vo.ListVo;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigCreateVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigDeleteVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigPageVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigParamNameVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigUpdateVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigViewVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserUpdateStatusVo;
import com.unswift.cloud.service.config.SystemConfigService;

@RestController
@RequestMapping("/systemConfig")
@Api(value="系统配置对外接口", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigController extends BaseController{
	
	@Autowired
	private SystemConfigService systemConfigService;
	
	@Active
	@Auth("system.config.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="系统配置分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemConfigPageVo>> findPageList(@RequestBody SystemConfigPageDto searchDto){
		SystemConfigPageBo searchBo=this.convertPojo(searchDto, SystemConfigPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemConfigService.findPageList(searchBo));
	}
	
	@Active
	@Auth("system.config.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="系统配置详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemConfigViewVo> view(@RequestBody SystemConfigViewDto viewDto){
		SystemConfigViewBo viewBo=this.convertPojo(viewDto, SystemConfigViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemConfigService.view(viewBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.config.create")
	@LockWait(prefix = "config" ,unique = "all")//受键唯一性影响，必须加锁
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建系统配置", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemConfigCreateVo> create(@RequestBody SystemConfigCreateDto createDto){
		SystemConfigCreateBo createBo=this.convertPojo(createDto, SystemConfigCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemConfigService.create(createBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.config.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新系统配置", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemConfigUpdateVo> update(@RequestBody SystemConfigUpdateDto updateDto){
		SystemConfigUpdateBo updateBo=this.convertPojo(updateDto, SystemConfigUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemConfigService.update(updateBo));
	}
	
	@Active
	@Auth("system.config.update")
	@RequestMapping(value="/updateStatus", method=RequestMethod.POST)
	@ApiMethod(value="更新系统配置", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemUserUpdateStatusVo> updateStatus(@RequestBody SystemConfigUpdateStatusDto updateDto){
		SystemConfigUpdateStatusBo updateBo=this.convertPojo(updateDto, SystemConfigUpdateStatusBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemConfigService.updateStatus(updateBo));
	}
	
	@Active
	@Auth("system.config.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除系统配置", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemConfigDeleteVo> delete(@RequestBody SystemConfigDeleteDto deleteDto){
		SystemConfigDeleteBo deleteBo=this.convertPojo(deleteDto, SystemConfigDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemConfigService.delete(deleteBo));
	}
	
	@Active
	@Auth("system.config.findParamNameList")
	@RequestMapping(value="/findParamNameList", method=RequestMethod.POST)
	@ApiMethod(value="获取系统中参数名称列表，分登录用户参数及配置的参数", params=@ApiField("权限key Dto参数"), returns=@ApiField("参数对象"))
	public ResponseBody<ListVo<SystemConfigParamNameVo>> findParamNameList(@RequestBody SystemConfigParamNameDto searchDto){
		SystemConfigParamNameBo searchBo=this.convertPojo(searchDto, SystemConfigParamNameBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemConfigService.findParamNameList(searchBo));
	}
}