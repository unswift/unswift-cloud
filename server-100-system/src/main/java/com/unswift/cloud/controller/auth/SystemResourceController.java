package com.unswift.cloud.controller.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockWait;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.resource.SystemResourceCreateBo;
import com.unswift.cloud.pojo.bo.system.resource.SystemResourceDeleteBo;
import com.unswift.cloud.pojo.bo.system.resource.SystemResourceTreeBo;
import com.unswift.cloud.pojo.bo.system.resource.SystemResourceUpdateBo;
import com.unswift.cloud.pojo.bo.system.resource.SystemResourceViewBo;
import com.unswift.cloud.pojo.bo.system.resource.input.SystemResourceInputAuthBo;
import com.unswift.cloud.pojo.bo.system.resource.output.SystemResourceOutputAuthBo;
import com.unswift.cloud.pojo.dto.system.resource.SystemResourceCreateDto;
import com.unswift.cloud.pojo.dto.system.resource.SystemResourceDeleteDto;
import com.unswift.cloud.pojo.dto.system.resource.SystemResourceTreeDto;
import com.unswift.cloud.pojo.dto.system.resource.SystemResourceUpdateDto;
import com.unswift.cloud.pojo.dto.system.resource.SystemResourceViewDto;
import com.unswift.cloud.pojo.dto.system.resource.input.SystemResourceInputAuthDto;
import com.unswift.cloud.pojo.dto.system.resource.output.SystemResourceOutputAuthDto;
import com.unswift.cloud.pojo.vo.ListVo;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.system.resource.SystemResourceCreateVo;
import com.unswift.cloud.pojo.vo.system.resource.SystemResourceDeleteVo;
import com.unswift.cloud.pojo.vo.system.resource.SystemResourceTreeVo;
import com.unswift.cloud.pojo.vo.system.resource.SystemResourceUpdateVo;
import com.unswift.cloud.pojo.vo.system.resource.SystemResourceViewVo;
import com.unswift.cloud.pojo.vo.system.resource.input.SystemResourceInputAuthVo;
import com.unswift.cloud.pojo.vo.system.resource.output.SystemResourceOutputAuthVo;
import com.unswift.cloud.service.auth.SystemResourceService;

@RestController
@RequestMapping("/systemResource")
@Api(value="系统资源对外接口", author="liyunlong", date="2023-10-05", version="1.0.0")
public class SystemResourceController extends BaseController{
	
	@Autowired
	private SystemResourceService systemResourceService;
	
	@Active
	@Auth("system.resource.treeList")
    @RequestMapping(value="/findTreeList", method=RequestMethod.POST)
    @ApiMethod(value="系统资源树列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的树数据"))
    public ResponseBody<SystemResourceTreeVo> findTreeList(@RequestBody SystemResourceTreeDto treeDto){
        SystemResourceTreeBo searchBo=this.convertPojo(treeDto, SystemResourceTreeBo.class);//将Dto转换为Bo
        return ResponseBody.success(systemResourceService.findTreeList(searchBo));
    }
	
//	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
//	@ApiMethod(value="系统资源分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
//	public ResponseBody<PageVo<SystemResourcePageVo>> findPageList(@RequestBody SystemResourcePageDto searchDto){
//		SystemResourcePageBo searchBo=this.convertPojo(searchDto, SystemResourcePageBo.class);//将Dto转换为Bo
//		return ResponseBody.success(systemResourceService.findPageList(searchBo));
//	}
	
	@Active
	@Auth("system.resource.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="系统资源详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemResourceViewVo> view(@RequestBody SystemResourceViewDto viewDto){
		SystemResourceViewBo viewBo=this.convertPojo(viewDto, SystemResourceViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemResourceService.view(viewBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.resource.create")
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@LockWait(prefix = "resource" ,unique = "all")//受权限key唯一性影响，必须加锁
	@ApiMethod(value="创建系统资源", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemResourceCreateVo> create(@RequestBody SystemResourceCreateDto createDto){
		SystemResourceCreateBo createBo=this.convertPojo(createDto, SystemResourceCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemResourceService.create(createBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.resource.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新系统资源", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemResourceUpdateVo> update(@RequestBody SystemResourceUpdateDto updateDto){
		SystemResourceUpdateBo updateBo=this.convertPojo(updateDto, SystemResourceUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemResourceService.update(updateBo));
	}
	
	@Active
	@Auth("system.resource.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除系统资源", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemResourceDeleteVo> delete(@RequestBody SystemResourceDeleteDto deleteDto){
		SystemResourceDeleteBo deleteBo=this.convertPojo(deleteDto, SystemResourceDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemResourceService.delete(deleteBo));
	}
	
	@Active
	@Auth("system.resource.input.findInputList")
	@RequestMapping(value="/findInputList", method=RequestMethod.POST)
	@ApiMethod(value="获取指定权限的入参列表", params=@ApiField("权限key Dto参数"), returns=@ApiField("参数对象"))
	public ResponseBody<ListVo<SystemResourceInputAuthVo>> findInputList(@RequestBody SystemResourceInputAuthDto searchDto){
		SystemResourceInputAuthBo searchBo=this.convertPojo(searchDto, SystemResourceInputAuthBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemResourceService.findInputList(searchBo));
	}
	
	@Active
	@Auth("system.resource.output.findOutputList")
	@RequestMapping(value="/findOutputList", method=RequestMethod.POST)
	@ApiMethod(value="获取指定权限的出参列表", params=@ApiField("权限key Dto参数"), returns=@ApiField("参数对象"))
	public ResponseBody<ListVo<SystemResourceOutputAuthVo>> findOutputList(@RequestBody SystemResourceOutputAuthDto searchDto){
		SystemResourceOutputAuthBo searchBo=this.convertPojo(searchDto, SystemResourceOutputAuthBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemResourceService.findOutputList(searchBo));
	}
}
