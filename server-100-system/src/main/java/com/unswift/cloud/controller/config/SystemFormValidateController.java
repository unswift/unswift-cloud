package com.unswift.cloud.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateCreateBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateDeleteBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidatePageBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateUpdateBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateUpdateStatusBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateViewBo;
import com.unswift.cloud.pojo.dto.system.form.validate.SystemFormValidateCreateDto;
import com.unswift.cloud.pojo.dto.system.form.validate.SystemFormValidateDeleteDto;
import com.unswift.cloud.pojo.dto.system.form.validate.SystemFormValidatePageDto;
import com.unswift.cloud.pojo.dto.system.form.validate.SystemFormValidateUpdateDto;
import com.unswift.cloud.pojo.dto.system.form.validate.SystemFormValidateUpdateStatusDto;
import com.unswift.cloud.pojo.dto.system.form.validate.SystemFormValidateViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateCreateVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateDeleteVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidatePageVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateUpdateStatusVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateUpdateVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateViewVo;
import com.unswift.cloud.service.config.SystemFormValidateService;

@RestController
@RequestMapping("/systemFormValidate")
@Api(value="系统表单验证对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateController extends BaseController{
	
	@Autowired
	private SystemFormValidateService systemFormValidateService;
	
	@Active
	@Auth("system.formValidate.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="系统表单验证分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemFormValidatePageVo>> findPageList(@RequestBody SystemFormValidatePageDto searchDto){
		SystemFormValidatePageBo searchBo=this.convertPojo(searchDto, SystemFormValidatePageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateService.findPageList(searchBo));
	}
	
	@Active
	@Auth("system.formValidate.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="系统表单验证详情", params=@ApiField("主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemFormValidateViewVo> view(@RequestBody SystemFormValidateViewDto viewDto){
		SystemFormValidateViewBo viewBo=this.convertPojo(viewDto, SystemFormValidateViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateService.view(viewBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.formValidate.create")
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建系统表单验证", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemFormValidateCreateVo> create(@RequestBody SystemFormValidateCreateDto createDto){
		SystemFormValidateCreateBo createBo=this.convertPojo(createDto, SystemFormValidateCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateService.create(createBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.formValidate.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新系统表单验证", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemFormValidateUpdateVo> update(@RequestBody SystemFormValidateUpdateDto updateDto){
		SystemFormValidateUpdateBo updateBo=this.convertPojo(updateDto, SystemFormValidateUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateService.update(updateBo));
	}
	
	@Active
	@Auth("system.formValidate.updateStatus")
	@RequestMapping(value="/updateStatus", method=RequestMethod.POST)
	@ApiMethod(value="修改表单验证为有效/无效", params=@ApiField("有效/无效的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemFormValidateUpdateStatusVo> updateStatus(@RequestBody SystemFormValidateUpdateStatusDto updateDto){
		SystemFormValidateUpdateStatusBo updateBo=this.convertPojo(updateDto, SystemFormValidateUpdateStatusBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateService.updateStatus(updateBo));
	}
	
	@Active
	@Auth("system.formValidate.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除系统表单验证", params=@ApiField("主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemFormValidateDeleteVo> delete(@RequestBody SystemFormValidateDeleteDto deleteDto){
		SystemFormValidateDeleteBo deleteBo=this.convertPojo(deleteDto, SystemFormValidateDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateService.delete(deleteBo));
	}
}
