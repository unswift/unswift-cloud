package com.unswift.cloud.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockWait;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageCreateBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageDeleteBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageListBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguagePageBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageUpdateBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageUpdateStatusBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageViewBo;
import com.unswift.cloud.pojo.dto.system.language.SystemLanguageListDto;
import com.unswift.cloud.pojo.dto.system.language.SystemLanguageCreateDto;
import com.unswift.cloud.pojo.dto.system.language.SystemLanguageDeleteDto;
import com.unswift.cloud.pojo.dto.system.language.SystemLanguagePageDto;
import com.unswift.cloud.pojo.dto.system.language.SystemLanguageUpdateDto;
import com.unswift.cloud.pojo.dto.system.language.SystemLanguageUpdateStatusDto;
import com.unswift.cloud.pojo.dto.system.language.SystemLanguageViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageCreateVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageDeleteVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageListVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguagePageVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageUpdateStatusVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageUpdateVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageViewVo;
import com.unswift.cloud.service.config.SystemLanguageService;

@RestController
@RequestMapping("/systemLanguage")
@Api(value="系统语言对外接口", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemLanguageController extends BaseController{
	
	@Autowired
	@ApiField("系统语言服务")
	private SystemLanguageService systemLanguageService;
	
	@Active
	@Auth("system.language.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="系统语言分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemLanguagePageVo>> findPageList(@RequestBody SystemLanguagePageDto searchDto){
		SystemLanguagePageBo searchBo=this.convertPojo(searchDto, SystemLanguagePageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemLanguageService.findPageList(searchBo));
	}
	
	@Active
	@Auth("system.language.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="系统语言详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemLanguageViewVo> view(@RequestBody SystemLanguageViewDto viewDto){
		SystemLanguageViewBo viewBo=this.convertPojo(viewDto, SystemLanguageViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemLanguageService.view(viewBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.language.create")
	@LockWait(prefix = "language" ,unique = "all")//受角色编码唯一性影响，必须加锁
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建系统语言", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemLanguageCreateVo> create(@RequestBody SystemLanguageCreateDto createDto){
		SystemLanguageCreateBo createBo=this.convertPojo(createDto, SystemLanguageCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemLanguageService.create(createBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.language.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新系统语言", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemLanguageUpdateVo> update(@RequestBody SystemLanguageUpdateDto updateDto){
		SystemLanguageUpdateBo updateBo=this.convertPojo(updateDto, SystemLanguageUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemLanguageService.update(updateBo));
	}
	
	@Active
	@Auth("system.language.update")
	@RequestMapping(value="/updateStatus", method=RequestMethod.POST)
	@ApiMethod(value="更新会员", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemLanguageUpdateStatusVo> updateStatus(@RequestBody SystemLanguageUpdateStatusDto updateDto){
		SystemLanguageUpdateStatusBo updateBo=this.convertPojo(updateDto, SystemLanguageUpdateStatusBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemLanguageService.updateStatus(updateBo));
	}
	
	@Active
	@Auth("system.language.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除系统语言", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemLanguageDeleteVo> delete(@RequestBody SystemLanguageDeleteDto deleteDto){
		SystemLanguageDeleteBo deleteBo=this.convertPojo(deleteDto, SystemLanguageDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemLanguageService.delete(deleteBo));
	}
	
	@Active
	@Auth({"system.dictionary.languageList"})
	@RequestMapping(value="/findList", method=RequestMethod.POST)
	@ApiMethod(value="获取系统语言列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<SystemLanguageListVo> findLanguageList(@RequestBody SystemLanguageListDto searchDto){
		SystemLanguageListBo searchBo=this.convertPojo(searchDto, SystemLanguageListBo.class);//将Dto转换为Bo
		return ResponseBody.success(new SystemLanguageListVo(systemLanguageService.findCacheList(searchBo)));
	}
}
