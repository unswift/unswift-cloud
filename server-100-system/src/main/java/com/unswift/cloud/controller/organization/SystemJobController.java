package com.unswift.cloud.controller.organization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockWait;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.job.SystemJobCreateBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobDeleteBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobListBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobPageBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobUpdateBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobUpdateStatusBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobViewBo;
import com.unswift.cloud.pojo.dto.system.job.SystemJobCreateDto;
import com.unswift.cloud.pojo.dto.system.job.SystemJobDeleteDto;
import com.unswift.cloud.pojo.dto.system.job.SystemJobListDto;
import com.unswift.cloud.pojo.dto.system.job.SystemJobPageDto;
import com.unswift.cloud.pojo.dto.system.job.SystemJobUpdateDto;
import com.unswift.cloud.pojo.dto.system.job.SystemJobUpdateStatusDto;
import com.unswift.cloud.pojo.dto.system.job.SystemJobViewDto;
import com.unswift.cloud.pojo.vo.ListVo;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobCreateVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobDeleteVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobListVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobPageVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobUpdateStatusVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobUpdateVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobViewVo;
import com.unswift.cloud.service.organization.SystemJobService;

@RestController
@RequestMapping("/systemJob")
@Api(value="职位对外接口", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemJobController extends BaseController{
	
	@Autowired
	@ApiField("职位服务")
	private SystemJobService systemJobService;
	
	@Active
	@Auth("system.job.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="职位分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemJobPageVo>> findPageList(@RequestBody SystemJobPageDto searchDto){
		SystemJobPageBo searchBo=this.convertPojo(searchDto, SystemJobPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemJobService.findPageList(searchBo));
	}
	
	@Active
	@RequestMapping(value="/findList", method=RequestMethod.POST)
	@ApiMethod(value="查询职位列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的列表数据"))
	public ResponseBody<ListVo<SystemJobListVo>> findList(@RequestBody SystemJobListDto searchDto){
		SystemJobListBo searchBo=this.convertPojo(searchDto, SystemJobListBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemJobService.findList(searchBo));
	}
	
	@Active
	@Auth("system.job.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="职位详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemJobViewVo> view(@RequestBody SystemJobViewDto viewDto){
		SystemJobViewBo viewBo=this.convertPojo(viewDto, SystemJobViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemJobService.view(viewBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.job.create")
	@LockWait(prefix = "job" ,unique = "all")//受职位编码唯一性影响，必须加锁
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建职位", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemJobCreateVo> create(@RequestBody SystemJobCreateDto createDto){
		SystemJobCreateBo createBo=this.convertPojo(createDto, SystemJobCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemJobService.create(createBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.job.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新职位", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemJobUpdateVo> update(@RequestBody SystemJobUpdateDto updateDto){
		SystemJobUpdateBo updateBo=this.convertPojo(updateDto, SystemJobUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemJobService.update(updateBo));
	}
	
	@Active
	@Auth("system.job.update")
	@RequestMapping(value="/updateStatus", method=RequestMethod.POST)
	@ApiMethod(value="启用/禁用职位", params=@ApiField("启用/禁用的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemJobUpdateStatusVo> updateStatus(@RequestBody SystemJobUpdateStatusDto updateDto){
		SystemJobUpdateStatusBo updateBo=this.convertPojo(updateDto, SystemJobUpdateStatusBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemJobService.updateStatus(updateBo));
	}
	
	@Active
	@Auth("system.job.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除职位", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemJobDeleteVo> delete(@RequestBody SystemJobDeleteDto deleteDto){
		SystemJobDeleteBo deleteBo=this.convertPojo(deleteDto, SystemJobDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemJobService.delete(deleteBo));
	}
}
