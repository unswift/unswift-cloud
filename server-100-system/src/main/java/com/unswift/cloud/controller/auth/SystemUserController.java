package com.unswift.cloud.controller.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockWait;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.user.SystemUserAuthorizeRoleListBo;
import com.unswift.cloud.pojo.bo.system.user.SystemUserCreateBo;
import com.unswift.cloud.pojo.bo.system.user.SystemUserDeleteBo;
import com.unswift.cloud.pojo.bo.system.user.SystemUserDepartmentJobTreeBo;
import com.unswift.cloud.pojo.bo.system.user.SystemUserPageBo;
import com.unswift.cloud.pojo.bo.system.user.SystemUserUpdateBo;
import com.unswift.cloud.pojo.bo.system.user.SystemUserUpdateStatusBo;
import com.unswift.cloud.pojo.bo.system.user.SystemUserViewBo;
import com.unswift.cloud.pojo.bo.system.user.role.SystemUserRoleUpdateBo;
import com.unswift.cloud.pojo.bo.system.user.role.SystemUserRoleUpdateListBo;
import com.unswift.cloud.pojo.dto.system.job.SystemJobUpdateStatusDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserAuthorizeRoleListDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserAuthorizeUpdateDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserAuthorizeUpdateListDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserCreateDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserDeleteDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserDepartmentJobTreeDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserPageDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserUpdateDto;
import com.unswift.cloud.pojo.dto.system.user.SystemUserViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserAuthorizeRoleListVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserCreateVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserDeleteVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserDepartmentJobTreeVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserPageVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserUpdateStatusVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserUpdateVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserViewVo;
import com.unswift.cloud.pojo.vo.system.user.role.SystemUserRoleUpdateVo;
import com.unswift.cloud.service.auth.SystemUserService;

@RestController
@RequestMapping("/systemUser")
@Api(value="会员对外接口", author="liyunlong", date="2023-09-28", version="1.0.0")
public class SystemUserController extends BaseController{
	
	@Autowired
	private SystemUserService systemUserService;
	
	@Active
	@Auth("system.user.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="会员分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemUserPageVo>> findPageList(@RequestBody SystemUserPageDto searchDto){
		SystemUserPageBo searchBo=this.convertPojo(searchDto, SystemUserPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserService.findPageList(searchBo));
	}
	
	@Active
	@Auth("system.user.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="会员详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemUserViewVo> view(@RequestBody SystemUserViewDto viewDto){
		SystemUserViewBo viewBo=this.convertPojo(viewDto, SystemUserViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserService.view(viewBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.user.create")
	@LockWait(prefix = "user" ,unique = "all")//受用户账号、手机号、邮箱唯一性影响，必须加锁
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建会员", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemUserCreateVo> create(@RequestBody SystemUserCreateDto createDto){
		SystemUserCreateBo createBo=this.convertPojo(createDto, SystemUserCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserService.create(createBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.user.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新会员", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemUserUpdateVo> update(@RequestBody SystemUserUpdateDto updateDto){
		SystemUserUpdateBo updateBo=this.convertPojo(updateDto, SystemUserUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserService.update(updateBo));
	}
	
	@Active
	@Auth("system.user.update")
	@RequestMapping(value="/updateStatus", method=RequestMethod.POST)
	@ApiMethod(value="更新会员", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemUserUpdateStatusVo> updateStatus(@RequestBody SystemJobUpdateStatusDto updateDto){
		SystemUserUpdateStatusBo updateBo=this.convertPojo(updateDto, SystemUserUpdateStatusBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserService.updateStatus(updateBo));
	}
	
	@Active
	@Auth("system.user.updateAuthorize")
	@RequestMapping(value="/updateAuthorize", method=RequestMethod.POST)
	@ApiMethod(value="更新用户授权", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemUserRoleUpdateVo> updateAuthorize(@RequestBody SystemUserAuthorizeUpdateListDto updateDto){
		SystemUserRoleUpdateListBo updateBo=this.convertPojo(updateDto, SystemUserRoleUpdateListBo.class, new PojoFieldExtend<List<SystemUserAuthorizeUpdateDto>, List<SystemUserRoleUpdateBo>>("list"){
			public List<SystemUserRoleUpdateBo> customAuto(List<SystemUserAuthorizeUpdateDto> s) {
				return convertPojoList(s, SystemUserRoleUpdateBo.class);
			};
		});
		return ResponseBody.success(systemUserService.updateAuthorize(updateBo));
	}
	
	@Active
	@Auth("system.user.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除会员", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemUserDeleteVo> delete(@RequestBody SystemUserDeleteDto deleteDto){
		SystemUserDeleteBo deleteBo=this.convertPojo(deleteDto, SystemUserDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserService.delete(deleteBo));
	}
	
	@Active
	@Auth("system.user.findAuthorizeRoleList")
	@RequestMapping(value="/findAuthorizeRoleList", method=RequestMethod.POST)
	@ApiMethod(value="获取授权角色列表", params=@ApiField("查询Dto对象"), returns=@ApiField("授权角色列表"))
	public ResponseBody<SystemUserAuthorizeRoleListVo> findAuthorizeRoleList(@RequestBody SystemUserAuthorizeRoleListDto searchDto){
		SystemUserAuthorizeRoleListBo searchBo=this.convertPojo(searchDto, SystemUserAuthorizeRoleListBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserService.findAuthorizeRoleList(searchBo));
	}
	
	@Active
	@Auth("system.user.findDepartmentJobTree")
	@RequestMapping(value="/findDepartmentJobTree", method=RequestMethod.POST)
	@ApiMethod(value="获取部门/职位树", params=@ApiField("选中的节点"), returns=@ApiField("部门/职位树数据"))
	public ResponseBody<SystemUserDepartmentJobTreeVo> findDepartmentJobTree(@RequestBody SystemUserDepartmentJobTreeDto treeDto){
		SystemUserDepartmentJobTreeBo treeBo=this.convertPojo(treeDto, SystemUserDepartmentJobTreeBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserService.findDepartmentJobTree(treeBo));
	}
}
