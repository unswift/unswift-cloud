package com.unswift.cloud.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostCreateBo;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostDeleteBo;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostPageBo;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostUpdateBo;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostViewBo;
import com.unswift.cloud.pojo.dto.system.cloud.host.SystemCloudHostCreateDto;
import com.unswift.cloud.pojo.dto.system.cloud.host.SystemCloudHostDeleteDto;
import com.unswift.cloud.pojo.dto.system.cloud.host.SystemCloudHostPageDto;
import com.unswift.cloud.pojo.dto.system.cloud.host.SystemCloudHostUpdateDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.dto.system.cloud.host.SystemCloudHostViewDto;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostCreateVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostDeleteVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostPageVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostUpdateVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostViewVo;
import com.unswift.cloud.service.config.SystemCloudHostService;
import com.unswift.cloud.pojo.vo.page.PageVo;

@RestController
@RequestMapping("/systemCloudHost")
@Api(value="微服务主机对外接口", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudHostController extends BaseController{
	
	@Autowired
	@ApiField("微服务主机服务")
	private SystemCloudHostService systemCloudHostService;
	
	@Active
	@Auth("system.cloud.host.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="微服务主机分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemCloudHostPageVo>> findPageList(@RequestBody SystemCloudHostPageDto searchDto){
		SystemCloudHostPageBo searchBo=this.convertPojo(searchDto, SystemCloudHostPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudHostService.findPageList(searchBo));
	}
	
	@Active
	@Auth("system.cloud.host.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="微服务主机详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemCloudHostViewVo> view(@RequestBody SystemCloudHostViewDto viewDto){
		SystemCloudHostViewBo viewBo=this.convertPojo(viewDto, SystemCloudHostViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudHostService.view(viewBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.cloud.host.create")
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建微服务主机", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemCloudHostCreateVo> create(@RequestBody SystemCloudHostCreateDto createDto){
		SystemCloudHostCreateBo createBo=this.convertPojo(createDto, SystemCloudHostCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudHostService.create(createBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.cloud.host.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新微服务主机", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemCloudHostUpdateVo> update(@RequestBody SystemCloudHostUpdateDto updateDto){
		SystemCloudHostUpdateBo updateBo=this.convertPojo(updateDto, SystemCloudHostUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudHostService.update(updateBo));
	}
	
	@Active
	@Auth("system.cloud.host.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除微服务主机", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemCloudHostDeleteVo> delete(@RequestBody SystemCloudHostDeleteDto deleteDto){
		SystemCloudHostDeleteBo deleteBo=this.convertPojo(deleteDto, SystemCloudHostDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemCloudHostService.delete(deleteBo));
	}
}