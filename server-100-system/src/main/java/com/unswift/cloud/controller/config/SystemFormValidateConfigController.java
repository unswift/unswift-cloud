package com.unswift.cloud.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigCreateBo;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigDeleteBo;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigPageBo;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigUpdateBo;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigViewBo;
import com.unswift.cloud.pojo.dto.system.form.validate.config.SystemFormValidateConfigCreateDto;
import com.unswift.cloud.pojo.dto.system.form.validate.config.SystemFormValidateConfigDeleteDto;
import com.unswift.cloud.pojo.dto.system.form.validate.config.SystemFormValidateConfigPageDto;
import com.unswift.cloud.pojo.dto.system.form.validate.config.SystemFormValidateConfigUpdateDto;
import com.unswift.cloud.pojo.dto.system.form.validate.config.SystemFormValidateConfigViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigCreateVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigDeleteVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigPageVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigUpdateVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigViewVo;
import com.unswift.cloud.service.config.SystemFormValidateConfigService;

@RestController
@RequestMapping("/systemFormValidateConfig")
@Api(value="系统表单验证基础配置对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateConfigController extends BaseController{
	
	@Autowired
	private SystemFormValidateConfigService systemFormValidateConfigService;
	
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="系统表单验证基础配置分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemFormValidateConfigPageVo>> findPageList(@RequestBody SystemFormValidateConfigPageDto searchDto){
		SystemFormValidateConfigPageBo searchBo=this.convertPojo(searchDto, SystemFormValidateConfigPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateConfigService.findPageList(searchBo));
	}
	
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="系统表单验证基础配置详情", params=@ApiField("主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemFormValidateConfigViewVo> view(@RequestBody SystemFormValidateConfigViewDto viewDto){
		SystemFormValidateConfigViewBo viewBo=this.convertPojo(viewDto, SystemFormValidateConfigViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateConfigService.view(viewBo));
	}
	
	@TrimHandle
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建系统表单验证基础配置", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemFormValidateConfigCreateVo> create(@RequestBody SystemFormValidateConfigCreateDto createDto){
		SystemFormValidateConfigCreateBo createBo=this.convertPojo(createDto, SystemFormValidateConfigCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateConfigService.create(createBo));
	}
	
	@TrimHandle
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新系统表单验证基础配置", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemFormValidateConfigUpdateVo> update(@RequestBody SystemFormValidateConfigUpdateDto updateDto){
		SystemFormValidateConfigUpdateBo updateBo=this.convertPojo(updateDto, SystemFormValidateConfigUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateConfigService.update(updateBo));
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除系统表单验证基础配置", params=@ApiField("主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemFormValidateConfigDeleteVo> delete(@RequestBody SystemFormValidateConfigDeleteDto deleteDto){
		SystemFormValidateConfigDeleteBo deleteBo=this.convertPojo(deleteDto, SystemFormValidateConfigDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemFormValidateConfigService.delete(deleteBo));
	}
}
