package com.unswift.cloud.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeCreateBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeDeleteBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypePageBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeUpdateBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeViewBo;
import com.unswift.cloud.pojo.dto.system.dictionary.type.SystemDictionaryTypeCreateDto;
import com.unswift.cloud.pojo.dto.system.dictionary.type.SystemDictionaryTypeDeleteDto;
import com.unswift.cloud.pojo.dto.system.dictionary.type.SystemDictionaryTypePageDto;
import com.unswift.cloud.pojo.dto.system.dictionary.type.SystemDictionaryTypeUpdateDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.dto.system.dictionary.type.SystemDictionaryTypeViewDto;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeCreateVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeDeleteVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypePageVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeUpdateVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeViewVo;
import com.unswift.cloud.service.config.SystemDictionaryTypeService;
import com.unswift.cloud.pojo.vo.page.PageVo;

@RestController
@RequestMapping("/systemDictionaryType")
@Api(value="数据字典类型对外接口", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypeController extends BaseController{
	
	@Autowired
	@ApiField("数据字典类型服务")
	private SystemDictionaryTypeService systemDictionaryTypeService;
	
	@Active
	@Auth("system.dictionary.type.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="数据字典类型分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemDictionaryTypePageVo>> findPageList(@RequestBody SystemDictionaryTypePageDto searchDto){
		SystemDictionaryTypePageBo searchBo=this.convertPojo(searchDto, SystemDictionaryTypePageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryTypeService.findPageList(searchBo));
	}
	
	@Active
	@Auth("system.dictionary.type.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="数据字典类型详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemDictionaryTypeViewVo> view(@RequestBody SystemDictionaryTypeViewDto viewDto){
		SystemDictionaryTypeViewBo viewBo=this.convertPojo(viewDto, SystemDictionaryTypeViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryTypeService.view(viewBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.dictionary.type.create")
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建数据字典类型", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemDictionaryTypeCreateVo> create(@RequestBody SystemDictionaryTypeCreateDto createDto){
		SystemDictionaryTypeCreateBo createBo=this.convertPojo(createDto, SystemDictionaryTypeCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryTypeService.create(createBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.dictionary.type.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新数据字典类型", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemDictionaryTypeUpdateVo> update(@RequestBody SystemDictionaryTypeUpdateDto updateDto){
		SystemDictionaryTypeUpdateBo updateBo=this.convertPojo(updateDto, SystemDictionaryTypeUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryTypeService.update(updateBo));
	}
	
	@Active
	@Auth("system.dictionary.type.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除数据字典类型", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemDictionaryTypeDeleteVo> delete(@RequestBody SystemDictionaryTypeDeleteDto deleteDto){
		SystemDictionaryTypeDeleteBo deleteBo=this.convertPojo(deleteDto, SystemDictionaryTypeDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDictionaryTypeService.delete(deleteBo));
	}
}