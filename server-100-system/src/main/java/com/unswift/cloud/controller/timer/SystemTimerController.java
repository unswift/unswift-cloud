package com.unswift.cloud.controller.timer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerCreateBo;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerDeleteBo;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerPageBo;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerUpdateBo;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerViewBo;
import com.unswift.cloud.pojo.dto.system.timer.SystemTimerCreateDto;
import com.unswift.cloud.pojo.dto.system.timer.SystemTimerDeleteDto;
import com.unswift.cloud.pojo.dto.system.timer.SystemTimerPageDto;
import com.unswift.cloud.pojo.dto.system.timer.SystemTimerUpdateDto;
import com.unswift.cloud.pojo.dto.system.timer.SystemTimerViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerCreateVo;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerDeleteVo;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerPageVo;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerUpdateVo;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerViewVo;
import com.unswift.cloud.service.timer.SystemTimerService;

@RestController
@RequestMapping("/systemTimer")
@Api(value="系统定时器对外接口", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemTimerController extends BaseController{
	
	@Autowired
	private SystemTimerService systemTimerService;
	
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="系统定时器分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemTimerPageVo>> findPageList(@RequestBody SystemTimerPageDto searchDto){
		SystemTimerPageBo searchBo=this.convertPojo(searchDto, SystemTimerPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemTimerService.findPageList(searchBo));
	}
	
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="系统定时器详情", params=@ApiField("主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemTimerViewVo> view(@RequestBody SystemTimerViewDto viewDto){
		SystemTimerViewBo viewBo=this.convertPojo(viewDto, SystemTimerViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemTimerService.view(viewBo));
	}
	
	@TrimHandle
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建系统定时器", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemTimerCreateVo> create(@RequestBody SystemTimerCreateDto createDto){
		SystemTimerCreateBo createBo=this.convertPojo(createDto, SystemTimerCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemTimerService.create(createBo));
	}
	
	@TrimHandle
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新系统定时器", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemTimerUpdateVo> update(@RequestBody SystemTimerUpdateDto updateDto){
		SystemTimerUpdateBo updateBo=this.convertPojo(updateDto, SystemTimerUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemTimerService.update(updateBo));
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除系统定时器", params=@ApiField("主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemTimerDeleteVo> delete(@RequestBody SystemTimerDeleteDto deleteDto){
		SystemTimerDeleteBo deleteBo=this.convertPojo(deleteDto, SystemTimerDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemTimerService.delete(deleteBo));
	}
}
