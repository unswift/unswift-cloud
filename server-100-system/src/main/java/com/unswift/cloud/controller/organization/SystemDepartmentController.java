package com.unswift.cloud.controller.organization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.LockWait;
import com.unswift.cloud.annotation.request.Active;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.annotation.trim.TrimHandle;
import com.unswift.cloud.controller.BaseController;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentCreateBo;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentDeleteBo;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentTreeBo;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentUpdateBo;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentUserPageBo;
import com.unswift.cloud.pojo.bo.system.department.SystemDepartmentViewBo;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentCreateDto;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentDeleteDto;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentTreeDto;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentUpdateDto;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentUserPageDto;
import com.unswift.cloud.pojo.dto.system.department.SystemDepartmentViewDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentCreateVo;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentDeleteVo;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentTreeVo;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentUpdateVo;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentUserPageVo;
import com.unswift.cloud.pojo.vo.system.department.SystemDepartmentViewVo;
import com.unswift.cloud.service.auth.SystemUserService;
import com.unswift.cloud.service.organization.SystemDepartmentService;

@RestController
@RequestMapping("/systemDepartment")
@Api(value="部门对外接口", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemDepartmentController extends BaseController{
	
	@Autowired
	@ApiField("部门服务")
	private SystemDepartmentService systemDepartmentService;
	@Autowired
	@ApiField("用户服务")
	private SystemUserService systemUserService;
	
	@Active
	@Auth("system.department.treeList")
    @RequestMapping(value="/findTreeList", method=RequestMethod.POST)
    @ApiMethod(value="部门树列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的树数据"))
    public ResponseBody<SystemDepartmentTreeVo> findTreeList(@RequestBody SystemDepartmentTreeDto treeDto){
		SystemDepartmentTreeBo searchBo=this.convertPojo(treeDto, SystemDepartmentTreeBo.class);//将Dto转换为Bo
        return ResponseBody.success(systemDepartmentService.findTreeList(searchBo));
    }
	
	@Active
	@Auth("system.department.user.pageList")
	@RequestMapping(value="/user/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="部门详情页-会员分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<SystemDepartmentUserPageVo>> findUserPageList(@RequestBody SystemDepartmentUserPageDto searchDto){
		SystemDepartmentUserPageBo searchBo=this.convertPojo(searchDto, SystemDepartmentUserPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemUserService.findDepartmentPageList(searchBo));
	}
	
	@Active
	@Auth("system.department.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="部门详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<SystemDepartmentViewVo> view(@RequestBody SystemDepartmentViewDto viewDto){
		SystemDepartmentViewBo viewBo=this.convertPojo(viewDto, SystemDepartmentViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDepartmentService.view(viewBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.department.create")
	@LockWait(prefix = "department" ,unique = "all")//受部门编码唯一性影响，必须加锁
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建部门", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<SystemDepartmentCreateVo> create(@RequestBody SystemDepartmentCreateDto createDto){
		SystemDepartmentCreateBo createBo=this.convertPojo(createDto, SystemDepartmentCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDepartmentService.create(createBo));
	}
	
	@Active
	@TrimHandle
	@Auth("system.department.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新部门", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<SystemDepartmentUpdateVo> update(@RequestBody SystemDepartmentUpdateDto updateDto){
		SystemDepartmentUpdateBo updateBo=this.convertPojo(updateDto, SystemDepartmentUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDepartmentService.update(updateBo));
	}
	
	@Active
	@Auth("system.department.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除部门", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<SystemDepartmentDeleteVo> delete(@RequestBody SystemDepartmentDeleteDto deleteDto){
		SystemDepartmentDeleteBo deleteBo=this.convertPojo(deleteDto, SystemDepartmentDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(systemDepartmentService.delete(deleteBo));
	}
}
