package com.unswift.cloud.service.config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.config.SystemConfigAdapter;
import com.unswift.cloud.annotation.logger.OperatorLogger;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigCreateBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigDeleteBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigPageBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigParamNameBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigUpdateBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigUpdateStatusBo;
import com.unswift.cloud.pojo.bo.system.config.SystemConfigViewBo;
import com.unswift.cloud.pojo.dao.BaseDo;
import com.unswift.cloud.pojo.dao.system.config.SystemConfigDataDo;
import com.unswift.cloud.pojo.dao.system.config.SystemConfigDeleteDo;
import com.unswift.cloud.pojo.dao.system.config.SystemConfigInsertDo;
import com.unswift.cloud.pojo.dao.system.config.SystemConfigPageDo;
import com.unswift.cloud.pojo.dao.system.config.SystemConfigSearchDo;
import com.unswift.cloud.pojo.dao.system.config.SystemConfigSingleDo;
import com.unswift.cloud.pojo.dao.system.config.SystemConfigUpdateDo;
import com.unswift.cloud.pojo.vo.BaseVo;
import com.unswift.cloud.pojo.vo.ListVo;
import com.unswift.cloud.pojo.vo.login.TokenUserVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigCreateVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigDeleteVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigPageVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigParamNameOptionVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigParamNameVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigUpdateVo;
import com.unswift.cloud.pojo.vo.system.config.SystemConfigViewVo;
import com.unswift.cloud.pojo.vo.system.user.SystemUserUpdateStatusVo;
import com.unswift.cloud.service.BaseService;
import com.unswift.cloud.utils.LoggerUtils;
import com.unswift.utils.ClassUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="系统配置服务", author="liyunlong", date="2023-11-27", version="1.0.0")
public class SystemConfigService extends BaseService{
	
	@Autowired
	@ApiField("系统配置公共服务")
	private SystemConfigAdapter systemConfigAdapter;
	
	@ApiMethod(value="查询系统配置分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<SystemConfigPageVo> findPageList(SystemConfigPageBo searchBo){
		SystemConfigPageDo search=this.convertPojo(searchBo, SystemConfigPageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<SystemConfigDataDo> page=systemConfigAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, SystemConfigPageVo.class);
	}
	
	@ApiMethod(value="查询系统配置详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("系统配置详情数据"))
	public SystemConfigViewVo view(SystemConfigViewBo viewBo){
		SystemConfigSingleDo search=this.convertPojo(viewBo, SystemConfigSingleDo.class);
		SystemConfigDataDo single=systemConfigAdapter.findSingle(search);
		return this.convertPojo(single, SystemConfigViewVo.class);//将Do转换为Vo
	}

	@OperatorLogger(type = OperatorTypeEnum.CREATE)
	@ApiMethod(value="创建系统配置", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public SystemConfigCreateVo create(SystemConfigCreateBo createBo){
		LoggerUtils.setModule(systemConfigAdapter.getModule());//设置日志所属模块
		SystemConfigInsertDo insert=this.convertPojo(createBo, SystemConfigInsertDo.class);//将Bo转换为Do
		insert.setStatus((byte)1);
		int result=systemConfigAdapter.save(insert, true);
		LoggerUtils.setId(insert.getId());//创建日志需要设置数据id
		return new SystemConfigCreateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新系统配置", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemConfigUpdateVo update(SystemConfigUpdateBo updateBo){
		LoggerUtils.setModule(systemConfigAdapter.getModule());//设置日志所属模块
		SystemConfigDataDo currConfig=systemConfigAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(currConfig);//设置日志原数据对象
		SystemConfigUpdateDo update=this.convertPojo(updateBo, SystemConfigUpdateDo.class);//将Bo转换为Do
		int result=systemConfigAdapter.update(update, true);
		return new SystemConfigUpdateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新系统配置状态", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemUserUpdateStatusVo updateStatus(SystemConfigUpdateStatusBo updateBo){
		LoggerUtils.setModule(systemConfigAdapter.getModule());//设置日志所属模块
		SystemConfigDataDo view=systemConfigAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		SystemConfigUpdateDo update=this.convertPojo(updateBo, SystemConfigUpdateDo.class);//将Bo转换为Do
		int result=systemConfigAdapter.update(update, false);
		return new SystemUserUpdateStatusVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.DELETE)
	@ApiMethod(value="删除系统配置", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public SystemConfigDeleteVo delete(SystemConfigDeleteBo deleteBo){
		LoggerUtils.setModule(systemConfigAdapter.getModule());//设置日志所属模块
		SystemConfigDeleteDo delete=this.convertPojo(deleteBo, SystemConfigDeleteDo.class);//将Bo转换为Do
		int result=systemConfigAdapter.delete(delete);
		return new SystemConfigDeleteVo(result);
	}
	
	@ApiMethod(value="查询参数赋值数据", params = @ApiField("参数查询"), returns = @ApiField("符合条件的参数值"))
	public ListVo<SystemConfigParamNameVo> findParamNameList(SystemConfigParamNameBo searchBo){
		List<SystemConfigParamNameVo> paramNameList=new ArrayList<SystemConfigParamNameVo>();
		List<SystemConfigParamNameOptionVo> optionsList = ClassUtils.getClassFieldList(TokenUserVo.class, SystemConfigParamNameOptionVo.class, new String[] {"key", "value"}, BaseVo.class, 0, new HashSet<String>());
		paramNameList.add(new SystemConfigParamNameVo("loginUser", "登录信息", "select", optionsList));
		SystemConfigSearchDo search = new SystemConfigSearchDo();
		List<BaseDo> configList = systemConfigAdapter.findList(search);
		if(ObjectUtils.isNotEmpty(configList)) {
			paramNameList.add(new SystemConfigParamNameVo("config", "配置信息", "select", this.convertPojoList(configList, SystemConfigParamNameVo.class, new String[] {"value","describe"},new String[] {"key","value"})));
		}
		paramNameList.add(new SystemConfigParamNameVo("fixed", "固定值", "text", null));	
		return new ListVo<SystemConfigParamNameVo>(paramNameList);
	}
}