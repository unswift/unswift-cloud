package com.unswift.cloud.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.config.SystemMessageAdapter;
import com.unswift.cloud.pojo.bo.system.message.SystemMessageCreateBo;
import com.unswift.cloud.pojo.bo.system.message.SystemMessageDeleteBo;
import com.unswift.cloud.pojo.bo.system.message.SystemMessagePageBo;
import com.unswift.cloud.pojo.bo.system.message.SystemMessageUpdateBo;
import com.unswift.cloud.pojo.bo.system.message.SystemMessageViewBo;
import com.unswift.cloud.pojo.dao.system.message.SystemMessageDataDo;
import com.unswift.cloud.pojo.dao.system.message.SystemMessageDeleteDo;
import com.unswift.cloud.pojo.dao.system.message.SystemMessageInsertDo;
import com.unswift.cloud.pojo.dao.system.message.SystemMessagePageDo;
import com.unswift.cloud.pojo.dao.system.message.SystemMessageSingleDo;
import com.unswift.cloud.pojo.dao.system.message.SystemMessageUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.message.SystemMessageCreateVo;
import com.unswift.cloud.pojo.vo.system.message.SystemMessageDeleteVo;
import com.unswift.cloud.pojo.vo.system.message.SystemMessagePageVo;
import com.unswift.cloud.pojo.vo.system.message.SystemMessageUpdateVo;
import com.unswift.cloud.pojo.vo.system.message.SystemMessageViewVo;
import com.unswift.cloud.service.BaseService;

@Service
@Api(value="系统消息服务", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemMessageService extends BaseService{
	
	@Autowired
	@ApiField("系统消息公共服务")
	private SystemMessageAdapter systemMessageAdapter;
	
	@ApiMethod(value="查询系统消息分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<SystemMessagePageVo> findPageList(SystemMessagePageBo searchBo){
		SystemMessagePageDo search=this.convertPojo(searchBo, SystemMessagePageDo.class);//将Bo转换为So
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<SystemMessageDataDo> page=systemMessageAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, SystemMessagePageVo.class);
	}
	
	@ApiMethod(value="查询系统消息详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("系统消息详情数据"))
	public SystemMessageViewVo view(SystemMessageViewBo viewBo){
		SystemMessageSingleDo search=this.convertPojo(viewBo, SystemMessageSingleDo.class);
		SystemMessageDataDo single=systemMessageAdapter.findSingle(search);
		return this.convertPojo(single, SystemMessageViewVo.class);//将Do转换为Vo
	}

	@ApiMethod(value="创建系统消息", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public SystemMessageCreateVo create(SystemMessageCreateBo createBo){
		SystemMessageInsertDo insert=this.convertPojo(createBo, SystemMessageInsertDo.class);//将Bo转换为Po
		int result=systemMessageAdapter.save(insert, true);
		return new SystemMessageCreateVo(result);
	}
	
	@ApiMethod(value="更新系统消息", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemMessageUpdateVo update(SystemMessageUpdateBo updateBo){
		SystemMessageUpdateDo update=this.convertPojo(updateBo, SystemMessageUpdateDo.class);//将Bo转换为Po
		int result=systemMessageAdapter.update(update, true);
		return new SystemMessageUpdateVo(result);
	}
	
	@ApiMethod(value="删除系统消息", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public SystemMessageDeleteVo delete(SystemMessageDeleteBo deleteBo){
		SystemMessageDeleteDo delete=this.convertPojo(deleteBo, SystemMessageDeleteDo.class);//将Bo转换为Po
		int result=systemMessageAdapter.delete(delete);
		return new SystemMessageDeleteVo(result);
	}
}
