package com.unswift.cloud.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.config.SystemDictionaryAdapter;
import com.unswift.cloud.adapter.system.config.SystemDictionaryTypeAdapter;
import com.unswift.cloud.annotation.logger.OperatorLogger;
import com.unswift.cloud.cache.CacheEnum;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeCreateBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeDeleteBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypePageBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeUpdateBo;
import com.unswift.cloud.pojo.bo.system.dictionary.type.SystemDictionaryTypeViewBo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeDataDo;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeDeleteDo;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeInsertDo;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypePageDo;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeSingleDo;
import com.unswift.cloud.pojo.dao.system.dictionary.type.SystemDictionaryTypeUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeCreateVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeDeleteVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypePageVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeUpdateVo;
import com.unswift.cloud.pojo.vo.system.dictionary.type.SystemDictionaryTypeViewVo;
import com.unswift.cloud.service.BaseService;
import com.unswift.cloud.utils.LoggerUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="数据字典类型服务", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemDictionaryTypeService extends BaseService{
	
	@Autowired
	@ApiField("数据字典类型公共服务")
	private SystemDictionaryTypeAdapter systemDictionaryTypeAdapter;
	
	@Autowired
	@ApiField("数据字典公共服务")
	private SystemDictionaryAdapter systemDictionaryAdapter;
	
	@ApiMethod(value="查询数据字典类型分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<SystemDictionaryTypePageVo> findPageList(SystemDictionaryTypePageBo searchBo){
		SystemDictionaryTypePageDo search=this.convertPojo(searchBo, SystemDictionaryTypePageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		systemDictionaryTypeAdapter.setCreateUserSql(search, "createUserName");
		systemDictionaryTypeAdapter.setFieldToLike(search, "code", ObjectUtils.asList("code_", "name_"));
		systemDictionaryTypeAdapter.addOrderBy(search, "create_time_", Sql.ORDER_BY_DESC);
		PageVo<SystemDictionaryTypeDataDo> page=systemDictionaryTypeAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, SystemDictionaryTypePageVo.class);
	}
	
	@ApiMethod(value="查询数据字典类型详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("数据字典类型详情数据"))
	public SystemDictionaryTypeViewVo view(SystemDictionaryTypeViewBo viewBo){
		SystemDictionaryTypeSingleDo search=this.convertPojo(viewBo, SystemDictionaryTypeSingleDo.class);
		SystemDictionaryTypeDataDo single=systemDictionaryTypeAdapter.findSingle(search);
		return this.convertPojo(single, SystemDictionaryTypeViewVo.class);//将Do转换为Vo
	}

	@OperatorLogger(type = OperatorTypeEnum.CREATE)
	@ApiMethod(value="创建数据字典类型", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public SystemDictionaryTypeCreateVo create(SystemDictionaryTypeCreateBo createBo){
		LoggerUtils.setModule(systemDictionaryTypeAdapter.getModule());//设置日志所属模块
		SystemDictionaryTypeInsertDo insert=this.convertPojo(createBo, SystemDictionaryTypeInsertDo.class);//将Bo转换为Do
		int result=systemDictionaryTypeAdapter.save(insert, true);
		LoggerUtils.setId(insert.getId());//创建日志需要设置数据id
		memoryCache.remove(CacheEnum.DICTIONARY_TYPE_MAP.getKey());
		return new SystemDictionaryTypeCreateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新数据字典类型", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemDictionaryTypeUpdateVo update(SystemDictionaryTypeUpdateBo updateBo){
		LoggerUtils.setModule(systemDictionaryTypeAdapter.getModule());//设置日志所属模块
		SystemDictionaryTypeDataDo view=systemDictionaryTypeAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		SystemDictionaryTypeUpdateDo update=this.convertPojo(updateBo, SystemDictionaryTypeUpdateDo.class);//将Bo转换为Do
		int result=systemDictionaryTypeAdapter.update(update, true);
		memoryCache.remove(CacheEnum.DICTIONARY_TYPE_MAP.getKey());
		return new SystemDictionaryTypeUpdateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.DELETE)
	@ApiMethod(value="删除数据字典类型", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public SystemDictionaryTypeDeleteVo delete(SystemDictionaryTypeDeleteBo deleteBo){
		LoggerUtils.setModule(systemDictionaryTypeAdapter.getModule());//设置日志所属模块
		SystemDictionaryTypeDataDo deleteData=systemDictionaryTypeAdapter.findById(deleteBo.getId());
		ExceptionUtils.empty(deleteData, "delete.object.not.exists", "数据字典类型信息");//删除对象必须存在
		SystemDictionaryTypeDeleteDo delete=this.convertPojo(deleteBo, SystemDictionaryTypeDeleteDo.class);//将Bo转换为Do
		systemDictionaryAdapter.deleteByType(deleteData.getCode());
		int result=systemDictionaryTypeAdapter.delete(delete);
		memoryCache.remove(CacheEnum.DICTIONARY_TYPE_MAP.getKey());
		return new SystemDictionaryTypeDeleteVo(result);
	}
}