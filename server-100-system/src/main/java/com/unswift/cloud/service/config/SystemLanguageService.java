package com.unswift.cloud.service.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.config.SystemLanguageAdapter;
import com.unswift.cloud.annotation.logger.OperatorLogger;
import com.unswift.cloud.cache.CacheEnum;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageCreateBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageDeleteBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageListBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguagePageBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageUpdateBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageUpdateStatusBo;
import com.unswift.cloud.pojo.bo.system.language.SystemLanguageViewBo;
import com.unswift.cloud.pojo.cho.system.language.SystemLanguageCho;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageDataDo;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageDeleteDo;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageInsertDo;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguagePageDo;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageSingleDo;
import com.unswift.cloud.pojo.dao.system.language.SystemLanguageUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageCreateVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageDeleteVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageListItemVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguagePageVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageUpdateStatusVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageUpdateVo;
import com.unswift.cloud.pojo.vo.system.language.SystemLanguageViewVo;
import com.unswift.cloud.service.BaseService;
import com.unswift.cloud.utils.LoggerUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="系统语言服务", author="liyunlong", date="2024-02-06", version="1.0.0")
public class SystemLanguageService extends BaseService{
	
	@Autowired
	@ApiField("系统语言公共服务")
	private SystemLanguageAdapter systemLanguageAdapter;
	
	@ApiMethod(value="查询系统语言分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<SystemLanguagePageVo> findPageList(SystemLanguagePageBo searchBo){
		SystemLanguagePageDo search=this.convertPojo(searchBo, SystemLanguagePageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		systemLanguageAdapter.setFieldToLike(search, "shortName", ObjectUtils.asList("short_name_", "full_name_"));
		systemLanguageAdapter.addOrderBy(search, "change_time_", Sql.ORDER_BY_DESC);
		PageVo<SystemLanguageDataDo> page=systemLanguageAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, SystemLanguagePageVo.class);
	}
	
	@ApiMethod(value="查询系统语言分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public List<SystemLanguageListItemVo> findCacheList(SystemLanguageListBo searchBo){
		List<SystemLanguageCho> languageList=this.cacheAdapter.findLanguageList(searchBo.getActiveStatus());
		return this.convertPojoList(languageList, SystemLanguageListItemVo.class);
	}
	
	@ApiMethod(value="查询系统语言详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("系统语言详情数据"))
	public SystemLanguageViewVo view(SystemLanguageViewBo viewBo){
		SystemLanguageSingleDo search=this.convertPojo(viewBo, SystemLanguageSingleDo.class);
		SystemLanguageDataDo single=systemLanguageAdapter.findSingle(search);
		return this.convertPojo(single, SystemLanguageViewVo.class);//将Do转换为Vo
	}

	@OperatorLogger(type = OperatorTypeEnum.CREATE)
	@ApiMethod(value="创建系统语言", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public SystemLanguageCreateVo create(SystemLanguageCreateBo createBo){
		LoggerUtils.setModule(systemLanguageAdapter.getModule());//设置日志所属模块
		SystemLanguageInsertDo insert=this.convertPojo(createBo, SystemLanguageInsertDo.class);//将Bo转换为Do
		insert.setActiveStatus((byte)0);
		int result=systemLanguageAdapter.save(insert, true);
		LoggerUtils.setId(insert.getId());//创建日志需要设置数据id
		memoryCache.remove(CacheEnum.LANGUAGE_LIST.getKey());
		return new SystemLanguageCreateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新系统语言", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemLanguageUpdateVo update(SystemLanguageUpdateBo updateBo){
		LoggerUtils.setModule(systemLanguageAdapter.getModule());//设置日志所属模块
		SystemLanguageDataDo view=systemLanguageAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		SystemLanguageUpdateDo update=this.convertPojo(updateBo, SystemLanguageUpdateDo.class);//将Bo转换为Do
		int result=systemLanguageAdapter.update(update, true);
		memoryCache.remove(CacheEnum.LANGUAGE_LIST.getKey());
		return new SystemLanguageUpdateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新职位", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemLanguageUpdateStatusVo updateStatus(SystemLanguageUpdateStatusBo updateBo){
		LoggerUtils.setModule(systemLanguageAdapter.getModule());//设置日志所属模块
		SystemLanguageDataDo view=systemLanguageAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		SystemLanguageUpdateDo update=this.convertPojo(updateBo, SystemLanguageUpdateDo.class);//将Bo转换为Do
		int result=systemLanguageAdapter.update(update, false);
		memoryCache.remove(CacheEnum.LANGUAGE_LIST.getKey());
		return new SystemLanguageUpdateStatusVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.DELETE)
	@ApiMethod(value="删除系统语言", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public SystemLanguageDeleteVo delete(SystemLanguageDeleteBo deleteBo){
		LoggerUtils.setModule(systemLanguageAdapter.getModule());//设置日志所属模块
		SystemLanguageDataDo deleteData=systemLanguageAdapter.findById(deleteBo.getId());
		ExceptionUtils.empty(deleteData, "delete.object.not.exists", "系统语言信息");//删除对象必须存在
		SystemLanguageDeleteDo delete=this.convertPojo(deleteBo, SystemLanguageDeleteDo.class);//将Bo转换为Do
		int result=systemLanguageAdapter.delete(delete);
		memoryCache.remove(CacheEnum.LANGUAGE_LIST.getKey());
		return new SystemLanguageDeleteVo(result);
	}
}
