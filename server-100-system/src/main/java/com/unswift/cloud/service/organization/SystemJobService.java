package com.unswift.cloud.service.organization;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.organization.SystemJobAdapter;
import com.unswift.cloud.annotation.logger.OperatorLogger;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.bo.system.job.SystemJobCreateBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobDeleteBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobListBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobPageBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobUpdateBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobUpdateStatusBo;
import com.unswift.cloud.pojo.bo.system.job.SystemJobViewBo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.job.SystemJobDataDo;
import com.unswift.cloud.pojo.dao.system.job.SystemJobDeleteDo;
import com.unswift.cloud.pojo.dao.system.job.SystemJobInsertDo;
import com.unswift.cloud.pojo.dao.system.job.SystemJobPageDo;
import com.unswift.cloud.pojo.dao.system.job.SystemJobSearchDo;
import com.unswift.cloud.pojo.dao.system.job.SystemJobSingleDo;
import com.unswift.cloud.pojo.dao.system.job.SystemJobUpdateDo;
import com.unswift.cloud.pojo.vo.ListVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobCreateVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobDeleteVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobListVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobPageVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobUpdateStatusVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobUpdateVo;
import com.unswift.cloud.pojo.vo.system.job.SystemJobViewVo;
import com.unswift.cloud.service.BaseService;
import com.unswift.cloud.utils.LoggerUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="职位服务", author="liyunlong", date="2024-01-11", version="1.0.0")
public class SystemJobService extends BaseService{
	
	@Autowired
	@ApiField("职位公共服务")
	private SystemJobAdapter systemJobAdapter;
	
	@ApiMethod(value="查询职位分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<SystemJobPageVo> findPageList(SystemJobPageBo searchBo){
		SystemJobPageDo search=this.convertPojo(searchBo, SystemJobPageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		systemJobAdapter.setCreateUserSql(search, "createUserName");
		systemJobAdapter.setFieldToLike(search, "name", ObjectUtils.asList("code_", "name_"));
		systemJobAdapter.addOrderBy(search, "sort_", Sql.ORDER_BY_ASC);
		PageVo<SystemJobDataDo> page=systemJobAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, SystemJobPageVo.class);
	}
	
	@ApiMethod(value="查询职位列表数据", params=@ApiField("查询条件对象"), returns=@ApiField("职位列表数据"))
	public ListVo<SystemJobListVo> findList(SystemJobListBo searchBo){
		SystemJobSearchDo search=this.convertPojo(searchBo, SystemJobSearchDo.class);//将Bo转换为Do
		systemJobAdapter.addOrderBy(search, "sort_", Sql.ORDER_BY_ASC);
		List<SystemJobDataDo> list=systemJobAdapter.findList(search);//将Do转换为Vo
		List<SystemJobListVo> listVo=this.convertPojoList(list, SystemJobListVo.class);
		return new ListVo<SystemJobListVo>(listVo);
	}
	
	@ApiMethod(value="查询职位详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("职位详情数据"))
	public SystemJobViewVo view(SystemJobViewBo viewBo){
		SystemJobSingleDo search=this.convertPojo(viewBo, SystemJobSingleDo.class);
		SystemJobDataDo single=systemJobAdapter.findSingle(search);
		return this.convertPojo(single, SystemJobViewVo.class);//将Do转换为Vo
	}

	@OperatorLogger(type = OperatorTypeEnum.CREATE)
	@ApiMethod(value="创建职位", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public SystemJobCreateVo create(SystemJobCreateBo createBo){
		LoggerUtils.setModule(systemJobAdapter.getModule());//设置日志所属模块
		SystemJobInsertDo insert=this.convertPojo(createBo, SystemJobInsertDo.class);//将Bo转换为Do
		systemJobAdapter.setInsertDefault(insert);
		int result=systemJobAdapter.save(insert, true);
		LoggerUtils.setId(insert.getId());//创建日志需要设置数据id
		return new SystemJobCreateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新职位", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemJobUpdateVo update(SystemJobUpdateBo updateBo){
		LoggerUtils.setModule(systemJobAdapter.getModule());//设置日志所属模块
		SystemJobDataDo view=systemJobAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		SystemJobUpdateDo update=this.convertPojo(updateBo, SystemJobUpdateDo.class);//将Bo转换为Do
		int result=systemJobAdapter.update(update, true);
		return new SystemJobUpdateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新职位", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemJobUpdateStatusVo updateStatus(SystemJobUpdateStatusBo updateBo){
		LoggerUtils.setModule(systemJobAdapter.getModule());//设置日志所属模块
		SystemJobDataDo view=systemJobAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		SystemJobUpdateDo update=this.convertPojo(updateBo, SystemJobUpdateDo.class);//将Bo转换为Do
		int result=systemJobAdapter.update(update, false);
		return new SystemJobUpdateStatusVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.DELETE)
	@ApiMethod(value="删除职位", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public SystemJobDeleteVo delete(SystemJobDeleteBo deleteBo){
		LoggerUtils.setModule(systemJobAdapter.getModule());//设置日志所属模块
		SystemJobDataDo deleteData=systemJobAdapter.findById(deleteBo.getId());
		ExceptionUtils.empty(deleteData, "delete.object.not.exists", "职位信息");//删除对象必须存在
		SystemJobDeleteDo delete=this.convertPojo(deleteBo, SystemJobDeleteDo.class);//将Bo转换为Do
		int result=systemJobAdapter.delete(delete);
		return new SystemJobDeleteVo(result);
	}
}
