package com.unswift.cloud.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.config.SystemFormValidateAdapter;
import com.unswift.cloud.annotation.logger.OperatorLogger;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateCreateBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateDeleteBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidatePageBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateUpdateBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateUpdateStatusBo;
import com.unswift.cloud.pojo.bo.system.form.validate.SystemFormValidateViewBo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.form.validate.SystemFormValidateDataDo;
import com.unswift.cloud.pojo.dao.system.form.validate.SystemFormValidateDeleteDo;
import com.unswift.cloud.pojo.dao.system.form.validate.SystemFormValidateInsertDo;
import com.unswift.cloud.pojo.dao.system.form.validate.SystemFormValidatePageDo;
import com.unswift.cloud.pojo.dao.system.form.validate.SystemFormValidateSingleDo;
import com.unswift.cloud.pojo.dao.system.form.validate.SystemFormValidateUpdateDo;
import com.unswift.cloud.pojo.dao.system.job.SystemJobUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.page.PageVo.PageRowHandler;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateCreateVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateDeleteVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidatePageVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateUpdateStatusVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateUpdateVo;
import com.unswift.cloud.pojo.vo.system.form.validate.SystemFormValidateViewVo;
import com.unswift.cloud.service.BaseService;
import com.unswift.cloud.utils.LoggerUtils;

@Service
@Api(value="系统表单验证服务", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateService extends BaseService{
	
	@Autowired
	@ApiField("系统表单验证公共服务")
	private SystemFormValidateAdapter systemFormValidateAdapter;
	
	@ApiMethod(value="查询系统表单验证分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<SystemFormValidatePageVo> findPageList(SystemFormValidatePageBo searchBo){
		SystemFormValidatePageDo search=this.convertPojo(searchBo, SystemFormValidatePageDo.class);//将Bo转换为So
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		systemFormValidateAdapter.addOrderBy(search, "module_", Sql.ORDER_BY_ASC);
		systemFormValidateAdapter.addOrderBy(search, "operator_", Sql.ORDER_BY_ASC);
		systemFormValidateAdapter.addOrderBy(search, "code_line_", Sql.ORDER_BY_ASC);
		PageVo<SystemFormValidateDataDo> page=systemFormValidateAdapter.findPageList(search);//将Do转换为Vo
		PageVo<SystemFormValidatePageVo> pageVo = this.convertPage(page, SystemFormValidatePageVo.class);
		pageVo.forEach(new PageRowHandler<SystemFormValidatePageVo>() {
			@Override
			public void handler(SystemFormValidatePageVo rowData, int index) {
				rowData.setServerName(cacheAdapter.findCloudNameByCode(rowData.getServer()));
				rowData.setModuleName(cacheAdapter.findDictionaryValueByKey("module", getLanguage(), rowData.getModule()));
				super.handler(rowData, index);
			}
		});
		return pageVo;
	}
	
	@ApiMethod(value="查询系统表单验证详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("系统表单验证详情数据"))
	public SystemFormValidateViewVo view(SystemFormValidateViewBo viewBo){
		SystemFormValidateSingleDo search=this.convertPojo(viewBo, SystemFormValidateSingleDo.class);
		SystemFormValidateDataDo single=systemFormValidateAdapter.findSingle(search);
		return this.convertPojo(single, SystemFormValidateViewVo.class);//将Do转换为Vo
	}

	@OperatorLogger(type = OperatorTypeEnum.CREATE)
	@ApiMethod(value="创建系统表单验证", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public SystemFormValidateCreateVo create(SystemFormValidateCreateBo createBo){
		LoggerUtils.setModule(systemFormValidateAdapter.getModule());//设置日志所属模块
		SystemFormValidateInsertDo insert=this.convertPojo(createBo, SystemFormValidateInsertDo.class);//将Bo转换为Po
		int result=systemFormValidateAdapter.save(insert, true);
		LoggerUtils.setId(insert.getId());
		return new SystemFormValidateCreateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新系统表单验证", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemFormValidateUpdateVo update(SystemFormValidateUpdateBo updateBo){
		LoggerUtils.setModule(systemFormValidateAdapter.getModule());//设置日志所属模块
		SystemFormValidateDataDo view=systemFormValidateAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		SystemFormValidateUpdateDo update=this.convertPojo(updateBo, SystemFormValidateUpdateDo.class);//将Bo转换为Po
		int result=systemFormValidateAdapter.update(update, true);
		return new SystemFormValidateUpdateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新表单验证状态为有效/无效", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemFormValidateUpdateStatusVo updateStatus(SystemFormValidateUpdateStatusBo updateBo){
		LoggerUtils.setModule(systemFormValidateAdapter.getModule());//设置日志所属模块
		SystemFormValidateDataDo view=systemFormValidateAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		SystemJobUpdateDo update=this.convertPojo(updateBo, SystemJobUpdateDo.class);//将Bo转换为Do
		int result=systemFormValidateAdapter.update(update, false);
		return new SystemFormValidateUpdateStatusVo(result);
	}
	
	@ApiMethod(value="删除系统表单验证", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public SystemFormValidateDeleteVo delete(SystemFormValidateDeleteBo deleteBo){
		SystemFormValidateDeleteDo delete=this.convertPojo(deleteBo, SystemFormValidateDeleteDo.class);//将Bo转换为Po
		int result=systemFormValidateAdapter.delete(delete);
		return new SystemFormValidateDeleteVo(result);
	}
}
