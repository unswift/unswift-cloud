package com.unswift.cloud.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.config.SystemCloudAdapter;
import com.unswift.cloud.annotation.logger.OperatorLogger;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudCreateBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudDeleteBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudPageBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudUpdateBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudUpdateStatusBo;
import com.unswift.cloud.pojo.bo.system.cloud.SystemCloudViewBo;
import com.unswift.cloud.pojo.dao.sql.Sql;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudDataDo;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudDeleteDo;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudInsertDo;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudPageDo;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudSingleDo;
import com.unswift.cloud.pojo.dao.system.cloud.SystemCloudUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.page.PageVo.PageRowHandler;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudCreateVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudDeleteVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudPageVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudUpdateStatusVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudUpdateVo;
import com.unswift.cloud.pojo.vo.system.cloud.SystemCloudViewVo;
import com.unswift.cloud.service.BaseService;
import com.unswift.cloud.sql.system.cloud.SystemCloudSql;
import com.unswift.cloud.utils.LoggerUtils;
import com.unswift.utils.ExceptionUtils;
import com.unswift.utils.ObjectUtils;

@Service
@Api(value="系统微服务服务", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudService extends BaseService{
	
	@Autowired
	@ApiField("系统微服务公共服务")
	private SystemCloudAdapter systemCloudAdapter;
	
	@Autowired
	@ApiField("系统微服务自定义Sql")
	private SystemCloudSql systemCloudSql;
	
	@ApiMethod(value="查询系统微服务分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<SystemCloudPageVo> findPageList(SystemCloudPageBo searchBo){
		SystemCloudPageDo search=this.convertPojo(searchBo, SystemCloudPageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		systemCloudAdapter.setFieldToLike(search, "name", ObjectUtils.asList("code_","name_"));
		systemCloudAdapter.addOrderBy(search, "sort_", Sql.ORDER_BY_ASC);
		PageVo<SystemCloudDataDo> page=systemCloudAdapter.findPageList(search);//将Do转换为Vo
		PageVo<SystemCloudPageVo> pageVo = this.convertPage(page, SystemCloudPageVo.class);
		pageVo.forEach(new PageRowHandler<SystemCloudPageVo>() {
			@Override
			public void handler(SystemCloudPageVo rowData, int index) {
				rowData.setRunningStatusName(cacheAdapter.findDictionaryValueByKey("runningStatus", getLanguage(), rowData.getRunningStatus()+""));
				super.handler(rowData, index);
			}
		});
		return pageVo;
	}
	
	@ApiMethod(value="查询系统微服务详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("系统微服务详情数据"))
	public SystemCloudViewVo view(SystemCloudViewBo viewBo){
		SystemCloudSingleDo search=this.convertPojo(viewBo, SystemCloudSingleDo.class);
		SystemCloudDataDo single=systemCloudAdapter.findSingle(search);
		return this.convertPojo(single, SystemCloudViewVo.class);//将Do转换为Vo
	}

	@OperatorLogger(type = OperatorTypeEnum.CREATE)
	@ApiMethod(value="创建系统微服务", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public SystemCloudCreateVo create(SystemCloudCreateBo createBo){
		LoggerUtils.setModule(systemCloudAdapter.getModule());//设置日志所属模块
		SystemCloudInsertDo insert=this.convertPojo(createBo, SystemCloudInsertDo.class);//将Bo转换为Do
		systemCloudAdapter.setInsertDefault(insert);
		int result=systemCloudAdapter.save(insert, true);
		LoggerUtils.setId(insert.getId());//创建日志需要设置数据id
		return new SystemCloudCreateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新系统微服务", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemCloudUpdateVo update(SystemCloudUpdateBo updateBo){
		LoggerUtils.setModule(systemCloudAdapter.getModule());//设置日志所属模块
		SystemCloudDataDo view=systemCloudAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		SystemCloudUpdateDo update=this.convertPojo(updateBo, SystemCloudUpdateDo.class);//将Bo转换为Do
		int result=systemCloudAdapter.update(update, true);
		return new SystemCloudUpdateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新系统微服务挂载状态", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemCloudUpdateStatusVo updateMountStatus(SystemCloudUpdateStatusBo updateBo){
		LoggerUtils.setModule(systemCloudAdapter.getModule());//设置日志所属模块
		SystemCloudDataDo view=systemCloudAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		SystemCloudUpdateDo update=this.convertPojo(updateBo, SystemCloudUpdateDo.class);//将Bo转换为Do
		int result=systemCloudAdapter.update(update, false);
		return new SystemCloudUpdateStatusVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.DELETE)
	@ApiMethod(value="删除系统微服务", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public SystemCloudDeleteVo delete(SystemCloudDeleteBo deleteBo){
		LoggerUtils.setModule(systemCloudAdapter.getModule());//设置日志所属模块
		SystemCloudDataDo deleteData=systemCloudAdapter.findById(deleteBo.getId());
		ExceptionUtils.empty(deleteData, "delete.object.not.exists", "系统微服务信息");//删除对象必须存在
		SystemCloudDeleteDo delete=this.convertPojo(deleteBo, SystemCloudDeleteDo.class);//将Bo转换为Do
		int result=systemCloudAdapter.delete(delete);
		return new SystemCloudDeleteVo(result);
	}
}
