package com.unswift.cloud.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.config.SystemCloudHostAdapter;
import com.unswift.cloud.annotation.logger.OperatorLogger;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostCreateBo;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostDeleteBo;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostPageBo;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostUpdateBo;
import com.unswift.cloud.pojo.bo.system.cloud.host.SystemCloudHostViewBo;
import com.unswift.cloud.pojo.dao.system.cloud.host.SystemCloudHostDataDo;
import com.unswift.cloud.pojo.dao.system.cloud.host.SystemCloudHostDeleteDo;
import com.unswift.cloud.pojo.dao.system.cloud.host.SystemCloudHostInsertDo;
import com.unswift.cloud.pojo.dao.system.cloud.host.SystemCloudHostPageDo;
import com.unswift.cloud.pojo.dao.system.cloud.host.SystemCloudHostSingleDo;
import com.unswift.cloud.pojo.dao.system.cloud.host.SystemCloudHostUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostCreateVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostDeleteVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostPageVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostUpdateVo;
import com.unswift.cloud.pojo.vo.system.cloud.host.SystemCloudHostViewVo;
import com.unswift.cloud.service.BaseService;
import com.unswift.cloud.utils.LoggerUtils;
import com.unswift.cloud.sql.system.cloud.host.SystemCloudHostSql;
import com.unswift.utils.ExceptionUtils;

@Service
@Api(value="微服务主机服务", author="liyunlong", date="2024-04-18", version="1.0.0")
public class SystemCloudHostService extends BaseService{
	
	@Autowired
	@ApiField("微服务主机公共服务")
	private SystemCloudHostAdapter systemCloudHostAdapter;
	
	@Autowired
	@ApiField("微服务主机自定义Sql")
	private SystemCloudHostSql systemCloudHostSql;
	
	@ApiMethod(value="查询微服务主机分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<SystemCloudHostPageVo> findPageList(SystemCloudHostPageBo searchBo){
		SystemCloudHostPageDo search=this.convertPojo(searchBo, SystemCloudHostPageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<SystemCloudHostDataDo> page=systemCloudHostAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, SystemCloudHostPageVo.class);
	}
	
	@ApiMethod(value="查询微服务主机详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("微服务主机详情数据"))
	public SystemCloudHostViewVo view(SystemCloudHostViewBo viewBo){
		SystemCloudHostSingleDo search=this.convertPojo(viewBo, SystemCloudHostSingleDo.class);
		SystemCloudHostDataDo single=systemCloudHostAdapter.findSingle(search);
		return this.convertPojo(single, SystemCloudHostViewVo.class);//将Do转换为Vo
	}

	@OperatorLogger(type = OperatorTypeEnum.CREATE)
	@ApiMethod(value="创建微服务主机", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public SystemCloudHostCreateVo create(SystemCloudHostCreateBo createBo){
		LoggerUtils.setModule(systemCloudHostAdapter.getModule());//设置日志所属模块
		SystemCloudHostInsertDo insert=this.convertPojo(createBo, SystemCloudHostInsertDo.class);//将Bo转换为Do
		int result=systemCloudHostAdapter.save(insert, true);
		LoggerUtils.setId(insert.getId());//创建日志需要设置数据id
		return new SystemCloudHostCreateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新微服务主机", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemCloudHostUpdateVo update(SystemCloudHostUpdateBo updateBo){
		LoggerUtils.setModule(systemCloudHostAdapter.getModule());//设置日志所属模块
		SystemCloudHostDataDo view=systemCloudHostAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		SystemCloudHostUpdateDo update=this.convertPojo(updateBo, SystemCloudHostUpdateDo.class);//将Bo转换为Do
		int result=systemCloudHostAdapter.update(update, true);
		return new SystemCloudHostUpdateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.DELETE)
	@ApiMethod(value="删除微服务主机", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public SystemCloudHostDeleteVo delete(SystemCloudHostDeleteBo deleteBo){
		LoggerUtils.setModule(systemCloudHostAdapter.getModule());//设置日志所属模块
		SystemCloudHostDataDo deleteData=systemCloudHostAdapter.findById(deleteBo.getId());
		ExceptionUtils.empty(deleteData, "delete.object.not.exists", "微服务主机信息");//删除对象必须存在
		SystemCloudHostDeleteDo delete=this.convertPojo(deleteBo, SystemCloudHostDeleteDo.class);//将Bo转换为Do
		int result=systemCloudHostAdapter.delete(delete);
		return new SystemCloudHostDeleteVo(result);
	}
}