package com.unswift.cloud.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.config.SystemCacheRefreshAdapter;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshCreateBo;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshDeleteBo;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshPageBo;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshUpdateBo;
import com.unswift.cloud.pojo.bo.system.cache.refresh.SystemCacheRefreshViewBo;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshDataDo;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshDeleteDo;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshInsertDo;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshPageDo;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshSingleDo;
import com.unswift.cloud.pojo.dao.system.cache.refresh.SystemCacheRefreshUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshCreateVo;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshDeleteVo;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshPageVo;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshUpdateVo;
import com.unswift.cloud.pojo.vo.system.cache.refresh.SystemCacheRefreshViewVo;
import com.unswift.cloud.service.BaseService;

@Service
@Api(value="缓存刷新服务", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemCacheRefreshService extends BaseService{
	
	@Autowired
	@ApiField("缓存刷新公共服务")
	private SystemCacheRefreshAdapter systemCacheRefreshAdapter;
	
	@ApiMethod(value="查询缓存刷新分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<SystemCacheRefreshPageVo> findPageList(SystemCacheRefreshPageBo searchBo){
		SystemCacheRefreshPageDo search=this.convertPojo(searchBo, SystemCacheRefreshPageDo.class);//将Bo转换为So
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<SystemCacheRefreshDataDo> page=systemCacheRefreshAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, SystemCacheRefreshPageVo.class);
	}
	
	@ApiMethod(value="查询缓存刷新详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("缓存刷新详情数据"))
	public SystemCacheRefreshViewVo view(SystemCacheRefreshViewBo viewBo){
		SystemCacheRefreshSingleDo search=this.convertPojo(viewBo, SystemCacheRefreshSingleDo.class);
		SystemCacheRefreshDataDo single=systemCacheRefreshAdapter.findSingle(search);
		return this.convertPojo(single, SystemCacheRefreshViewVo.class);//将Do转换为Vo
	}

	@ApiMethod(value="创建缓存刷新", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public SystemCacheRefreshCreateVo create(SystemCacheRefreshCreateBo createBo){
		SystemCacheRefreshInsertDo insert=this.convertPojo(createBo, SystemCacheRefreshInsertDo.class);//将Bo转换为Po
		int result=systemCacheRefreshAdapter.save(insert, true);
		return new SystemCacheRefreshCreateVo(result);
	}
	
	@ApiMethod(value="更新缓存刷新", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemCacheRefreshUpdateVo update(SystemCacheRefreshUpdateBo updateBo){
		SystemCacheRefreshUpdateDo update=this.convertPojo(updateBo, SystemCacheRefreshUpdateDo.class);//将Bo转换为Po
		int result=systemCacheRefreshAdapter.update(update, true);
		return new SystemCacheRefreshUpdateVo(result);
	}
	
	@ApiMethod(value="删除缓存刷新", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public SystemCacheRefreshDeleteVo delete(SystemCacheRefreshDeleteBo deleteBo){
		SystemCacheRefreshDeleteDo delete=this.convertPojo(deleteBo, SystemCacheRefreshDeleteDo.class);//将Bo转换为Po
		int result=systemCacheRefreshAdapter.delete(delete);
		return new SystemCacheRefreshDeleteVo(result);
	}
}
