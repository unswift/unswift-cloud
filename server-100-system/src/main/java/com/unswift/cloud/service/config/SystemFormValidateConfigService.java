package com.unswift.cloud.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.config.SystemFormValidateConfigAdapter;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigCreateBo;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigDeleteBo;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigPageBo;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigUpdateBo;
import com.unswift.cloud.pojo.bo.system.form.validate.config.SystemFormValidateConfigViewBo;
import com.unswift.cloud.pojo.dao.system.form.validate.config.SystemFormValidateConfigDataDo;
import com.unswift.cloud.pojo.dao.system.form.validate.config.SystemFormValidateConfigDeleteDo;
import com.unswift.cloud.pojo.dao.system.form.validate.config.SystemFormValidateConfigInsertDo;
import com.unswift.cloud.pojo.dao.system.form.validate.config.SystemFormValidateConfigPageDo;
import com.unswift.cloud.pojo.dao.system.form.validate.config.SystemFormValidateConfigSingleDo;
import com.unswift.cloud.pojo.dao.system.form.validate.config.SystemFormValidateConfigUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigCreateVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigDeleteVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigPageVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigUpdateVo;
import com.unswift.cloud.pojo.vo.system.form.validate.config.SystemFormValidateConfigViewVo;
import com.unswift.cloud.service.BaseService;

@Service
@Api(value="系统表单验证基础配置服务", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemFormValidateConfigService extends BaseService{
	
	@Autowired
	@ApiField("系统表单验证基础配置公共服务")
	private SystemFormValidateConfigAdapter systemFormValidateConfigAdapter;
	
	@ApiMethod(value="查询系统表单验证基础配置分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<SystemFormValidateConfigPageVo> findPageList(SystemFormValidateConfigPageBo searchBo){
		SystemFormValidateConfigPageDo search=this.convertPojo(searchBo, SystemFormValidateConfigPageDo.class);//将Bo转换为So
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<SystemFormValidateConfigDataDo> page=systemFormValidateConfigAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, SystemFormValidateConfigPageVo.class);
	}
	
	@ApiMethod(value="查询系统表单验证基础配置详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("系统表单验证基础配置详情数据"))
	public SystemFormValidateConfigViewVo view(SystemFormValidateConfigViewBo viewBo){
		SystemFormValidateConfigSingleDo search=this.convertPojo(viewBo, SystemFormValidateConfigSingleDo.class);
		SystemFormValidateConfigDataDo single=systemFormValidateConfigAdapter.findSingle(search);
		return this.convertPojo(single, SystemFormValidateConfigViewVo.class);//将Do转换为Vo
	}

	@ApiMethod(value="创建系统表单验证基础配置", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public SystemFormValidateConfigCreateVo create(SystemFormValidateConfigCreateBo createBo){
		SystemFormValidateConfigInsertDo insert=this.convertPojo(createBo, SystemFormValidateConfigInsertDo.class);//将Bo转换为Po
		int result=systemFormValidateConfigAdapter.save(insert, true);
		return new SystemFormValidateConfigCreateVo(result);
	}
	
	@ApiMethod(value="更新系统表单验证基础配置", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemFormValidateConfigUpdateVo update(SystemFormValidateConfigUpdateBo updateBo){
		SystemFormValidateConfigUpdateDo update=this.convertPojo(updateBo, SystemFormValidateConfigUpdateDo.class);//将Bo转换为Po
		int result=systemFormValidateConfigAdapter.update(update, true);
		return new SystemFormValidateConfigUpdateVo(result);
	}
	
	@ApiMethod(value="删除系统表单验证基础配置", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public SystemFormValidateConfigDeleteVo delete(SystemFormValidateConfigDeleteBo deleteBo){
		SystemFormValidateConfigDeleteDo delete=this.convertPojo(deleteBo, SystemFormValidateConfigDeleteDo.class);//将Bo转换为Po
		int result=systemFormValidateConfigAdapter.delete(delete);
		return new SystemFormValidateConfigDeleteVo(result);
	}
}
