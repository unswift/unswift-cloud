package com.unswift.cloud.service.timer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.system.timer.SystemTimerAdapter;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerCreateBo;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerDeleteBo;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerPageBo;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerUpdateBo;
import com.unswift.cloud.pojo.bo.system.timer.SystemTimerViewBo;
import com.unswift.cloud.pojo.dao.system.timer.SystemTimerDataDo;
import com.unswift.cloud.pojo.dao.system.timer.SystemTimerDeleteDo;
import com.unswift.cloud.pojo.dao.system.timer.SystemTimerInsertDo;
import com.unswift.cloud.pojo.dao.system.timer.SystemTimerPageDo;
import com.unswift.cloud.pojo.dao.system.timer.SystemTimerSingleDo;
import com.unswift.cloud.pojo.dao.system.timer.SystemTimerUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerCreateVo;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerDeleteVo;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerPageVo;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerUpdateVo;
import com.unswift.cloud.pojo.vo.system.timer.SystemTimerViewVo;
import com.unswift.cloud.service.BaseService;

@Service
@Api(value="系统定时器服务", author="unswift", date="2023-08-13", version="1.0.0")
public class SystemTimerService extends BaseService{
	
	@Autowired
	@ApiField("系统定时器公共服务")
	private SystemTimerAdapter systemTimerAdapter;
	
	@ApiMethod(value="查询系统定时器分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<SystemTimerPageVo> findPageList(SystemTimerPageBo searchBo){
		SystemTimerPageDo search=this.convertPojo(searchBo, SystemTimerPageDo.class);//将Bo转换为So
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<SystemTimerDataDo> page=systemTimerAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, SystemTimerPageVo.class);
	}
	
	@ApiMethod(value="查询系统定时器详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("系统定时器详情数据"))
	public SystemTimerViewVo view(SystemTimerViewBo viewBo){
		SystemTimerSingleDo search=this.convertPojo(viewBo, SystemTimerSingleDo.class);
		SystemTimerDataDo single=systemTimerAdapter.findSingle(search);
		return this.convertPojo(single, SystemTimerViewVo.class);//将Do转换为Vo
	}

	@ApiMethod(value="创建系统定时器", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public SystemTimerCreateVo create(SystemTimerCreateBo createBo){
		SystemTimerInsertDo insert=this.convertPojo(createBo, SystemTimerInsertDo.class);//将Bo转换为Po
		int result=systemTimerAdapter.save(insert, true);
		return new SystemTimerCreateVo(result);
	}
	
	@ApiMethod(value="更新系统定时器", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public SystemTimerUpdateVo update(SystemTimerUpdateBo updateBo){
		SystemTimerUpdateDo update=this.convertPojo(updateBo, SystemTimerUpdateDo.class);//将Bo转换为Po
		int result=systemTimerAdapter.update(update, true);
		return new SystemTimerUpdateVo(result);
	}
	
	@ApiMethod(value="删除系统定时器", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public SystemTimerDeleteVo delete(SystemTimerDeleteBo deleteBo){
		SystemTimerDeleteDo delete=this.convertPojo(deleteBo, SystemTimerDeleteDo.class);//将Bo转换为Po
		int result=systemTimerAdapter.delete(delete);
		return new SystemTimerDeleteVo(result);
	}
}
