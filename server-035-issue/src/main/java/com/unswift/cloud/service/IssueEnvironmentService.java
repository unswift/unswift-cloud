package com.unswift.cloud.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.adapter.issue.environment.IssueEnvironmentAdapter;
import com.unswift.cloud.annotation.logger.OperatorLogger;
import com.unswift.cloud.enums.OperatorTypeEnum;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentCreateBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentDeleteBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentPageBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentUpdateBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentViewBo;
import com.unswift.cloud.pojo.dao.issue.environment.IssueEnvironmentDataDo;
import com.unswift.cloud.pojo.dao.issue.environment.IssueEnvironmentDeleteDo;
import com.unswift.cloud.pojo.dao.issue.environment.IssueEnvironmentInsertDo;
import com.unswift.cloud.pojo.dao.issue.environment.IssueEnvironmentPageDo;
import com.unswift.cloud.pojo.dao.issue.environment.IssueEnvironmentSingleDo;
import com.unswift.cloud.pojo.dao.issue.environment.IssueEnvironmentUpdateDo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentCreateVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentDeleteVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentPageVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentUpdateVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentViewVo;
import com.unswift.cloud.utils.LoggerUtils;
import com.unswift.cloud.sql.issue.environment.IssueEnvironmentSql;
import com.unswift.utils.ExceptionUtils;

@Service
@Api(value="发布环境服务", author="liyunlong", date="2024-04-16", version="1.0.0")
public class IssueEnvironmentService extends BaseService{
	
	@Autowired
	@ApiField("发布环境公共服务")
	private IssueEnvironmentAdapter issueEnvironmentAdapter;
	
	@Autowired
	@ApiField("发布环境自定义Sql")
	private IssueEnvironmentSql issueEnvironmentSql;
	
	@ApiMethod(value="查询发布环境分页数据", params=@ApiField("分页对象，包含分页信息及查询条件"), returns=@ApiField("包含查询数据的分页对象"))
	public PageVo<IssueEnvironmentPageVo> findPageList(IssueEnvironmentPageBo searchBo){
		IssueEnvironmentPageDo search=this.convertPojo(searchBo, IssueEnvironmentPageDo.class);//将Bo转换为Do
		search.setFirstSize((search.getCurrPage()-1)*search.getPageSize());
		PageVo<IssueEnvironmentDataDo> page=issueEnvironmentAdapter.findPageList(search);//将Do转换为Vo
		return this.convertPage(page, IssueEnvironmentPageVo.class);
	}
	
	@ApiMethod(value="查询发布环境详情", params=@ApiField("详情业务实体，包含id字段"), returns=@ApiField("发布环境详情数据"))
	public IssueEnvironmentViewVo view(IssueEnvironmentViewBo viewBo){
		IssueEnvironmentSingleDo search=this.convertPojo(viewBo, IssueEnvironmentSingleDo.class);
		IssueEnvironmentDataDo single=issueEnvironmentAdapter.findSingle(search);
		return this.convertPojo(single, IssueEnvironmentViewVo.class);//将Do转换为Vo
	}

	@OperatorLogger(type = OperatorTypeEnum.CREATE)
	@ApiMethod(value="创建发布环境", params=@ApiField("创建的业务实体"), returns=@ApiField("创建结果{0：未创建，1：已创建}"))
	public IssueEnvironmentCreateVo create(IssueEnvironmentCreateBo createBo){
		LoggerUtils.setModule(issueEnvironmentAdapter.getModule());//设置日志所属模块
		IssueEnvironmentInsertDo insert=this.convertPojo(createBo, IssueEnvironmentInsertDo.class);//将Bo转换为Do
		int result=issueEnvironmentAdapter.save(insert, true);
		LoggerUtils.setId(insert.getId());//创建日志需要设置数据id
		return new IssueEnvironmentCreateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.UPDATE)
	@ApiMethod(value="更新发布环境", params=@ApiField("更新的业务实体"), returns=@ApiField("更新结果{0：未创建，1：已创建}"))
	public IssueEnvironmentUpdateVo update(IssueEnvironmentUpdateBo updateBo){
		LoggerUtils.setModule(issueEnvironmentAdapter.getModule());//设置日志所属模块
		IssueEnvironmentDataDo view=issueEnvironmentAdapter.findById(updateBo.getId());
		LoggerUtils.setOriginalData(view);//设置日志原数据对象
		IssueEnvironmentUpdateDo update=this.convertPojo(updateBo, IssueEnvironmentUpdateDo.class);//将Bo转换为Do
		int result=issueEnvironmentAdapter.update(update, true);
		return new IssueEnvironmentUpdateVo(result);
	}
	
	@OperatorLogger(type = OperatorTypeEnum.DELETE)
	@ApiMethod(value="删除发布环境", params=@ApiField("删除业务实体，包含id字段"), returns=@ApiField("删除结果{0：未创建，1：已创建}"))
	public IssueEnvironmentDeleteVo delete(IssueEnvironmentDeleteBo deleteBo){
		LoggerUtils.setModule(issueEnvironmentAdapter.getModule());//设置日志所属模块
		IssueEnvironmentDataDo deleteData=issueEnvironmentAdapter.findById(deleteBo.getId());
		ExceptionUtils.empty(deleteData, "delete.object.not.exists", "发布环境信息");//删除对象必须存在
		IssueEnvironmentDeleteDo delete=this.convertPojo(deleteBo, IssueEnvironmentDeleteDo.class);//将Bo转换为Do
		int result=issueEnvironmentAdapter.delete(delete);
		return new IssueEnvironmentDeleteVo(result);
	}
}
