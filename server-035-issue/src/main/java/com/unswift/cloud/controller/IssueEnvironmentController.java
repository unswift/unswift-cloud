package com.unswift.cloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unswift.annotation.api.Api;
import com.unswift.annotation.api.ApiField;
import com.unswift.annotation.api.ApiMethod;
import com.unswift.cloud.annotation.request.Auth;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentCreateBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentDeleteBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentPageBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentUpdateBo;
import com.unswift.cloud.pojo.bo.issue.environment.IssueEnvironmentViewBo;
import com.unswift.cloud.pojo.dto.issue.environment.IssueEnvironmentCreateDto;
import com.unswift.cloud.pojo.dto.issue.environment.IssueEnvironmentDeleteDto;
import com.unswift.cloud.pojo.dto.issue.environment.IssueEnvironmentPageDto;
import com.unswift.cloud.pojo.dto.issue.environment.IssueEnvironmentUpdateDto;
import com.unswift.cloud.pojo.vo.ResponseBody;
import com.unswift.cloud.pojo.dto.issue.environment.IssueEnvironmentViewDto;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentCreateVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentDeleteVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentPageVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentUpdateVo;
import com.unswift.cloud.pojo.vo.issue.environment.IssueEnvironmentViewVo;
import com.unswift.cloud.pojo.vo.page.PageVo;
import com.unswift.cloud.service.IssueEnvironmentService;

@RestController
@RequestMapping("/issueEnvironment")
@Api(value="发布环境对外接口", author="liyunlong", date="2024-04-16", version="1.0.0")
public class IssueEnvironmentController extends BaseController{
	
	@Autowired
	@ApiField("发布环境服务")
	private IssueEnvironmentService issueEnvironmentService;
	
	@Auth("issue.environment.pageList")
	@RequestMapping(value="/findPageList", method=RequestMethod.POST)
	@ApiMethod(value="发布环境分页列表", params=@ApiField("查询条件"), returns=@ApiField("匹配查询条件的分页数据"))
	public ResponseBody<PageVo<IssueEnvironmentPageVo>> findPageList(@RequestBody IssueEnvironmentPageDto searchDto){
		IssueEnvironmentPageBo searchBo=this.convertPojo(searchDto, IssueEnvironmentPageBo.class);//将Dto转换为Bo
		return ResponseBody.success(issueEnvironmentService.findPageList(searchBo));
	}
	
	@Auth("issue.environment.view")
	@RequestMapping(value="/view", method=RequestMethod.POST)
	@ApiMethod(value="发布环境详情", params=@ApiField("详情Dto对象，包含主键"), returns=@ApiField("详情数据"))
	public ResponseBody<IssueEnvironmentViewVo> view(@RequestBody IssueEnvironmentViewDto viewDto){
		IssueEnvironmentViewBo viewBo=this.convertPojo(viewDto, IssueEnvironmentViewBo.class);//将Dto转换为Bo
		return ResponseBody.success(issueEnvironmentService.view(viewBo));
	}
	
	@Auth("issue.environment.create")
	@RequestMapping(value="/create", method=RequestMethod.POST)
	@ApiMethod(value="创建发布环境", params=@ApiField("创建的数据对象"), returns=@ApiField("创建结果Vo->result:{0：未创建，1：已创建}"))
	public ResponseBody<IssueEnvironmentCreateVo> create(@RequestBody IssueEnvironmentCreateDto createDto){
		IssueEnvironmentCreateBo createBo=this.convertPojo(createDto, IssueEnvironmentCreateBo.class);//将Dto转换为Bo
		return ResponseBody.success(issueEnvironmentService.create(createBo));
	}
	
	@Auth("issue.environment.update")
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ApiMethod(value="更新发布环境", params=@ApiField("更新的数据对象"), returns=@ApiField("更新结果Vo->result:{0：未更新，1：已更新}"))
	public ResponseBody<IssueEnvironmentUpdateVo> update(@RequestBody IssueEnvironmentUpdateDto updateDto){
		IssueEnvironmentUpdateBo updateBo=this.convertPojo(updateDto, IssueEnvironmentUpdateBo.class);//将Dto转换为Bo
		return ResponseBody.success(issueEnvironmentService.update(updateBo));
	}
	
	@Auth("issue.environment.delete")
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ApiMethod(value="删除发布环境", params=@ApiField("删除Dto对象，包含主键"), returns=@ApiField("删除结果Vo->result:{0：未删除，1：已删除}"))
	public ResponseBody<IssueEnvironmentDeleteVo> delete(@RequestBody IssueEnvironmentDeleteDto deleteDto){
		IssueEnvironmentDeleteBo deleteBo=this.convertPojo(deleteDto, IssueEnvironmentDeleteBo.class);//将Dto转换为Bo
		return ResponseBody.success(issueEnvironmentService.delete(deleteBo));
	}
}
